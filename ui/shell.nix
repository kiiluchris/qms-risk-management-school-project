{ pkgs ? import <nixpkgs> {} }:
  pkgs.mkShell {
    name = "qms-ui";
    
    nativeBuildInputs = with pkgs; [
      nodejs
    ];
}
      