const path = require("path");
const CracoAlias = require("craco-alias");
const webpack = require("webpack");

module.exports = {
  style: {
    postcss: {
      plugins: [require("tailwindcss"), require("autoprefixer")],
    },
  },
  webpack: {
    plugins: [
      new webpack.DefinePlugin({
        // process.env.NODE === "production" ? :
        API_URL: "http://localhost:5000",
      }),
    ],
  },
  plugins: [
    {
      plugin: CracoAlias,
      options: {
        source: "tsconfig",
        baseUrl: "./src",
        tsConfigPath: "./tsconfig.paths.json",
      },
    },
  ],
};
