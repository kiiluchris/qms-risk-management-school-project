import { TaskTeam } from "@/domain/models/task-team";
import {
  DashboardCrudFacade,
  makeDashboardCrudService,
} from "@/domain/ports/DashboardCrudFacade";
import { rootClient } from "./client";

export type TaskTeamFacade = DashboardCrudFacade<TaskTeam>;

const taskTeamClient = rootClient.nest("/task-teams");

export const taskTeamApiService: TaskTeamFacade = makeDashboardCrudService(
  taskTeamClient
);
