import { User, UserAccount } from "@/domain/models/user";
import { UserFacade } from "@/domain/ports/UserFacade";
import { rootClient } from "./client";

const userClient = rootClient.nest("/users");

export const accountApiService = {
  updateAccount(user: UserAccount) {
    return userClient
      .put("/account", user, { authenticate: true })
      .then(({ data }) => {
        return data as number;
      });
  },
};

export const userApiService: UserFacade = {
  getUsers() {
    return userClient.get("/").then(({ data }) => {
      return data as User[];
    });
  },
  getUser(userId) {
    return userClient.get(`/${userId}`).then(({ data }) => {
      return data as User;
    });
  },
  editUser(userId, user) {
    return userClient
      .put(`/${userId}`, user, { authenticate: true })
      .then(({ data }) => {
        return data as number;
      });
  },
  deleteUser(userId) {
    return userClient
      .delete(`/${userId}`, { authenticate: true })
      .then(({ data }) => {
        return data as number;
      });
  },
  createUser(user) {
    return userClient
      .post("/", user, { authenticate: true })
      .then(({ data }) => {
        return data as number;
      });
  },
};
