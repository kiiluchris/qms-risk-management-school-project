import { Risk } from "@/domain/models/risk";
import {
  DashboardCrudFacade,
  makeDashboardCrudService,
} from "@/domain/ports/DashboardCrudFacade";
import { rootClient } from "./client";

export type RiskFacade = DashboardCrudFacade<Risk>;

const riskClient = rootClient.nest("/risks");

export const riskApiService: RiskFacade = makeDashboardCrudService(riskClient);
