import { ProcessInput } from "@/domain/models/process-input";
import {
  DashboardCrudFacade,
  makeDashboardCrudService,
} from "@/domain/ports/DashboardCrudFacade";
import { rootClient } from "./client";

export type ProcessInputFacade = DashboardCrudFacade<ProcessInput>;

const processInputClient = rootClient.nest("/process-inputs");

export const processInputApiService: ProcessInputFacade = makeDashboardCrudService(
  processInputClient
);
