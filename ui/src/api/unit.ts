import { Unit } from "@/domain/models/unit";
import {
  DashboardCrudFacade,
  makeDashboardCrudService,
} from "@/domain/ports/DashboardCrudFacade";
import { rootClient } from "./client";

export type UnitFacade = DashboardCrudFacade<Unit>;

const unitClient = rootClient.nest("/units");

export const unitApiService: UnitFacade = makeDashboardCrudService(unitClient);
