import { Objective } from "@/domain/models/objective";
import {
  DashboardCrudFacade,
  makeDashboardCrudService,
} from "@/domain/ports/DashboardCrudFacade";
import { rootClient } from "./client";

export type ObjectiveFacade = DashboardCrudFacade<Objective>;

const objectiveClient = rootClient.nest("/objectives");

export const objectiveApiService: ObjectiveFacade = makeDashboardCrudService(
  objectiveClient
);
