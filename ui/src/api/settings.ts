import { Settings, SettingsUpdate } from "@/domain/models/settings";
import { rootClient } from "./client";

const settingsClient = rootClient.nest("/settings");

export const settingsApiService = {
  create(item: Settings) {
    return settingsClient
      .post("/", item, { authenticate: true })
      .then(({ data }) => {
        return data as number;
      });
  },
  update(item: SettingsUpdate) {
    return settingsClient
      .put("/", item, { authenticate: true })
      .then(({ data }) => {
        return data as number;
      });
  },
  generateToken() {
    return settingsClient
      .post("/generate-token", {}, { authenticate: true })
      .then(({ data }) => {
        return data as string | null;
      });
  },
  delete() {
    return settingsClient
      .delete("/", { authenticate: true })
      .then(({ data }) => {
        return data as number;
      });
  },
};

export type SettingsFacade = typeof settingsApiService;
