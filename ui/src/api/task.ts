import { Task } from "@/domain/models/task";
import {
  DashboardCrudFacade,
  makeDashboardCrudService,
} from "@/domain/ports/DashboardCrudFacade";
import { rootClient } from "./client";

export type TaskFacade = DashboardCrudFacade<Task>;

const taskClient = rootClient.nest("/tasks");

export const taskApiService: TaskFacade = makeDashboardCrudService(taskClient);
