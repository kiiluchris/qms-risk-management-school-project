import { ProcessOutput } from "@/domain/models/process-output";
import {
  DashboardCrudFacade,
  makeDashboardCrudService,
} from "@/domain/ports/DashboardCrudFacade";
import { rootClient } from "./client";

export type ProcessOutputFacade = DashboardCrudFacade<ProcessOutput>;

const processOutputClient = rootClient.nest("/process-outputs");

export const processOutputApiService: ProcessOutputFacade = makeDashboardCrudService(
  processOutputClient
);
