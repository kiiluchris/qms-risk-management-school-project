import { createContext } from "react";
import { authApiService } from "./auth";
import { accountApiService, userApiService } from "./user";
import { roleApiService } from "./role";
import { objectiveApiService } from "./objective";
import { processApiService } from "./process";
import { incidentApiService } from "./incident";
import { capaApiService } from "./capa";
import { taskApiService } from "./task";
import { taskTeamApiService } from "./task-team";
import { processInputApiService } from "./process-input";
import { processOutputApiService } from "./process-output";
import { riskApiService } from "./risk";
import { unitApiService } from "./unit";
import { settingsApiService } from "./settings";

export const apiService = {
  authService: authApiService,
  userService: userApiService,
  roleService: roleApiService,
  processService: processApiService,
  objectiveService: objectiveApiService,
  incidentService: incidentApiService,
  capaService: capaApiService,
  taskService: taskApiService,
  taskTeamService: taskTeamApiService,
  processInputService: processInputApiService,
  processOutputService: processOutputApiService,
  riskService: riskApiService,
  unitService: unitApiService,
  settingsService: settingsApiService,
  accountService: accountApiService,
};

const ApiContext = createContext(apiService);

export const { Provider, Consumer } = ApiContext;

export default ApiContext;
