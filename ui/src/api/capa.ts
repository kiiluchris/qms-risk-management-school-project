import { Capa } from "@/domain/models/capa";
import {
  DashboardCrudFacade,
  makeDashboardCrudService,
} from "@/domain/ports/DashboardCrudFacade";
import { rootClient } from "./client";

export type CapaFacade = DashboardCrudFacade<Capa>;

const capaClient = rootClient.nest("/capas");

export const capaApiService: CapaFacade = makeDashboardCrudService(capaClient);
