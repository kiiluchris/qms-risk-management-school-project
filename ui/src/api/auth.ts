import { LoginTokens } from "@/domain/models/auth";
import { AuthFacade } from "@/domain/ports/AuthFacade";
import { rootClient } from "./client";

const authClient = rootClient.nest("/auth");

export const authApiService: AuthFacade = {
  verifyOrganizationExists(domain) {
    return authClient
      .post("/organization/verify", domain, {})
      .then(({ data }) => data as boolean);
  },
  registerOrganizationAccount(details) {
    return authClient.post("/register", details).then(({ data }) => {
      return data !== -1;
    });
  },
  signIn(credentials) {
    console.log(credentials);
    return authClient.post("/login", credentials).then(({ data }) => {
      console.log("Login", data);
      const d = data as LoginTokens;
      authClient.setAuthTokens(d.access_token, d.refresh_token);
      return d;
    });
  },
  refreshAccessToken() {
    const tokens = authClient.retrieveTokens();
    if (!tokens) {
      return Promise.resolve(null);
    }
    return authClient
      .post("/refresh", tokens.refreshToken, {
        authenticate: true,
      })
      .then(({ data }) => {
        const d = data as LoginTokens | null;
        d && authClient.setAuthTokens(d.access_token, d.refresh_token);
        return d;
      });
  },
  setAuthTokens(accessToken: string, refreshToken: string) {
    authClient.setAuthTokens(accessToken, refreshToken);
  },
  deleteAuthTokens() {
    authClient.deleteTokens();
  },
  requestPasswordReset(data) {
    return authClient.post("/reset-password", data).then(({ data }) => {
      const d = data as boolean;
      return d;
    });
  },
  resetPassword(data) {
    return authClient.post("/reset-password/confirm", data).then(({ data }) => {
      const d = data as boolean;
      return d;
    });
  },
  logOut() {
    return authClient
      .post("/logout", "", { authenticate: true })
      .then(({ data }) => {
        return data as boolean;
      });
  },
};
