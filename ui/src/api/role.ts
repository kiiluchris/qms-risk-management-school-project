import { rootClient } from "./client";

const roleClient = rootClient.nest("/roles");

export type Role = {
  name: string;
};

export interface RoleFacade {
  create(role: Role): Promise<number>;
  update(roleId: number | string, role: Role): Promise<number>;
  delete(roleId: number | string): Promise<number>;
}

export const roleApiService: RoleFacade = {
  create(role) {
    return roleClient
      .post("/", role, { authenticate: true })
      .then(({ data }) => {
        return data as number;
      });
  },
  update(roleId: number | string, role: Role) {
    return roleClient
      .put(`/${roleId}`, role, { authenticate: true })
      .then(({ data }) => {
        return data as number;
      });
  },
  delete(roleId) {
    return roleClient
      .delete(`/${roleId}`, { authenticate: true })
      .then(({ data }) => {
        return data as number;
      });
  },
};
