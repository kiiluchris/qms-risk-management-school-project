import makeClient from "@/utils/client";
import { mkJsonFetcher, mkFileFetcher, mkGqlFetcher } from "@/utils/fetch";

const defaultHeaders = {
  "Content-Type": "application/json; charset=utf-8",
  Accept: "application/json",
};
const rootUrl =
  process.env.NODE_ENV === "development"
    ? "http://localhost:5000"
    : "http://208.68.39.117:5000";

export const makeRemoteUrl = (urlPath: string) => rootUrl + urlPath;

export const rootClient = makeClient(rootUrl, defaultHeaders);

export const fetcher = mkJsonFetcher(rootUrl, defaultHeaders);
export const gqlFetcher = mkGqlFetcher(rootUrl + "/graphql", defaultHeaders);
export const fileFetcher = mkFileFetcher(rootUrl, defaultHeaders);
