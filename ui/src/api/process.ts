import { Process } from "@/domain/models/process";
import {
  DashboardCrudFacade,
  makeDashboardCrudService,
} from "@/domain/ports/DashboardCrudFacade";
import { rootClient } from "./client";

export type ProcessFacade = DashboardCrudFacade<Process>;

const processClient = rootClient.nest("/processes");

export const processApiService: ProcessFacade = makeDashboardCrudService(
  processClient
);
