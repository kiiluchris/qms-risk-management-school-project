import { Incident } from "@/domain/models/incident";
import {
  DashboardCrudFacade,
  makeDashboardCrudService,
} from "@/domain/ports/DashboardCrudFacade";
import { rootClient } from "./client";

export type IncidentFacade = DashboardCrudFacade<Incident>;

const incidentClient = rootClient.nest("/incidents");

export const incidentApiService: IncidentFacade = makeDashboardCrudService(
  incidentClient
);
