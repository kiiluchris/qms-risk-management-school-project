import auth from "@/storage/authSlice";
import dashboard from "@/storage/dashboardSlice";
import { combineReducers } from "@reduxjs/toolkit";
import { useSelector } from "react-redux";
import { persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";

const authPersistConfig = {
  key: "auth",
  storage,
  blacklist: ["isRefreshingToken"],
};

const rootReducer = combineReducers({
  auth: persistReducer(authPersistConfig, auth),
  dashboard: dashboard,
});

export type RootState = ReturnType<typeof rootReducer>;

export const useRootState = <T>(fn: (state: RootState) => T) =>
  useSelector((rootState: RootState) => fn(rootState));

export const useIsLoggedIn = () =>
  useRootState((state) => {
    const { tokens, isRefreshingToken } = state.auth;
    return isRefreshingToken || !!tokens;
  });

export default rootReducer;
