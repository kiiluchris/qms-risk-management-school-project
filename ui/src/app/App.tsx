import React, { useEffect } from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";

import Home from "../views/home/Home";
import AppShell from "@/views/app/AppShell";
import Auth from "../views/auth/Auth";
import Error404 from "../views/errors/404";

type Props = {
  theme: "blue" | "cyan";
};

const App: React.FC<Props> = ({ theme }) => {
  useEffect(() => {
    document.body.classList.add(`laatu-${theme}-theme`);
    document.getElementById("loader-wrapper")?.classList.add("loaded");
  });
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/">
          <Home />
        </Route>
        <Route path="/auth" component={Auth} />
        <AppShell />
        <Route component={Error404} />
      </Switch>
    </BrowserRouter>
  );
};

export default App;
