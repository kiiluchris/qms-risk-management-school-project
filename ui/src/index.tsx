import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import store, { persistor } from "./app/store";
import { PersistGate } from "redux-persist/integration/react";

import "@fontsource/roboto";
import "./index.scss";
import App from "./app/App";
import reportWebVitals from "./reportWebVitals";
import ApiContext, { apiService } from "./api/api";
import UnimplementedContext from "./components/shared/UnimplementedContext";
import { ConfirmProvider } from "material-ui-confirm";
import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles";
import DateFnsUtils from "@date-io/date-fns";
import { MuiPickersUtilsProvider } from "@material-ui/pickers";

const theme = createMuiTheme();

ReactDOM.render(
  <React.StrictMode>
    <MuiThemeProvider theme={theme}>
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <ApiContext.Provider value={apiService}>
              <UnimplementedContext.Provider value={false}>
                <ConfirmProvider>
                  <App theme="blue" />
                </ConfirmProvider>
              </UnimplementedContext.Provider>
            </ApiContext.Provider>
          </MuiPickersUtilsProvider>
        </PersistGate>
      </Provider>
    </MuiThemeProvider>
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
