import { AppThunk } from "@/app/store";
import { AuthFacade } from "@/domain/ports/AuthFacade";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { rootClient } from "@/api/client";
import { AuthTokens, loginTokensToCamelCase } from "@/domain/models/auth";
import { LoginCredentials } from "@/domain/models/auth/LoginCredentials";
import { mutate } from "swr";
import { RolePermission } from "@/domain/models/role";

type PartialBy<T, K extends keyof T> = Omit<T, K> & Partial<Pick<T, K>>;

type LoginError = { key: string; message: string };

type AuthState = {
  organization: string;
  email: string;
  registrationErrors: string[];
  tokens: PartialBy<AuthTokens, "refreshToken"> | null;
  loginErrors: LoginError[];
  isNewRegister: boolean;
  isRefreshingToken: boolean;
  authError: string | null;
  role: RolePermission;
};

const tokens = rootClient.retrieveTokens();
const initialState: AuthState = {
  organization: "",
  email: "",
  registrationErrors: [],
  loginErrors: [],
  tokens: tokens,
  isNewRegister: false,
  isRefreshingToken: false,
  authError: null,
  role: "Employee",
};
console.log(initialState);
const authSlice = createSlice({
  name: "auth",
  initialState: initialState,
  reducers: {
    setOrganization(state, action: PayloadAction<string>) {
      const organization = action.payload;
      state.organization = organization;
    },
    setEmail(state, action: PayloadAction<string>) {
      const email = action.payload;
      state.email = email;
    },
    setAccessToken(state, action: PayloadAction<string>) {
      if (!state.tokens) {
        state.tokens = { accessToken: action.payload };
      } else {
        state.tokens.accessToken = action.payload;
      }
    },
    setRefreshToken(state, action: PayloadAction<string>) {
      if (state.tokens) {
        state.tokens.refreshToken = action.payload;
      }
    },
    setTokens(state, action: PayloadAction<AuthTokens>) {
      state.tokens = action.payload;
    },
    setRegistrationErrors(state, action: PayloadAction<string[]>) {
      const registrationErrors = action.payload;
      state.registrationErrors = registrationErrors;
    },
    setLoginErrors(state, action: PayloadAction<LoginError[]>) {
      state.loginErrors = action.payload;
    },
    setAuthError(state, action: PayloadAction<string | null>) {
      state.authError = action.payload;
    },
    clearSession(state) {
      state.tokens = null;
      state.email = "";
      state.organization = "";
      state.isRefreshingToken = false;
      state.authError = null;
      state.role = "Employee";
    },
    setIsNewRegister(state, action: PayloadAction<boolean>) {
      state.isNewRegister = action.payload;
    },
    setIsRefreshingToken(state, action: PayloadAction<boolean>) {
      state.isRefreshingToken = action.payload;
    },
    setAccountRole(state, action: PayloadAction<RolePermission>) {
      state.role = action.payload;
    },
  },
});

export const {
  setOrganization,
  setEmail,
  setRegistrationErrors,
  setAccessToken,
  setRefreshToken,
  setTokens,
  clearSession,
  setIsNewRegister,
  setIsRefreshingToken,
  setLoginErrors,
  setAuthError,
  setAccountRole,
} = authSlice.actions;

const auth = authSlice.reducer;

export default auth;

export const fetchAccessToken = (
  authService: AuthFacade,
  data: LoginCredentials
): AppThunk => async (dispatch) => {
  try {
    const tokens = await authService.signIn(data);
    dispatch(setTokens(loginTokensToCamelCase(tokens)));
    dispatch(setEmail(data.email));
    dispatch(setOrganization(data.organization));
    dispatch(setAccountRole(tokens.role));
    dispatch(setLoginErrors([]));
  } catch (err) {
    if (!err.error) {
      setLoginErrors([{ key: "network", message: "Network Error" }]);
    } else {
      setLoginErrors(
        err.messages.map(([value, key]: [string, string]) => {
          return { key, message: value };
        })
      );
    }
  }
};

export const refreshAccessToken = (
  authService: AuthFacade,
  errorMessages: string[][],
  urlsToRefresh: string[] = []
): AppThunk => async (dispatch, getState) => {
  const { isRefreshingToken } = getState().auth;
  if (isRefreshingToken) {
    return;
  }
  if (errorMessages[0][0] === "does_not_exist") {
    dispatch(clearSession());
    authService.deleteAuthTokens();
    return;
  }
  try {
    dispatch(setIsRefreshingToken(true));
    const tokens = await authService.refreshAccessToken();
    if (!tokens) {
      console.log("Refresh tokens not generated");
      throw new Error("Refresh tokens not generated: Null result");
    }
    dispatch(setTokens(loginTokensToCamelCase(tokens)));
    dispatch(setAccountRole(tokens.role));
    authService.setAuthTokens(tokens.access_token, tokens.refresh_token);
    urlsToRefresh.forEach((url) => {
      mutate(url);
    });
  } catch (e) {
    console.log("Refresh Error", e);
    dispatch(clearSession());
    authService.deleteAuthTokens();
  } finally {
    dispatch(setIsRefreshingToken(false));
  }
};

export const clearLoginSession = (authService: AuthFacade): AppThunk => async (
  dispatch
) => {
  try {
    await authService.logOut();
    dispatch(clearSession());
  } catch (e) {
    dispatch(setAuthError("Could not log out"));
  }
};
