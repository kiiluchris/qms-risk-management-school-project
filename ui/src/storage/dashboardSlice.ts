import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { Role } from "@/domain/models/role";
import { DashboardUser } from "@/domain/models/user";
import { PaginationOptions } from "@/domain/models/shared";
import { ObjectiveWithId } from "@/domain/models/objective";
import { ProcessWithId } from "@/domain/models/process";
import { IncidentWithId } from "@/domain/models/incident";
import { CapaWithId } from "@/domain/models/capa";
import { TaskWithId } from "@/domain/models/task";
import { TaskTeamWithId } from "@/domain/models/task-team";
import { RiskWithId } from "@/domain/models/risk";
import { ProcessInputWithId } from "@/domain/models/process-input";
import { ProcessOutputWithId } from "@/domain/models/process-output";
import { UnitWithId } from "@/domain/models/unit";

type DashboardSection<T> = {
  items: T[];
};

type DashboardState = {
  roles: DashboardSection<Role>;
  users: DashboardSection<DashboardUser>;
  objectives: DashboardSection<ObjectiveWithId>;
  processes: DashboardSection<ProcessWithId>;
  incidents: DashboardSection<IncidentWithId>;
  capas: DashboardSection<CapaWithId>;
  tasks: DashboardSection<TaskWithId>;
  taskTeams: DashboardSection<TaskTeamWithId>;
  risks: DashboardSection<RiskWithId>;
  processInputs: DashboardSection<ProcessInputWithId>;
  processOutputs: DashboardSection<ProcessOutputWithId>;
  units: DashboardSection<UnitWithId>;
  pagination: Omit<PaginationOptions, "currentPage">;
  pageTitle: string;
};

const initialSectionState = <T>(items: T[]) => {
  return {
    items,
  };
};

const initialState: DashboardState = {
  roles: initialSectionState([]),
  users: initialSectionState([]),
  objectives: initialSectionState([]),
  processes: initialSectionState([]),
  tasks: initialSectionState([]),
  capas: initialSectionState([]),
  incidents: initialSectionState([]),
  taskTeams: initialSectionState([]),
  processInputs: initialSectionState([]),
  processOutputs: initialSectionState([]),
  risks: initialSectionState([]),
  units: initialSectionState([]),
  pagination: {
    pageSize: 10,
  },
  pageTitle: "",
};

const dashboardSlice = createSlice({
  name: "dashboard",
  initialState,
  reducers: {
    setDashboardUsers(state, action: PayloadAction<DashboardUser[]>) {
      state.users.items = action.payload;
    },
    setDashboardRoles(state, action: PayloadAction<Role[]>) {
      state.roles.items = action.payload;
    },
    setDashboardObjectives(state, action: PayloadAction<ObjectiveWithId[]>) {
      state.objectives.items = action.payload;
    },
    setDashboardProcesses(state, action: PayloadAction<ProcessWithId[]>) {
      state.processes.items = action.payload;
    },
    setDashboardIncidents(state, action: PayloadAction<IncidentWithId[]>) {
      state.incidents.items = action.payload;
    },
    setDashboardCapas(state, action: PayloadAction<CapaWithId[]>) {
      state.capas.items = action.payload;
    },
    setDashboardTasks(state, action: PayloadAction<TaskWithId[]>) {
      state.tasks.items = action.payload;
    },
    setDashboardTaskTeams(state, action: PayloadAction<TaskTeamWithId[]>) {
      state.taskTeams.items = action.payload;
    },
    setDashboardRisks(state, action: PayloadAction<RiskWithId[]>) {
      state.risks.items = action.payload;
    },
    setDashboardUnits(state, action: PayloadAction<UnitWithId[]>) {
      state.units.items = action.payload;
    },
    setDashboardProcessInputs(
      state,
      action: PayloadAction<ProcessInputWithId[]>
    ) {
      state.processInputs.items = action.payload;
    },
    setDashboardProcessOutputs(
      state,
      action: PayloadAction<ProcessOutputWithId[]>
    ) {
      state.processOutputs.items = action.payload;
    },
    changePageSize(state, action: PayloadAction<number>) {
      state.pagination.pageSize = action.payload;
    },
    setPageTitle(state, action: PayloadAction<string>) {
      state.pageTitle = action.payload;
    },
  },
});

export const {
  setDashboardRoles,
  setDashboardUsers,
  setDashboardObjectives,
  setDashboardProcesses,
  setDashboardCapas,
  setDashboardIncidents,
  setDashboardTasks,
  setDashboardTaskTeams,
  setDashboardProcessInputs,
  setDashboardProcessOutputs,
  setDashboardRisks,
  setDashboardUnits,
  setPageTitle,
  changePageSize,
} = dashboardSlice.actions;

export default dashboardSlice.reducer;
