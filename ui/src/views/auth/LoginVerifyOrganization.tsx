import AuthFormContainer from "@/components/forms/AuthFormContainer";
import styles from "@/components/forms/AuthFormContainer.module.scss";
import AuthSubmitButton from "@/components/forms/AuthSubmitButton";
import FormInputError from "@/components/forms/FormInputError";
import FormInput from "@/components/forms/FormInput";
import classnamesB from "classnames/bind";
import React, { useContext, useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { Redirect, Link, useLocation } from "react-router-dom";
import ApiContext from "@/api/api";
import * as z from "zod";
import { zodResolver } from "@hookform/resolvers/zod";

const schema = z.object({
  organization: z
    .string()
    .min(8)
    .max(16)
    .regex(
      /^[a-zA-Z][a-zA-Z0-9_-]+$/,
      "Domain must start with an alphabetic character and has no special characters except _ and -"
    ),
});

const cx = classnamesB.bind(styles);

type Props = {
  baseUrl: String;
};

const OrganizationUserLogin: React.FC<Props> = ({ baseUrl }) => {
  const location = useLocation();
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: zodResolver(schema),
  });
  const [error, setError] = useState("");
  const [redirect, setRedirect] = useState("");
  const { authService } = useContext(ApiContext);
  const redirectToLoginUser = (organization: string) => {
    setRedirect(`${location.pathname}/${organization}`);
  };
  useEffect(() => {
    document.title = "Verify Organization";
  });

  const submitForm = (data: any) => {
    console.log(data);
    setError("");
    authService.verifyOrganizationExists(data.organization).then(
      (exists) => {
        redirectToLoginUser(data.organization);
      },
      (err) => {
        if (err instanceof TypeError) {
          setError("Network Error");
          console.dir(err);
        }
      }
    );
  };

  if (redirect !== "") {
    return <Redirect push to={redirect} />;
  }

  return (
    <AuthFormContainer label="">
      <form className={cx("authForm")} onSubmit={handleSubmit(submitForm)}>
        <FormInput
          name="organization"
          placeholder="Organization Domain"
          type="text"
          register={register}
        />
        <FormInputError message={error || errors.organization?.message} />
        <AuthSubmitButton text="Select Organization" />
      </form>
      <div className="text-center">
        <p> Don't have an account?</p>
        <p>
          <Link to={`${baseUrl}/register`}>Register</Link>
        </p>
      </div>
    </AuthFormContainer>
  );
};

export default OrganizationUserLogin;
