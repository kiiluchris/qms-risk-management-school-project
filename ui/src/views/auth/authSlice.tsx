import { AppThunk } from "@/app/store";
import { LoginCredentials } from "@/domain/models/auth/LoginCredentials";
import { AuthFacade } from "@/domain/ports/AuthFacade";
import { OrganizationRegistrationDetails } from "@/domain/models/auth/OrganizationRegistrationDetails";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { Option, some, none } from "fp-ts/lib/Option";

type AuthState = {
  organization: Option<string>;
  email: Option<string>;
  registrationErrors: string[];
  accessToken: Option<string>;
  refreshToken: Option<string>;
};

const initialState: AuthState = {
  organization: none,
  email: none,
  registrationErrors: [],
  accessToken: none,
  refreshToken: none,
};

const authSlice = createSlice({
  name: "auth",
  initialState: initialState,
  reducers: {
    setOrganization(state, action: PayloadAction<string>) {
      const organization = action.payload;
      state.organization = some(organization);
    },
    setEmail(state, action: PayloadAction<string>) {
      const email = action.payload;
      state.email = some(email);
    },
    saveAccessToken(state, action: PayloadAction<string>) {
      state.accessToken = some(action.payload);
    },
    saveRefreshToken(state, action: PayloadAction<string>) {
      state.refreshToken = some(action.payload);
    },
    setRegistrationErrors(state, action: PayloadAction<string[]>) {
      const registrationErrors = action.payload;
      state.registrationErrors = registrationErrors;
    },
  },
});

export const {
  setOrganization,
  setEmail,
  setRegistrationErrors,
  saveAccessToken,
  saveRefreshToken,
} = authSlice.actions;

const authReducer = authSlice.reducer;

export default authReducer;

export const verifyOrganizationExists = (
  authFacade: AuthFacade,
  org: string
): AppThunk => (dispatch) => {
  return authFacade.verifyOrganizationExists(org).then((exists) => {
    dispatch(setOrganization(org));
  });
};

export const registerOrganizationAccount = (
  authFacade: AuthFacade,
  acc: OrganizationRegistrationDetails
): AppThunk => (dispatch) => {
  return authFacade
    .registerOrganizationAccount(acc)
    .catch((errors) => dispatch(setRegistrationErrors(errors)));
};
