import React, { useContext, useEffect } from "react";
import { useHistory, useRouteMatch } from "react-router-dom";

import classnamesB from "classnames/bind";
import AuthFormContainer from "@/components/forms/AuthFormContainer";
import AuthSubmitButton from "@/components/forms/AuthSubmitButton";
import styles from "@/components/forms/AuthFormContainer.module.scss";
import FormInput from "@/components/forms/FormInput";
import { useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import * as z from "zod";
import ApiContext from "@/api/api";
import { useQuery } from "@/utils/hooks";
import { ResetPasswordDetails } from "@/domain/models/auth";
import FormInputError from "@/components/forms/FormInputError";

const cx = classnamesB.bind(styles);

type Props = {
  baseUrl: string;
};

const schema = z
  .object({
    email: z.string().email(),
    organization: z.string(),
    password: z.string(),
    confirm_password: z.string(),
    token: z.string(),
  })
  .refine((data) => data.password === data.confirm_password, {
    message: "Passwords don't match",
    path: ["confirm_password"],
  });

type UrlProps = {
  organization: string;
};
const ResetPassword: React.FC<Props> = ({ baseUrl }) => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: zodResolver(schema),
  });
  const { authService } = useContext(ApiContext);
  const query = useQuery();
  const history = useHistory();
  const { params } = useRouteMatch<UrlProps>();
  const submitForm = (data: z.infer<typeof schema>) => {
    const req: ResetPasswordDetails = {
      email: data.email,
      password: data.password,
      token: data.token,
      organization: data.organization,
    };
    authService.resetPassword(req).then((_) => {
      history.push("/auth/login/" + data.organization);
    });
  };
  useEffect(() => {
    document.title = "Reset Password";
  });

  return (
    <AuthFormContainer label="Reset Password">
      <form
        action="#"
        className={cx("authForm")}
        onSubmit={handleSubmit(submitForm)}
      >
        <input
          type="hidden"
          value={query.get("t") || undefined}
          {...register("token")}
        />
        <FormInputError message={errors.token?.message} />
        <input
          type="hidden"
          value={params.organization}
          {...register("organization")}
        />
        <FormInputError message={errors.organization?.message} />
        <input
          type="hidden"
          value={query.get("e") || undefined}
          {...register("email")}
        />
        <FormInputError message={errors.email?.message} />
        <FormInput
          register={register}
          name="password"
          placeholder="Password"
          type="password"
        />
        <FormInput
          register={register}
          name="confirm_password"
          placeholder="Confirm Password"
          type="password"
        />
        <FormInputError message={errors.confirm_password?.message} />
        <AuthSubmitButton text="Reset Password" />
      </form>
    </AuthFormContainer>
  );
};

export default ResetPassword;
