import ApiContext from "@/api/api";
import { useRootState } from "@/app/rootReducer";
import AuthFormContainer from "@/components/forms/AuthFormContainer";
import styles from "@/components/forms/AuthFormContainer.module.scss";
import AuthSubmitButton from "@/components/forms/AuthSubmitButton";
import FormInput from "@/components/forms/FormInput";
import FormInputError from "@/components/forms/FormInputError";
import { LoginCredentials } from "@/domain/models/auth/LoginCredentials";
import { usePageTitle, useQuery } from "@/utils/hooks";
import { zodResolver } from "@hookform/resolvers/zod";
import classnamesB from "classnames/bind";
import React, { useContext, useState } from "react";
import { useForm } from "react-hook-form";
import { useDispatch } from "react-redux";
import { Link, Redirect, useRouteMatch } from "react-router-dom";
import * as z from "zod";
import { fetchAccessToken } from "@/storage/authSlice";

const schema = z.object({
  organization: z.string(),
  email: z.string().email(),
  password: z.string(),
  token: z.string(),
});
type Schema = z.infer<typeof schema>;
const cx = classnamesB.bind(styles);

type Props = {
  baseUrl: string;
};

type UrlProps = {
  organization: string;
};

const Login: React.FC<Props> = ({ baseUrl }) => {
  const [responseError, setResponseError] = useState("");
  const { authService } = useContext(ApiContext);
  const { params } = useRouteMatch<UrlProps>();
  const {
    register,
    handleSubmit,
    formState: { errors },
    setError,
  } = useForm({
    resolver: zodResolver(schema),
  });
  const { tokens, loginErrors } = useRootState((state) => state.auth);
  const dispatch = useDispatch();
  const query = useQuery();
  usePageTitle("Login");
  loginErrors.forEach((e) => {
    if (e.key === "network") {
      setResponseError(e.message);
    } else {
      setError(e.key as keyof Schema, { message: e.message });
    }
  });

  const submitForm = (data: LoginCredentials) => {
    setResponseError("");
    dispatch(fetchAccessToken(authService, data));
  };
  if (!!tokens) {
    return <Redirect push to={"/dashboard"} />;
  }

  return (
    <AuthFormContainer label="Login">
      <form
        action="#"
        className={cx("authForm")}
        onSubmit={handleSubmit(submitForm)}
      >
        <input
          type="hidden"
          value={query.get("t") || ""}
          {...register("token")}
        />
        <FormInputError message={responseError} />
        <FormInput
          name="organization"
          placeholder=""
          type="hidden"
          value={params.organization}
          register={register}
        />
        <FormInput
          autocomplete="email"
          name="email"
          placeholder="Email Address"
          type="email"
          register={register}
        />
        <FormInputError message={errors.email?.message} />
        <FormInput
          autocomplete="current-password"
          name="password"
          placeholder="Password"
          type="password"
          register={register}
        />
        <FormInputError message={errors.password?.message} />
        <AuthSubmitButton text="Login" />
      </form>
      <Link to={`${baseUrl}/forgot-password/${params.organization}`}>
        Forgot Password?
      </Link>
    </AuthFormContainer>
  );
};

export default Login;
