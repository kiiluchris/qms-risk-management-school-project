import React, { useContext, useEffect, useState } from "react";
import { useRouteMatch } from "react-router-dom";

import classnamesB from "classnames/bind";
import AuthFormContainer from "@/components/forms/AuthFormContainer";
import AuthSubmitButton from "@/components/forms/AuthSubmitButton";
import styles from "@/components/forms/AuthFormContainer.module.scss";
import FormInput from "@/components/forms/FormInput";
import { useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import * as z from "zod";
import ApiContext from "@/api/api";
import { ResetPasswordUser } from "@/domain/models/auth";

const cx = classnamesB.bind(styles);

type Props = {
  baseUrl: string;
};

const schema = z.object({
  email: z.string().email(),
  organization: z.string(),
  callback_url: z.string(),
});

type UrlProps = {
  organization: string;
};
const ForgotPassword: React.FC<Props> = ({ baseUrl }) => {
  const [isSent, setIsSent] = useState(false);
  const { register, handleSubmit } = useForm({
    resolver: zodResolver(schema),
  });
  const { authService } = useContext(ApiContext);
  const { params } = useRouteMatch<UrlProps>();
  const submitForm = (data: z.infer<typeof schema>) => {
    const req: ResetPasswordUser = {
      ...data,
      callback_url: data.callback_url + data.organization,
    };
    authService.requestPasswordReset(req).then(() => {
      setIsSent(true);
    });
  };
  useEffect(() => {
    document.title = "Forgot Password";
  });

  return (
    <AuthFormContainer label="Forgot Password">
      <p className="w-11/12">
        {!isSent
          ? "Enter your email address to be sent an email with a link to reset your password."
          : "Email has already been sent"}
      </p>

      <form
        action="#"
        className={cx("authForm")}
        onSubmit={handleSubmit(submitForm)}
      >
        <input
          type="hidden"
          value={`${window.location.origin}/auth/reset-password/`}
          {...register("callback_url")}
        />
        <input
          type="hidden"
          value={params.organization}
          {...register("organization")}
        />
        <FormInput
          register={register}
          autocomplete="email"
          name="email"
          placeholder="Email Address"
          type="email"
          disabled={isSent}
        />
        <AuthSubmitButton text="Request Password Reset" />
      </form>
    </AuthFormContainer>
  );
};

export default ForgotPassword;
