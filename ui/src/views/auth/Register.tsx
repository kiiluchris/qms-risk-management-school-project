import React, { useContext, useState } from "react";
import { Link, useHistory } from "react-router-dom";

import classnamesB from "classnames/bind";
import AuthFormContainer from "@/components/forms/AuthFormContainer";
import AuthSubmitButton from "@/components/forms/AuthSubmitButton";
import FormInputError from "@/components/forms/FormInputError";
import styles from "@/components/forms/AuthFormContainer.module.scss";
import FormInput from "@/components/forms/FormInput";
import { useForm } from "react-hook-form";
import * as z from "zod";
import { zodResolver } from "@hookform/resolvers/zod";
import ApiContext from "@/api/api";
import { OrganizationRegistrationDetails } from "@/domain/models/auth/OrganizationRegistrationDetails";
import { useDispatch } from "react-redux";
import {
  setEmail,
  setIsNewRegister,
  setOrganization,
} from "@/storage/authSlice";
import { usePageTitle } from "@/utils/hooks";

const schema = z.object({
  name: z.string(),
  domain: z.string().min(8).max(16),
  first_name: z.string().max(30),
  last_name: z.string().max(30),
  email: z.string().email(),
  phone: z.string().regex(/^[0-9]+$/, {
    message: "Phone number only consists of digits",
  }),
  password: z.string(),
  callback_url: z.string(),
});

const cx = classnamesB.bind(styles);

type Props = {
  baseUrl: string;
};

const Register: React.FC<Props> = ({ baseUrl }) => {
  const [responseError, setResponseError] = useState("");
  const {
    formState: { errors },
    register,
    handleSubmit,
    setError,
  } = useForm({
    resolver: zodResolver(schema),
  });
  const { authService } = useContext(ApiContext);
  const history = useHistory();
  const dispatch = useDispatch();
  usePageTitle("Register");

  const submitForm = (data: OrganizationRegistrationDetails) => {
    data.callback_url += data.domain;
    setResponseError("");
    authService.registerOrganizationAccount(data).then(
      (created) => {
        if (created) {
          dispatch(setOrganization(data.domain));
          dispatch(setEmail(data.email));
          dispatch(setIsNewRegister(true));
          history.push(`/auth/register/verify`); // TODO: Trigger account verification via email
        } else {
          setResponseError("Domain name already used");
        }
      },
      (err) => {
        if (!err.data) {
          setResponseError("Network Error");
          return;
        }
        const data = err.data;
        data.messages.forEach(
          ([value, key]: [string, keyof typeof schema._type]) => {
            setError(key, { message: value });
          }
        );
      }
    );
  };

  return (
    <AuthFormContainer label="Register">
      <form className={cx("authForm")} onSubmit={handleSubmit(submitForm)}>
        <input
          value={window.location.origin + "/auth/login/"}
          type="hidden"
          {...register("callback_url")}
        />
        <FormInputError message={responseError} />
        <FormInput
          name="name"
          placeholder="Organization"
          type="text"
          register={register}
        />
        <FormInputError message={errors.name?.message} />
        <FormInput
          name="domain"
          placeholder="Short name for organization."
          type="text"
          register={register}
        />
        <FormInputError message={errors.domain?.message} />
        <FormInput
          autocomplete="given-name"
          name="first_name"
          placeholder="First Name"
          type="text"
          register={register}
        />
        <FormInputError message={errors.first_name?.message} />
        <FormInput
          autocomplete="family-name"
          name="last_name"
          placeholder="Last Name"
          type="text"
          register={register}
        />
        <FormInputError message={errors.last_name?.message} />
        <FormInput
          autocomplete="email"
          name="email"
          placeholder="Email Address"
          type="email"
          register={register}
        />
        <FormInput
          name="phone"
          placeholder="Phone Number"
          type="tel"
          register={register}
        />
        <FormInputError message={errors.email?.message} />
        <FormInput
          autocomplete="current-password"
          name="password"
          placeholder="Password"
          type="password"
          register={register}
        />
        <FormInputError message={errors.password?.message} />
        <AuthSubmitButton text="Register" />
      </form>
      <div className="text-center">
        <p> Already have an account?</p>
        <Link to={`${baseUrl}/login`}>Login</Link>
      </div>
    </AuthFormContainer>
  );
};

export default Register;
