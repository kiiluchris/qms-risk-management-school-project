import React, { useEffect } from "react";
import AuthFormContainer from "@/components/forms/AuthFormContainer";
import { useSelector } from "react-redux";
import { RootState } from "@/app/rootReducer";
import { Redirect } from "react-router-dom";

export default function VerifyAccount() {
  const { isNewRegister, email } = useSelector(
    (state: RootState) => state.auth
  );
  useEffect(() => {
    document.title = "Verify Account";
  });
  if (!isNewRegister) {
    return <Redirect to="/auth/login" />;
  }
  return (
    <AuthFormContainer label="Verify Account">
      Check your email at the address {email} to verify your account.
    </AuthFormContainer>
  );
}
