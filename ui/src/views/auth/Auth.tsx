import React from "react";
import { match as Match, RouteProps } from "react-router";
import { Switch, Route, Redirect } from "react-router-dom";

import LoginVerifyOrganization from "./LoginVerifyOrganization";
import OrganizationUserLogin from "./OrganizationUserLogin";
import Register from "./Register";
import ForgotPassword from "./ForgotPassword";
import ResetPassword from "./ResetPassword";
import VerifyAccount from "./VerifyAccount";
import { useIsLoggedIn } from "@/app/rootReducer";

type Props = {
  match: Match<{}>;
};

const NoAuthRoute: React.FC<RouteProps> = ({ children, ...props }) => {
  const isLoggedIn = useIsLoggedIn();
  if (isLoggedIn) {
    return <Redirect to="/dashboard" />;
  }

  return <Route {...props}>{children}</Route>;
};

const Auth: React.FC<Props> = ({ match }) => {
  const { path } = match;

  return (
    <Switch>
      <NoAuthRoute exact path={`${path}/register`}>
        <Register baseUrl={path} />
      </NoAuthRoute>
      <NoAuthRoute exact path={`${path}/register/verify`}>
        <VerifyAccount />
      </NoAuthRoute>
      <NoAuthRoute path={`${path}/login/:organization`}>
        <OrganizationUserLogin baseUrl={path} />
      </NoAuthRoute>
      <NoAuthRoute path={`${path}/login`}>
        <LoginVerifyOrganization baseUrl={path} />
      </NoAuthRoute>
      <Route path={`${path}/forgot-password/:organization`}>
        <ForgotPassword baseUrl={path} />
      </Route>
      <Route path={`${path}/reset-password/:organization`}>
        <ResetPassword baseUrl={path} />
      </Route>
    </Switch>
  );
};

export default Auth;
