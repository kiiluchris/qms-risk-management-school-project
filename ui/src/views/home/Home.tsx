import React, { useState, useEffect, useRef } from "react";
import { Link } from "react-router-dom";
import classnames from "classnames";
import classes from "./Home.module.scss";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import InfoIcon from "@material-ui/icons/Info";
import ErrorOutlineIcon from "@material-ui/icons/ErrorOutline";
import FormatListBulletedIcon from "@material-ui/icons/FormatListBulleted";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import ButtonGroup from "@material-ui/core/ButtonGroup";

type BenefitProp = {
  iconProps: JSX.Element;
  text: string;
  tagline: string;
};

const Benefit: React.FC<BenefitProp> = ({ iconProps, text, tagline }) => {
  useEffect(() => {
    document.title = "Home";
  });
  return (
    <div className="max-w-64 text-center">
      {iconProps}
      <Typography variant="h5" color="textPrimary">
        {tagline}
      </Typography>
      <Typography color="textSecondary">{text}</Typography>
    </div>
  );
};

type Props = {};

const useScrolledPast = (element: React.RefObject<HTMLAnchorElement>) => {
  const [scrolledPast, setScrolledPast] = useState(false);
  useEffect(() => {
    const listener = () => {
      const target = element?.current;
      if (!target) return;
      const position = target.getBoundingClientRect();
      setScrolledPast(position.top < window.scrollY);
    };
    window.addEventListener("scroll", listener);
    return () => window.removeEventListener("scroll", listener);
  });

  return scrolledPast;
};

const Home: React.FC<Props> = () => {
  const getStartedButton = useRef<HTMLAnchorElement>(null);
  const firstYear = 2020;
  const yearNow = new Date().getFullYear();
  const hasScrolledPastGetStarted = useScrolledPast(getStartedButton);

  return (
    <div style={{ backgroundColor: "#e5e5e5" }}>
      <header
        className={classnames(
          "flex flex-row justify-end gap-4 p-4 bg-transparent top-0 right-0 w-screen",
          hasScrolledPastGetStarted ? "fixed laatu-bg-1" : "absolute"
        )}
        style={{ zIndex: 100000 }}
      >
        <Link to="/auth/register" className="text-white">
          Get Started
        </Link>
        <Link to="/auth/login" className="text-white">
          Sign in
        </Link>
      </header>
      <section
        className={classnames(classes.landing, "bg-cover")}
        style={{
          background: "rgba(28, 68, 97, 0.5)",
        }}
      >
        <div className="w-full h-full bg-blue-600 bg-black bg-opacity-20 grid content-center justify-center">
          <div
            className={classnames(
              classes.landingText,
              "text-white text-xl text-center flex flex-col gap-2"
            )}
          >
            <p className="text-3xl">Manage and respond to risk</p>
            <p>Ensure the quality of your business</p>
            <Link
              to="/auth/register"
              className="rounded laatu-bg-1 mt-2 p-2 self-center w-48 text-white"
            >
              Get Started
            </Link>
          </div>
        </div>
      </section>
      <section className="py-8 px-4" ref={getStartedButton}>
        <Paper elevation={10} className="py-8  rounded relative -top-16">
          <Typography
            variant="h3"
            color="textPrimary"
            className="text-center mb-24"
          >
            Modules
          </Typography>
          <div
            className="grid md:grid-cols-3 gap-8 px-4"
            style={{ padding: "70px 0" }}
          >
            <Benefit
              tagline="Risk Management"
              text="Records of business process outputs are used to check that if re-occurence of incidents is possible and preventive measures suggested"
              iconProps={<InfoIcon style={{ fontSize: "3.5rem" }} />}
            />
            <Benefit
              tagline="Process Management"
              text="Monitor your business processes to make find ensure standard are met and they operate as expected"
              iconProps={
                <FormatListBulletedIcon style={{ fontSize: "3.5rem" }} />
              }
            />
            <Benefit
              tagline="Incident Management"
              text="All parties responsible for a business process are notified when an incident occurs and possible preventive measures are suggested."
              iconProps={<ErrorOutlineIcon style={{ fontSize: "3.5rem" }} />}
            />
          </div>
          <div
            className="flex flex-col justify-center text-center items-center w-full"
            style={{ padding: "70px 0" }}
          >
            <Typography variant="h3" color="textPrimary">
              Work with us
            </Typography>
            <form
              onSubmit={(e) => {
                e.preventDefault();
              }}
            >
              <div className="md:flex w-full gap-4">
                <TextField label="Your Name" className="w-11/12 md:w-1/2" />
                <TextField label="Your Email" className="w-11/12 md:w-1/2" />
              </div>
              <div className="md:flex w-full gap-4">
                <TextField
                  multiline
                  label="Your Email"
                  className="w-11/12 md:w-1/2"
                />
              </div>
              <Button>SEND EMAIL</Button>
            </form>
          </div>
        </Paper>
      </section>
      <footer className="py-2">
        <p
          className="text-right font-medium pr-12"
          style={{ color: "#dadada" }}
        >
          &copy; {firstYear}
          {yearNow !== firstYear && ` - ${yearNow}`}
        </p>
      </footer>
    </div>
  );
};

export default Home;
