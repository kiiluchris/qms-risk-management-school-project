import ApiContext from "@/api/api";
import { useRootState } from "@/app/rootReducer";
import DashboardHeader from "@/components/dashboard/DashboardHeader";
import Sidebar from "@/components/dashboard/Sidebar";
import AuthRoute from "@/components/shared/AuthRoute";
import React, { useContext, useState } from "react";
import { Route, Switch } from "react-router-dom";
import CapaCrud from "../app/capa/CapaCrud";
import IncidentCrud from "../app/incident/IncidentCrud";
import MainDashboard from "../app/main/MainDashboard";
import ObjectiveCrud from "../app/objective/ObjectiveCrud";
import ProcessCrud from "../app/process/ProcessCrud";
import RiskCrud from "../app/risk/RiskCrud";
import RoleCrud from "../app/role/RoleCrud";
import TaskTeamCrud from "../app/task-team/TaskTeamCrud";
import TaskCrud from "../app/task/TaskCrud";
import UserCrud from "../app/user/UserCrud";
import Visualization from "../app/visualization/Visualization";
import Settings from "../app/settings/Settings";

type Props = {};

const Dashboard: React.FC<Props> = () => {
  const authOpError = useRootState((state) => state.auth.authError);
  const { email, organization } = useRootState((state) => state.auth);
  const { authService } = useContext(ApiContext);
  const [hideInMobile, setHideInMobile] = useState(true);
  const toggleSidebar = () => {
    setHideInMobile(!hideInMobile);
  };

  return (
    <div className="md:flex md:flex-row">
      <Sidebar
        hideInMobile={hideInMobile}
        toggleSidebar={() => {
          !hideInMobile && toggleSidebar();
        }}
        links={[]}
      />
      <section className="w-full">
        {authOpError && (
          <div className="w-full h-7 bg-red-500 text-white text-center font-bold">
            {authOpError}
          </div>
        )}
        <DashboardHeader
          organization={organization}
          email={email}
          authService={authService}
        />
        <div className="content min-h-screen bg-white">
          <Switch>
            <Route path="/roles">
              <RoleCrud />
            </Route>
            <Route path="/users">
              <UserCrud />
            </Route>
            <Route path="/objectives">
              <ObjectiveCrud />
            </Route>
            <Route path="/processes">
              <ProcessCrud />
            </Route>
            <Route path="/incidents">
              <IncidentCrud />
            </Route>
            <Route path="/capas">
              <CapaCrud />
            </Route>
            <Route path="/tasks">
              <TaskCrud />
            </Route>
            <Route path="/task-teams">
              <TaskTeamCrud />
            </Route>
            <Route path="/visualization">
              <Visualization />
            </Route>
            <Route path="/risks">
              <RiskCrud />
            </Route>
            <AuthRoute exact path="/dashboard">
              <MainDashboard />
            </AuthRoute>
          </Switch>
        </div>
        <span
          className="absolute bottom-3 right-3 w-12 h-12 laatu-bg-4 rounded-full md:hidden"
          onClick={() => toggleSidebar()}
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
            className="text-white text-center relative w-3/4 h-3/4 top-1.5 left-1.5"
          >
            {hideInMobile ? (
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth={2}
                d="M4 6h16M4 12h16M4 18h16"
              />
            ) : (
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth={2}
                d="M6 18L18 6M6 6l12 12"
              />
            )}
          </svg>
        </span>
      </section>
    </div>
  );
};

export default Dashboard;
