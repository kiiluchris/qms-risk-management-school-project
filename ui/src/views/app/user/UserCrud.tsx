import ApiContext from "@/api/api";
import { useRootState } from "@/app/rootReducer";
import DashboardCrud from "@/components/dashboard/DashboardCrud";
import { DetailItem } from "@/components/dashboard/DetailItem";
import { canDisplayMinimumRole } from "@/domain/models/role";
import { DashboardUser, gqlQuery, GqlUserWithDeps } from "@/domain/models/user";
import { setDashboardUsers } from "@/storage/dashboardSlice";
import Button from "@material-ui/core/Button";
import FormControl from "@material-ui/core/FormControl";
import FormHelperText from "@material-ui/core/FormHelperText";
import Paper from "@material-ui/core/Paper";
import TableCell from "@material-ui/core/TableCell";
import TextField from "@material-ui/core/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete";
import React, { useContext } from "react";
import { Controller } from "react-hook-form";
import { Link, useHistory } from "react-router-dom";
import * as z from "zod";
import { useSnackbar } from "@/utils/hooks";

const schema = z.object({
  first_name: z.string(),
  last_name: z.string(),
  email: z.string().email(),
  role: z.object({
    id: z.number(),
    name: z.string(),
  }),
  phone: z.string(),
  callback_url: z.string(),
});

export default function UserCrud() {
  const history = useHistory();
  const { users, accountRole } = useRootState((state) => ({
    users: state.dashboard.users.items,
    accountRole: state.auth.role,
  }));
  const { userService } = useContext(ApiContext);
  const { organization, email } = useRootState((state) => state.auth);
  const { Snackbar, setSnackbarSuccess, setSnackbarError } = useSnackbar();
  const formSubmitAction = (
    action: string,
    setError: React.Dispatch<React.SetStateAction<string>>
  ) => (itemId: string | number) => {
    if (itemId === -1) {
      setSnackbarError("User could not be " + action);
      setError("User could not be " + action);
    } else {
      setSnackbarSuccess("Successfully " + action + " user");
      setError("");
      history.push("/users");
    }
  };
  return (
    <DashboardCrud<
      DashboardUser,
      typeof schema,
      GqlUserWithDeps,
      GqlUserWithDeps["user"]
    >
      minimumRole="Admin"
      dashboardPath="users"
      schema={schema}
      items={users}
      itemsUpdater={setDashboardUsers}
      gqlQueries={gqlQuery}
      callback_url={`${window.location.origin}/auth/login/${organization}`}
      pageTitles={{
        create: "Create User",
        update: "Update User",
        listing: "Users",
        detail: "User Detail",
      }}
      itemName="User"
      remoteUrl="/users"
      readableKey="email"
      headers={["First Name", "Last Name", "Email", "Role"]}
      deletableItem={(user) => user.email !== email}
      renderItem={(user) => (
        <>
          <TableCell>{user.first_name}</TableCell>
          <TableCell>{user.last_name}</TableCell>
          <TableCell>
            <Link to={`/users/${user.id}`}>{user.email}</Link>
          </TableCell>
          <TableCell>{user.role.value}</TableCell>
        </>
      )}
      detailView={(user, deps, minimumRole) => {
        return (
          <Paper className="w-11/12 flex flex-col p-8 items-stretch gap-4">
            <div className="w-full flex justify-end">
              {canDisplayMinimumRole(minimumRole, accountRole) && (
                <Button
                  variant="contained"
                  color="primary"
                  component={Link}
                  to={`/users/${user.id}/edit`}
                >
                  Edit
                </Button>
              )}
            </div>
            <div className="w-full md:flex gap-4">
              <DetailItem
                title="Full Name"
                value={user.first_name + " " + user.last_name}
                className="w-4/5 md:w-1/2"
              />
              <DetailItem
                title="Role"
                value={user.role.name}
                className="w-4/5 md:w-1/2"
              />
            </div>
            <div className="w-full md:flex gap-4">
              <DetailItem
                title="Email"
                value={user.email}
                className="w-4/5 md:w-1/2"
              />

              <DetailItem
                title="Phone"
                value={user.phone}
                className="w-4/5 md:w-1/2"
              />
            </div>
          </Paper>
        );
      }}
      renderForm={(user, deps, useFormMethods) => {
        const {
          register,
          control,
          formState: { errors },
        } = useFormMethods;
        console.log(errors);
        return (
          <>
            <Snackbar />
            <input
              type="hidden"
              defaultValue={
                !user ? `${window.location.origin}/auth/reset-password/` : ""
              }
              {...register("callback_url")}
            />
            <div className="w-full md:flex gap-4">
              <Controller
                name="first_name"
                defaultValue={user?.first_name}
                control={control}
                render={({ field }) => (
                  <TextField
                    {...field}
                    className="w-11/12 md:w-1/2"
                    label="First Name"
                    error={!!errors.first_name}
                    helperText={errors.first_name?.message}
                  />
                )}
              />
              <Controller
                name="last_name"
                defaultValue={user?.last_name}
                control={control}
                render={({ field }) => (
                  <TextField
                    {...field}
                    className="w-11/12 md:w-1/2"
                    label="Last Name"
                    error={!!errors.last_name}
                    helperText={errors.last_name?.message}
                  />
                )}
              />
            </div>
            <div className="w-full md:flex gap-4">
              <Controller
                name="email"
                defaultValue={user?.email}
                control={control}
                render={({ field }) => (
                  <TextField
                    {...field}
                    label="Email"
                    className="w-11/12 md:w-1/2"
                    type="email"
                    error={!!errors.email}
                    helperText={errors.email?.message}
                  />
                )}
              />
              <Controller
                name="phone"
                defaultValue={user?.phone}
                control={control}
                render={({ field }) => (
                  <TextField
                    {...field}
                    label="Phone"
                    type="phone"
                    className="w-11/12 md:w-1/2"
                    error={!!errors.phone}
                    helperText={errors.phone?.message}
                  />
                )}
              />
            </div>
            <div className="w-full md:flex">
              <FormControl
                className="w-11/12 md:w-1/2"
                error={!!errors.role?.value}
              >
                <Controller
                  name="role"
                  control={control}
                  defaultValue={user?.role}
                  render={({ field }) => (
                    <Autocomplete
                      {...field}
                      onChange={(_evt, option) => {
                        field.onChange(option);
                      }}
                      options={deps?.roles || []}
                      getOptionLabel={(option, ...args) => option.name}
                      value={field.value}
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          label="Role"
                          className="w-full"
                        />
                      )}
                    />
                  )}
                />
                <FormHelperText>{errors.role?.value?.message}</FormHelperText>
              </FormControl>
            </div>
          </>
        );
      }}
      actions={{
        create: (data, useFormMethods, setGeneralError) => {
          userService
            .createUser({
              ...data,
              role: {
                id: data.role.id,
                value: data.role.name,
              },
              password: "",
            })
            .then(formSubmitAction("created", setGeneralError));
        },
        update: (data, useFormMethods, setGeneralError, itemId) => {
          userService
            .editUser(itemId!!, {
              ...data,
              role: {
                id: data.role.id,
                value: data.role.name,
              },
            })
            .then(formSubmitAction("updated", setGeneralError));
        },
        delete: userService.deleteUser,
      }}
    />
  );
}
