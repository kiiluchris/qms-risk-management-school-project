import ApiContext from "@/api/api";
import { useRootState } from "@/app/rootReducer";
import DashboardCrud from "@/components/dashboard/DashboardCrud";
import DashboardSelectInput from "@/components/dashboard/DashboardSelectInput";
import { DetailItem } from "@/components/dashboard/DetailItem";
import {
  GqlProcessWithDeps,
  gqlQuery,
  Process,
  ProcessWithId,
} from "@/domain/models/process";
import { GqlProcessValue } from "@/domain/models/process-value";
import { canDisplayMinimumRole } from "@/domain/models/role";
import { RelatedItem } from "@/domain/models/shared";
import { userSchema } from "@/domain/models/user";
import { setDashboardProcesses } from "@/storage/dashboardSlice";
import {
  genericSchemaToRelatedItem,
  reactSelectOptionToRelatedItem,
  userSchemaToRelatedItem,
  zodReactSelectOption,
  zodReactSelectOptionString,
} from "@/utils/form";

import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import { useSnackbar } from "@/utils/hooks";
import Button from "@material-ui/core/Button";
import green from "@material-ui/core/colors/green";
import red from "@material-ui/core/colors/red";
import FormControl from "@material-ui/core/FormControl";
import FormHelperText from "@material-ui/core/FormHelperText";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Paper from "@material-ui/core/Paper";
import Select from "@material-ui/core/Select";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import CancelIcon from "@material-ui/icons/Cancel";
import CheckCircleIcon from "@material-ui/icons/CheckCircle";
import Autocomplete, {
  createFilterOptions,
} from "@material-ui/lab/Autocomplete";
import React, { useContext } from "react";
import { Controller } from "react-hook-form";
import { Link, useHistory } from "react-router-dom";
import * as z from "zod";
import { userFullName } from "../main/risk-graph-utils";
import { genericSchema } from "@/utils/form";

const outputCreateFilter = createFilterOptions<
  GqlProcessValue & { inputValue?: string }
>();

const schema = z.object({
  name: z.string(),
  objective: genericSchema,
  process_inputs: z.array(genericSchema),
  process_outputs: z.array(genericSchema),
  monitoring_schedule: z.string().optional(),
  person_responsible: userSchema.optional(),
  team_responsible: genericSchema,
  /* tasks: z.array(
   *   z.object({
   *     is_complete: z.boolean(),
   *     name: z.string(),
   *     due_by: z.date(),
   *     id: z.string(),
   *   })
   * ), */
});

const formDataToDto = (
  data: z.infer<typeof schema>,
  process?: RelatedItem
): Process => {
  return {
    name: data.name,
    tasks: [],
    /* tasks: data.tasks
     *   .filter((task) => task.name !== "")
     *   .map((task) => ({
     *     ...task,
     *     due_by: jsDateFormatters.toIsoDate(task.due_by),
     *     id: task.id ? parseInt(task.id) : undefined,
     *     ...{ process },
     *   })), */
    objective: {
      id: data.objective.id,
      value: data.objective.name,
    },
    inputs: data.process_inputs.map((inp) => {
      return genericSchemaToRelatedItem(inp);
    }),
    outputs: data.process_outputs.map((outp) => {
      return genericSchemaToRelatedItem(outp);
    }),
    monitoring_schedule: data.monitoring_schedule,
    person_responsible:
      data.person_responsible &&
      userSchemaToRelatedItem(data.person_responsible),
    team_responsible:
      data.team_responsible &&
      genericSchemaToRelatedItem(data.team_responsible),
  };
};

export function ProcessCrud() {
  const history = useHistory();
  const { processService } = useContext(ApiContext);
  const { processes, accountRole } = useRootState((state) => ({
    processes: state.dashboard.processes.items,
    accountRole: state.auth.role,
  }));
  const { Snackbar, setSnackbarSuccess, setSnackbarError } = useSnackbar();
  const formSubmitAction = (
    action: string,
    setError: React.Dispatch<React.SetStateAction<string>>
  ) => (itemId: string | number) => {
    if (itemId === -1) {
      setSnackbarError("Process could not be " + action);
      setError("Process could not be " + action);
    } else {
      setSnackbarSuccess("Process successfully " + action);
      setError("");
      history.push("/processes");
    }
  };
  return (
    <DashboardCrud<
      ProcessWithId,
      typeof schema,
      GqlProcessWithDeps,
      GqlProcessWithDeps["process"]
    >
      minimumRole="Manager"
      minimumRoleReadOnly="Employee"
      dashboardPath="processes"
      headers={["Name", "Objective", "Team Responsible", "Related Tasks"]}
      schema={schema}
      defaultValues={{ person_responsible: undefined }}
      items={processes}
      itemsUpdater={setDashboardProcesses}
      gqlQueries={gqlQuery}
      renderItem={(process) => (
        <>
          <TableCell>
            <Link to={`/processes/${process.id}`}>{process.name}</Link>
          </TableCell>
          <TableCell>
            <Link to={`/objectives/${process.objective.id}`}>
              {process.objective.value}
            </Link>
          </TableCell>
          <TableCell>
            {process.team_responsible && (
              <Link to={`/task-teams/${process.team_responsible.id}`}>
                {process.team_responsible!!.readable}
              </Link>
            )}
          </TableCell>
          <TableCell>
            <Link to={`/tasks/?process=${process.id}`}>Tasks</Link>
          </TableCell>
        </>
      )}
      detailView={(process, deps, minimumRole) => {
        return (
          <Paper className="w-11/12 flex flex-col p-8 items-stretch gap-4">
            <div className="w-full flex justify-end">
              {canDisplayMinimumRole(minimumRole, accountRole) && (
                <Button
                  variant="contained"
                  color="primary"
                  component={Link}
                  to={`/processes/${process.id}/edit`}
                >
                  Edit
                </Button>
              )}
            </div>
            <div className="w-full md:flex gap-4">
              <DetailItem
                title="Process Name"
                value={process.name}
                className="w-4/5 md:w-1/2"
              />
              <DetailItem
                title="Objective"
                value={process.objective.name}
                className="w-4/5 md:w-1/2"
              />
            </div>
            <div className="w-full md:flex gap-4">
              <DetailItem
                title="Monitoring Schedule"
                value={process.monitoring_schedule || "None"}
                className="w-4/5 md:w-1/2"
              />
              <DetailItem
                title="Team Responsible"
                value={process.team_responsible.name}
                className="w-4/5 md:w-1/2"
              />
            </div>
            <div className="w-full flex xs:flex-col gap-4">
              <div className="w-11/12 md:w-1/2">
                <Typography variant="h6" color="textPrimary">
                  Inputs
                </Typography>
                <List className="overflow-auto" style={{ maxHeight: "12rem" }}>
                  {process.inputs.length ? (
                    process.inputs.map((r) => (
                      <ListItem key={`kr-${r.id}`}>
                        <ListItemText primary={r.value} />
                      </ListItem>
                    ))
                  ) : (
                    <ListItem>
                      <ListItemText primary="None" />
                    </ListItem>
                  )}
                </List>
              </div>
              <div className="w-11/12 md:w-1/2">
                <Typography variant="h6" color="textPrimary">
                  Outputs
                </Typography>
                <List className="overflow-auto" style={{ maxHeight: "12rem" }}>
                  {process.outputs.length ? (
                    process.outputs.map((r) => (
                      <ListItem key={`pr-${r.id}`}>
                        <ListItemText primary={r.value} />
                      </ListItem>
                    ))
                  ) : (
                    <ListItem>
                      <ListItemText primary="None" />
                    </ListItem>
                  )}
                </List>
              </div>
            </div>
            <div className="w-full flex xs:flex-col gap-4">
              <div className="w-11/12 md:w-1/2">
                <Typography variant="h6" color="textPrimary">
                  Known Risks
                </Typography>
                <List className="overflow-auto" style={{ maxHeight: "12rem" }}>
                  {process.known_risks.length ? (
                    process.known_risks.map((r) => (
                      <ListItem key={`kr-${r.id}`}>
                        <ListItemText primary={r.value} />
                      </ListItem>
                    ))
                  ) : (
                    <ListItem>
                      <ListItemText primary="None" />
                    </ListItem>
                  )}
                </List>
              </div>
              <div className="w-11/12 md:w-1/2">
                <Typography variant="h6" color="textPrimary">
                  Potential Risks
                </Typography>
                <List className="overflow-auto" style={{ maxHeight: "12rem" }}>
                  {process.potential_risks.length ? (
                    process.potential_risks.map((r) => (
                      <ListItem key={`pr-${r.id}`}>
                        <ListItemText primary={r.value} />
                      </ListItem>
                    ))
                  ) : (
                    <ListItem>
                      <ListItemText primary="None" />
                    </ListItem>
                  )}
                </List>
              </div>
            </div>
            <div className="w-full flex flex-col gap-4">
              <Typography variant="h6" color="textPrimary">
                Tasks
              </Typography>
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell>Name</TableCell>
                    <TableCell align="center">Is Completed</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {process.tasks.map((task) => {
                    return (
                      <TableRow key={task.name}>
                        <TableCell>{task.name}</TableCell>
                        <TableCell align="center">
                          {task.is_complete ? (
                            <CheckCircleIcon style={{ color: green[500] }} />
                          ) : (
                            <CancelIcon style={{ color: red[500] }} />
                          )}
                        </TableCell>
                      </TableRow>
                    );
                  })}
                </TableBody>
              </Table>
            </div>
          </Paper>
        );
      }}
      renderForm={(process, deps, useFormMethods): JSX.Element => {
        const { register, formState, control } = useFormMethods;
        const { errors } = formState;
        console.log(errors, formState);

        return (
          <>
            <Snackbar />
            <div className="w-full md:flex gap-4">
              <Controller
                name="name"
                defaultValue={process?.name}
                control={control}
                render={({ field }) => (
                  <TextField
                    {...field}
                    label="Name"
                    error={!!errors.name?.message}
                    helperText={errors.name?.message}
                    className="w-11/12 md:w-1/2"
                  />
                )}
              />
              <FormControl className="w-11/12 md:w-1/2">
                <InputLabel id="monitoring-schedule">
                  Monitoring Schedule
                </InputLabel>
                <Controller
                  name="monitoring_schedule"
                  defaultValue={process?.monitoring_schedule}
                  control={control}
                  render={({ field }) => (
                    <Select
                      className="w-full"
                      labelId="monitoring-schedule"
                      id="monitoring_schedule"
                      {...field}
                      error={!!errors?.monitoring_schedule}
                    >
                      {[
                        "Daily",
                        "Weekly",
                        "Monthly",
                        "Quarterly",
                        "Biannually",
                        "Annually",
                      ].map((model) => (
                        <MenuItem key={`statusel}`} value={model}>
                          {model}
                        </MenuItem>
                      ))}
                    </Select>
                  )}
                />
                <FormHelperText>
                  {errors.monitoring_schedule?.message}
                </FormHelperText>
              </FormControl>
            </div>
            <div className="w-full md:flex gap-4">
              <FormControl
                className="w-11/12 md:w-1/2"
                error={!!errors.objective}
              >
                <Controller
                  name="objective"
                  defaultValue={process?.objective}
                  control={control}
                  render={({ field }) => (
                    <Autocomplete
                      {...field}
                      onChange={(_, opt) => field.onChange(opt)}
                      options={deps?.objectives || []}
                      getOptionLabel={(option) => option.name}
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          label="Objective"
                          className="w-full"
                        />
                      )}
                    />
                  )}
                />
                <FormHelperText>{errors.objective?.message}</FormHelperText>
              </FormControl>
              {/* <FormControl
                  className="w-11/12 md:w-1/2"
                  error={!!errors.person_responsible}
                  >
                  <Controller
                  name="person_responsible"
                  defaultValue={process?.person_responsible}
                  control={control}
                  rules={{ required: false }}
                  render={({ field }) => (
                  <Autocomplete
                  {...field}
                  onChange={(_, opt) => field.onChange(opt)}
                  options={deps?.users || []}
                  getOptionLabel={(option) => userFullName(option)}
                  renderInput={(params) => (
                  <TextField
                  {...params}
                  label="Person Responsible"
                  className="w-full"
                  error={!!errors.person_responsible}
                  />
                  )}
                  />
                  )}
                  />
                  <FormHelperText>
                  {errors.person_responsible?.message}
                  </FormHelperText>
                  </FormControl> */}
              <FormControl
                className="w-11/12 md:w-1/2"
                error={!!errors.team_responsible}
              >
                <Controller
                  name="team_responsible"
                  defaultValue={process?.team_responsible}
                  control={control}
                  render={({ field }) => (
                    <Autocomplete
                      {...field}
                      onChange={(_, opt) => field.onChange(opt)}
                      options={deps?.task_teams || []}
                      getOptionLabel={(option) => option.name}
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          label="Team Responsible"
                          className="w-full"
                          error={!!errors.team_responsible}
                        />
                      )}
                    />
                  )}
                />
                <FormHelperText>
                  {errors.team_responsible?.message}
                </FormHelperText>
              </FormControl>
            </div>
            <div className="w-full md:flex gap-4">
              <FormControl
                className="w-11/12 md:w-1/2"
                error={!!errors.process_inputs}
              >
                <Controller
                  name="process_inputs"
                  defaultValue={process?.inputs}
                  control={control}
                  render={({ field }) => (
                    <Autocomplete
                      {...field}
                      className="w-full"
                      multiple
                      onChange={(_, opt) => field.onChange(opt)}
                      options={
                        deps?.process_values.filter((v) => {
                          return !process?.outputs.some((i) => i.id === v.id);
                        }) || []
                      }
                      getOptionLabel={(option) => option.name}
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          label="Process Inputs"
                          className="w-full"
                        />
                      )}
                    />
                  )}
                />
                <FormHelperText>
                  {errors.process_inputs?.message}
                </FormHelperText>
              </FormControl>
              <FormControl
                className="w-11/12 md:w-1/2"
                error={!!errors.process_outputs}
              >
                <Controller
                  name="process_outputs"
                  defaultValue={process?.outputs}
                  control={control}
                  render={({ field }) => (
                    <Autocomplete
                      {...field}
                      className="w-full"
                      multiple
                      selectOnFocus
                      clearOnBlur
                      handleHomeEndKeys
                      onChange={(_, opt) => {
                        field.onChange(
                          opt.map((v) => {
                            if (typeof v === "string") {
                              return {
                                id: -1,
                                name: v,
                              };
                            } else if (v?.inputValue) {
                              return {
                                id: -1,
                                name: v.inputValue,
                              };
                            } else {
                              return v;
                            }
                          })
                        );
                      }}
                      options={
                        deps?.process_values.filter((v) => {
                          return !process?.inputs.some((i) => i.id === v.id);
                        }) || []
                      }
                      filterOptions={(options, params) => {
                        const filtered = outputCreateFilter(options, params);
                        if (params.inputValue !== "") {
                          filtered.push({
                            inputValue: params.inputValue,
                            name: `Add "${params.inputValue}"`,
                            id: -1,
                          });
                        }
                        return filtered;
                      }}
                      getOptionLabel={(option) => {
                        if (typeof option === "string") {
                          return option;
                        }
                        if (option.inputValue) {
                          return option.inputValue;
                        }
                        return option.name;
                      }}
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          label="Process Outputs"
                          className="w-full"
                        />
                      )}
                    />
                  )}
                />
                <FormHelperText>
                  {errors.process_outputs?.message}
                </FormHelperText>
              </FormControl>
            </div>
          </>
        );
      }}
      pageTitles={{
        create: "Create Process",
        update: "Update Process",
        listing: "Processs",
        detail: "Process Detail",
      }}
      itemName="Process"
      remoteUrl="/processes"
      readableKey="name"
      actions={{
        create: (data, useFormMethods, setGeneralError) => {
          processService
            .create(formDataToDto(data))
            .then(formSubmitAction("created", setGeneralError));
        },
        update: (data, useFormMethods, setGeneralError, itemId) => {
          processService
            .update(
              itemId!!,
              formDataToDto(data, {
                id: itemId!!,
                value: "",
              })
            )
            .then(formSubmitAction("updated", setGeneralError));
        },
        delete: processService.delete,
      }}
    />
  );
}

export default ProcessCrud;
