import ApiContext from "@/api/api";
import Loader from "@/components/shared/Loader";
import { buildGqlQuery } from "@/domain/models/gql";
import { gqlQuery, SettingsUpdate } from "@/domain/models/settings";
import {
  useGqlQuery,
  usePageTitle,
  useRefreshLoginSession,
} from "@/utils/hooks";
import Button from "@material-ui/core/Button";
import FormControl from "@material-ui/core/FormControl";
import IconButton from "@material-ui/core/IconButton";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import Paper from "@material-ui/core/Paper";
import Select from "@material-ui/core/Select";
import Slider from "@material-ui/core/Slider";
import Snackbar from "@material-ui/core/Snackbar";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import AssignmentIcon from "@material-ui/icons/Assignment";
import Alert, { Color } from "@material-ui/lab/Alert";
import React, { useContext, useRef, useState } from "react";
import { useForm, Controller } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import { mutate } from "swr";
import * as z from "zod";
import {
  impactConfig,
  likelihoodConfigReversed,
  RiskLevelBounds,
  riskLevelStyle,
  riskLevelText,
} from "../main/risk-graph-utils";

const useStyles = makeStyles({
  slider: {
    width: 250,
  },
});

const makeWeightedRiskScoring = (
  riskMatrixWeight_: number
): RiskLevelBounds => {
  const riskMatrixWeight = 25 - riskMatrixWeight_;
  const upperRangeSize = 25 - riskMatrixWeight;
  const upperMidBound = Math.floor(upperRangeSize / 6) + riskMatrixWeight;
  const upperBound = riskMatrixWeight + Math.ceil((25 - upperMidBound) / 2);
  const lowerRangeSize = riskMatrixWeight - 1;
  const lowerMidBound = riskMatrixWeight - Math.floor(lowerRangeSize / 6);
  const lowerBound = Math.floor((lowerMidBound - 1) / 2) + 1;

  return { upperMidBound, upperBound, lowerMidBound, lowerBound };
};

const schema = z.object({
  name: z.string(),
  ml_model: z.string(),
  /* risk_likelihood_weight: z.number(),
   * risk_impact_weight: z.number(), */
});

export default function SettingsPage() {
  usePageTitle("Settings");
  const classes = useStyles();
  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: zodResolver(schema),
  });
  const [snackbar, setSnackBarState] = useState<{
    open: boolean;
    message: string;
    severity: Color;
  }>({
    open: false,
    severity: "info",
    message: "",
  });
  const handleClose = () => {
    setSnackBarState({ open: false, severity: "info", message: "" });
  };
  const { settingsService } = useContext(ApiContext);
  const { gqlData, gqlError } = useGqlQuery(gqlQuery);
  const queryKey = buildGqlQuery(gqlQuery);
  useRefreshLoginSession(gqlError?.status === 401 ? gqlError : undefined, [
    queryKey,
  ]);
  const org = gqlData?.settings;
  const apiTokenRef = useRef<HTMLInputElement>(null);

  if (!org || gqlError) {
    console.log(gqlError);
    return <Loader />;
  }
  const riskMatrixWeight =
    org?.risk_impact_weight * org?.risk_likelihood_weight;
  const weightedRiskScoring = makeWeightedRiskScoring(riskMatrixWeight);
  const submitForm = (formData: z.infer<typeof schema>) => {
    settingsService
      .update({
        ...formData,
        ml_model: formData.ml_model,
        risk_impact_weight: 3,
        risk_likelihood_weight: 3,
      })
      .then((result) => {
        if (result === -1) {
          setSnackBarState({
            message: "Account was not found and thus could not be updated",
            severity: "error",
            open: true,
          });
        } else {
          setSnackBarState({
            message: "Successfully updated account",
            severity: "success",
            open: true,
          });
          mutate(queryKey);
        }
      })
      .catch((e) => {
        setSnackBarState({
          message: "Account was not found and thus could not be updated",
          severity: "error",
          open: true,
        });
      });
  };
  const generateApiToken = () => {
    settingsService
      .generateToken()
      .then((token) => {
        setSnackBarState({
          message: "Successfully generated a new token",
          severity: "success",
          open: true,
        });
        mutate(queryKey);
      })
      .catch(() => {
        setSnackBarState({
          message: "Token generation failed",
          severity: "error",
          open: true,
        });
      });
  };
  const copyApiToken = () => {
    var copyText = apiTokenRef.current;
    if (!copyText) return;

    /* Select the text field */
    copyText.select();
    copyText.setSelectionRange(0, 99999); /* For mobile devices */

    /* Copy the text inside the text field */
    document.execCommand("copy");
  };

  return (
    <>
      <Snackbar
        open={snackbar.open}
        autoHideDuration={6000}
        onClose={handleClose}
      >
        <Alert onClose={handleClose} severity={snackbar.severity}>
          {snackbar.message}
        </Alert>
      </Snackbar>
      <Paper className="flex w-11/12 md:w-4/5 gap-6 flex-col items-center mx-auto py-8 px-8">
        <Typography variant="h5">Organization Settings</Typography>
        <form
          className="w-full flex flex-col gap-6 items-center px-2"
          noValidate
          onSubmit={handleSubmit(submitForm)}
        >
          <div className="md:flex w-full justify-around gap-4">
            <Controller
              name="organization_name"
              control={control}
              defaultValue={org.name}
              render={({ field }) => (
                <TextField
                  {...field}
                  id="organization_name"
                  label="Organization Name"
                  className="w-11/12 md:w-1/2"
                  error={!!errors.organization_name}
                  helperText={errors.organization_name?.message}
                />
              )}
            />
            <TextField
              disabled
              id="organization_domain"
              label="Organization Domain"
              value={org.domain}
              className="w-11/12 md:w-1/2"
            />
          </div>
          <div className="md:flex w-full gap-4">
            <Controller
              name="api_token"
              control={control}
              defaultValue={org.api_token}
              render={({ field }) => (
                <TextField
                  {...field}
                  id="api_token"
                  label="Api Token"
                  className="w-11/12 md:w-1/2"
                  InputProps={{
                    readOnly: true,
                  }}
                  inputRef={apiTokenRef}
                />
              )}
            />
            <Button
              variant="outlined"
              color="primary"
              onClick={() => generateApiToken()}
            >
              <Typography>Generate Token</Typography>
            </Button>
            <IconButton color="primary" onClick={() => copyApiToken()}>
              <AssignmentIcon />
            </IconButton>
          </div>
          <div className="md:flex w-full gap-4">
            {/* <FormControl className="w-11/12 md:w-1/2">
                <InputLabel id="ml-model-label">
                Main Machine Learning Model
                </InputLabel>
                <Controller
                name="ml_model"
                defaultValue={org.ml_model}
                control={control}
                render={({ field }) => (
                <Select labelId="ml-model-label" id="ml_model" {...field}>
                {gqlData?.ml_models.map((model) => (
                <MenuItem key={`ml_model_${model}`} value={model}>
                {model}
                </MenuItem>
                ))}
                </Select>
                )}
                />
                </FormControl>
                </div>
                <div className="w-full">
                <Typography variant="h6">Risk Matrix Weighting</Typography>
                <div className="flex">
                <div className="flex items-center">
                <Typography
                id="likelihood-slider"
                gutterBottom
                style={{
                transform: "rotate(-90deg)",
                }}
                >
                Likelihood
                </Typography>
                <Controller
                name="likelihood"
                control={control}
                defaultValue={org.risk_likelihood_weight}
                render={({ field }) => (
                <Slider
                {...field}
                style={{ height: 250 }}
                orientation="vertical"
                valueLabelDisplay="on"
                step={1}
                min={1}
                max={5}
                marks={true}
                aria-labelledby="likelihood-slider"
                />
                )}
                />
                </div>
                <Table
                cellPadding="5px"
                className="overflow-auto"
                style={{ display: "block" }}
                >
                <TableHead>
                <TableRow>
                <TableCell className="border border-black text-center"></TableCell>
                {impactConfig.map((impact) => {
                return (
                <TableCell
                key={`matrix-impact-${impact.name}`}
                className="border border-black text-center"
                >
                {impact.name}
                </TableCell>
                );
                })}
                </TableRow>
                </TableHead>
                <TableBody>
                {likelihoodConfigReversed.map((likelihood) => {
                return (
                <TableRow key={`heatmap-row-${likelihood.value}`}>
                <TableCell className="border border-black text-center">
                {likelihood.name}
                </TableCell>
                {impactConfig.map((impact) => {
                const score = likelihood.value * impact.value;
                return (
                <TableCell
                key={`heatmap-${score}`}
                style={riskLevelStyle(score, weightedRiskScoring)}
                className="border border-black text-center"
                >
                <Typography
                className="text-center text-white"
                style={{ fontWeight: 900 }}
                >
                {riskLevelText(score, weightedRiskScoring)}
                </Typography>
                </TableCell>
                );
                })}
                </TableRow>
                );
                })}
                </TableBody>
                </Table>
                </div>
                <div className="w-full flex flex-col items-center">
                <Controller
                name="impact"
                defaultValue={org.risk_impact_weight}
                control={control}
                render={({ field }) => (
                <Slider
                {...field}
                className={classes.slider}
                valueLabelDisplay="on"
                step={1}
                min={1}
                max={5}
                marks={true}
                aria-labelledby="impact-slider"
                />
                )}
                />
                <Typography id="likelihood-impact" gutterBottom>
                Impact
                </Typography>
                </div> */}
          </div>
          <Button variant="contained" color="primary" type="submit">
            Save Settings
          </Button>
        </form>
      </Paper>
    </>
  );
}
