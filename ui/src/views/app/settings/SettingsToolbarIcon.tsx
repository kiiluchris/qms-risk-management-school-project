import ApiContext from "@/api/api";
import { useRootState } from "@/app/rootReducer";
import { canDisplayMinimumRole } from "@/domain/models/role";
import { clearLoginSession } from "@/storage/authSlice";
import Divider from "@material-ui/core/Divider";
import IconButton from "@material-ui/core/IconButton";
import ListItemText from "@material-ui/core/ListItemText";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import React, { useContext } from "react";
import { useDispatch } from "react-redux";
import { Link } from "react-router-dom";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      paddingTop: 0,
      paddingBottom: 0,
      textAlign: "center",
    },
  })
);

export default function SettingsToolbarIcon() {
  const { authService } = useContext(ApiContext);
  const { email, organization, role: accountRole } = useRootState(
    (state) => state.auth
  );

  const dispatch = useDispatch();
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };
  const classes = useStyles();

  return (
    <>
      <IconButton
        color="inherit"
        aria-label="open account settings popup"
        edge="start"
        onClick={handleClick}
        aria-controls="account-settings-menu"
        aria-haspopup="true"
      >
        <AccountCircleIcon />
      </IconButton>
      <Menu
        id="account-settings-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
        /* style={{ ...styles.popper, top: "1rem" }}
        className={"min-w-content rounded-lg inline shadow p-4" + tooltipClass} */
      >
        <div className="flex w-64 justify-between text-center">
          <ListItemText primary="Organization" secondary={organization} />
          <ListItemText primary="Account" secondary={email} />
        </div>
        <Divider />
        {canDisplayMinimumRole("Admin", accountRole) && (
          <MenuItem
            button
            className={classes.root}
            to="/settings"
            component={Link}
            onClick={handleClose}
          >
            <ListItemText primary="Organization Settings" />
          </MenuItem>
        )}
        <MenuItem
          button
          className={classes.root}
          to="/account"
          component={Link}
          onClick={handleClose}
        >
          <ListItemText primary="Account Settings" />
        </MenuItem>
        <MenuItem
          className={classes.root}
          button
          onClick={() => dispatch(clearLoginSession(authService))}
        >
          <ListItemText primary={"Logout"} />
        </MenuItem>
      </Menu>
    </>
  );
}
