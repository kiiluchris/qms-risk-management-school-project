import Loader from "@/components/shared/Loader";
import { buildGqlQuery, GqlQuery } from "@/domain/models/gql";
import {
  useGqlQuery,
  usePageTitle,
  useRefreshLoginSession,
} from "@/utils/hooks";
import Box from "@material-ui/core/Box";
import Paper from "@material-ui/core/Paper";
import Tab from "@material-ui/core/Tab";
import Tabs from "@material-ui/core/Tabs";
import Typography from "@material-ui/core/Typography";
import React, { useState } from "react";
import {
  Bar,
  BarChart,
  CartesianGrid,
  Cell,
  Legend,
  Line,
  LineChart,
  Pie,
  PieChart,
  ResponsiveContainer,
  Tooltip as ChartTooltip,
  XAxis,
  YAxis,
} from "recharts";
import { graphColors } from "../main/risk-graph-utils";

type Summary = {
  count: number;
  label: string;
};
type ObjectiveSummary = {
  closed: Summary[];
  late: Summary[];
};
type GraphData = {
  reports: {
    incidents: {
      yearly: Summary[];
      monthly: {
        _2021: Summary[];
      };
    };
    objectives: {
      all_time: ObjectiveSummary;
      _2021: ObjectiveSummary;
    };
    capas: {
      preventive: Summary[];
      corrective: Summary[];
    };
  };
};
const gqlQuery: GqlQuery<GraphData, undefined> = {
  dependencies: `
     reports {
      incidents {
        yearly {
          count
          label
        }
        monthly {
          _2021 {
            count
            label
          }
        }
      }
      objectives {
        all_time {
          closed {
            count
            label
          }
          late {
            count
            label
          }
        }
        _2021 {
          closed {
            count
            label
          }
          late {
            count
            label
          }
        }
      }
      capas {
        corrective {
          count
          label
        }
        preventive {
          count
          label
        }
      }
    }
  `,
};

interface TabPanelProps {
  children?: React.ReactNode;
  index: any;
  value: any;
}

function TabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`scrollable-auto-tabpanel-${index}`}
      aria-labelledby={`scrollable-auto-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

function a11yProps(index: any) {
  return {
    id: `scrollable-auto-tab-${index}`,
    "aria-controls": `scrollable-auto-tabpanel-${index}`,
  };
}

const months: Record<string, string> = {
  1: "Jan",
  2: "Feb",
  3: "Mar",
  4: "Apr",
  5: "May",
  6: "June",
  7: "Jul",
  8: "Aug",
  9: "Sep",
  10: "Oct",
  11: "Nov",
  12: "Dec",
};

export function Reports(): JSX.Element {
  usePageTitle("Reports");
  const [currentTab, setCurrentTab] = useState(0);
  const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    setCurrentTab(newValue);
  };
  const { gqlData, gqlError } = useGqlQuery(gqlQuery);
  useRefreshLoginSession(gqlError?.status === 401 ? gqlError : undefined, [
    buildGqlQuery(gqlQuery),
  ]);
  if (!gqlData) {
    return <Loader />;
  }
  console.log(gqlData, gqlError);
  const { incidents, objectives, capas } = gqlData.reports;
  return (
    <Paper className="w-full flex flex-col gap-4">
      <Tabs
        value={currentTab}
        onChange={handleChange}
        indicatorColor="primary"
        textColor="primary"
        variant="scrollable"
        scrollButtons="auto"
        aria-label="scrollable auto tabs example"
      >
        <Tab label="Incidents" {...a11yProps(0)} />
        <Tab label="Objectives" {...a11yProps(1)} />
        <Tab label="Corrective and Preventive Actions" {...a11yProps(2)} />
      </Tabs>
      <TabPanel value={currentTab} index={0}>
        {/* <ButtonGroup
            variant="text"
            color="primary"
            aria-label="text button group primary"
            className="pl-9"
            >
            <Button>Monthly</Button>
            </ButtonGroup> */}
        <div className="w-full md:flex gap-4">
          <div className="w-1/2">
            <h3 className="text-xl mb-4 h-20">Incidents per month</h3>
            <ResponsiveContainer width="100%" height={300}>
              <LineChart
                data={incidents.monthly._2021.sort(
                  (a, b) => +a.label - +b.label
                )}
              >
                <XAxis
                  dataKey="label"
                  type="category"
                  tickFormatter={(x) => months[x]}
                />
                <YAxis />
                <Line dataKey="count" />
                <CartesianGrid stroke="#eee" strokeDasharray="5 5" />
                <ChartTooltip cursor={false} />
              </LineChart>
            </ResponsiveContainer>
          </div>
          <div className="w-1/2">
            <h3 className="text-xl mb-4 h-20">Yearly Incidents</h3>
            <ResponsiveContainer width="100%" height={300}>
              <BarChart
                data={incidents.yearly.sort((a, b) => +a.label - +b.label)}
              >
                <XAxis dataKey="label" type="category" />
                <YAxis />
                <Line dataKey="count" />
                <CartesianGrid stroke="#eee" strokeDasharray="5 5" />
                <Bar dataKey="count">
                  {incidents.yearly.map((_, index) => (
                    <Cell key={`cell-${index}`} fill={graphColors[index]} />
                  ))}
                </Bar>
                <ChartTooltip cursor={false} />
              </BarChart>
            </ResponsiveContainer>
          </div>
        </div>
      </TabPanel>
      <TabPanel value={currentTab} index={1}>
        <div className="w-full md:flex gap-4">
          <div className="w-full md:w-1/2">
            <Typography variant="h5" className="text-center text-blue-900">
              Completed Objectives
            </Typography>
            <ResponsiveContainer className="mx-auto" width="90%" height={300}>
              <PieChart width={400} height={400}>
                <Pie
                  data={objectives.all_time.closed}
                  nameKey="label"
                  dataKey="count"
                  stroke="#8884d8"
                >
                  {objectives.all_time.closed.map(({ label }, index) => (
                    <Cell
                      key={`cell-${index}`}
                      fill={graphColors[label === "Closed" ? 2 : 0]}
                    />
                  ))}
                </Pie>
                <ChartTooltip />
              </PieChart>
            </ResponsiveContainer>
          </div>
          <div className="w-full md:w-1/2">
            <Typography variant="h5" className="text-center text-blue-900">
              Delayed Objectives
            </Typography>
            <ResponsiveContainer className="mx-auto" width="90%" height={300}>
              <PieChart width={400} height={400}>
                <Pie
                  data={objectives.all_time.late}
                  nameKey="label"
                  dataKey="count"
                  stroke="#8884d8"
                >
                  {objectives.all_time.late.map(({ label }, index) => (
                    <Cell
                      key={`cell-${index}`}
                      fill={graphColors[label === "Timely" ? 2 : 0]}
                    />
                  ))}
                </Pie>
                <ChartTooltip />
              </PieChart>
            </ResponsiveContainer>
          </div>
        </div>
      </TabPanel>
      <TabPanel value={currentTab} index={2}>
        <div className="w-full md:flex gap-4">
          <div className="w-full md:w-1/2">
            <Typography variant="h5" className="text-center text-blue-900">
              Corrective Actions Response Time
            </Typography>
            <ResponsiveContainer className="mx-auto" width="90%" height={300}>
              <PieChart width={400} height={400}>
                <Pie
                  data={capas.corrective}
                  nameKey="label"
                  dataKey="count"
                  stroke="#8884d8"
                >
                  {capas.corrective.map(({ label }, index) => (
                    <Cell key={`cell-${index}`} fill={graphColors[index]} />
                  ))}
                </Pie>
                <Legend />
                <ChartTooltip />
              </PieChart>
            </ResponsiveContainer>
          </div>
          <div className="w-full md:w-1/2">
            <Typography variant="h5" className="text-center text-blue-900">
              Preventive Actions Response Time
            </Typography>
            <ResponsiveContainer className="mx-auto" width="90%" height={300}>
              <PieChart width={400} height={400}>
                <Pie
                  data={capas.preventive}
                  nameKey="label"
                  dataKey="count"
                  stroke="#8884d8"
                >
                  {capas.preventive.map(({ label }, index) => (
                    <Cell key={`cell-${index}`} fill={graphColors[index]} />
                  ))}
                </Pie>
                <Legend />
                <ChartTooltip />
              </PieChart>
            </ResponsiveContainer>
          </div>
        </div>
      </TabPanel>
    </Paper>
  );
}

export default Reports;
