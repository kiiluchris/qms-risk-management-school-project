import ApiContext from "@/api/api";
import DashboardCrud from "@/components/dashboard/DashboardCrud";
import DashboardInput from "@/components/dashboard/DashboardInput";
import React, { useContext } from "react";
import { useHistory } from "react-router-dom";
import * as z from "zod";
import { Role, GqlRoleWithDeps, gqlQuery } from "@/domain/models/role";
import { useRootState } from "@/app/rootReducer";
import { setDashboardRoles } from "@/storage/dashboardSlice";
import TableCell from "@material-ui/core/TableCell";

const schema = z.object({
  name: z.string(),
});

export default function RoleCrud() {
  const history = useHistory();
  const roles = useRootState((state) => state.dashboard.roles.items);
  const { roleService } = useContext(ApiContext);
  const formSubmitAction = (
    action: string,
    setError: React.Dispatch<React.SetStateAction<string>>
  ) => (itemId: string | number) => {
    if (itemId === -1) {
      setError("Role could not be " + action);
    } else {
      setError("");
      history.push("/roles");
    }
  };
  return (
    <DashboardCrud<
      Role,
      typeof schema,
      GqlRoleWithDeps,
      GqlRoleWithDeps["role"]
    >
      minimumRole="Admin"
      dashboardPath="roles"
      items={roles}
      gqlQueries={gqlQuery}
      itemsUpdater={setDashboardRoles}
      headers={["Name"]}
      schema={schema}
      renderForm={(role, deps, { register }) => (
        <>
          <div className="w-full">
            <DashboardInput
              name="name"
              placeholder="Name"
              defaultValue={role?.name}
              register={register}
            />
          </div>
        </>
      )}
      renderItem={(role) => (
        <>
          <TableCell>{role.name}</TableCell>
        </>
      )}
      pageTitles={{
        create: "Create Role",
        update: "Update Role",
        listing: "Roles",
        detail: "Role Detail",
      }}
      itemName="Role"
      remoteUrl="/roles"
      readableKey="name"
      actions={{
        create: (data, useFormMethods, setGeneralError) => {
          roleService
            .create({
              name: data.name,
            })
            .then(formSubmitAction("created", setGeneralError));
        },
        update: (data, useFormMethods, setGeneralError, itemId) => {
          roleService
            .update(itemId!!, data)
            .then(formSubmitAction("updated", setGeneralError));
        },
        delete: roleService.delete,
      }}
      deletableItem={(role) => role.name !== "Admin"}
      updatableItem={(role) => role.name !== "Admin"}
    />
  );
}
