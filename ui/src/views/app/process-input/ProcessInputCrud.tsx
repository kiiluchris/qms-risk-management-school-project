import ApiContext from "@/api/api";
import DashboardCrud from "@/components/dashboard/DashboardCrud";
import DashboardInput from "@/components/dashboard/DashboardInput";
import DashboardSelectInput from "@/components/dashboard/DashboardSelectInput";
import React, { useContext } from "react";
import { useHistory } from "react-router-dom";
import * as z from "zod";
import {
  GqlProcessInputWithDeps,
  ProcessInput,
  ProcessInputWithId,
  gqlQuery,
} from "@/domain/models/process-input";
import { useRootState } from "@/app/rootReducer";
import { setDashboardProcessInputs } from "@/storage/dashboardSlice";
import {
  reactSelectOptionToRelatedItem,
  zodReactSelectOption,
} from "@/utils/form";
import TableCell from "@material-ui/core/TableCell";

const schema = z.object({
  name: z.string(),
  value: z.number(),
  unit: zodReactSelectOption,
});

const formDataToDto = (data: z.infer<typeof schema>): ProcessInput => {
  return {
    ...data,
    unit: reactSelectOptionToRelatedItem(data.unit),
  };
};

export default function ProcessInputCrud() {
  const history = useHistory();
  const processInputs = useRootState(
    (state) => state.dashboard.processInputs.items
  );
  const { processInputService } = useContext(ApiContext);
  const formSubmitAction = (
    action: string,
    setError: React.Dispatch<React.SetStateAction<string>>
  ) => (itemId: string | number) => {
    if (itemId === -1) {
      setError("Process Input could not be " + action);
    } else {
      setError("");
      history.push("/process-inputs");
    }
  };
  return (
    <DashboardCrud<
      ProcessInputWithId,
      typeof schema,
      GqlProcessInputWithDeps,
      GqlProcessInputWithDeps["process_input"]
    >
      dashboardPath="process-inputs"
      items={processInputs}
      gqlQueries={gqlQuery}
      itemsUpdater={setDashboardProcessInputs}
      headers={["Name", "Number of Members"]}
      schema={schema}
      renderForm={(processInput, deps, useFormMethods) => {
        const {
          register,
          formState: { errors },
        } = useFormMethods;
        return (
          <>
            <div className="w-full">
              <DashboardInput
                name="name"
                placeholder="Name"
                defaultValue={processInput?.name}
                register={register}
                error={errors.name?.message}
              />
            </div>
          </>
        );
      }}
      renderItem={(processInput) => (
        <>
          <TableCell>{processInput.name}</TableCell>
        </>
      )}
      pageTitles={{
        create: "Create Process Input",
        update: "Update Process Input",
        listing: "Process Inputs",
detail: "Process Input Detail",
      }}
      itemName="Process Input"
      remoteUrl="/process-inputs"
      readableKey="name"
      actions={{
        create: (data, useFormMethods, setGeneralError) => {
          processInputService
            .create(formDataToDto(data))
            .then(formSubmitAction("created", setGeneralError));
        },
        update: (data, useFormMethods, setGeneralError, itemId) => {
          processInputService
            .update(itemId!!, formDataToDto(data))
            .then(formSubmitAction("updated", setGeneralError));
        },
        delete: processInputService.delete,
      }}
    />
  );
}
