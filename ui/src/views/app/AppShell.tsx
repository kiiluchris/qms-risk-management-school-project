import { useRootState } from "@/app/rootReducer";
import AuthRoute from "@/components/shared/AuthRoute";
import Account from "@/views/app/account/Account";
import CapaCrud from "@/views/app/capa/CapaCrud";
import IncidentCrud from "@/views/app/incident/IncidentCrud";
import MainDashboard from "@/views/app/main/MainDashboard";
import ModelAnalysis from "@/views/app/model-analysis/ModelAnalysis";
import ObjectiveCrud from "@/views/app/objective/ObjectiveCrud";
import ProcessCrud from "@/views/app/process/ProcessCrud";
import Reports from "@/views/app/reports/Reports";
import RiskCrud from "@/views/app/risk/RiskCrud";
import RoleCrud from "@/views/app/role/RoleCrud";
import Settings from "@/views/app/settings/Settings";
import TaskTeamCrud from "@/views/app/task-team/TaskTeamCrud";
import TaskCrud from "@/views/app/task/TaskCrud";
import UserCrud from "@/views/app/user/UserCrud";
import Visualization from "@/views/app/visualization/Visualization";
import { SvgIconProps } from "@material-ui/core";
import AppBar from "@material-ui/core/AppBar";
import Divider from "@material-ui/core/Divider";
import Drawer from "@material-ui/core/Drawer";
import Hidden from "@material-ui/core/Hidden";
import IconButton from "@material-ui/core/IconButton";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import {
  createStyles,
  makeStyles,
  Theme,
  useTheme,
} from "@material-ui/core/styles";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import AdjustIcon from "@material-ui/icons/Adjust";
import AssignmentIcon from "@material-ui/icons/Assignment";
import BarChartIcon from "@material-ui/icons/BarChart";
import DashboardIcon from "@material-ui/icons/Dashboard";
import ErrorOutlineIcon from "@material-ui/icons/ErrorOutline";
import FormatListBulletedIcon from "@material-ui/icons/FormatListBulleted";
import HealingIcon from "@material-ui/icons/Healing";
import InfoIcon from "@material-ui/icons/Info";
import MenuIcon from "@material-ui/icons/Menu";
import PeopleOutlineIcon from "@material-ui/icons/PeopleOutline";
import PersonOutlineIcon from "@material-ui/icons/PersonOutline";
import TableChartIcon from "@material-ui/icons/TableChart";
import React from "react";
import { Link, Route, Switch } from "react-router-dom";
import SettingsToolbarIcon from "./settings/SettingsToolbarIcon";
const drawerWidth = 240;

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: "flex",
    },
    drawer: {
      [theme.breakpoints.up("sm")]: {
        width: drawerWidth,
        flexShrink: 0,
      },
    },
    appBar: {
      [theme.breakpoints.up("sm")]: {
        width: `calc(100% - ${drawerWidth}px)`,
        marginLeft: drawerWidth,
      },
    },
    menuButton: {
      marginRight: theme.spacing(2),
      [theme.breakpoints.up("sm")]: {
        display: "none",
      },
    },
    // necessary for content to be below app bar
    toolbar: theme.mixins.toolbar,
    drawerPaper: {
      width: drawerWidth,
    },
    content: {
      flexGrow: 1,
      padding: theme.spacing(3),
    },
  })
);

interface Props {
  /**
   * Injected by the documentation to work in an iframe.
   * You won't need it on your project.
   */
  window?: () => Window;
}

type DrawerLink = {
  path: string;
  text: string;
  icon?: React.ReactElement<SvgIconProps>;
  /* icon?: JSX.Element; */
  separator?: boolean;
};

const drawerSeparator: DrawerLink = {
  text: "",
  path: "",
  separator: true,
  icon: <DashboardIcon />,
};

const drawerLinks: DrawerLink[] = [
  drawerSeparator,
  { text: "Dashboard", path: "/dashboard", icon: <DashboardIcon /> },
  { text: "Users", path: "/users", icon: <PersonOutlineIcon /> },
  { text: "Departments", path: "/departments", icon: <PeopleOutlineIcon /> },
  drawerSeparator,
  { text: "Objectives", path: "/objectives", icon: <AdjustIcon /> },
  { text: "Processes", path: "/processes", icon: <FormatListBulletedIcon /> },
  { text: "Tasks", path: "/tasks", icon: <AssignmentIcon /> },
  drawerSeparator,
  { text: "Incidents", path: "/incidents", icon: <ErrorOutlineIcon /> },
  { text: "Risks", path: "/risks", icon: <InfoIcon /> },
  { text: "Capas", path: "/capas", icon: <HealingIcon /> },
  drawerSeparator,
  { text: "Model Analysis", path: "/model-analysis", icon: <BarChartIcon /> },
  { text: "Reports", path: "/reports", icon: <TableChartIcon /> },
  drawerSeparator,
];

export default function AppShell(props: Props) {
  const { window } = props;
  const classes = useStyles();
  const theme = useTheme();
  const [mobileOpen, setMobileOpen] = React.useState(false);
  const { pageTitle, accountRole } = useRootState((state) => {
    return {
      pageTitle: state.dashboard.pageTitle,
      accountRole: state.auth.role,
    };
  });

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  const drawer = (
    <div>
      <div className={classes.toolbar} />
      <List>
        {drawerLinks
          .filter(({ text, path }) => {
            console.log("AccountRole", accountRole);
            return (
              accountRole === "Admin" || !["/roles", "/users"].includes(path)
            );
          })
          .map(({ text, path, icon, separator = false }, index) =>
            separator ? (
              <Divider key={`separator-${index}`} />
            ) : (
              <ListItem button key={text} component={Link} to={path}>
                <ListItemIcon>{icon}</ListItemIcon>
                <ListItemText primary={text} />
              </ListItem>
            )
          )}
      </List>
    </div>
  );

  const container =
    window !== undefined ? () => window().document.body : undefined;

  return (
    <div className={classes.root}>
      <AppBar position="fixed" className={classes.appBar}>
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            edge="start"
            onClick={handleDrawerToggle}
            className={classes.menuButton}
          >
            <MenuIcon />
          </IconButton>
          <div className="w-full flex justify-between items-center">
            <Typography variant="h6" noWrap className="align-middle">
              {pageTitle}
            </Typography>
            <SettingsToolbarIcon />
          </div>
        </Toolbar>
      </AppBar>
      <nav className={classes.drawer} aria-label="mailbox folders">
        {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
        <Hidden smUp implementation="css">
          <Drawer
            container={container}
            variant="temporary"
            anchor={theme.direction === "rtl" ? "right" : "left"}
            open={mobileOpen}
            onClose={handleDrawerToggle}
            classes={{
              paper: classes.drawerPaper,
            }}
            ModalProps={{
              keepMounted: true, // Better open performance on mobile.
            }}
          >
            {drawer}
          </Drawer>
        </Hidden>
        <Hidden xsDown implementation="css">
          <Drawer
            classes={{
              paper: classes.drawerPaper,
            }}
            variant="permanent"
            open
          >
            {drawer}
          </Drawer>
        </Hidden>
      </nav>
      <main className={classes.content}>
        <div className={classes.toolbar} />
        <Switch>
          <Route path="/roles">
            <RoleCrud />
          </Route>
          <Route path="/users">
            <UserCrud />
          </Route>
          <Route path="/objectives">
            <ObjectiveCrud />
          </Route>
          <Route path="/processes">
            <ProcessCrud />
          </Route>
          <Route path="/incidents">
            <IncidentCrud />
          </Route>
          <Route path="/capas">
            <CapaCrud />
          </Route>
          <Route path="/tasks">
            <TaskCrud />
          </Route>
          <Route path="/departments">
            <TaskTeamCrud />
          </Route>
          <Route path="/risks">
            <RiskCrud />
          </Route>
          <AuthRoute exact path="/dashboard">
            <MainDashboard />
          </AuthRoute>
          <AuthRoute exact path="/settings">
            <Settings />
          </AuthRoute>
          <AuthRoute exact path="/account">
            <Account />
          </AuthRoute>
          <AuthRoute exact path="/reports">
            <Reports />
          </AuthRoute>
          <AuthRoute exact path="/model-analysis">
            <ModelAnalysis />
          </AuthRoute>
        </Switch>
      </main>
    </div>
  );
}
