import ApiContext from "@/api/api";
import { useRootState } from "@/app/rootReducer";
import DashboardCrud from "@/components/dashboard/DashboardCrud";
import DashboardDatePickerInput from "@/components/dashboard/DashboardDatePickerInput";
import DashboardInput from "@/components/dashboard/DashboardInput";
import DashboardSelectInput from "@/components/dashboard/DashboardSelectInput";
import { DetailItem } from "@/components/dashboard/DetailItem";
import { FormSubmitError } from "@/domain/models/errors";
import {
  GqlObjectiveWithDeps,
  gqlQuery,
  Objective,
  ObjectiveWithId,
} from "@/domain/models/objective";
import { setDashboardObjectives } from "@/storage/dashboardSlice";
import {
  reactSelectOptionToRelatedItem,
  toRelatedItem,
  zodReactSelectOption,
} from "@/utils/form";
import { jsDateFormatters, jsDateParsers } from "@/utils/time";
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Typography from "@material-ui/core/Typography";
import React, { useContext } from "react";
import { Link, useHistory } from "react-router-dom";
import * as z from "zod";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { Controller } from "react-hook-form";
import TextField from "@material-ui/core/TextField";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import FormHelperText from "@material-ui/core/FormHelperText";
import { canDisplayMinimumRole } from "@/domain/models/role";
import { useSnackbar } from "@/utils/hooks";
import { KeyboardDatePicker } from "@material-ui/pickers";
import { userSchema } from "@/domain/models/user";
import { userFullName } from "../main/risk-graph-utils";

const schema = z.object({
  name: z.string(),
  kpi: z.string(),
  current_status: z.string(),
  target_date: z.date(),
  description: z.string().optional(),
  person_responsible: userSchema,
});

const formDataToDto = (data: z.infer<typeof schema>): Objective => {
  return {
    ...data,
    outputs: [],
    target_date: jsDateFormatters.toIsoDate(data.target_date),
    person_responsible: toRelatedItem(data.person_responsible, userFullName)!,
    current_status: data.current_status,
    description: "",
  };
};

export function ObjectiveCrud() {
  const history = useHistory();
  const { objectiveService } = useContext(ApiContext);
  const { objectives, accountRole } = useRootState((state) => ({
    objectives: state.dashboard.objectives.items,
    accountRole: state.auth.role,
  }));
  const { Snackbar, setSnackbarSuccess, setSnackbarError } = useSnackbar();
  const formSubmitErrorHandler = (
    /* {
     * messages,
     * error,
     * headers, */
    err: FormSubmitError
  ) => {
    console.log(err);
    setSnackbarError("Network Error");
  };
  const formSubmitAction = (
    action: string,
    setError: React.Dispatch<React.SetStateAction<string>>
  ) => (itemId: string | number) => {
    if (itemId === -1) {
      setSnackbarError("Objective could not be " + action);
      setError("Objective could not be " + action);
    } else {
      setSnackbarSuccess("Objective successfully " + action);
      setError("");
      history.push("/objectives");
    }
  };
  return (
    <DashboardCrud<
      ObjectiveWithId,
      typeof schema,
      GqlObjectiveWithDeps,
      GqlObjectiveWithDeps["objective"]
    >
      minimumRole="Manager"
      minimumRoleReadOnly="Employee"
      dashboardPath="objectives"
      headers={[
        "Name",
        "Key Performance Indicator",
        "Status",
        "Target Date Complete",
      ]}
      renderItem={(objective) => (
        <>
          <TableCell>
            <Link to={`/objectives/${objective.id}`}>{objective.name}</Link>
          </TableCell>
          <TableCell>{objective.kpi}</TableCell>
          <TableCell>{objective.current_status}</TableCell>
          <TableCell>{objective.target_date}</TableCell>
        </>
      )}
      gqlQueries={gqlQuery}
      schema={schema}
      items={objectives}
      itemsUpdater={setDashboardObjectives}
      detailView={(objective, deps, minimumRole) => {
        return (
          <Paper className="w-11/12 flex flex-col p-8 items-stretch gap-4">
            <div className="w-full flex justify-end">
              {canDisplayMinimumRole(minimumRole, accountRole) && (
                <Button
                  variant="contained"
                  color="primary"
                  component={Link}
                  to={`/objectives/${objective.id}/edit`}
                >
                  Edit
                </Button>
              )}
            </div>
            <div className="w-full md:flex gap-4">
              <DetailItem
                title="Objective Name"
                value={objective.name}
                className="w-4/5 md:w-1/2"
              />
              <DetailItem
                title="Current Status"
                value={objective.current_status}
                className="w-4/5 md:w-1/2"
              />
            </div>
            <div className="w-full md:flex  gap-4">
              <DetailItem
                title="Key Performance Indicator"
                value={objective.kpi}
                className="w-4/5 md:w-1/2"
              />
              <DetailItem
                title="Deadline"
                value={objective.target_date}
                className="w-4/5 md:w-1/2"
              />
            </div>
            <div className="w-full md:flex  gap-4">
              <DetailItem
                title="Person Responsible"
                value={
                  objective.person_responsible.first_name +
                  " " +
                  objective.person_responsible.last_name
                }
                className="w-4/5 md:w-1/2"
              />
            </div>
            <div className="w-full flex flex-col gap-4">
              <Typography variant="h6" color="textPrimary">
                Processes
              </Typography>
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell>Name</TableCell>
                    <TableCell>Team Responsible</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {objective.processes.map((proc) => {
                    return (
                      <TableRow key={proc.name}>
                        <TableCell>{proc.name}</TableCell>
                        <TableCell>{proc.team_responsible.name}</TableCell>
                      </TableRow>
                    );
                  })}
                </TableBody>
              </Table>
            </div>
          </Paper>
        );
      }}
      renderForm={(objective, deps, useFormMethods) => {
        const {
          register,
          formState: { errors },
          control,
        } = useFormMethods;
        return (
          <>
            <Snackbar />
            <div className="w-full md:flex gap-4">
              <Controller
                name="name"
                defaultValue={objective?.name}
                control={control}
                render={({ field }) => (
                  <TextField
                    {...field}
                    label="Name"
                    className="w-11/12 md:w-1/2"
                    error={!!errors.name}
                    helperText={errors.name?.message}
                  />
                )}
              />
              <FormControl className="w-11/12 md:w-1/2">
                <InputLabel id="current-status">Current Status</InputLabel>
                <Controller
                  name="current_status"
                  defaultValue={objective?.current_status}
                  control={control}
                  render={({ field }) => (
                    <Select
                      className="w-full"
                      labelId="current-status"
                      id="current_status"
                      {...field}
                      error={!!errors?.current_status}
                    >
                      {["Open", "Closed"].map((model) => (
                        <MenuItem key={`status${model}`} value={model}>
                          {model}
                        </MenuItem>
                      ))}
                    </Select>
                  )}
                />
                <FormHelperText>
                  {errors.current_status?.message}
                </FormHelperText>
              </FormControl>
            </div>
            <div className="w-full md:flex gap-4">
              <Controller
                name="kpi"
                defaultValue={objective?.kpi}
                control={control}
                render={({ field }) => (
                  <TextField
                    {...field}
                    label="Key Performance Indicator"
                    error={!!errors.kpi?.message}
                    helperText={errors.kpi?.message}
                    className="w-11/12 md:w-1/2"
                  />
                )}
              />
              <Controller
                name="target_date"
                defaultValue={
                  objective?.target_date
                    ? jsDateParsers.fromIso(objective?.target_date)
                    : new Date()
                }
                control={control}
                render={({ field }) => (
                  <FormControl
                    className="w-11/12 md:w-1/2"
                    error={!!errors.target_date?.message}
                  >
                    <KeyboardDatePicker
                      {...field}
                      disableToolbar
                      variant="inline"
                      format="yyyy-MM-dd"
                      className="w-11/12 md:w-1/2"
                      label="Target Date of Completion"
                      error={!!errors.target_date?.message}
                    />
                    <FormHelperText>
                      {errors.target_date?.message}
                    </FormHelperText>
                  </FormControl>
                )}
              />
            </div>
            <div className="w-full md:flex gap-4">
              <FormControl
                className="w-11/12 md:w-1/2"
                error={!!errors.person_responsible}
              >
                <Controller
                  name="person_responsible"
                  defaultValue={objective?.person_responsible}
                  control={control}
                  render={({ field }) => (
                    <Autocomplete
                      {...field}
                      onChange={(_, opt) => field.onChange(opt)}
                      options={deps?.users || []}
                      getOptionLabel={(option) => userFullName(option)}
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          label="Person Responsible"
                          className="w-full"
                        />
                      )}
                    />
                  )}
                />
                <FormHelperText>
                  {errors.person_responsible?.message}
                </FormHelperText>
              </FormControl>
              <div></div>
            </div>
            <div className="w-full flex"></div>
          </>
        );
      }}
      pageTitles={{
        create: "Create Objective",
        update: "Update Objective",
        listing: "Objectives",
        detail: "Objective Detail",
      }}
      itemName="Objective"
      remoteUrl="/objectives"
      readableKey="name"
      actions={{
        create: (data, _useFormMethods, setGeneralError): void => {
          objectiveService
            .create(formDataToDto(data))
            .then(formSubmitAction("created", setGeneralError))
            .catch(formSubmitErrorHandler);
        },
        update: (data, _useFormMethods, setGeneralError, itemId) => {
          objectiveService
            .update(itemId!!, formDataToDto(data))
            .then(formSubmitAction("updated", setGeneralError));
        },
        delete: objectiveService.delete,
      }}
    />
  );
}

export default ObjectiveCrud;
