import ApiContext from "@/api/api";
import { useRootState } from "@/app/rootReducer";
import DashboardCrud from "@/components/dashboard/DashboardCrud";
import DashboardDatePickerInput from "@/components/dashboard/DashboardDatePickerInput";
import DashboardInput from "@/components/dashboard/DashboardInput";
import DashboardSelectInput from "@/components/dashboard/DashboardSelectInput";
import { DetailItem } from "@/components/dashboard/DetailItem";
import {
  GqlIncidentWithDeps,
  gqlQuery,
  Incident,
  IncidentWithId,
} from "@/domain/models/incident";
import { setDashboardIncidents } from "@/storage/dashboardSlice";
import {
  genericSchema,
  genericSchemaToRelatedItem,
  reactSelectOptionToRelatedItem,
  reactSelectOptionToRelatedItemOpt,
  toRelatedItem,
  userSchemaToRelatedItem,
  zodReactSelectOption,
} from "@/utils/form";
import { jsDateFormatters, jsDateParsers } from "@/utils/time";
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Typography from "@material-ui/core/Typography";
import React, { useContext } from "react";
import { Link, useHistory } from "react-router-dom";
import * as z from "zod";
import { userFullName } from "../main/risk-graph-utils";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { Controller } from "react-hook-form";
import TextField from "@material-ui/core/TextField";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import FormHelperText from "@material-ui/core/FormHelperText";
import { canDisplayMinimumRole } from "@/domain/models/role";
import { useSnackbar } from "@/utils/hooks";
import { KeyboardDatePicker } from "@material-ui/pickers";
import { userSchema } from "@/domain/models/user";

const schema = z.object({
  name: z.string(),
  risk: genericSchema.optional(),
  reported_by: userSchema,
  location: z.string(),
  date_occurred: z.date(),
  lag_time: z.string(),
  damage: z.string(),
  category: genericSchema,
  person_responsible: userSchema,
  first_aid_provided_by: userSchema,
  affected_processes: z.array(genericSchema),
});

const formDataToDto = (data: z.infer<typeof schema>): Incident => {
  return {
    name: data.name,
    risk: data.risk && genericSchemaToRelatedItem(data.risk),
    reported_by: userSchemaToRelatedItem(data.reported_by),
    location: data.location,
    date_occurred: jsDateFormatters.toIsoDate(data.date_occurred),
    lag_time: data.lag_time,
    /* activity: "",
    activity_details: "", */
    damage: data.damage,
    category: genericSchemaToRelatedItem(data.category),
    person_responsible: userSchemaToRelatedItem(data.person_responsible),
    first_aid_provided_by: userSchemaToRelatedItem(data.first_aid_provided_by),
    affected_processes: data.affected_processes.map(genericSchemaToRelatedItem),
  };
};
export function IncidentCrud() {
  const history = useHistory();
  const { incidentService } = useContext(ApiContext);
  const { incidents, accountRole } = useRootState((state) => ({
    incidents: state.dashboard.incidents.items,
    accountRole: state.auth.role,
  }));
  const { Snackbar, setSnackbarSuccess, setSnackbarError } = useSnackbar();
  const formSubmitAction = (
    action: string,
    setError: React.Dispatch<React.SetStateAction<string>>
  ) => (itemId: string | number) => {
    if (itemId === -1) {
      setSnackbarError("Incident could not be " + action);
      setError("Incident could not be " + action);
    } else {
      setSnackbarSuccess("Incident successfully " + action);
      setError("");
      history.push("/incidents");
    }
  };
  return (
    <DashboardCrud<
      IncidentWithId,
      typeof schema,
      GqlIncidentWithDeps,
      GqlIncidentWithDeps["incident"]
    >
      dashboardPath="incidents"
      headers={[
        "Name",
        "Category",
        "Damage",
        "Date",
        "Lag Time",
        "Location",
        "Reported By",
        "Person Responsible",
        // "Branch", "Department"
      ]}
      gqlQueries={gqlQuery}
      schema={schema}
      items={incidents}
      itemsUpdater={setDashboardIncidents}
      renderItem={(incident) => (
        <>
          <TableCell>
            <Link to={`/incidents/${incident.id}`}>{incident.name}</Link>
          </TableCell>
          <TableCell>{incident.category.readable}</TableCell>
          <TableCell>{incident.damage}</TableCell>
          <TableCell>{incident.date_occurred}</TableCell>
          <TableCell>{incident.lag_time}</TableCell>
          <TableCell>{incident.location}</TableCell>
          <TableCell>{incident.reported_by.readable}</TableCell>
          <TableCell>{incident.person_responsible.readable}</TableCell>
        </>
      )}
      detailView={(incident, deps, minimumRole) => {
        return (
          <Paper className="w-11/12 flex flex-col p-8 items-stretch gap-4">
            <div className="w-full flex justify-end">
              {canDisplayMinimumRole(minimumRole, accountRole) && (
                <Button
                  variant="contained"
                  color="primary"
                  component={Link}
                  to={`/incidents/${incident.id}/edit`}
                >
                  Edit
                </Button>
              )}
            </div>
            <div className="w-full md:flex gap-4">
              <DetailItem
                title="Incident Name"
                value={incident.name}
                className="w-4/5 md:w-1/2"
              />
              <DetailItem
                title="Date of Incident"
                value={incident.date_occurred}
                className="w-4/5 md:w-1/2"
              />
            </div>
            <div className="w-full md:flex gap-4">
              <DetailItem
                title="Category"
                value={incident.category.name}
                className="w-4/5 md:w-1/2"
              />
              <DetailItem
                title="Damage"
                value={incident.damage || ""}
                className="w-4/5 md:w-1/2"
              />
            </div>
            <div className="w-full md:flex gap-4">
              <DetailItem
                title="Location"
                value={incident.location}
                className="w-4/5 md:w-1/2"
              />
              <DetailItem
                title="Lag Time"
                value={incident.lag_time || ""}
                className="w-4/5 md:w-1/2"
              />
            </div>
            <div className="w-full md:flex gap-4">
              <DetailItem
                title="Reported By"
                value={userFullName(incident.reported_by)}
                className="w-4/5 md:w-1/2"
              />
              <DetailItem
                title="Person Responsible"
                value={userFullName(incident.person_responsible)}
                className="w-4/5 md:w-1/2"
              />
            </div>
            <div className="w-full md:flex gap-4">
              <DetailItem
                title="First Aid Provided By"
                value={
                  incident.first_aid_provided_by
                    ? userFullName(incident.first_aid_provided_by)
                    : ""
                }
                className="w-4/5 md:w-1/2"
              />
              <div></div>
            </div>
            <div className="w-full flex flex-col gap-4">
              <Typography variant="h6" color="textPrimary">
                Affected Processes
              </Typography>
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell>Name</TableCell>
                    <TableCell>Team Responsible</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {incident.affected_processes.map((proc) => {
                    return (
                      <TableRow key={proc.name}>
                        <TableCell>{proc.name}</TableCell>
                        <TableCell>{proc.team_responsible.name}</TableCell>
                      </TableRow>
                    );
                  })}
                </TableBody>
              </Table>
            </div>
          </Paper>
        );
      }}
      renderForm={(incident, deps, useFormMethods) => {
        const {
          register,
          control,
          formState: { errors },
        } = useFormMethods;
        return (
          <>
            <Snackbar />
            <div className="w-full md:flex gap-4">
              <Controller
                name="name"
                defaultValue={incident?.name}
                control={control}
                render={({ field }) => (
                  <TextField
                    {...field}
                    label="Name"
                    error={!!errors.name?.message}
                    helperText={errors.name?.message}
                    className="w-11/12 md:w-1/2"
                  />
                )}
              />
              <Controller
                name="date_occurred"
                defaultValue={
                  incident?.date_occurred
                    ? jsDateParsers.fromIso(incident?.date_occurred)
                    : new Date()
                }
                control={control}
                render={({ field }) => (
                  <FormControl
                    className="w-11/12 md:w-1/2"
                    error={!!errors.date_occurred?.message}
                  >
                    <KeyboardDatePicker
                      {...field}
                      disableToolbar
                      variant="inline"
                      format="yyyy-MM-dd"
                      className="w-full"
                      label="Date Occurred"
                      error={!!errors.date_occurred?.message}
                    />
                    <FormHelperText>
                      {errors.date_occurred?.message}
                    </FormHelperText>
                  </FormControl>
                )}
              />
            </div>
            <div className="w-full md:flex gap-4">
              <Controller
                name="damage"
                defaultValue={incident?.damage}
                control={control}
                render={({ field }) => (
                  <TextField
                    {...field}
                    label="Damage"
                    error={!!errors.damage?.message}
                    helperText={errors.damage?.message}
                    className="w-11/12 md:w-1/2"
                  />
                )}
              />
              <Controller
                name="location"
                defaultValue={incident?.location}
                control={control}
                render={({ field }) => (
                  <TextField
                    {...field}
                    label="Location"
                    error={!!errors.location?.message}
                    helperText={errors.location?.message}
                    className="w-11/12 md:w-1/2"
                  />
                )}
              />
            </div>
            <div className="w-full md:flex gap-4">
              <Controller
                name="lag_time"
                defaultValue={incident?.lag_time}
                control={control}
                render={({ field }) => (
                  <TextField
                    {...field}
                    label="Lag Time"
                    error={!!errors.lag_time?.message}
                    helperText={errors.lag_time?.message}
                    className="w-11/12 md:w-1/2"
                  />
                )}
              />
              <FormControl
                className="w-11/12 md:w-1/2"
                error={!!errors.category}
              >
                <Controller
                  name="category"
                  defaultValue={incident?.category}
                  control={control}
                  render={({ field }) => (
                    <Autocomplete
                      {...field}
                      onChange={(_, opt) => field.onChange(opt)}
                      options={deps?.risk_categories || []}
                      getOptionLabel={(option) => option.name}
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          label="Category"
                          className="w-full"
                        />
                      )}
                    />
                  )}
                />
                <FormHelperText>{errors.category?.message}</FormHelperText>
              </FormControl>
            </div>
            <div className="w-full md:flex gap-4">
              <FormControl
                className="w-11/12 md:w-1/2"
                error={!!errors.affected_processes}
              >
                <Controller
                  name="affected_processes"
                  defaultValue={incident?.affected_processes}
                  control={control}
                  render={({ field }) => (
                    <Autocomplete
                      {...field}
                      multiple
                      onChange={(_, opt) => field.onChange(opt)}
                      options={deps?.processes || []}
                      getOptionLabel={(option) => option.name}
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          label="Affected Processes"
                          className="w-full"
                        />
                      )}
                    />
                  )}
                />
                <FormHelperText>
                  {errors.affected_processes?.message}
                </FormHelperText>
              </FormControl>
              <FormControl
                className="w-11/12 md:w-1/2"
                error={!!errors.reported_by}
              >
                <Controller
                  name="reported_by"
                  defaultValue={incident?.reported_by}
                  control={control}
                  render={({ field }) => (
                    <Autocomplete
                      {...field}
                      onChange={(_, opt) => field.onChange(opt)}
                      options={deps?.users || []}
                      getOptionLabel={(option) => userFullName(option)}
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          label="Reported By"
                          className="w-full"
                        />
                      )}
                    />
                  )}
                />
                <FormHelperText>{errors.reported_by?.message}</FormHelperText>
              </FormControl>
            </div>
            <div className="w-full md:flex gap-4">
              <FormControl
                className="w-11/12 md:w-1/2"
                error={!!errors.person_responsible}
              >
                <Controller
                  name="person_responsible"
                  defaultValue={incident?.person_responsible}
                  control={control}
                  render={({ field }) => (
                    <Autocomplete
                      {...field}
                      onChange={(_, opt) => field.onChange(opt)}
                      options={deps?.users || []}
                      getOptionLabel={(option) => userFullName(option)}
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          label="Person Responsible"
                          className="w-full"
                        />
                      )}
                    />
                  )}
                />
                <FormHelperText>
                  {errors.person_responsible?.message}
                </FormHelperText>
              </FormControl>
              <FormControl
                className="w-11/12 md:w-1/2"
                error={!!errors.first_aid_provided_by}
              >
                <Controller
                  name="first_aid_provided_by"
                  defaultValue={incident?.first_aid_provided_by}
                  control={control}
                  render={({ field }) => (
                    <Autocomplete
                      {...field}
                      onChange={(_, opt) => field.onChange(opt)}
                      options={deps?.users || []}
                      getOptionLabel={(option) => userFullName(option)}
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          label="First Aid Provided By"
                          className="w-full"
                        />
                      )}
                    />
                  )}
                />
                <FormHelperText>
                  {errors.first_aid_provided_by?.message}
                </FormHelperText>
              </FormControl>
            </div>
          </>
        );
      }}
      pageTitles={{
        create: "Create Incident",
        update: "Update Incident",
        listing: "Incidents",
        detail: "Incident Detail",
      }}
      itemName="Incident"
      remoteUrl="/incidents"
      readableKey="name"
      actions={{
        create: (data, useFormMethods, setGeneralError) => {
          incidentService
            .create(formDataToDto(data))
            .then(formSubmitAction("created", setGeneralError));
        },
        update: (data, useFormMethods, setGeneralError, itemId) => {
          incidentService
            .update(itemId!!, formDataToDto(data))
            .then(formSubmitAction("updated", setGeneralError));
        },
        delete: incidentService.delete,
      }}
    />
  );
}

export default IncidentCrud;
