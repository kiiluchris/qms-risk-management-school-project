import ApiContext from "@/api/api";
import DashboardCrud from "@/components/dashboard/DashboardCrud";
import DashboardInput from "@/components/dashboard/DashboardInput";
import DashboardSelectInput from "@/components/dashboard/DashboardSelectInput";
import React, { useContext } from "react";
import { useHistory } from "react-router-dom";
import * as z from "zod";
import {
  GqlProcessOutputWithDeps,
  ProcessOutput,
  ProcessOutputWithId,
  gqlQuery,
} from "@/domain/models/process-output";
import { useRootState } from "@/app/rootReducer";
import { setDashboardProcessOutputs } from "@/storage/dashboardSlice";
import {
  reactSelectOptionToRelatedItem,
  zodReactSelectOption,
} from "@/utils/form";
import TableCell from "@material-ui/core/TableCell";

const schema = z.object({
  name: z.string(),
});

const formDataToDto = (data: z.infer<typeof schema>): ProcessOutput => {
  return {
    name: data.name,
  };
};

export default function ProcessOutputCrud() {
  const history = useHistory();
  const processOutputs = useRootState(
    (state) => state.dashboard.processOutputs.items
  );
  const { processOutputService } = useContext(ApiContext);
  const formSubmitAction = (
    action: string,
    setError: React.Dispatch<React.SetStateAction<string>>
  ) => (itemId: string | number) => {
    if (itemId === -1) {
      setError("Process Output could not be " + action);
    } else {
      setError("");
      history.push("/process-outputs");
    }
  };
  return (
    <DashboardCrud<
      ProcessOutputWithId,
      typeof schema,
      GqlProcessOutputWithDeps,
      GqlProcessOutputWithDeps["process_output"]
    >
      dashboardPath="process-outputs"
      items={processOutputs}
      gqlQueries={gqlQuery}
      itemsUpdater={setDashboardProcessOutputs}
      headers={["Name", "Number of Members"]}
      schema={schema}
      renderForm={(processOutput, deps, useFormMethods) => {
        const {
          register,
          formState: { errors },
        } = useFormMethods;
        return (
          <>
            <div className="w-full">
              <DashboardInput
                name="name"
                placeholder="Name"
                defaultValue={processOutput?.name}
                register={register}
                error={errors.name?.message}
              />
            </div>
          </>
        );
      }}
      renderItem={(processOutput) => (
        <>
          <TableCell>{processOutput.name}</TableCell>
        </>
      )}
      pageTitles={{
        create: "Create Process Output",
        update: "Update Process Output",
        listing: "Process Outputs",
detail: "Process Output Detail",
      }}
      itemName="Process Output"
      remoteUrl="/process-outputs"
      readableKey="name"
      actions={{
        create: (data, useFormMethods, setGeneralError) => {
          processOutputService
            .create(formDataToDto(data))
            .then(formSubmitAction("created", setGeneralError));
        },
        update: (data, useFormMethods, setGeneralError, itemId) => {
          processOutputService
            .update(itemId!!, formDataToDto(data))
            .then(formSubmitAction("updated", setGeneralError));
        },
        delete: processOutputService.delete,
      }}
    />
  );
}
