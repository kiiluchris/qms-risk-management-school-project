import Loader from "@/components/shared/Loader";
import { buildGqlQuery, GqlQuery } from "@/domain/models/gql";
import {
  useGqlQuery,
  usePageTitle,
  useRefreshLoginSession,
} from "@/utils/hooks";
import Box from "@material-ui/core/Box";
import Paper from "@material-ui/core/Paper";
import Tab from "@material-ui/core/Tab";
import Tabs from "@material-ui/core/Tabs";
import Typography from "@material-ui/core/Typography";
import React, { useState } from "react";
import {
  Bar,
  BarChart,
  CartesianGrid,
  Cell,
  Legend,
  Line,
  LineChart,
  Pie,
  PieChart,
  ResponsiveContainer,
  Tooltip as ChartTooltip,
  XAxis,
  YAxis,
} from "recharts";
import { graphColors } from "../main/risk-graph-utils";

type ItemAnalysis = {
  model: string;
} & { [label: string]: number };

type AccuracyAnalysis = {
  model: string;
  accuracy: number;
};

type ModelAnalysisGroup = {
  precision: ItemAnalysis[];
  recall: ItemAnalysis[];
  fscore: ItemAnalysis[];
  accuracy: AccuracyAnalysis[];
  labels: string[];
};

type ModelAnalysis = {
  model: ModelAnalysisGroup;
  label: ModelAnalysisGroup;
};

type RootModelAnalysis = {
  category: ModelAnalysis;
  likelihood: ModelAnalysis;
  impact: ModelAnalysis;
};
type GraphData = {
  static_model_analysis: RootModelAnalysis;
};
const gqlQuery: GqlQuery<GraphData, undefined> = {
  dependencies: `
    static_model_analysis {
      category {
        model {
          precision
          recall
          fscore
          labels
        }
        label {
          precision
          recall
          fscore
          labels
        }
        accuracy {
          model
          accuracy
        }
      }
      impact {
        model {
          precision
          recall
          fscore
          labels
        }
        label {
          precision
          recall
          fscore
          labels
        }
        accuracy {
          model
          accuracy
        }
      }
      likelihood {
        model {
          precision
          recall
          fscore
          labels
        }
        label {
          precision
          recall
          fscore
          labels
        }
        accuracy {
          model
          accuracy
        }
      }
    }
  `,
};

interface TabPanelProps {
  children?: React.ReactNode;
  index: any;
  value: any;
  className?: string;
}

function TabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`scrollable-auto-tabpanel-${index}`}
      aria-labelledby={`scrollable-auto-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

function a11yProps(index: any) {
  return {
    id: `scrollable-auto-tab-${index}`,
    "aria-controls": `scrollable-auto-tabpanel-${index}`,
  };
}

function ModelAnalysisCharts(props: { name: string; data: ModelAnalysis }) {
  const { data, name } = props;
  return (
    <div className="flex flex-col gap-10 w-full">
      <div className="w-full md:flex flex-col">
        <div className="w-full">
          <h3 className="text-xl mb-4 h-20">{name} Precision Metrics</h3>
          <ResponsiveContainer width="90%" height={300}>
            <LineChart data={data.label.precision}>
              <XAxis dataKey="model" type="category" width={100} />
              <YAxis />
              <CartesianGrid stroke="#eee" strokeDasharray="5 5" />

              {data.label.labels.map((label, i) => (
                <Line
                  key={`precision-${label}-line`}
                  dataKey={label}
                  stroke={graphColors[i]}
                />
              ))}
              <ChartTooltip cursor={false} />
              <Legend verticalAlign="bottom" height={36} margin={{ top: 10 }} />
            </LineChart>
          </ResponsiveContainer>
        </div>
        <div className="w-full">
          <h3 className="text-xl mb-4 h-20">{name} Recall Metrics</h3>
          <ResponsiveContainer width="90%" height={300}>
            <LineChart data={data.label.recall}>
              <XAxis dataKey="model" type="category" width={100} />
              <YAxis />
              <CartesianGrid stroke="#eee" strokeDasharray="5 5" />

              {data.label.labels.map((label, i) => (
                <Line
                  key={`precision-${label}-line`}
                  dataKey={label}
                  stroke={graphColors[i]}
                />
              ))}
              <ChartTooltip cursor={false} />
              <Legend verticalAlign="bottom" height={36} margin={{ top: 10 }} />
            </LineChart>
          </ResponsiveContainer>
        </div>
      </div>
      <div className="w-full md:flex flex-col">
        <div className="w-full">
          <h3 className="text-xl mb-4 h-20">{name} Fscore Metrics</h3>
          <ResponsiveContainer width="90%" height={300}>
            <LineChart data={data.label.fscore}>
              <XAxis dataKey="model" type="category" width={100} />
              <YAxis />
              <CartesianGrid stroke="#eee" strokeDasharray="5 5" />

              {data.label.labels.map((label, i) => (
                <Line
                  key={`precision-${label}-line`}
                  dataKey={label}
                  stroke={graphColors[i]}
                />
              ))}
              <ChartTooltip cursor={false} />
              <Legend verticalAlign="bottom" height={36} margin={{ top: 10 }} />
            </LineChart>
          </ResponsiveContainer>
        </div>
      </div>
    </div>
  );
}

interface ModelAnalysisComponentProps {
  title: string;
  data: RootModelAnalysis;
}

function ModelAnalysisComponent({ title, data }: ModelAnalysisComponentProps) {
  const { category, impact, likelihood } = data;
  usePageTitle(title);
  const [currentTab, setCurrentTab] = useState(0);
  const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    setCurrentTab(newValue);
  };

  return (
    <Paper className="w-full flex flex-col gap-4">
      <Tabs
        value={currentTab}
        onChange={handleChange}
        indicatorColor="primary"
        textColor="primary"
        variant="scrollable"
        scrollButtons="auto"
        aria-label="scrollable auto tabs example"
      >
        <Tab label="Category" {...a11yProps(0)} />
        <Tab label="Impact" {...a11yProps(1)} />
        <Tab label="Likelihood" {...a11yProps(2)} />
      </Tabs>
      <TabPanel value={currentTab} index={0} className="flex flex-col gap-10">
        <ModelAnalysisCharts name="Category" data={category} />
      </TabPanel>
      <TabPanel value={currentTab} index={1} className="flex flex-col gap-10">
        <ModelAnalysisCharts name="Impact" data={impact} />
      </TabPanel>
      <TabPanel value={currentTab} index={2} className="flex flex-col gap-10">
        <ModelAnalysisCharts name="Likelihood" data={likelihood} />
      </TabPanel>
    </Paper>
  );
}

export function StaticModelAnalysisComponent(): JSX.Element {
  const { gqlData, gqlError } = useGqlQuery(gqlQuery);
  useRefreshLoginSession(gqlError?.status === 401 ? gqlError : undefined, [
    buildGqlQuery(gqlQuery),
  ]);
  if (!gqlData) {
    return <Loader />;
  }
  console.log(gqlData, gqlError);

  return (
    <ModelAnalysisComponent
      title="Model Analysis"
      data={gqlData.static_model_analysis}
    />
  );
}

export default StaticModelAnalysisComponent;
