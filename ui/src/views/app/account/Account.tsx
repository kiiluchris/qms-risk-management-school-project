import ApiContext from "@/api/api";
import Loader from "@/components/shared/Loader";
import { buildGqlQuery } from "@/domain/models/gql";
import { gqlAccountQuery } from "@/domain/models/user";
import {
  useGqlQuery,
  usePageTitle,
  useRefreshLoginSession,
} from "@/utils/hooks";
import { zodResolver } from "@hookform/resolvers/zod";
import Button from "@material-ui/core/Button";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormHelperText from "@material-ui/core/FormHelperText";
import Paper from "@material-ui/core/Paper";
import Snackbar from "@material-ui/core/Snackbar";
import { makeStyles } from "@material-ui/core/styles";
import Switch from "@material-ui/core/Switch";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import Alert, { Color } from "@material-ui/lab/Alert";
import React, { useContext, useState } from "react";
import { Controller, useForm } from "react-hook-form";
import { mutate } from "swr";
import * as z from "zod";

const useStyles = makeStyles({
  slider: {
    width: 250,
  },
});

const schema = z.object({
  first_name: z.string(),
  last_name: z.string(),
  phone: z.string(),
  use_sms: z.boolean(),
});

export default function AccountSettingsPage() {
  usePageTitle("Account");
  const classes = useStyles();
  const [snackbar, setSnackBarState] = useState<{
    open: boolean;
    message: string;
    severity: Color;
  }>({
    open: false,
    severity: "info",
    message: "",
  });
  const handleClose = () => {
    setSnackBarState({ open: false, severity: "info", message: "" });
  };
  const { accountService } = useContext(ApiContext);
  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: zodResolver(schema),
  });
  const { gqlData, gqlError } = useGqlQuery(gqlAccountQuery);
  const queryKey = buildGqlQuery(gqlAccountQuery);
  useRefreshLoginSession(gqlError?.status === 401 ? gqlError : undefined, [
    queryKey,
  ]);
  const account = gqlData?.account;

  if (!account || gqlError) {
    console.log(gqlError);
    return <Loader />;
  }
  const submitForm = (formData: z.infer<typeof schema>) => {
    accountService
      .updateAccount({
        ...formData,
      })
      .then((result) => {
        if (result === -1) {
          setSnackBarState({
            message: "Unable to update account settings",
            severity: "error",
            open: true,
          });
        } else {
          setSnackBarState({
            message: "Successfully updated account",
            severity: "success",
            open: true,
          });
          mutate(queryKey);
        }
      })
      .catch((e) => {
        setSnackBarState({
          message: "Unable to update account settings",
          severity: "error",
          open: true,
        });
      });
  };

  return (
    <>
      <Snackbar
        open={snackbar.open}
        autoHideDuration={6000}
        onClose={handleClose}
      >
        <Alert onClose={handleClose} severity={snackbar.severity}>
          {snackbar.message}
        </Alert>
      </Snackbar>
      <Paper className="flex w-11/12 md:w-4/5 gap-6 flex-col items-center mx-auto py-8 px-8">
        <Typography variant="h5">Account Settings</Typography>
        <form
          className="w-full flex flex-col gap-6 items-center px-2"
          noValidate
          onSubmit={handleSubmit(submitForm)}
        >
          <div className="md:flex w-full justify-around gap-4">
            <Controller
              name="first_name"
              control={control}
              defaultValue={account.first_name}
              render={({ field }) => (
                <TextField
                  {...field}
                  id="first_name"
                  label="First Name"
                  className="w-11/12 md:w-1/2"
                  error={!!errors.first_name}
                  helperText={errors.first_name?.message}
                />
              )}
            />
            <Controller
              name="last_name"
              control={control}
              defaultValue={account.last_name}
              render={({ field }) => (
                <TextField
                  {...field}
                  id="last_name"
                  label="Last Name"
                  className="w-11/12 md:w-1/2"
                  error={!!errors.last_name}
                  helperText={errors.last_name?.message}
                />
              )}
            />
          </div>
          <div className="md:flex w-full justify-around gap-4">
            <Controller
              name="phone"
              control={control}
              defaultValue={account.phone}
              render={({ field }) => (
                <TextField
                  {...field}
                  id="phone"
                  label="Phone"
                  className="w-11/12 md:w-1/2"
                  error={!!errors.phone}
                  helperText={errors.phone?.message}
                />
              )}
            />
            <Controller
              name="use_sms"
              control={control}
              defaultValue={account.use_sms}
              render={({ field: { value, ...field } }) => (
                <div className="w-11/12 md:w-1/2">
                  <FormControlLabel
                    labelPlacement="top"
                    label="Use Sms"
                    checked={value}
                    control={<Switch {...field} id="use_sms" />}
                  />
                  {errors.use_sms?.message && (
                    <FormHelperText error={true}>
                      {errors.use_sms?.message}
                    </FormHelperText>
                  )}
                </div>
              )}
            />
          </div>
          <Button variant="contained" color="primary" type="submit">
            Save Settings
          </Button>
        </form>
      </Paper>
    </>
  );
}
