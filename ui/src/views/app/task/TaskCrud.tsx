import ApiContext from "@/api/api";
import { useRootState } from "@/app/rootReducer";
import DashboardCrud from "@/components/dashboard/DashboardCrud";
import { DetailItem } from "@/components/dashboard/DetailItem";
import { gqlTaskQuery, GqlTaskWithDeps } from "@/domain/models/process";
import { canDisplayMinimumRole } from "@/domain/models/role";
import { Task, TaskWithId } from "@/domain/models/task";
import { userSchema } from "@/domain/models/user";
import { setDashboardTasks } from "@/storage/dashboardSlice";
import {
  genericSchema,
  genericSchemaToRelatedItem,
  userSchemaToRelatedItem,
} from "@/utils/form";
import { useSnackbar } from "@/utils/hooks";
import { jsDateFormatters } from "@/utils/time";
import { FormControlLabel, Switch } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import FormControl from "@material-ui/core/FormControl";
import FormHelperText from "@material-ui/core/FormHelperText";
import Paper from "@material-ui/core/Paper";
import TableCell from "@material-ui/core/TableCell";
import TextField from "@material-ui/core/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete";
import React, { useContext, useRef, useState } from "react";
import { Controller } from "react-hook-form";
import { Link, useHistory } from "react-router-dom";
import * as z from "zod";
import { userFullName } from "../main/risk-graph-utils";

const schema = z.object({
  is_complete: z.boolean(),
  name: z.string(),
  due_by: z.date().optional(),
  process: genericSchema,
  person_responsible: userSchema,
});

const formDataToDto = (data: z.infer<typeof schema>): Task => {
  return {
    name: data.name,
    due_by: data.due_by && jsDateFormatters.toIsoDate(data.due_by),
    process: genericSchemaToRelatedItem(data.process),
    is_complete: data.is_complete,
    user_owner: userSchemaToRelatedItem(data.person_responsible),
  };
};

export function TaskCrud() {
  const history = useHistory();
  const { taskService } = useContext(ApiContext);
  const { tasks, accountRole } = useRootState((state) => ({
    tasks: state.dashboard.tasks.items,
    accountRole: state.auth.role,
  }));
  const { Snackbar, setSnackbarSuccess, setSnackbarError } = useSnackbar();
  const dateToday = useRef(new Date());
  const [deadline, setDeadline] = useState<Date | undefined>(undefined);

  const handleTaskCompletion = (evt: any, isComplete: boolean) => {
    console.log("Toggle", evt, isComplete);
  };
  const formSubmitAction = (
    action: string,
    setError: React.Dispatch<React.SetStateAction<string>>
  ) => (itemId: string | number) => {
    if (itemId === -1) {
      setSnackbarError("Task could not be " + action);
      setError("Task could not be " + action);
    } else {
      setSnackbarSuccess("Task successfully" + action);
      setError("");
      history.push("/tasks");
    }
  };
  return (
    <DashboardCrud<
      TaskWithId,
      typeof schema,
      GqlTaskWithDeps,
      GqlTaskWithDeps["task"]
    >
      minimumRole="Manager"
      minimumRoleReadOnly="Employee"
      dashboardPath="tasks"
      headers={[
        "Name",
        "Process", // "Due By",
        "Completion",
      ]}
      queryParamMaker={(queryParams) => {
        const remoteUrlSearchParams = new URLSearchParams();
        const qpProcessId = queryParams.get("process");
        if (qpProcessId) {
          remoteUrlSearchParams.set("process_id", qpProcessId);
        }
        return remoteUrlSearchParams;
      }}
      gqlQueries={gqlTaskQuery}
      schema={schema}
      items={tasks}
      itemsUpdater={setDashboardTasks}
      renderItem={(task) => {
        //const dueBy = jsDateParsers.fromIso(task.due_by);

        return (
          <>
            <TableCell>
              <Link to={`/tasks/${task.id}`}>{task.name}</Link>
            </TableCell>
            <TableCell>
              <Link to={`/processes/${task.process!!.id}`}>
                {task.process!!.value}
              </Link>
            </TableCell>
            {/* <TableCell>{jsDateFormatters.format(dueBy)}</TableCell> */}
            <TableCell>
              {task.is_complete ? "Completed" : "Unfinished"}
            </TableCell>
          </>
        );
      }}
      detailView={(task, deps, minimumRole) => {
        return (
          <Paper className="w-11/12 flex flex-col p-8 items-stretch gap-4">
            <div className="w-full flex justify-end">
              {canDisplayMinimumRole(minimumRole, accountRole) && (
                <Button
                  variant="contained"
                  color="primary"
                  component={Link}
                  to={`/tasks/${task.id}/edit`}
                >
                  Edit
                </Button>
              )}
            </div>
            <div className="w-full md:flex gap-4 items-start">
              <DetailItem
                title="Task Name"
                value={task.name}
                className="w-4/5 md:w-1/2"
              />
              <DetailItem
                title="Task Completion"
                value={task.is_complete ? "Completed" : "Incomplete"}
                className={
                  (task.is_complete ? "text-green-500" : "text-red-500") +
                  " w-4/5 md:w-1/2"
                }
              />
            </div>
            <div className="w-full md:flex gap-4 items-start">
              <DetailItem
                title="Parent Process"
                value={task.process.name}
                className="w-4/5 md:w-1/2"
              />
              {task.user_owner && (
                <DetailItem
                  title="Person Responsible"
                  value={userFullName(task.user_owner)}
                  className="w-4/5 md:w-1/2"
                />
              )}
            </div>
          </Paper>
        );
      }}
      renderForm={(task, deps, useFormMethods): JSX.Element => {
        const {
          register,
          control,
          formState: { errors },
        } = useFormMethods;
        console.log(errors);
        return (
          <>
            <Snackbar />
            <div className="w-full md:flex gap-4">
              <Controller
                name="name"
                defaultValue={task?.name}
                control={control}
                render={({ field }) => (
                  <TextField
                    {...field}
                    label="Name"
                    className="w-11/12 md:w-1/2"
                    error={!!errors.name?.message}
                    helperText={errors.name?.message}
                  />
                )}
              />
              <FormControl
                className="w-11/12 md:w-1/2"
                error={!!errors.process}
              >
                <Controller
                  name="process"
                  defaultValue={task?.process}
                  control={control}
                  render={({ field }) => (
                    <Autocomplete
                      {...field}
                      onChange={(_, opt) => field.onChange(opt)}
                      options={deps?.processes || []}
                      getOptionLabel={(option) => option.name}
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          label="Process"
                          className="w-full"
                          error={!!errors.process}
                        />
                      )}
                    />
                  )}
                />
                <FormHelperText>{errors.process?.message}</FormHelperText>
              </FormControl>
            </div>
            <div className="w-full md:flex gap-4">
              {/* <DashboardDatePickerInput
                  name="due_by"
                  initialDate={task?.due_by || dateToday.current}
                  minDate={dateToday.current}
                  maxDate={
                  task?.process
                  ? jsDateParsers.fromIso(task.process.objective.target_date)
                  : deadline
                  }
                  useFormMethods={useFormMethods}
                  error={errors.is_complete?.message}
                  /> */}
              <FormControl
                className="w-11/12 md:w-1/2"
                error={!!errors.person_responsible}
              >
                <Controller
                  name="person_responsible"
                  defaultValue={task?.user_owner}
                  control={control}
                  render={({ field }) => (
                    <Autocomplete
                      {...field}
                      onChange={(_, opt) => field.onChange(opt)}
                      options={deps?.users || []}
                      getOptionLabel={(option) => userFullName(option)}
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          label="Person Responsible"
                          className="w-full"
                          error={!!errors.person_responsible}
                        />
                      )}
                    />
                  )}
                />
                <FormHelperText>
                  {errors.person_responsible?.message}
                </FormHelperText>
              </FormControl>
              <Controller
                name="is_complete"
                control={control}
                defaultValue={task?.is_complete || false}
                render={({ field: { value, ...field } }) => (
                  <div className="w-11/12 md:w-1/2">
                    <FormControlLabel
                      labelPlacement="top"
                      label="Is Complete"
                      checked={value}
                      control={<Switch {...field} id="is_complete" />}
                    />
                    {errors.is_complete?.message && (
                      <FormHelperText error={true}>
                        {errors.is_complete?.message}
                      </FormHelperText>
                    )}
                  </div>
                )}
              />
            </div>
          </>
        );
      }}
      pageTitles={{
        create: "Create Task",
        update: "Update Task",
        listing: "Tasks",
        detail: "Task Detail",
      }}
      itemName="Task"
      remoteUrl="/tasks"
      readableKey="name"
      actions={{
        create: (data, useFormMethods, setGeneralError) => {
          taskService
            .create(formDataToDto(data))
            .then(formSubmitAction("created", setGeneralError));
        },
        update: (data, useFormMethods, setGeneralError, itemId) => {
          taskService
            .update(itemId!!, formDataToDto(data))
            .then(formSubmitAction("updated", setGeneralError));
        },
        delete: taskService.delete,
      }}
    />
  );
}

export default TaskCrud;
