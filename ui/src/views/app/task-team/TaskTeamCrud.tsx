import ApiContext from "@/api/api";
import { useRootState } from "@/app/rootReducer";
import DashboardCrud from "@/components/dashboard/DashboardCrud";
import DashboardInput from "@/components/dashboard/DashboardInput";
import DashboardSelectInput from "@/components/dashboard/DashboardSelectInput";
import { DetailItem } from "@/components/dashboard/DetailItem";
import {
  gqlQuery,
  GqlTaskTeamWithDeps,
  TaskTeam,
  TaskTeamWithId,
} from "@/domain/models/task-team";
import { userSchema } from "@/domain/models/user";
import { setDashboardTaskTeams } from "@/storage/dashboardSlice";
import {
  reactSelectOptionToRelatedItem,
  toRelatedItem,
  zodReactSelectOption,
} from "@/utils/form";
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import TableHead from "@material-ui/core/TableHead";
import TableBody from "@material-ui/core/TableBody";
import Typography from "@material-ui/core/Typography";
import { Link } from "react-router-dom";
import React, { useContext } from "react";
import { useHistory } from "react-router-dom";
import * as z from "zod";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { Controller } from "react-hook-form";
import TextField from "@material-ui/core/TextField";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import FormHelperText from "@material-ui/core/FormHelperText";
import { canDisplayMinimumRole } from "@/domain/models/role";
import { userFullName } from "../main/risk-graph-utils";
import { useSnackbar } from "@/utils/hooks";

const schema = z.object({
  name: z.string(),
  leader: userSchema,
  members: z.array(userSchema),
});

const formDataToDto = (data: z.infer<typeof schema>): TaskTeam => {
  return {
    name: data.name,
    leader: toRelatedItem(data.leader, userFullName)!,
    members: data.members.map((m) => {
      return toRelatedItem(m, userFullName)!;
    }),
  };
};

export default function TaskTeamCrud() {
  const history = useHistory();
  const { accountRole, taskTeams } = useRootState((state) => {
    return {
      taskTeams: state.dashboard.taskTeams.items,
      accountRole: state.auth.role,
    };
  });
  const { taskTeamService } = useContext(ApiContext);
  const { Snackbar, setSnackbarSuccess, setSnackbarError } = useSnackbar();
  const formSubmitAction = (
    action: string,
    setError: React.Dispatch<React.SetStateAction<string>>
  ) => (itemId: string | number) => {
    if (itemId === -1) {
      setSnackbarError("Department could not be " + action);
      setError("Department could not be " + action);
    } else {
      setSnackbarSuccess("Department successfully " + action);
      setError("");
      history.push("/departments");
    }
  };
  return (
    <DashboardCrud<
      TaskTeamWithId,
      typeof schema,
      GqlTaskTeamWithDeps,
      GqlTaskTeamWithDeps["task_team"]
    >
      minimumRole="Manager"
      minimumRoleReadOnly="Employee"
      dashboardPath="departments"
      items={taskTeams}
      gqlQueries={gqlQuery}
      itemsUpdater={setDashboardTaskTeams}
      schema={schema}
      headers={["Name", "Leader", "Number of Members"]}
      renderItem={(taskTeam) => (
        <>
          <TableCell>
            <Link to={`/departments/${taskTeam.id}`}>{taskTeam.name}</Link>
          </TableCell>
          <TableCell>{taskTeam.leader.readable}</TableCell>
          <TableCell>{taskTeam.members.length}</TableCell>
        </>
      )}
      detailView={(taskTeam, deps, minimumRole) => {
        return (
          <Paper className="w-11/12 flex flex-col p-8 items-stretch gap-4">
            <div className="w-full flex justify-end">
              {canDisplayMinimumRole(minimumRole, accountRole) && (
                <Button
                  variant="contained"
                  color="primary"
                  component={Link}
                  to={`/departments/${taskTeam.id}/edit`}
                >
                  Edit
                </Button>
              )}
            </div>
            <div className="w-full md:flex gap-4">
              <DetailItem
                title="Department Name"
                value={taskTeam.name}
                className="w-4/5 md:w-1/2"
              />
              <DetailItem
                title="Leader"
                value={
                  taskTeam.leader.first_name + " " + taskTeam.leader.last_name
                }
                className="w-4/5 md:w-1/2"
              />
            </div>
            <div className="w-full flex flex-col gap-4">
              <Typography variant="h6" color="textPrimary">
                Members
              </Typography>
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell>First Name</TableCell>
                    <TableCell>Last Name</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {taskTeam.members.map((member) => {
                    const name = member.first_name + " " + member.last_name;
                    return (
                      <TableRow key={name}>
                        <TableCell>{member.first_name}</TableCell>
                        <TableCell>{member.last_name}</TableCell>
                      </TableRow>
                    );
                  })}
                </TableBody>
              </Table>
            </div>
          </Paper>
        );
      }}
      renderForm={(taskTeam, deps, useFormMethods) => {
        const {
          register,
          control,
          formState: { errors },
        } = useFormMethods;
        return (
          <>
            <Snackbar />
            <div className="w-full md:flex gap-4">
              <Controller
                name="name"
                defaultValue={taskTeam?.name}
                control={control}
                render={({ field }) => (
                  <TextField
                    {...field}
                    className="w-11/12 md:w-1/2"
                    label="Name"
                    error={!!errors.name}
                    helperText={errors.name?.message}
                  />
                )}
              />
              <FormControl className="w-11/12 md:w-1/2" error={!!errors.leader}>
                <Controller
                  name="leader"
                  defaultValue={taskTeam?.leader}
                  control={control}
                  render={({ field }) => (
                    <Autocomplete
                      {...field}
                      onChange={(_, opt) => field.onChange(opt)}
                      options={
                        deps?.users.filter((u) => u.role.name !== "Employee") ||
                        []
                      }
                      getOptionLabel={(option) => userFullName(option)}
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          label="Leader"
                          className="w-full"
                          error={!!errors.leader}
                        />
                      )}
                    />
                  )}
                />
                <FormHelperText>{errors.leader?.message}</FormHelperText>
              </FormControl>
            </div>
            <div className="w-full md:flex gap-4">
              <FormControl
                className="w-11/12 md:w-1/2"
                error={!!errors.members}
              >
                <Controller
                  name="members"
                  defaultValue={taskTeam?.members}
                  control={control}
                  render={({ field }) => (
                    <Autocomplete
                      {...field}
                      multiple
                      onChange={(_, opt) => field.onChange(opt)}
                      options={
                        deps?.users.filter((u) => u.role.name === "Employee") ||
                        []
                      }
                      getOptionLabel={(option) => userFullName(option)}
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          label="Members"
                          className="w-full"
                        />
                      )}
                    />
                  )}
                />
                <FormHelperText>{errors.members?.message}</FormHelperText>
              </FormControl>
            </div>
          </>
        );
      }}
      pageTitles={{
        create: "Create Department",
        update: "Update Department",
        listing: "Departments",
        detail: "Department Detail",
      }}
      itemName="Department"
      remoteUrl="/task-teams"
      readableKey="name"
      actions={{
        create: (data, useFormMethods, setGeneralError) => {
          taskTeamService
            .create(formDataToDto(data))
            .then(formSubmitAction("created", setGeneralError));
        },
        update: (data, useFormMethods, setGeneralError, itemId) => {
          taskTeamService
            .update(itemId!!, formDataToDto(data))
            .then(formSubmitAction("updated", setGeneralError));
        },
        delete: taskTeamService.delete,
      }}
    />
  );
}
