import ApiContext from "@/api/api";
import { useRootState } from "@/app/rootReducer";
import DashboardCrud from "@/components/dashboard/DashboardCrud";
import DashboardDatePickerInput from "@/components/dashboard/DashboardDatePickerInput";
import DashboardInput from "@/components/dashboard/DashboardInput";
import DashboardSelectInput from "@/components/dashboard/DashboardSelectInput";
import { DetailItem } from "@/components/dashboard/DetailItem";
import {
  Capa,
  CapaClassification,
  CapaRating,
  CapaWithId,
  GqlCapaWithDeps,
  gqlQuery,
} from "@/domain/models/capa";
import { setDashboardCapas } from "@/storage/dashboardSlice";
import {
  reactSelectOptionToRelatedItem,
  reactSelectOptionToRelatedItemOpt,
  toRelatedItem,
  zodReactSelectOption,
  genericSchema,
  genericSchemaToRelatedItem,
  userSchemaToRelatedItem,
} from "@/utils/form";
import { jsDateFormatters, jsDateParsers } from "@/utils/time";
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import TableCell from "@material-ui/core/TableCell";
import React, { useContext, useState } from "react";
import { Link, useHistory } from "react-router-dom";
import * as z from "zod";
import { userFullName } from "../main/risk-graph-utils";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { Controller } from "react-hook-form";
import TextField from "@material-ui/core/TextField";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import FormHelperText from "@material-ui/core/FormHelperText";
import { canDisplayMinimumRole } from "@/domain/models/role";
import { useSnackbar } from "@/utils/hooks";
import { KeyboardDatePicker } from "@material-ui/pickers";
import { userSchema } from "@/domain/models/user";

const schema = z.object({
  name: z.string(),
  action_type: z.string(),
  description: z.string(),
  measures_taken: z.string(),
  date_created: z.date(),
  category: genericSchema,
  rating: z.string(),
  target_effective_date: z.date(),
  comments: z.string(),
  /* proposed_actions: z.string(), */
  approved_by: userSchema.optional().nullable(),
  prepared_by: userSchema,
  incident: genericSchema.optional(),
  risks_to_prevent: z.array(genericSchema).optional(),
});

const formDataToDto = (data: z.infer<typeof schema>): Capa => {
  const res: Capa = {
    ...data,
    category: genericSchemaToRelatedItem(data.category),
    target_effective_date: jsDateFormatters.toIsoDate(
      data.target_effective_date
    ),
    date_created: jsDateFormatters.toIsoDate(data.date_created),
    approved_by:
      data.approved_by !== null && data.approved_by
        ? userSchemaToRelatedItem(data.approved_by)
        : undefined,
    prepared_by: userSchemaToRelatedItem(data.prepared_by),
    action_type: data.action_type as CapaClassification,
    rating: data.rating as CapaRating,
    incident: undefined,
    risks_to_prevent:
      data.risks_to_prevent?.map(genericSchemaToRelatedItem) || [],
  };
  if (res.category.value === "Corrective") {
    res.incident = data.incident && genericSchemaToRelatedItem(data.incident);
  }
  return res;
};

export function CapaCrud() {
  const history = useHistory();
  const { capaService } = useContext(ApiContext);
  const { capas, accountRole } = useRootState((state) => ({
    capas: state.dashboard.capas.items,
    accountRole: state.auth.role,
  }));
  const { Snackbar, setSnackbarSuccess, setSnackbarError } = useSnackbar();
  const formSubmitAction = (
    action: string,
    setError: React.Dispatch<React.SetStateAction<string>>
  ) => (itemId: string | number) => {
    if (itemId === -1) {
      setSnackbarError("Capa could not be " + action);
      setError("Capa could not be " + action);
    } else {
      setSnackbarSuccess("Capa successfully " + action);
      setError("");
      history.push("/capas");
    }
  };
  const [actionType, setActionType] = useState<string | null>(null);
  return (
    <DashboardCrud<
      CapaWithId,
      typeof schema,
      GqlCapaWithDeps,
      GqlCapaWithDeps["capa"]
    >
      dashboardPath="capas"
      headers={[
        "Name",
        "Type",
        "Category",
        "Date Created",
        "Target Effective Date",
        "Rating",
      ]}
      schema={schema}
      items={capas}
      itemsUpdater={setDashboardCapas}
      gqlQueries={gqlQuery}
      renderItem={(capa) => (
        <>
          <TableCell>
            <Link to={`/capas/${capa.id}`}>{capa.name}</Link>
          </TableCell>
          <TableCell>{capa.action_type}</TableCell>
          <TableCell>{capa.category.readable}</TableCell>
          <TableCell>
            {jsDateFormatters.format(jsDateParsers.fromIso(capa.date_created))}
          </TableCell>
          <TableCell>
            {jsDateFormatters.format(
              jsDateParsers.fromIso(capa.target_effective_date)
            )}
          </TableCell>
          <TableCell>{capa.rating}</TableCell>
        </>
      )}
      detailView={(capa, deps, minimumRole) => {
        return (
          <Paper className="w-11/12 flex flex-col p-8 items-stretch gap-4">
            <div className="w-full flex justify-end">
              {canDisplayMinimumRole(minimumRole, accountRole) && (
                <Button
                  variant="contained"
                  color="primary"
                  component={Link}
                  to={`/capas/${capa.id}/edit`}
                >
                  Edit
                </Button>
              )}
            </div>
            <div className="w-full md:flex xs:flex-col gap-4">
              <DetailItem
                title="Capa Name"
                value={capa.name}
                className="w-4/5 md:w-1/2"
              />
              <DetailItem
                title="Category"
                value={capa.category.name}
                className="w-4/5 md:w-1/2"
              />
            </div>
            <div className="w-full md:flex gap-4">
              <DetailItem
                title="Action Type"
                value={capa.action_type}
                className="w-4/5 md:w-1/2"
              />
              {capa.action_type === "Corrective" ? (
                <DetailItem
                  title="Incident"
                  value={capa.incident?.name || ""}
                  className="w-4/5 md:w-1/2"
                />
              ) : (
                <DetailItem
                  title="Risks To Prevent"
                  className="w-4/5 md:w-1/2"
                  value={
                    capa.risks_to_prevent?.map((r) => r.name).join(" | ") || ""
                  }
                />
              )}
            </div>
            <div className="w-full md:flex gap-4">
              <DetailItem
                title="Description"
                value={capa.description}
                className="w-4/5 md:w-1/2"
              />
              <DetailItem
                title="Measures Taken"
                value={capa.measures_taken}
                className="w-4/5 md:w-1/2"
              />
            </div>
            <div className="w-full md:flex gap-4">
              <DetailItem
                title="Date Created"
                value={capa.date_created}
                className="w-4/5 md:w-1/2"
              />
              <DetailItem
                title="Targetted Date to be Effective"
                value={capa.target_effective_date}
                className="w-4/5 md:w-1/2"
              />
            </div>
            <div className="w-full md:flex gap-4">
              <DetailItem
                title="Comments"
                value={capa.comments}
                className="w-4/5 md:w-1/2"
              />
              <DetailItem
                title="Rating"
                value={capa.rating}
                className="w-4/5 md:w-1/2"
              />
            </div>
            <div className="w-full md:flex gap-4">
              <DetailItem
                title="Prepared By"
                value={userFullName(capa.prepared_by)}
                className="w-4/5 md:w-1/2"
              />
              <DetailItem
                title="Approved By"
                value={capa.approved_by ? userFullName(capa.approved_by) : ""}
                className="w-4/5 md:w-1/2"
              />
            </div>
          </Paper>
        );
      }}
      renderForm={(capa, deps, useFormMethods, isEditForm) => {
        const {
          register,
          formState: { errors },
          control,
        } = useFormMethods;
        console.log(errors, capa, deps);
        const showIncident =
          (!isEditForm
            ? actionType === null
              ? "Corrective"
              : actionType
            : actionType === null
            ? capa?.action_type
            : actionType) === "Corrective";
        console.log(showIncident);
        return (
          <>
            <Snackbar />
            <div className="w-full md:flex gap-4">
              <Controller
                name="name"
                defaultValue={capa?.name}
                control={control}
                render={({ field }) => (
                  <TextField
                    {...field}
                    label="Name"
                    error={!!errors.name?.message}
                    helperText={errors.name?.message}
                    className="w-11/12 md:w-1/2"
                  />
                )}
              />
              <Controller
                name="description"
                defaultValue={capa?.description}
                control={control}
                render={({ field }) => (
                  <TextField
                    {...field}
                    label="Description"
                    error={!!errors.description?.message}
                    helperText={errors.description?.message}
                    className="w-11/12 md:w-1/2"
                  />
                )}
              />
            </div>
            <div className="w-full md:flex gap-4">
              <FormControl className="w-11/12 md:w-1/2">
                <InputLabel id="action-type">Current Status</InputLabel>
                <Controller
                  name="action_type"
                  defaultValue={capa?.action_type || "Corrective"}
                  control={control}
                  render={({ field }) => (
                    <Select
                      className="w-full"
                      labelId="action-type"
                      id="action_type"
                      {...field}
                      onChange={(e) => {
                        const item = e.target?.value as string | null;
                        setActionType(item ? item : null);
                        field.onChange(item);
                      }}
                      error={!!errors?.action_type}
                    >
                      {["Corrective", "Preventive"].map((model) => (
                        <MenuItem key={`status${model}`} value={model}>
                          {model}
                        </MenuItem>
                      ))}
                    </Select>
                  )}
                />
                <FormHelperText>{errors.action_type?.message}</FormHelperText>
              </FormControl>
              <FormControl className="w-11/12 md:w-1/2">
                <InputLabel id="action-type">Rating</InputLabel>
                <Controller
                  name="rating"
                  defaultValue={capa?.rating || "Low"}
                  control={control}
                  render={({ field }) => (
                    <Select
                      className="w-full"
                      labelId="action-type"
                      id="rating"
                      {...field}
                      error={!!errors?.rating}
                    >
                      {["Low", "Moderate", "High"].map((model) => (
                        <MenuItem key={`status${model}`} value={model}>
                          {model}
                        </MenuItem>
                      ))}
                    </Select>
                  )}
                />
                <FormHelperText>{errors.rating?.message}</FormHelperText>
              </FormControl>
            </div>
            <div className="w-full md:flex gap-4">
              <FormControl
                className="w-11/12 md:w-1/2"
                error={!!errors.incident}
                style={{ display: showIncident ? "inherit" : "none" }}
              >
                <Controller
                  name="incident"
                  defaultValue={capa?.incident}
                  control={control}
                  render={({ field }) => (
                    <Autocomplete
                      {...field}
                      onChange={(_, opt) => field.onChange(opt)}
                      options={deps?.incidents || []}
                      getOptionLabel={(option) => option.name}
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          label="Incident"
                          className="w-full"
                        />
                      )}
                    />
                  )}
                />
                <FormHelperText>{errors.incident?.message}</FormHelperText>
              </FormControl>
              <FormControl
                className="w-11/12 md:w-1/2 "
                error={!!errors.risks_to_prevent}
                style={{ display: showIncident ? "none" : "inherit" }}
              >
                <Controller
                  name="risks_to_prevent"
                  defaultValue={
                    Array.isArray(capa?.risks_to_prevent)
                      ? capa?.risks_to_prevent
                      : []
                  }
                  control={control}
                  render={({ field }) => (
                    <Autocomplete
                      {...field}
                      multiple
                      onChange={(_, opt) => field.onChange(opt)}
                      options={deps?.risks || []}
                      getOptionLabel={(option) => option.name}
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          label="Risks To Prevent"
                          className="w-full"
                          error={!!errors.risks_to_prevent}
                        />
                      )}
                    />
                  )}
                />
                <FormHelperText>
                  {errors.risks_to_prevent?.message}
                </FormHelperText>
              </FormControl>
              <FormControl
                className="w-11/12 md:w-1/2"
                error={!!errors.category}
              >
                <Controller
                  name="category"
                  defaultValue={capa?.category}
                  control={control}
                  render={({ field }) => (
                    <Autocomplete
                      {...field}
                      onChange={(_, opt) => field.onChange(opt)}
                      options={deps?.risk_categories || []}
                      getOptionLabel={(option) => option.name}
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          label="Category"
                          className="w-full"
                          error={!!errors.category}
                        />
                      )}
                    />
                  )}
                />
                <FormHelperText>{errors.category?.message}</FormHelperText>
              </FormControl>
            </div>
            <div className="w-full md:flex gap-4">
              <Controller
                name="date_created"
                defaultValue={
                  capa?.date_created
                    ? jsDateParsers.fromIso(capa?.date_created)
                    : new Date()
                }
                control={control}
                render={({ field }) => (
                  <FormControl
                    className="w-11/12 md:w-1/2"
                    error={!!errors.date_created?.message}
                  >
                    <KeyboardDatePicker
                      {...field}
                      disableToolbar
                      variant="inline"
                      format="yyyy-MM-dd"
                      className="w-full"
                      label="Date Created"
                      error={!!errors.date_created?.message}
                    />
                    <FormHelperText>
                      {errors.date_created?.message}
                    </FormHelperText>
                  </FormControl>
                )}
              />
              <Controller
                name="target_effective_date"
                defaultValue={
                  capa?.target_effective_date
                    ? jsDateParsers.fromIso(capa?.target_effective_date)
                    : new Date()
                }
                control={control}
                render={({ field }) => (
                  <FormControl
                    className="w-11/12 md:w-1/2"
                    error={!!errors.target_effective_date?.message}
                  >
                    <KeyboardDatePicker
                      {...field}
                      disableToolbar
                      variant="inline"
                      format="yyyy-MM-dd"
                      className="w-full"
                      label="Target Effective Date"
                      error={!!errors.target_effective_date?.message}
                    />
                    <FormHelperText>
                      {errors.target_effective_date?.message}
                    </FormHelperText>
                  </FormControl>
                )}
              />
            </div>
            <div className="w-full md:flex gap-4">
              <Controller
                name="measures_taken"
                defaultValue={capa?.measures_taken}
                control={control}
                render={({ field }) => (
                  <TextField
                    {...field}
                    label="Measures Taken"
                    error={!!errors.measures_taken?.message}
                    helperText={errors.measures_taken?.message}
                    className="w-11/12 md:w-1/2"
                  />
                )}
              />
              <Controller
                name="comments"
                defaultValue={capa?.comments || ""}
                control={control}
                render={({ field }) => (
                  <TextField
                    {...field}
                    label="Extra Comments"
                    error={!!errors.comments?.message}
                    helperText={errors.comments?.message}
                    className="w-11/12 md:w-1/2"
                  />
                )}
              />
            </div>
            <div className="w-full md:flex gap-4">
              <FormControl
                className="w-11/12 md:w-1/2"
                error={!!errors.prepared_by}
              >
                <Controller
                  name="prepared_by"
                  defaultValue={capa?.prepared_by}
                  control={control}
                  render={({ field }) => (
                    <Autocomplete
                      {...field}
                      onChange={(_, opt) => field.onChange(opt)}
                      options={deps?.users || []}
                      getOptionLabel={(option) => userFullName(option)}
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          label="Prepared By"
                          className="w-full"
                        />
                      )}
                    />
                  )}
                />
                <FormHelperText>{errors.prepared_by?.message}</FormHelperText>
              </FormControl>
              <FormControl
                className="w-11/12 md:w-1/2"
                error={!!errors.approved_by}
              >
                <Controller
                  name="approved_by"
                  defaultValue={capa?.approved_by || null}
                  control={control}
                  render={({ field }) => (
                    <Autocomplete
                      {...field}
                      onChange={(_, opt) => field.onChange(opt)}
                      options={deps?.users || []}
                      getOptionLabel={(option) => userFullName(option)}
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          required={false}
                          label="Approved By"
                          className="w-full"
                          error={!!errors.approved_by?.message}
                        />
                      )}
                    />
                  )}
                />
                <FormHelperText>{errors.approved_by?.message}</FormHelperText>
              </FormControl>
            </div>
          </>
        );
      }}
      pageTitles={{
        create: "Create Capa",
        update: "Update Capa",
        listing: "Capas",
        detail: "Capa Detail",
      }}
      itemName="Capa"
      remoteUrl="/capas"
      readableKey="name"
      actions={{
        create: (data, useFormMethods, setGeneralError) => {
          capaService
            .create(formDataToDto(data))
            .then(formSubmitAction("created", setGeneralError));
        },
        update: (data, useFormMethods, setGeneralError, itemId) => {
          capaService
            .update(itemId!!, formDataToDto(data))
            .then(formSubmitAction("updated", setGeneralError));
        },
        delete: capaService.delete,
      }}
    />
  );
}

export default CapaCrud;
