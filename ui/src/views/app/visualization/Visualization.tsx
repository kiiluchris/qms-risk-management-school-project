import { GqlCapa } from "@/domain/models/capa";
import { GqlQuery } from "@/domain/models/gql";
import { GqlIncident } from "@/domain/models/incident";
import { GqlProcess } from "@/domain/models/process";
import { GqlRisk } from "@/domain/models/risk";
import { useGqlQuery } from "@/utils/hooks";
import { jsDateParsers } from "@/utils/time";
import Slider from "@material-ui/core/Slider";
import Typography from "@material-ui/core/Typography";
import * as d3 from "d3";
import add from "date-fns/add";
import differenceInCalendarDays from "date-fns/differenceInCalendarDays";
import sub from "date-fns/sub";
import * as R from "ramda";
import React, { useState } from "react";
import {
  CartesianGrid,
  Line,
  LineChart,
  Bar,
  BarChart,
  Cell,
  ResponsiveContainer,
  Tooltip as ChartTooltip,
  XAxis,
  YAxis,
} from "recharts";
import {
  impactConfig,
  likelihoodConfigReversed,
  RiskLevelBounds,
  riskLevelStyle,
  riskLevelText,
  graphColors,
} from "../main/risk-graph-utils";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableBody from "@material-ui/core/TableBody";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import Loader from "@/components/shared/Loader";

type GraphCapa = Pick<
  GqlCapa,
  "date_created" | "target_effective_date" | "action_type" | "category"
>;
type GraphIncident = Pick<
  GqlIncident,
  "date_occurred" | "lag_time" | "category"
>;
type GraphRisk = Pick<GqlRisk, "category" | "likelihood" | "impact"> & {
  affected_processes: Pick<GqlProcess, "team_responsible">[];
};
type GraphModellingMetric = {
  model_name: string;
  recall: number[];
  precision: number[];
  f1_score: number[];
};
type GraphData = {
  capas: GraphCapa[];
  incidents: GraphIncident[];
  model_metrics: {
    category: GraphModellingMetric[];
    category_labels: string[];
  };
};
const groupMetrics = (
  metrics: GraphModellingMetric[],
  categories: string[]
) => {
  type Group = { [model: string]: { [category: string]: number } };
  const flatten = (group: Group) => {
    return Object.entries(group).flatMap(([model, categories]) => {
      return Object.entries(categories).reduce(
        (acc, [category, value]) => {
          return {
            ...acc,
            [category]: value,
          };
        },
        { model } as { model: string } & { [category: string]: number }
      );
    });
  };
  const groups = metrics.reduce(
    (acc, m) => {
      const modelName = m.model_name;
      const { precision, recall, f1_score } = acc;
      precision[modelName] = precision[modelName] || {};
      recall[modelName] = recall[modelName] || {};
      f1_score[modelName] = f1_score[modelName] || {};
      for (let i = 0; i < categories.length; i++) {
        const category = categories[i];
        precision[modelName][category] = m.precision[i];
        recall[modelName][category] = m.recall[i];
        f1_score[modelName][category] = m.f1_score[i];
      }
      return { precision, recall, f1_score };
    },
    { precision: {}, recall: {}, f1_score: {} } as {
      precision: Group;
      f1_score: Group;
      recall: Group;
    }
  );
  return {
    precision: flatten(groups.precision),
    recall: flatten(groups.recall),
    f1_score: flatten(groups.f1_score),
  };
};
const gqlQuery: GqlQuery<GraphData, undefined> = {
  dependencies: `
        capas {
            date_created
            target_effective_date
            action_type
            category {
              name
            }
        }
        incidents {
            category {
              name
            }
            date_occurred
            lag_time
        }
        model_metrics {
          category {
            model_name
            recall
            precision
            f1_score
          }
          category_labels
        }
    `,
};
/*
        risks {
          category
          likelihood
          impact
          affected_processes {
            team_responsible {
              name
            }
          }
        }
 */
const getTicks = (startDate: Date, endDate: Date, num: number) => {
  const diffDays = differenceInCalendarDays(endDate, startDate);

  let current = startDate,
    velocity = Math.round(diffDays / (num - 1));

  const ticks = [startDate.getTime()];

  for (let i = 1; i < num - 1; i++) {
    ticks.push(add(current, { days: i * velocity }).getTime());
  }

  ticks.push(endDate.getTime());
  return ticks;
};

const prepareCapaGraphData = (capas: GraphCapa[]) => {
  const capasWithTimeParsed = R.map((c) => {
    return {
      ...c,
      date_created: jsDateParsers.fromIso(c.date_created),
      target_effective_date: jsDateParsers.fromIso(c.target_effective_date),
    };
  }, capas);
  const capasSortedByDateCreated = R.sortBy(
    (c) => c.date_created.getTime(),
    capasWithTimeParsed
  );
  const capasGroupedByMonthAndYear = R.groupWith((a, b) => {
    return (
      a.date_created.getFullYear() === b.date_created.getFullYear() &&
      a.date_created.getMonth() === b.date_created.getMonth()
    );
  }, capasSortedByDateCreated);
  return R.map((group) => {
    const first = group[0];
    const last = group[group.length - 1];
    return {
      date: first.date_created.getTime(),
      date_last: last.date_created.getTime(),
      num_created: group.length,
    };
  }, capasGroupedByMonthAndYear);
};
const prepareIncidentGraphData = (incidents: GraphIncident[]) => {
  const incidentsWithTimeParsed = R.map((c) => {
    return {
      ...c,
      date_occurred: jsDateParsers.fromIso(c.date_occurred),
    };
  }, incidents);
  const incidentsSortedByDateCreated = R.sortBy(
    (c) => c.date_occurred.getTime(),
    incidentsWithTimeParsed
  );
  const incidentsGroupedByMonthAndYear = R.groupWith((a, b) => {
    return (
      a.date_occurred.getFullYear() === b.date_occurred.getFullYear() &&
      a.date_occurred.getMonth() === b.date_occurred.getMonth()
    );
  }, incidentsSortedByDateCreated);
  return R.map((group) => {
    const first = group[0];
    return {
      date: first.date_occurred.getTime(),
      num_occurred: group.length,
    };
  }, incidentsGroupedByMonthAndYear);
};

export function Visualization(): JSX.Element {
  const [riskMatrixWeight, setRiskMatrixWeight] = useState(13);
  const { gqlData, gqlError } = useGqlQuery(gqlQuery);
  if (!gqlData) {
    return <Loader />;
  }
  console.log(gqlData, gqlError);
  const { capas: capas_, incidents: incidents_, model_metrics } = gqlData;
  console.log(model_metrics);
  const [correctiveCapas, preventiveCapas] = R.partition(
    (c) => c.action_type === "Corrective",
    capas_
  );
  const capas = prepareCapaGraphData(correctiveCapas);
  const incidents = prepareIncidentGraphData(incidents_);
  const categoryMetrics = groupMetrics(
    model_metrics.category,
    model_metrics.category_labels
  );
  console.log(categoryMetrics);
  /* const [correctiveCapas, preventiveCapas] = R.partition((c) => c.action_type === "Corrective", capas) */
  const endDate = new Date();
  const startDate = sub(endDate, {
    years: 1,
  });
  const domainBounds = (numYears: number): [number, number] => {
    const dateToday = new Date();
    const beginningDate = sub(dateToday, {
      years: numYears,
    });

    return [beginningDate.getTime(), dateToday.getTime()];
  };

  const weightedRiskScoring: RiskLevelBounds = (() => {
    const upperRangeSize = 25 - riskMatrixWeight;
    const upperMidBound = Math.floor(upperRangeSize / 6) + riskMatrixWeight;
    const upperBound = riskMatrixWeight + Math.ceil((25 - upperMidBound) / 2);
    const lowerRangeSize = riskMatrixWeight - 1;
    const lowerMidBound = riskMatrixWeight - Math.floor(lowerRangeSize / 6);
    const lowerBound = Math.floor((lowerMidBound - 1) / 2) + 1;

    return { upperMidBound, upperBound, lowerMidBound, lowerBound };
  })();
  return (
    <div className="p-6 bg-white">
      <div className="w-full flex"></div>
      <div className="w-full flex"></div>
      <div className="w-full flex"></div>
      <div className="w-full flex">
        <div className="w-1/2">
          <h3 className="text-xl mb-4 h-20">
            Corrective and preventive measures per month
          </h3>
          <ResponsiveContainer width="100%" height={300}>
            <LineChart data={capas}>
              <XAxis
                dataKey="date"
                tickFormatter={d3.timeFormat("%B")}
                type="number"
                domain={domainBounds(1)}
              />
              <YAxis />
              <CartesianGrid stroke="#eee" strokeDasharray="5 5" />
              <Line type="monotone" dataKey="num_created" stroke="#8884d8" />
              <ChartTooltip
                labelFormatter={(value: Date) => d3.timeFormat("%B %Y")(value)}
              />
              {/* <Line type="monotone" dataKey="target_effective_date" stroke="#82ca9d" /> */}
            </LineChart>
          </ResponsiveContainer>
        </div>
        <div className="w-1/2">
          <h3 className="text-xl mb-4 h-20">Incidents per month</h3>
          <ResponsiveContainer width="100%" height={300}>
            <LineChart data={incidents}>
              <XAxis
                dataKey="date"
                tickFormatter={d3.timeFormat("%B")}
                type="number"
                domain={domainBounds(1)}
              />
              <YAxis />
              <CartesianGrid stroke="#eee" strokeDasharray="5 5" />
              <Line type="monotone" dataKey="num_occurred" stroke="#8884d8" />
              <ChartTooltip
                labelFormatter={(value: Date) => d3.timeFormat("%B-%Y")(value)}
              />
              {/* <Line type="monotone" dataKey="target_effective_date" stroke="#82ca9d" /> */}
            </LineChart>
          </ResponsiveContainer>
        </div>
      </div>
      <div className="w-full">
        <Table cellPadding="5px">
          <TableBody>
            {likelihoodConfigReversed.map((likelihood) => {
              return (
                <TableRow key={`heatmap-row-${likelihood.value}`}>
                  {impactConfig.map((impact) => {
                    const score = likelihood.value * impact.value;
                    return (
                      <TableCell
                        key={`heatmap-${score}`}
                        style={riskLevelStyle(score, weightedRiskScoring)}
                        className="border border-black text-center"
                      >
                        <Typography
                          className="text-center text-white"
                          style={{ fontWeight: 900 }}
                        >
                          {riskLevelText(score, weightedRiskScoring)}
                        </Typography>
                      </TableCell>
                    );
                  })}
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
        <div>
          <Typography id="likelihood-slider" gutterBottom>
            Matrix Weight
          </Typography>
          <Slider
            name="likelihood"
            defaultValue={13}
            step={1}
            min={1}
            max={25}
            marks={true}
            valueLabelDisplay="auto"
            aria-labelledby="likelihood-slider"
            onChange={(_e, value) => setRiskMatrixWeight(value as number)}
          />
        </div>
      </div>
      <div className="w-full flex">
        <div className="w-1/2">
          <h3 className="text-xl mb-4 h-20">Category Precision Metrics</h3>
          <ResponsiveContainer width="90%" height={300}>
            <BarChart data={categoryMetrics.precision}>
              <XAxis dataKey="model" type="category" width={100} />
              <YAxis />
              <CartesianGrid stroke="#eee" strokeDasharray="5 5" />

              {model_metrics.category_labels.map((category, i) => (
                <Bar
                  key={`precision-${category}-bar`}
                  dataKey={category}
                  fill={graphColors[i]}
                />
              ))}
              <ChartTooltip cursor={false} />
              {/* <Line type="monotone" dataKey="target_effective_date" stroke="#82ca9d" /> */}
            </BarChart>
          </ResponsiveContainer>
        </div>
        <div className="w-1/2">
          <h3 className="text-xl mb-4 h-20">Category Recall Metrics</h3>
          <ResponsiveContainer width="90%" height={300}>
            <BarChart data={categoryMetrics.recall} layout="vertical">
              <YAxis dataKey="model" type="category" />
              <XAxis />
              <CartesianGrid stroke="#eee" strokeDasharray="5 5" />

              {model_metrics.category_labels.map((category, i) => (
                <Bar
                  key={`recall-${category}-bar`}
                  dataKey={category}
                  fill={graphColors[i]}
                />
              ))}
              <ChartTooltip cursor={false} />
              {/* <Line type="monotone" dataKey="target_effective_date" stroke="#82ca9d" /> */}
            </BarChart>
          </ResponsiveContainer>
        </div>
      </div>
      <div className="w-full flex">
        <div className="w-1/2">
          <h3 className="text-xl mb-4 h-20">Category F1 Score Metrics</h3>
          <ResponsiveContainer width="90%" height={300}>
            <BarChart data={categoryMetrics.f1_score}>
              <XAxis dataKey="model" type="category" />
              <YAxis />
              <CartesianGrid stroke="#eee" strokeDasharray="5 5" />

              {model_metrics.category_labels.map((category, i) => (
                <Bar
                  key={`precision-${category}-bar`}
                  dataKey={category}
                  fill={graphColors[i]}
                />
              ))}
              <ChartTooltip cursor={false} />
              {/* <Line type="monotone" dataKey="target_effective_date" stroke="#82ca9d" /> */}
            </BarChart>
          </ResponsiveContainer>
        </div>
        <div className="w-1/2">
          <h3 className="text-xl mb-4 h-20">Category Recall Metrics</h3>
          <ResponsiveContainer width="90%" height={300}>
            <BarChart data={categoryMetrics.recall}>
              <XAxis dataKey="model" type="category" />
              <YAxis />
              <CartesianGrid stroke="#eee" strokeDasharray="5 5" />

              {model_metrics.category_labels.map((category, i) => (
                <Bar
                  key={`precision-${category}-bar`}
                  dataKey={category}
                  fill={graphColors[i]}
                />
              ))}
              <ChartTooltip cursor={false} />
              {/* <Line type="monotone" dataKey="target_effective_date" stroke="#82ca9d" /> */}
            </BarChart>
          </ResponsiveContainer>
        </div>
      </div>
    </div>
  );
}

export default Visualization;
