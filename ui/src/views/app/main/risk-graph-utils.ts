import { GqlRisk } from "@/domain/models/risk";
import { GqlProcess } from "@/domain/models/process";
import { GqlUser } from "@/domain/models/user";

export type RiskLevelBounds = {
  lowerBound: number;
  lowerMidBound: number;
  upperMidBound: number;
  upperBound: number;
};

export const defaultRiskLevelBounds: RiskLevelBounds = {
  lowerBound: 5,
  lowerMidBound: 10,
  upperMidBound: 15,
  upperBound: 20,
};

export const riskMetricStyle = (score: number): React.CSSProperties => {
  const backgroundColor = (() => {
    if (score === 1) return "#00b14e";
    else if (score === 2) return "#92d04e";
    else if (score === 3) return "#fefe00";
    else if (score === 4) return "#fec000";
    else return "#fe0000";
  })();

  return { backgroundColor };
};

export const riskLevelValueGetter = <T>(
  low: T,
  lowMid: T,
  mid: T,
  lowHigh: T,
  high: T
) => {
  return (score: number, riskLevelBounds: RiskLevelBounds): T => {
    const {
      upperMidBound,
      upperBound,
      lowerBound,
      lowerMidBound,
    } = riskLevelBounds;
    if (score === 25 && upperMidBound === 24) return mid;
    else if (score < lowerBound) return low;
    else if (score < lowerMidBound) return lowMid;
    else if (score < upperMidBound) return mid;
    else if (score < upperBound) return lowHigh;
    else return high;
  };
};

export const riskLevelStyle = (
  score: number,
  riskLevelBounds: RiskLevelBounds
): React.CSSProperties => {
  const backgroundColor = riskLevelValueGetter(
    "#00b14e",
    "#92d04e",
    "#fefe00",
    "#fec000",
    "#fe0000"
  )(score, riskLevelBounds);

  return { backgroundColor };
};

export const riskMetricText = (score: number) => {
  if (score === 1) return "Low";
  else if (score === 2) return "Low Med";
  else if (score === 3) return "Medium";
  else if (score === 4) return "Low High";
  else return "High";
};

export const riskLevelText = (
  score: number,
  riskLevelBounds: RiskLevelBounds
) => {
  return riskLevelValueGetter(
    "Low",
    "Low Med",
    "Medium",
    "Low High",
    "High"
  )(score, riskLevelBounds);
};

type RiskMatrixConfig = {
  name: string;
  value: number;
}[];

export const likelihoodConfig: RiskMatrixConfig = [
  { name: "Very Unlikely", value: 1 },
  { name: "Unlikely", value: 2 },
  { name: "Probably", value: 3 },
  { name: "Likely", value: 4 },
  { name: "Very Likely", value: 5 },
];

export const likelihoodConfigReversed = likelihoodConfig.slice().reverse();

export const impactConfig: RiskMatrixConfig = [
  { name: "Negligible", value: 1 },
  { name: "Minor", value: 2 },
  { name: "Moderate", value: 3 },
  { name: "Significant", value: 4 },
  { name: "Severe", value: 5 },
];

export type GraphRisk = Pick<GqlRisk, "category" | "likelihood" | "impact"> & {
  affected_processes: Pick<GqlProcess, "team_responsible">[];
};

export const groupRisksByLevel = (
  risks: GraphRisk[],
  riskLevelBounds: RiskLevelBounds
) => {
  const groupedRisks = risks.reduce((acc, risk) => {
    const level = risk.likelihood * risk.impact;
    const levelText = riskLevelText(level, riskLevelBounds);
    acc[levelText] = (acc[levelText] || 0) + 1;
    return acc;
  }, {} as Record<string, number>);

  return Object.entries(groupedRisks).map(([level, count]) => ({
    level,
    count,
  }));
};

export const userFullName = (user: {
  first_name: string;
  last_name: string;
}): string => {
  return user.first_name + " " + user.last_name;
};
export const groupRisksByOwner = (
  risks: (GraphRisk & { risk_owner?: GqlUser })[]
) => {
  type Owner = string;
  const initialGroups: Record<Owner, number> = {};
  const groupedRisks = risks.reduce((groups, risk) => {
    const name = risk.risk_owner ? userFullName(risk.risk_owner) : "Unknown";
    groups[name] = (groups[name] || 0) + 1;
    return groups;
  }, initialGroups);
  return Object.entries(groupedRisks).map(([owner, count]) => ({
    owner,
    count,
  }));
};

export const groupRisksByCategory = (risks: GraphRisk[]) => {
  const groupedRisks = risks.reduce((acc, risk) => {
    const category = risk.category.name;
    const existingCount = acc[category]?.count || 0;
    acc[category] = {
      category,
      count: existingCount + 1,
    };
    return acc;
  }, {} as Record<string, { count: number; category: string }>);

  return Object.values(groupedRisks);
};

export const groupRisksByTeamAndCategory = (risks: GraphRisk[]) => {
  const teamNames = new Set<string>();
  const groups = risks.reduce((acc, risk) => {
    const category = risk.category.name;
    return risk.affected_processes.reduce((res, p) => {
      const team = p.team_responsible.name;
      teamNames.add(team);
      res[team] = res[team] || {};
      const nextCount = (res[team][category] || 0) + 1;
      res[team][category] = nextCount;
      return res;
    }, acc);
  }, {} as { [team: string]: { [category: string]: number } });

  return Object.entries(groups).flatMap(([team, categories]) => {
    return Object.entries(categories).reduce(
      (acc, [category, count]) => ({
        ...acc,
        [category]: count,
      }),
      { team } as { team: string } & { [category: string]: number }
    );
  });
};

export const flattenDoubleNestedData = <K extends string>(
  data: { main_key: string; sub_key: string; value: number }[],
  key: K
): ({ [key in K]: string } & { [category: string]: number })[] => {
  const groups = data.reduce((acc, data) => {
    acc[data.main_key] = acc[data.main_key] || {};
    acc[data.main_key][data.sub_key] = data.value;
    return acc;
  }, {} as { [main_key: string]: { [sub_key: string]: number } });
  return Object.entries(groups).map(([main_key, sub_groups]) => {
    return Object.entries(sub_groups).reduce(
      (acc, [sub_key, count]) => ({
        ...acc,
        [sub_key]: count,
      }),
      { [key]: main_key } as { [key in K]: string } & {
        [sub_key: string]: number;
      }
    );
  });
};

export const graphColors = [
  "#ff5a66",
  "#4a6483",
  "#0fbab2",
  "#f9b733",
  "#2e99dd",
  "#3a67d6",
  "#8855d1",
  "#a6b1c4",
];
