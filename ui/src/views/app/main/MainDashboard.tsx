import { GqlCapa } from "@/domain/models/capa";
import { buildGqlQuery, GqlQuery } from "@/domain/models/gql";
import { GqlIncident } from "@/domain/models/incident";
import { GqlTask } from "@/domain/models/process";
import {
  useGqlQuery,
  usePageTitle,
  useRefreshLoginSession,
} from "@/utils/hooks";
import { jsDateFormatters, jsDateParsers } from "@/utils/time";
import { IconButton } from "@material-ui/core";
import Paper from "@material-ui/core/Paper";
import Tooltip from "@material-ui/core/Tooltip";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import differenceInDays from "date-fns/differenceInDays";
import format from "date-fns/format";
import parseISO from "date-fns/parseISO";
import React, { useState } from "react";
/* import Loader from "react-loader-spinner"; */
import Loader from "@/components/shared/Loader";
import { Link } from "react-router-dom";
import {
  Bar,
  BarChart,
  CartesianGrid,
  Cell,
  Pie,
  PieChart,
  ResponsiveContainer,
  Tooltip as ChartTooltip,
  XAxis,
  YAxis,
} from "recharts";
import {
  defaultRiskLevelBounds,
  flattenDoubleNestedData,
  graphColors,
  GraphRisk,
  groupRisksByCategory,
  groupRisksByLevel,
  groupRisksByOwner,
  groupRisksByTeamAndCategory,
} from "./risk-graph-utils";
type CounterProps<T> = {
  label: string;
  url: string;
  error?: any;
  data?: T[];
  tooltip?: string;
  onClick?: React.MouseEventHandler<HTMLElement>;
};

const Counter = <T,>({
  label,
  url,
  error,
  data,
  tooltip,
  onClick,
}: CounterProps<T>) => {
  let color: string, text: string;
  if (error) {
    text = `Error fetching ${label}`;
    color = "text-red-500";
  } else {
    const count = data?.length || 0;
    text = `${count}`;
    if (count < 5) {
      color = "text-green-500";
    } else if (count < 20) {
      color = "text-yellow-500";
    } else {
      color = "text-red-500";
    }
  }

  return (
    <div
      className="w-full md:w-1/3 h-32 bg-white text-center p-2 font-bold text-2xl shadow-md"
      onClick={onClick}
    >
      {onClick && (
        <div className="relative">
          <IconButton
            style={{ right: "12px", top: "8px", position: "absolute" }}
            onClick={(e) => onClick(e)}
          >
            <ExpandMoreIcon />
          </IconButton>
        </div>
      )}
      {tooltip ? (
        <Tooltip title={tooltip}>
          <h3 className="text-blue-900 mb-4">{label}</h3>
        </Tooltip>
      ) : (
        <h3 className="text-blue-900 mb-4">{label}</h3>
      )}
      <p className={(data ? "text-4xl " : "") + color}>{text}</p>
    </div>
  );
};

type DashboardIncident = Pick<
  GqlIncident,
  "id" | "name" | "date_occurred" | "location" | "damage"
>;

type SingleNestedGraphGroup = {
  key: string;
  value: number;
};

type DoubleNestedGraphGroup = {
  main_key: string;
  sub_key: string;
  value: number;
};
type RiskGraphGroups = {
  level: SingleNestedGraphGroup[];
  owner: SingleNestedGraphGroup[];
  category: SingleNestedGraphGroup[];
  team_and_category: DoubleNestedGraphGroup[];
  categories: string[];
};

type MainDashboardQuery = {
  tasks: GqlTask[];
  capas: Pick<GqlCapa, "id">[];
  incidents: DashboardIncident[];
  risks: GraphRisk[];
  risk_graph_groups: RiskGraphGroups;
};

const gqlQuery: GqlQuery<MainDashboardQuery, undefined> = {
  dependencies: `
      incidents {
        id
        name
        date_occurred
      }
      capas {
        id
      }
      tasks(pending: true, current_user: true) {
        id
        name
        due_by
      }
      risk_graph_groups {
        level {
          key
          value
        }
        owner {
          key
          value
        }
        category {
          key
          value
        }
        team_and_category {
          main_key
          sub_key
          value
        }
        categories
      }
    `,
};

const IncidentsToHandle = ({
  incidents,
}: {
  incidents: DashboardIncident[];
}) => {
  return (
    <div className="w-full bg-white p-8 shadow">
      <h2 className="text-3xl mb-6">Incidents to Handle</h2>
      {!incidents || incidents.length === 0 ? (
        <p>No incidents left to handle</p>
      ) : (
        <table className="table-auto w-full p-4 text-center">
          <thead>
            <tr className="font-bold text-xl border-b-2 h-12">
              <td>Name</td>
              <td>Date</td>
              <td>Location</td>
              <td>Damage</td>
              <td></td>
            </tr>
          </thead>
          <tbody>
            {incidents.map((t, i) => {
              return (
                <tr key={t.id} className="border-b-2 py-2 h-12">
                  <td>{t.name}</td>
                  <td>
                    {jsDateFormatters.format(
                      jsDateParsers.fromIso(t.date_occurred)
                    )}
                  </td>
                  <td>{t.location}</td>
                  <td>{t.damage}</td>
                  <td>
                    <Link to={`/incidents/${t.id}`}>View Incident</Link>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      )}
    </div>
  );
};
const PendingTasks = ({ urgentTasks }: { urgentTasks: GqlTask[] }) => {
  return (
    <div className="w-full bg-white p-8 shadow">
      <h2 className="text-3xl mb-6">Urgent Tasks</h2>
      {!urgentTasks || urgentTasks.length === 0 ? (
        <p>No urgent tasks remaining</p>
      ) : (
        <table className="table-auto w-full p-4 text-center">
          <thead>
            <tr className="font-bold text-xl border-b-2 h-12">
              <td>Name</td>
              <td>Deadline</td>
              {/* <td>Remaining SubTasks</td> */}
              <td></td>
            </tr>
          </thead>
          <tbody>
            {urgentTasks.map((t, i) => {
              return (
                <tr key={i} className="border-b-2 py-2 h-12">
                  <td>{t.name}</td>
                  <td>{format(parseISO(t.due_by), "d-M-Y")}</td>
                  {/* <td>{t.sub_tasks.filter((s) => !s.is_complete).length}</td> */}
                  <td>
                    <Link to={`/tasks/${t.id}`}>View Task</Link>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      )}
    </div>
  );
};
export default function MainDashboard() {
  usePageTitle("Dashboard");
  const { gqlData, gqlError } = useGqlQuery(gqlQuery, undefined);
  useRefreshLoginSession(gqlError?.status === 401 ? gqlError : undefined, [
    buildGqlQuery(gqlQuery),
  ]);
  type RecordKey = "incidents" | "pending_tasks" | undefined;
  const [showRecordsOfKey, setShowRecordsOfKey] = useState<RecordKey>(
    undefined
  );
  const toggleRecordKey = (key: RecordKey) => () => {
    if (showRecordsOfKey === key) {
      setShowRecordsOfKey(undefined);
    } else {
      setShowRecordsOfKey(key);
    }
  };
  console.log(gqlError);
  if (!gqlData) {
    //&& !gqlError){
    return <Loader />;
  }
  const { tasks: pending_tasks, incidents, capas, risk_graph_groups } = gqlData;
  //
  const risk_graph_groups_team_and_category = flattenDoubleNestedData(
    risk_graph_groups.team_and_category,
    "key"
  );

  console.log(pending_tasks);
  const urgentTasks =
    pending_tasks?.filter(
      (t) => differenceInDays(new Date(), parseISO(t.due_by)) < 7
    ) || [];
  return (
    <div className="w-full px-6 py-8 flex flex-col gap-y-8 bg-gray-100">
      <div className="flex flex-col md:flex-row justify-center gap-4 w-full">
        <Counter
          url="/incidents"
          label="Incidents"
          data={incidents}
          onClick={toggleRecordKey("incidents")}
        />
        <Counter
          url="/capas"
          label="CAPAs"
          data={capas}
          tooltip="Corrective and Preventive Actions"
        />
        <Counter
          url="/tasks?pending"
          label="Pending Tasks"
          data={pending_tasks}
          onClick={toggleRecordKey("pending_tasks")}
        />
      </div>
      {showRecordsOfKey === "incidents" && (
        <IncidentsToHandle incidents={incidents} />
      )}
      {showRecordsOfKey === "pending_tasks" && (
        <PendingTasks urgentTasks={urgentTasks} />
      )}
      <div className="w-full flex flex-col md:flex-row gap-2">
        <Paper className="w-11/12 md:w-1/2 p-2">
          <Typography variant="h5" className="text-center text-blue-900">
            Risks by level
          </Typography>
          <ResponsiveContainer className="mx-auto" width="90%" height={300}>
            <PieChart width={400} height={400}>
              <Pie
                data={risk_graph_groups.level}
                nameKey="key"
                dataKey="value"
                stroke="#8884d8"
                label={(props) => `${props.key}: ${props.value}`}
              >
                {risk_graph_groups.level.map((_, index) => (
                  <Cell key={`cell-${index}`} fill={graphColors[index]} />
                ))}
              </Pie>
              <ChartTooltip />
            </PieChart>
          </ResponsiveContainer>
        </Paper>
        <Paper className="w-11/12 md:w-1/2 p-2">
          <Typography variant="h5" className="text-center text-blue-900">
            Risks by categories
          </Typography>
          <ResponsiveContainer className="mx-auto" width="90%" height={300}>
            <BarChart data={risk_graph_groups.category} layout="vertical">
              <XAxis dataKey="value" type="number" />
              <YAxis type="category" dataKey="key" width={100} />
              <CartesianGrid stroke="#eee" strokeDasharray="5 5" />
              <Bar type="monotone" dataKey="value" stroke="#8884d8">
                {risk_graph_groups.category.map((_, index) => (
                  <Cell key={`cell-${index}`} fill={graphColors[index]} />
                ))}
              </Bar>
              <ChartTooltip cursor={false} />
            </BarChart>
          </ResponsiveContainer>
        </Paper>
      </div>
      <div className="w-full flex flex-col md:flex-row gap-2">
        <Paper className="w-11/12 md:w-1/2">
          <Typography variant="h5" className="text-center text-blue-900">
            Risks by team and categories
          </Typography>
          <ResponsiveContainer width="90%" height={300}>
            <BarChart
              data={risk_graph_groups_team_and_category}
              layout="vertical"
            >
              <YAxis type="category" dataKey="key" width={100} />
              <XAxis type="number" />
              <CartesianGrid stroke="#eee" strokeDasharray="5 5" />
              {risk_graph_groups.categories.map((category, i) => (
                <Bar
                  key={`team-${category}-bar`}
                  dataKey={category}
                  fill={graphColors[i]}
                  stackId="a"
                />
              ))}
              <ChartTooltip cursor={false} />
            </BarChart>
          </ResponsiveContainer>
        </Paper>
        <Paper className="w-11/12 md:w-1/2">
          <Typography variant="h5" className="text-center text-blue-900">
            Risks by owner
          </Typography>
          <ResponsiveContainer width="90%" height={300}>
            <BarChart data={risk_graph_groups.owner} layout="vertical">
              <XAxis type="number" dataKey="value" />
              <YAxis type="category" dataKey="key" width={100} />
              <CartesianGrid stroke="#eee" strokeDasharray="5 5" />
              <ChartTooltip cursor={false} />
              <Bar type="monotone" dataKey="value" stroke="#8884d8">
                {risk_graph_groups.owner.map((_, index) => (
                  <Cell key={`cell-${index}`} fill={graphColors[index]} />
                ))}
              </Bar>
            </BarChart>
          </ResponsiveContainer>
        </Paper>
      </div>
    </div>
  );
}
