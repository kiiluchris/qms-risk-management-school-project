import { IdObj } from "@/domain/models/shared";
import {
  useGqlQuery,
  usePageTitle,
  useRefreshLoginSession,
} from "@/utils/hooks";
import Container from "@material-ui/core/Container";
import React, { useState } from "react";
import Loader from "react-loader-spinner";
import { Redirect, useRouteMatch } from "react-router-dom";
import * as z from "zod";
import { buildGqlQuery, GqlQuery } from "@/domain/models/gql";

interface RiskViewProps<Query, QueryItem extends IdObj> {
  gqlQueries: GqlQuery<Query, QueryItem>;
  children?: React.ReactNode;
}

export function RiskView<Query, QueryItem extends IdObj>(
  props: RiskViewProps<Query, QueryItem>
) {
  type RouteParams = { item_id: string };
  const { params } = useRouteMatch<RouteParams>();
  const itemId = +params.item_id;

  const { gqlQueries } = props;
  usePageTitle("Risk Detailssd");
  const [generalError, setGeneralError] = useState("");
  const { gqlData, gqlError, hasGqlQuery } = useGqlQuery(gqlQueries, itemId);
  let depsErrorMessage = gqlError ? "Failed to load dependencies " : "";
  useRefreshLoginSession(gqlError?.status === 401 ? gqlError : undefined, [
    buildGqlQuery(gqlQueries, itemId),
  ]);
  if (
    typeof gqlData === "undefined" &&
    typeof gqlError === "undefined" &&
    hasGqlQuery
  ) {
    return (
      <div className="w-full h-full grid min-h-screen justify-center content-center">
        <Loader type="Oval" color="#00BFFF" height={100} width={100} />
      </div>
    );
  }
  const item = gqlData ? gqlQueries?.item?.extract(gqlData) : undefined;
  if (!item) {
    return <Redirect to={`/risks`} />;
  }
  return <Container>asd</Container>;
}

export default RiskView;
