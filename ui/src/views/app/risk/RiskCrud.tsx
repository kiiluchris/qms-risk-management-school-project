import ApiContext from "@/api/api";
import { useRootState } from "@/app/rootReducer";
import DashboardCrud from "@/components/dashboard/DashboardCrud";
import DashboardInput from "@/components/dashboard/DashboardInput";
import Slider from "@material-ui/core/Slider";
import DashboardSelectInput from "@/components/dashboard/DashboardSelectInput";
import {
  gqlQuery,
  GqlRiskWithDeps,
  Risk,
  RiskWithId,
} from "@/domain/models/risk";
import { setDashboardRisks } from "@/storage/dashboardSlice";
import {
  reactSelectOptionToRelatedItem,
  zodReactSelectOption,
  genericSchema,
  genericSchemaToRelatedItem,
  userSchemaToRelatedItem,
} from "@/utils/form";
import { Typography } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import green from "@material-ui/core/colors/green";
import red from "@material-ui/core/colors/red";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import CancelIcon from "@material-ui/icons/Cancel";
import CheckCircleIcon from "@material-ui/icons/CheckCircle";
import React, { useContext } from "react";
import { Link, useHistory } from "react-router-dom";
import * as z from "zod";
import {
  defaultRiskLevelBounds,
  riskLevelStyle,
  riskMetricStyle,
} from "../main/risk-graph-utils";
import { userFullName } from "../main/risk-graph-utils";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { Controller } from "react-hook-form";
import TextField from "@material-ui/core/TextField";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import FormHelperText from "@material-ui/core/FormHelperText";
import { canDisplayMinimumRole } from "@/domain/models/role";
import { useSnackbar } from "@/utils/hooks";
import { KeyboardDatePicker } from "@material-ui/pickers";
import { userSchema } from "@/domain/models/user";

const schema = z.object({
  name: z.string(),
  description: z.string(),
  recommended_actions: z.string(),
  /* category: z.string(), */
  likelihood: z.number().optional(),
  impact: z.number().optional(),
  /* category: zodReactSelectOption, */
  risk_owner: userSchema,
  affected_processes: z.array(genericSchema),
  current_status: z.string().optional(),
  /* affected_inputs: z.array(zodReactSelectOption).optional(),
   * affected_outputs: z.array(zodReactSelectOption).optional(),
   * affected_values: z.array(zodReactSelectOption), */
});

const formDataToDto = (
  data: z.infer<typeof schema>,
  isEditForm: boolean = false
): Risk => {
  return {
    ...data,
    affected_processes:
      data.affected_processes?.map((i) => genericSchemaToRelatedItem(i)) || [],
    category: { id: -1, value: "" },
    likelihood: !isEditForm ? 1 : data.likelihood || 1,
    impact: !isEditForm ? 1 : data.impact || 1,
    is_finalized: false,
    risk_owner: userSchemaToRelatedItem(data.risk_owner),
    current_status: data.current_status || "Identified",
  };
};

export default function RiskCrud() {
  const history = useHistory();
  const { risks, accountRole } = useRootState((state) => ({
    risks: state.dashboard.risks.items,
    accountRole: state.auth.role,
  }));
  const { Snackbar, setSnackbarSuccess, setSnackbarError } = useSnackbar();
  const { riskService } = useContext(ApiContext);
  const formSubmitAction = (
    action: string,
    setError: React.Dispatch<React.SetStateAction<string>>
  ) => (itemId: string | number) => {
    if (itemId === -1) {
      setSnackbarError("Risk could not be " + action);
      setError("Risk could not be " + action);
    } else {
      setSnackbarSuccess("Risk successfully " + action);
      setError("");
      history.push("/risks/");
    }
  };

  return (
    <DashboardCrud<
      RiskWithId,
      typeof schema,
      GqlRiskWithDeps,
      GqlRiskWithDeps["risk"]
    >
      dashboardPath="risks"
      items={risks}
      gqlQueries={gqlQuery}
      itemsUpdater={setDashboardRisks}
      detailView={(risk, deps, minimumRole) => {
        const riskLevel = risk.likelihood * risk.impact;
        const { backgroundColor: riskColor } = riskLevelStyle(
          riskLevel,
          defaultRiskLevelBounds
        );
        return (
          <div className="flex flex-col gap-5">
            <div className="flex justify-end">
              {/* {!risk.is_finalized && (
                  <Button variant="contained" color="primary" onClick={() => {}}>
                  Approve
                  </Button>
                  )} */}
              {canDisplayMinimumRole(minimumRole, accountRole) && (
                <Button variant="contained" color="primary" onClick={() => {}}>
                  <Link style={{ color: "#fff" }} to={`/risks/${risk.id}/edit`}>
                    Edit
                  </Link>
                </Button>
              )}
            </div>
            <Paper className="w-full flex flex-col md:flex-row justify-between">
              <div className="w-1/3 text-center">
                <Typography color="textPrimary">Risk Level</Typography>
                <Typography style={{ color: riskColor }}>
                  {riskLevel}
                </Typography>
              </div>
              <div className="w-1/3 text-center">
                <Typography color="textPrimary">Category</Typography>
                <Typography color="textSecondary">
                  {risk.category.name}
                </Typography>
              </div>
              <div className="w-1/3 text-center">
                <Typography color="textPrimary">Current Status</Typography>
                <Typography color="textSecondary">
                  {risk.current_status}
                </Typography>
              </div>
            </Paper>
            <Paper className="w-full flex flex-col xs:gap-2 md:flex-row p-3">
              <div className="w-full md:w-1/3 flex flex-col gap-2 border-r-2">
                <div>
                  <Typography color="textPrimary">Risk Name</Typography>
                  <Typography color="textSecondary">{risk.name}</Typography>
                </div>
                <div>
                  <Typography color="textPrimary">Risk Impact</Typography>
                  <Typography
                    style={{
                      color: riskMetricStyle(risk.impact).backgroundColor,
                    }}
                  >
                    {risk.impact}
                  </Typography>
                </div>
                <div>
                  <Typography color="textPrimary">Risk Likelihood</Typography>
                  <Typography
                    style={{
                      color: riskMetricStyle(risk.likelihood).backgroundColor,
                    }}
                  >
                    {risk.likelihood}
                  </Typography>
                </div>
                <div>
                  <Typography color="textPrimary">Risk Description</Typography>
                  <Typography color="textSecondary">
                    {risk.description}
                  </Typography>
                </div>
                <div>
                  <Typography color="textPrimary">
                    Risk Recommended Actions
                  </Typography>
                  <Typography color="textSecondary">
                    {risk.recommended_actions}
                  </Typography>
                </div>
              </div>
              <div className="w-full md:w-2/3 p-2">
                <Typography color="textPrimary">Affected Processes</Typography>

                <Table>
                  <TableHead>
                    <TableRow>
                      <TableCell>Name</TableCell>
                      <TableCell>Deadline</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {risk.affected_processes.map((proc) => {
                      return (
                        <TableRow>
                          <TableCell> {proc.name}</TableCell>
                          <TableCell>{proc.objective.target_date}</TableCell>
                        </TableRow>
                      );
                    })}
                  </TableBody>
                </Table>
                <Typography color="textPrimary">
                  Existing Preventive Actions
                </Typography>
                <Table>
                  <TableHead>
                    <TableRow>
                      <TableCell>Name</TableCell>
                      <TableCell>Target Date to be Effective</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {risk.preventive_capas.map((capa) => {
                      return (
                        <TableRow>
                          <TableCell> {capa.name}</TableCell>
                          <TableCell>{capa.target_effective_date}</TableCell>
                        </TableRow>
                      );
                    })}
                  </TableBody>
                </Table>
                <Typography color="textPrimary">Suggested Actions</Typography>
                <Table>
                  <TableHead>
                    <TableRow>
                      <TableCell>Name</TableCell>
                      <TableCell>Target Date to be Effective</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {risk.suggested_capas.length ? (
                      risk.suggested_capas.map((capa) => {
                        return (
                          <TableRow>
                            <TableCell> {capa.name}</TableCell>
                            <TableCell>{capa.target_effective_date}</TableCell>
                          </TableRow>
                        );
                      })
                    ) : (
                      <TableRow>
                        <TableCell colSpan={2}>None found</TableCell>
                      </TableRow>
                    )}
                  </TableBody>
                </Table>
              </div>
            </Paper>
          </div>
        );
      }}
      headers={["Name", "Category", "Likelihood", "Impact", "Current Status"]}
      renderItem={(risk) => (
        console.log(risk),
        (
          <>
            <TableCell>
              <Link to={`/risks/${risk.id}`}>{risk.name}</Link>
            </TableCell>
            <TableCell align="center">{risk.category.readable}</TableCell>
            <TableCell align="center">{risk.likelihood}</TableCell>
            <TableCell align="center">{risk.impact}</TableCell>
            {/* <TableCell>{risk.is_positive ? "Positive" : "Negative"}</TableCell>
              <TableCell>{risk.person_responsible.readable}</TableCell> */}
            <TableCell align="center">{risk.current_status}</TableCell>
          </>
        )
      )}
      schema={schema}
      renderForm={(risk, deps, useFormMethods, isEditForm) => {
        const {
          register,
          formState: { errors },
          control,
        } = useFormMethods;
        console.log(errors, risk);

        return (
          <>
            <div className="w-full md:flex gap-4">
              <Controller
                control={control}
                name="name"
                defaultValue={risk?.name}
                render={({ field }) => (
                  <TextField
                    {...field}
                    label="Name"
                    error={!!errors.name?.message}
                    helperText={errors.name?.message}
                    className="w-4/5 md:w-1/2"
                  />
                )}
              />
              <Controller
                control={control}
                name="description"
                defaultValue={risk?.description}
                render={({ field }) => (
                  <TextField
                    {...field}
                    label="Description"
                    error={!!errors.description?.message}
                    helperText={errors.description?.message}
                    className="w-4/5 md:w-1/2"
                  />
                )}
              />
            </div>
            <div className="w-full md:flex gap-4">
              <Controller
                control={control}
                name="recommended_actions"
                defaultValue={risk?.recommended_actions}
                render={({ field }) => (
                  <TextField
                    {...field}
                    label="Recommended Actions"
                    error={!!errors.recommended_actions?.message}
                    helperText={errors.recommended_actions?.message}
                    className="w-4/5 md:w-1/2"
                  />
                )}
              />
              <FormControl
                className="w-11/12 md:w-1/2"
                error={!!errors.affected_processes}
              >
                <Controller
                  name="affected_processes"
                  defaultValue={risk?.affected_processes}
                  control={control}
                  render={({ field }) => (
                    <Autocomplete
                      {...field}
                      multiple
                      onChange={(_, opt) => field.onChange(opt)}
                      options={deps?.processes || []}
                      getOptionLabel={(option) => option.name}
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          label="Affected Processes"
                          className="w-full"
                        />
                      )}
                    />
                  )}
                />
                <FormHelperText>
                  {errors.affected_processes?.message}
                </FormHelperText>
              </FormControl>
            </div>
            <div className="w-full md:flex">
              <FormControl
                className="w-11/12 md:w-1/2"
                error={!!errors.risk_owner}
              >
                <Controller
                  name="risk_owner"
                  defaultValue={risk?.risk_owner}
                  control={control}
                  render={({ field }) => (
                    <Autocomplete
                      {...field}
                      onChange={(_, opt) => field.onChange(opt)}
                      options={
                        deps?.users.filter((u) => u.role.name === "Manager") ||
                        []
                      }
                      getOptionLabel={(option) => userFullName(option)}
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          label="Risk Owner"
                          className="w-full"
                          error={!!errors.risk_owner}
                        />
                      )}
                    />
                  )}
                />
                <FormHelperText>{errors.risk_owner?.message}</FormHelperText>
              </FormControl>
              {isEditForm && (
                <FormControl className="w-11/12 md:w-1/2">
                  <InputLabel id="current-status">Current Status</InputLabel>
                  <Controller
                    name="current_status"
                    defaultValue={risk?.current_status || "Identified"}
                    control={control}
                    render={({ field }) => (
                      <Select
                        className="w-full"
                        labelId="current-status"
                        id="current_status"
                        {...field}
                        onChange={(e) => {
                          const item = e.target?.value as string | null;
                          field.onChange(item);
                        }}
                        error={!!errors?.current_status}
                      >
                        {["Identified", "Assessed", "Controlled"].map(
                          (model) => (
                            <MenuItem key={`status${model}`} value={model}>
                              {model}
                            </MenuItem>
                          )
                        )}
                      </Select>
                    )}
                  />
                  <FormHelperText>
                    {errors.current_status?.message}
                  </FormHelperText>
                </FormControl>
              )}
            </div>
            {isEditForm && (
              <div className="w-full md:flex">
                <div className="md:w-1/2">
                  <div className="mx-auto w-11/12">
                    <Typography id="likelihood-slider" gutterBottom>
                      Likelihood
                    </Typography>
                    <Controller
                      name="likelihood"
                      control={control}
                      defaultValue={risk?.likelihood ? +risk?.likelihood : 1}
                      render={({ field }) => (
                        <Slider
                          {...field}
                          step={1}
                          min={1}
                          max={5}
                          marks={true}
                          valueLabelDisplay="auto"
                          aria-labelledby="likelihood-slider"
                        />
                      )}
                    />
                  </div>
                </div>
                <div className="md:w-1/2">
                  <div className="mx-auto w-11/12">
                    <Typography id="impact-slider" gutterBottom>
                      Impact
                    </Typography>
                    <Controller
                      name="impact"
                      control={control}
                      defaultValue={risk?.impact ? +risk?.impact : 1}
                      render={({ field }) => (
                        <Slider
                          {...field}
                          step={1}
                          min={1}
                          max={5}
                          marks={true}
                          valueLabelDisplay="auto"
                          aria-labelledby="impact-slider"
                        />
                      )}
                    />
                  </div>
                </div>
              </div>
            )}
          </>
        );
      }}
      pageTitles={{
        create: "Create Risk",
        update: "Update Risk",
        listing: "Risks",
        detail: "Risk Detail",
      }}
      itemName="Risk"
      remoteUrl="/risks"
      readableKey="name"
      actions={{
        create: (data, useFormMethods, setGeneralError) => {
          riskService
            .create(formDataToDto(data))
            .then(formSubmitAction("created", setGeneralError));
        },
        update: (data, useFormMethods, setGeneralError, itemId, isEditForm) => {
          riskService
            .update(itemId!!, formDataToDto(data, isEditForm))
            .then(formSubmitAction("updated", setGeneralError));
        },
        delete: riskService.delete,
      }}
    />
  );
}
