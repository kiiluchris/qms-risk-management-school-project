import AuthFormContainer from "@/components/forms/AuthFormContainer";

export default function Error404() {
  return (
    <AuthFormContainer label="404">
      <p>The page you have requested does not exist</p>
    </AuthFormContainer>
  );
}
