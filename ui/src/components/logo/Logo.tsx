import React from "react";
import styles from "./Logo.module.css";
import cx from "classnames";

type Props = {
  style?: React.CSSProperties;
  className?: string;
  onClick?: React.MouseEventHandler<HTMLSpanElement>;
  text?: string;
};

const Logo: React.FC<Props> = ({ style, className, onClick, text }) => {
  return (
    <span
      className={cx(styles.logo, className)}
      style={style}
      onClick={onClick}
    >
      {typeof text === "string" ? text : "rQMS"}
    </span>
  );
};

export default Logo;
