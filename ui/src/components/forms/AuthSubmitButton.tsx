import React from "react";
import cx from "classnames";
import styles from "./AuthFormContainer.module.scss";

type Props = {
  text: string;
  onClick?: React.MouseEventHandler;
};

const AuthSubmitButton: React.FC<Props> = ({ text, onClick }) => {
  const className = cx(styles.submitButtonWrapper);
  return (
    <div className={className}>
      <button type="submit" className="laatu-bg-2 w-full" onClick={onClick}>
        {text}
      </button>
    </div>
  );
};

export default AuthSubmitButton;
