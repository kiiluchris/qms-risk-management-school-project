import React from "react";

type ErrorTypes =
  | "minLength"
  | "maxLength"
  | "pattern"
  | "min"
  | "max"
  | "required"
  | "validate";

type Props = {
  errorType?: ErrorTypes;
  actualType?: ErrorTypes;
  message: string;
};

const FormInputError: React.FC<Props> = ({
  message,
  errorType,
  actualType,
}) => {
  return (
    <React.Fragment>
      {((!errorType && !errorType && message) || errorType === actualType) && (
        <span className="text-center text-red-500">{message}</span>
      )}
    </React.Fragment>
  );
};

export default FormInputError;
