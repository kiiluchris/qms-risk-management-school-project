import React, { useEffect } from "react";
import styles from "./AuthFormContainer.module.scss";
import Logo from "@/components/logo/Logo";

type Props = {
  label: string;
};

const AuthFormContainer: React.FC<Props> = ({ label, children }) => {
  useEffect(() => {
    document.body.classList.add("laatu-bg-1");
    return () => {
      document.body.classList.remove("laatu-bg-1");
    };
  });

  return (
    <div className="laatu-bg-1">
      <div className={styles.formContainer}>
        <div className={styles.formWrapper}>
          <h1
            className="text-3xl text-center md:text-left md:w-11/12
            pl-2 flex flex-col gap-10 my-3 items-center"
          >
            <Logo
              className="laatu-txt-1"
              text=""
              style={{ fontSize: "5.25rem" }}
            />
            <span>{label}</span>
          </h1>
          {children}
        </div>
      </div>
    </div>
  );
};

export default AuthFormContainer;
