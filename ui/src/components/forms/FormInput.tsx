import { UseFormRegister } from "react-hook-form";
import styles from "./FormInput.module.scss";

export type FormInputProps = {
  type?: string;
  autocomplete?: string;
  placeholder: string;
  disabled?: boolean;
  name: string;
  value?: any;
  register: UseFormRegister<Record<string, any>>;
  onChange?: React.ChangeEventHandler<HTMLInputElement>;
};

const FormInput: React.FC<FormInputProps> = ({
  name,
  placeholder,
  autocomplete,
  type: type_ = "text",
  disabled = false,
  value,
  register,
}) => {
  return (
    <div className={styles.formInputWrapper}>
      <label htmlFor={name} className="sr-only">
        {placeholder}
      </label>
      <input
        id={name}
        autoComplete={autocomplete}
        type={type_}
        placeholder={placeholder}
        required
        className={styles.formInput}
        disabled={disabled}
        value={value}
        {...register(name)}
      />
    </div>
  );
};

export default FormInput;
