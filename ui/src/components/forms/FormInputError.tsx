import React from "react";

type Props = {
  message?: string;
};

const FormInputError: React.FC<Props> = ({ message }) => {
  return (
    <React.Fragment>
      {message && <span className="text-center text-red-500">{message}</span>}
    </React.Fragment>
  );
};

export default FormInputError;
