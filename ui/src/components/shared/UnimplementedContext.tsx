import {createContext} from 'react'


const UnimplementedContext = createContext(true);

export const UnimplementedProvider = UnimplementedContext.Provider;
export const UnimplementedConsumer = UnimplementedContext.Consumer;

export default UnimplementedContext;
