import React, { useContext } from 'react'
import UnimplementedContext from './UnimplementedContext'

type Props = {
  name: string;
  reasons?: string[];
}

const Unimplemented: React.FC<Props> = ({
  name,
  reasons = [],
  children
}) => {
  const unimplemented = useContext(UnimplementedContext);


  if(unimplemented) {
    return (
      <React.Fragment>
        {children}
      </React.Fragment>
    );
  } else {
    return (
      <div style={{display: 'none'}}>
        <p>{name}</p>
        <ul>
          {reasons.map((reason, idx) => (
            <li key={idx}>{reason}</li>
          ))}
        </ul>
      </div>
    );
  }
};

export default Unimplemented;
