import React from "react";
import { Redirect, Route, RouteProps } from "react-router-dom";
import { useRootState } from "@/app/rootReducer";
import { RolePermission } from "@/domain/models/role";

const AuthRoute: React.FC<RouteProps & { minimumRole?: RolePermission }> = ({
  minimumRole,
  children,
  ...props
}) => {
  const { tokens, isRefreshingToken, role } = useRootState(
    (state) => state.auth
  );
  const isLoggedIn = !!tokens;
  if (!isRefreshingToken && !isLoggedIn) {
    return <Redirect push to="/auth/login" />;
  }
  if (
    (minimumRole === "Admin" && role !== "Admin") ||
    (minimumRole === "Manager" && role === "Employee")
  ) {
    return <Redirect push to="/dashboard" />;
  }
  return <Route {...props}>{children}</Route>;
};
export default AuthRoute;
