import useSWR from "swr";

import { fetcher } from "@/api/client";
import { useRefreshLoginSession } from "@/utils/hooks";

type FetchedComponentProps<T> = {
  remoteUrl: string;
  render: (item: T | undefined, error: Error | undefined) => React.ReactNode;
};

export function FetchedComponent<T>({
  remoteUrl,
  render,
}: FetchedComponentProps<T>) {
  const { data: items, error } = useSWR<T>(remoteUrl, fetcher, {
    revalidateOnFocus: false,
  });
  useRefreshLoginSession(error);

  return <>{render(items, error)}</>;
}

export default FetchedComponent;
