import {
  DashboardCreateForm,
  DashboardUpdateForm,
} from "@/components/dashboard/DashboardForm";
import DashboardItemListing from "@/components/dashboard/DashboardItemListing";
import { TableExtraColumns } from "@/components/dashboard/DashboardTable";
import AuthRoute from "@/components/shared/AuthRoute";
import { GqlQuery } from "@/domain/models/gql";
import { RolePermission } from "@/domain/models/role";
import { IdObj } from "@/domain/models/shared";
import { ActionCreatorWithPayload } from "@reduxjs/toolkit";
import React from "react";
import { DefaultValues, UseFormReturn } from "react-hook-form";
import { Switch } from "react-router-dom";
import * as z from "zod";
import { ItemViewProps, ItemView } from "./ItemDetail";

type DashboardCrudProps<
  T,
  Schema extends z.ZodObject<any, any, any>,
  Query,
  QueryItem extends IdObj
> = {
  dashboardPath: string;
  headers: string[];
  schema: Schema;
  itemName: string;
  remoteUrl: string;
  callback_url?: string;
  readableKey: keyof T;
  actions: {
    create: (
      data: z.infer<Schema>,
      useFormMethods: UseFormReturn<Record<string, any>>,
      setGeneralError: React.Dispatch<React.SetStateAction<string>>,
      itemId: string | number | undefined,
      isEditForm: boolean
    ) => void;
    update: (
      data: z.infer<Schema>,
      useFormMethods: UseFormReturn<Record<string, any>>,
      setGeneralError: React.Dispatch<React.SetStateAction<string>>,
      itemId: string | number | undefined,
      isEditForm: boolean
    ) => void;
    delete: (valueId: number | string) => Promise<number>;
  };
  pageTitles: {
    create: string;
    update: string;
    listing: string;
    detail: string;
  };
  detailView?: (
    item: QueryItem,
    deps: Query,
    minimumRoleReadOnly?: RolePermission
  ) => React.ReactNode;
  renderItem: (item: T) => React.ReactNode;
  updatableItem?: (item: T) => boolean;
  deletableItem?: (item: T) => boolean;
  renderForm: (
    item: QueryItem | undefined,
    deps: Query | undefined,
    useFormMethods: UseFormReturn<Record<string, any>>,
    isEditForm: boolean
  ) => React.ReactNode;
  items: T[];
  itemsUpdater: ActionCreatorWithPayload<T[], string>;
  defaultValues?: DefaultValues<z.infer<Schema>>;
  gqlQueries: GqlQuery<Query, QueryItem>;
  extraColumns?: TableExtraColumns<T>[];
  queryParamMaker?: (qp: URLSearchParams) => URLSearchParams;
  minimumRole?: RolePermission;
  minimumRoleReadOnly?: RolePermission;
};

export function DashboardCrud<
  T extends IdObj,
  Schema extends z.ZodObject<any, any, any>,
  Query,
  QueryItem extends IdObj
>({
  dashboardPath,
  headers,
  schema,
  renderForm,
  renderItem,
  pageTitles,
  itemName,
  actions,
  readableKey,
  remoteUrl,
  updatableItem,
  deletableItem,
  items,
  itemsUpdater,
  defaultValues,
  gqlQueries,
  extraColumns,
  queryParamMaker,
  detailView,
  minimumRole,
  minimumRoleReadOnly,
  callback_url,
}: DashboardCrudProps<T, Schema, Query, QueryItem>) {
  return (
    <div className="w-full min-h-screen">
      <Switch>
        <AuthRoute
          exact
          path={`/${dashboardPath}`}
          minimumRole={minimumRoleReadOnly || minimumRole}
        >
          <DashboardItemListing<T>
            headers={headers}
            remoteUrl={remoteUrl}
            readableKey={readableKey}
            renderItem={renderItem}
            deleteItem={actions.delete}
            pageTitle={pageTitles.listing}
            itemName={itemName}
            dashboardPath={dashboardPath}
            updatable={updatableItem}
            deletable={deletableItem}
            callback_url={callback_url}
            items={items}
            itemsUpdater={itemsUpdater}
            extraColumns={extraColumns}
            queryParamMaker={queryParamMaker}
            minimumRoleReadOnly={minimumRoleReadOnly}
          />
        </AuthRoute>
        <AuthRoute
          exact
          path={`/${dashboardPath}/new`}
          minimumRole={minimumRole}
        >
          <DashboardCreateForm<T, Schema, Query, QueryItem>
            renderForm={renderForm}
            defaultValues={defaultValues}
            schema={schema}
            pageTitle={pageTitles.create}
            submitAction={actions.create}
            itemName={itemName}
            dashboardPath={dashboardPath}
            gqlQueries={gqlQueries}
          />
        </AuthRoute>
        {detailView && (
          <AuthRoute
            exact
            path={`/${dashboardPath}/:item_id`}
            minimumRole={minimumRoleReadOnly || minimumRole}
          >
            <ItemView<Query, QueryItem>
              dashboardPath={dashboardPath}
              pageTitle={pageTitles.detail}
              gqlQueries={gqlQueries}
              itemName={itemName}
              render={detailView}
              minimumRoleReadOnly={minimumRoleReadOnly}
            />
          </AuthRoute>
        )}
        <AuthRoute
          exact
          path={`/${dashboardPath}/:item_id/edit`}
          minimumRole={minimumRole}
        >
          <DashboardUpdateForm<T, Schema, Query, QueryItem>
            renderForm={renderForm}
            defaultValues={defaultValues}
            schema={schema}
            pageTitle={pageTitles.update}
            submitAction={actions.update}
            itemName={itemName}
            remoteUrl={remoteUrl}
            dashboardPath={dashboardPath}
            gqlQueries={gqlQueries}
          />
        </AuthRoute>
      </Switch>
    </div>
  );
}
export default DashboardCrud;
