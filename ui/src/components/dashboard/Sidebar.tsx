import React from "react";
import { Link } from "react-router-dom";
import classnames from "classnames";

import Logo from "@/components/logo/Logo";

type LinkProps = {
  to: string;
  text: string;
  icon?: string;
  toggleSidebar: () => void;
};

const SidebarLink: React.FC<LinkProps> = ({ to, text, toggleSidebar }) => {
  return (
    <li className="text-center hover:bg-gray-100 hover:bg-opacity-10 rounded">
      <Link
        to={to}
        className="text-white font-bold text-xl"
        onClick={() => toggleSidebar()}
      >
        {text}
      </Link>
    </li>
  );
};

type Props = {
  className?: string;
  style?: React.CSSProperties;
  hideInMobile: boolean;
  toggleSidebar: () => void;
  links: { text: string; path: string }[];
};

const Sidebar: React.FC<Props> = ({
  className,
  style,
  hideInMobile,
  toggleSidebar,
  links,
}) => {
  const sidebarStyle: React.CSSProperties = {
    transition: "width 1s ease-in-out",
    ...style,
  };
  return (
    <aside
      className={classnames(
        "md:bg-transparent md:w-0  static md:w-auto",
        hideInMobile ? "" : "bg-black bg-opacity-20 w-screen h-screen"
      )}
    >
      <div
        className={classnames(
          "laatu-bg-1 min-h-full md:px-5 md:py-2 overflow-hidden",
          "fixed md:static md:w-64",
          hideInMobile ? "w-0 h-0 p-0" : "w-5/6 px-5 py-2"
        )}
        style={sidebarStyle}
      >
        <nav>
          <div className="text-center w-full">
            <Link to="/dashboard" onClick={() => toggleSidebar()}>
              <Logo
                style={{ fontSize: "3.5rem", visibility: "hidden" }}
                className="text-white"
              />
            </Link>
          </div>
          <ul>
            {links.map(({ text, path }) => (
              <SidebarLink
                to={path}
                text={text}
                toggleSidebar={toggleSidebar}
                key={text}
              />
            ))}
          </ul>
        </nav>
      </div>
    </aside>
  );
};

export default Sidebar;
