import DashboardInput from "@/components/dashboard/DashboardInput";
import React, { useState } from "react";
import { UseFormReturn } from "react-hook-form";
import TableIcon from "./TableIconColumn";

const addNewInput = <T,>(
  index: number,
  setItems: (value: React.SetStateAction<T[]>) => void,
  emptyItem: T,
  isItemEqual: (item: T, other: T) => boolean
) => {
  setItems((items) => {
    console.log(items);
    return index === -1 || !isItemEqual(items[index], emptyItem)
      ? items.concat(emptyItem)
      : items;
  });
};

const updateInputState = <T,>(
  index: number,
  setItems: (value: React.SetStateAction<T[]>) => void,
  newItem: T
) => {
  setItems((items) => {
    return items.map((item, idx) => {
      if (idx === index) {
        console.log(idx, newItem, item);
        return newItem;
      } else {
        return item;
      }
    });
  });
};

const removeItemAtIndex = <T,>(
  index: number,
  setItems: (value: React.SetStateAction<T[]>) => void
) => {
  setItems((items) => {
    return items.filter((_, idx) => {
      return idx !== index;
    });
  });
};

type DashboardMultiInputProps<T> = {
  useFormMethods: UseFormReturn<Record<string, any>>;
  fetchedItems?: T[];
  emptyItem: T;
  name: string;
  placeholder?: string;
  isItemEqual: (item: T, other: T) => boolean;
  error?: string;
  itemName: string;
  render: {
    header: string;
    className: string;
    wrapperClassName?: string;
    fn: (props: {
      name: string;
      item: T;
      updateValue: (value: T) => void;
      index: number;
      useFormMethods: UseFormReturn<Record<string, any>>;
    }) => JSX.Element;
  }[];
};

export function DashboardMultiInput<T>({
  fetchedItems,
  useFormMethods,
  name,
  placeholder,
  render,
  emptyItem,
  isItemEqual,
  error,
  itemName,
}: DashboardMultiInputProps<T>) {
  const [items, setItems] = useState(fetchedItems || []);
  /* useEffect(() => {
   *   setItems(fetchedItems || [emptyItem]);
   *   // eslint-disable-next-line
   * }, [fetchedItems]); */
  const lastIndex = items.length - 1;
  const lastColumn = render.length - 1;

  return (
    <DashboardInput name={name} placeholder={placeholder} error={error}>
      <div className="flex">
        {render.map(({ fn, header, className, wrapperClassName }, index) => {
          return (
            <div
              key={`${name}.${header}[${index}]`}
              className={className + " block"}
            >
              <h3 className="font-bold">{header}</h3>
              {items.map((item, itemIndex) => {
                const key = `${name}.${header}[${index}][${itemIndex}]`;
                return (
                  <div
                    key={key}
                    className={
                      "w-11/12 flex" +
                      (wrapperClassName ? " " + wrapperClassName : "")
                    }
                  >
                    {fn({
                      name,
                      item,
                      useFormMethods,
                      updateValue: (value) => {
                        updateInputState(itemIndex, setItems, value);
                      },
                      index: itemIndex,
                    })}
                    {index === lastColumn && (
                      <TableIcon
                        action={() => removeItemAtIndex(itemIndex, setItems)}
                      >
                        <path
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          strokeWidth={2}
                          d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"
                        />
                      </TableIcon>
                    )}
                  </div>
                );
              })}
            </div>
          );
        })}
      </div>
      <div className="flex">
        {render.map(({ fn, header, className, wrapperClassName }, index) => {
          const key = `${name}.${header}.add_item`;
          if (index !== lastColumn) {
            return <span key={key} className={className + " block"}></span>;
          }

          return (
            <button
              key={key}
              onClick={(e) => {
                e.preventDefault();
                addNewInput(lastIndex, setItems, emptyItem, isItemEqual);
              }}
              className="min-w-8 p-2 h-8 ml-2 border"
            >
              Add {itemName}
            </button>
          );
        })}
      </div>
    </DashboardInput>
  );
}

export function DashboardStringMultiInput(
  props: Omit<DashboardMultiInputProps<string>, "render" | "emptyItem">
) {
  return (
    <DashboardMultiInput
      {...props}
      emptyItem=""
      isItemEqual={(x, y) => x === y}
      render={[
        {
          header: props.name,
          className: "",
          fn: ({
            name,
            item,
            index,
            updateValue,
            useFormMethods: { register },
          }) => (
            <input
              {...register(`${name}[${index}]`)}
              className="border-b-2 border-black block max-w-32 my-2 w-5/6"
              defaultValue={item}
              onChange={(e) => updateValue(e.target.value)}
            />
          ),
        },
      ]}
    />
  );
}

export default DashboardMultiInput;
