import React from "react";
import Typography from "@material-ui/core/Typography";

export type DetailItemProps = {
  title: string;
  value: string;
  className?: string;
  style?: React.CSSProperties;
};

export const DetailItem: React.FC<DetailItemProps> = ({
  title,
  value,
  className = "",
  style,
}) => {
  return (
    <div style={style} className={className + "flex flex-col gap-2"}>
      <Typography variant="h6" color="textPrimary">
        {title}
      </Typography>
      <Typography color="textSecondary">{value}</Typography>
    </div>
  );
};
