import { fetcher } from "@/api/client";
import DashboardTable, {
  TableExtraColumns,
} from "@/components/dashboard/DashboardTable";
import { IdObj } from "@/domain/models/shared";
import { usePageTitle, useQuery, useRefreshLoginSession } from "@/utils/hooks";
import { ActionCreatorWithPayload } from "@reduxjs/toolkit";
import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import useSWR, { mutate } from "swr";
import DashboardUploadModal, {
  useUploadModalState,
} from "./DashboardUploadModal";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import { canDisplayMinimumRole, RolePermission } from "@/domain/models/role";
import { useRootState } from "@/app/rootReducer";

type DashboardItemListingProps<T> = {
  headers: string[];
  remoteUrl: string;
  readableKey: keyof T;
  renderItem: (item: T) => React.ReactNode;
  deleteItem: (itemId: number | string) => Promise<number>;
  pageTitle: string;
  itemName: string;
  dashboardPath: string;
  updatable?: (item: T) => boolean;
  deletable?: (item: T) => boolean;
  items: T[];
  itemsUpdater: ActionCreatorWithPayload<T[], string>;
  extraColumns?: TableExtraColumns<T>[];
  queryParamMaker?: (qp: URLSearchParams) => URLSearchParams;
  minimumRoleReadOnly?: RolePermission;
  callback_url?: string;
};

function useUpdateItems<T>(
  itemsUpdater: ActionCreatorWithPayload<T[], string>,
  items?: T[]
) {
  const dispatch = useDispatch();
  useEffect(() => {
    if (items) {
      dispatch(itemsUpdater(items));
    }
    // eslint-disable-next-line
  }, [items]);
}

export function DashboardItemListing<T extends IdObj>({
  headers,
  remoteUrl,
  readableKey,
  renderItem,
  deleteItem,
  pageTitle,
  itemName,
  dashboardPath,
  updatable,
  deletable,
  items,
  itemsUpdater,
  extraColumns,
  queryParamMaker,
  minimumRoleReadOnly,
  callback_url,
}: DashboardItemListingProps<T>) {
  usePageTitle(itemName + " Listing");
  const { role: accountRole } = useRootState((state) => state.auth);
  const queryParams = useQuery();
  const remoteQueryParams = queryParamMaker?.(queryParams);
  const remoteUrlWithQp = remoteQueryParams
    ? remoteUrl + "?" + remoteQueryParams.toString()
    : remoteUrl;
  const { data: fetchedItems, error } = useSWR<T[] | undefined>(
    remoteUrlWithQp,
    fetcher
  );
  useRefreshLoginSession(error, [remoteUrl]);
  useUpdateItems(itemsUpdater, fetchedItems);
  const {
    uploadModalState,
    showUploadModal,
    hideUploadModal,
  } = useUploadModalState();
  const deleteValue = (itemId: number | string) => {
    deleteItem(itemId).then((deletedId) => {
      deletedId === itemId && mutate(remoteUrl);
    });
  };

  let unloadedDataComponent = null;
  if (error) {
    unloadedDataComponent = (
      <div className="text-xl text-center text-red-500 mt-10">
        {error instanceof Error ? "Failed to fetch" : error.error}
      </div>
    );
  }
  // else if (!fetchedItems) {
  // Maybe: Loading assumes items is an array of zero items at load
  /* unloadedDataComponent = (
   *   <div className="text-xl text-center mt-10">Loading...</div>
   * ); */
  // }

  return (
    <div className="p-6 w-full flex flex-col gap-4">
      {/* <Typography variant="h4">{itemName} Listing</Typography> */}
      <ButtonGroup
        variant="text"
        color="primary"
        aria-label="text primary button group"
        className="gap-2"
      >
        {canDisplayMinimumRole(minimumRoleReadOnly, accountRole) && (
          <Button
            variant="contained"
            to={`${dashboardPath}/new`}
            component={Link}
          >
            Create New {itemName}
          </Button>
        )}
        <Button
          variant="contained"
          onClick={() => {
            showUploadModal(minimumRoleReadOnly);
          }}
        >
          Batch Upload
        </Button>
      </ButtonGroup>
      {unloadedDataComponent || (
        <DashboardTable
          extraColumns={extraColumns}
          items={items!!}
          itemName={itemName}
          headers={headers}
          updatable={updatable}
          deletable={{
            predicate: deletable || ((_item) => true),
            readableKey: readableKey,
            action: (_item, _readableKey, itemId) => deleteValue(itemId),
          }}
          dashboardPath={dashboardPath}
          renderItem={renderItem}
          minimumRoleReadOnly={minimumRoleReadOnly}
        />
      )}
      {!uploadModalState.hidden && (
        <DashboardUploadModal
          name={itemName}
          hide={hideUploadModal}
          action={() => {}}
          remoteUrl={remoteUrl}
          callback_url={callback_url}
          {...uploadModalState}
        />
      )}
    </div>
  );
}

export default DashboardItemListing;
