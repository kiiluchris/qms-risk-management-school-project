import DashboardInput from "@/components/dashboard/DashboardInput";
import { RelatedItem } from "@/domain/models/shared";
import { defaultFetchedSelectValue, ReactSelectOption } from "@/utils/form";
import React from "react";
import { Controller, UseFormReturn } from "react-hook-form";
import Select from "react-select";
import CreatableSelect from "react-select/creatable";

type DashboardSelectInputProps<T> = {
  useFormMethods: UseFormReturn<Record<string, any>>;
  name: string;
  placeholder?: string;
  error?: string;
  items?: T[];
  itemRelatedToParent?: RelatedItem;
  itemToOption: (item: T) => ReactSelectOption;
  defaultItem?: T | T[];
  noDefault?: boolean;
  onChange?: (item: T | undefined, option: ReactSelectOption) => void;
  creatable?: (value: ReactSelectOption) => ReactSelectOption;
  className?: string;
  forceHalfSize?: boolean;
  hidePlaceholder?: boolean;
  itemValue?: T | T[];
  isMulti?: boolean;
};

export function DashboardSelectInput<T>({
  useFormMethods,
  name,
  placeholder,
  error,
  items,
  itemToOption,
  itemRelatedToParent,
  defaultItem,
  itemValue,
  onChange,
  forceHalfSize,
  noDefault = false,
  hidePlaceholder = false,
  isMulti = false,
  creatable,
}: DashboardSelectInputProps<T>) {
  const { control } = useFormMethods;
  const defaultValue = defaultItem
    ? Array.isArray(defaultItem)
      ? defaultItem.map(itemToOption)
      : itemToOption(defaultItem)
    : noDefault
    ? undefined
    : defaultFetchedSelectValue(items, itemToOption, itemRelatedToParent);

  return (
    <DashboardInput
      hidePlaceholder={hidePlaceholder}
      forceHalfSize={forceHalfSize}
      name={name}
      placeholder={placeholder}
      error={error}
    >
      {items === undefined ? (
        error ? (
          ""
        ) : (
          "Loading..."
        )
      ) : (
        <Controller
          name={name}
          control={control}
          defaultValue={
            typeof itemValue === "undefined" ? defaultValue : undefined
          }
          render={({ field }) => {
            const value =
              typeof itemValue === "undefined"
                ? field.value
                : Array.isArray(itemValue)
                ? itemValue.map((item) => itemToOption(item))
                : itemToOption(itemValue);
            const options = items?.map((item) => {
              return itemToOption(item);
            });
            const className = "w-11/12 max-w-32";

            return creatable ? (
              <CreatableSelect
                {...field}
                isMulti={isMulti}
                value={value}
                className={className}
                options={options}
                onChange={(option_) => {
                  if (option_ === null) return;
                  const option = Array.isArray(option_)
                    ? option_.map((o) => (o.__isNew__ ? creatable(o) : o))
                    : option_.__isNew__
                    ? creatable(option_)
                    : option_;
                  field.onChange(option);
                  if (onChange) {
                    const item = items?.find(
                      (item) => itemToOption(item).value === option.value
                    );
                    onChange(item, option);
                  }
                }}
              />
            ) : (
              <Select
                {...field}
                isMulti={isMulti}
                value={value}
                className="w-11/12 max-w-32"
                options={options}
                onChange={(option) => {
                  if (option === null) return;
                  field.onChange(option);
                  if (onChange) {
                    const item = items?.find(
                      (item) => itemToOption(item).value === option.value
                    );
                    item && onChange(item, option);
                  }
                }}
              />
            );
          }}
        />
      )}
    </DashboardInput>
  );
}

export default DashboardSelectInput;
