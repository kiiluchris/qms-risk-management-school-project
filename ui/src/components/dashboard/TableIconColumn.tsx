import React from "react";
import { Link } from "react-router-dom";

export const TableIcon: React.FC<{
  action?: (e: React.MouseEvent<HTMLAnchorElement>) => void;
  to?: string;
}> = ({ action, children, to = "#" }) => {
  return (
    <Link to={to} onClick={action}>
      <svg
        xmlns="http://www.w3.org/2000/svg"
        fill="none"
        viewBox="0 0 24 24"
        stroke="currentColor"
        width="20px"
        height="20px"
      >
        {children}
      </svg>
    </Link>
  );
};

export default TableIcon;
