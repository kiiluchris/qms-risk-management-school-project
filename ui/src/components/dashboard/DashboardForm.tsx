import DashboardSubmit from "@/components/dashboard/DashboardSubmit";
import FormInputError from "@/components/forms/FormInputError";
import { FormSubmitError, GqlError } from "@/domain/models/errors";
import { buildGqlQuery, GqlQuery } from "@/domain/models/gql";
import { IdObj } from "@/domain/models/shared";
import {
  useGqlQuery,
  usePageTitle,
  useRefreshLoginSession,
} from "@/utils/hooks";
import { zodResolver } from "@hookform/resolvers/zod";
import React, { useState } from "react";
import { DefaultValues, useForm, UseFormReturn } from "react-hook-form";
import Loader from "@/components/shared/Loader";
import { Redirect, useRouteMatch } from "react-router-dom";
import * as z from "zod";

export type FormDependencies<D> = {
  data?: D;
  error?: GqlError;
};

type DashboardCreateFormProps<
  T,
  Schema extends z.ZodObject<any, any, any>,
  Query,
  QueryItem extends IdObj
> = {
  item?: T;
  itemId?: number;
  defaultValues?: DefaultValues<z.infer<Schema>>;
  errorMessage?: string;
  error?: Error | FormSubmitError;
  pageTitle: string;
  schema: Schema;
  isEditForm?: boolean;
  submitAction: (
    formData: z.infer<Schema>,
    useFormMethods: UseFormReturn<Record<string, any>>,
    setGeneralError: React.Dispatch<React.SetStateAction<string>>,
    itemId: string | number | undefined,
    isEditForm: boolean
  ) => void;
  renderForm: (
    item: QueryItem | undefined,
    deps: Query | undefined,
    useFormMethods: UseFormReturn<Record<string, any>>,
    isEditForm: boolean
  ) => React.ReactNode;
  itemName: string;
  updatable?: (item: T) => boolean;
  dashboardPath: string;
  gqlQueries: GqlQuery<Query, QueryItem>;
};

export function DashboardCreateForm<
  T extends IdObj,
  Schema extends z.ZodObject<any, any, any>,
  Query,
  QueryItem extends IdObj
>({
  pageTitle,
  schema,
  submitAction,
  renderForm,
  errorMessage = "",
  error,
  updatable,
  dashboardPath,
  defaultValues,
  gqlQueries,
  itemId,
  isEditForm = false,
}: DashboardCreateFormProps<T, Schema, Query, QueryItem>) {
  usePageTitle(pageTitle);
  const [generalError, setGeneralError] = useState("");
  const { gqlData, gqlError, hasGqlQuery } = useGqlQuery(gqlQueries, itemId);
  let depsErrorMessage = gqlError ? "Failed to load dependencies " : "";
  useRefreshLoginSession(
    error || (gqlError?.status === 401 ? gqlError : undefined),
    [buildGqlQuery(gqlQueries, itemId)]
  );
  const useFormMethods = useForm<z.infer<Schema>>({
    resolver: zodResolver(schema),
    defaultValues: defaultValues,
  });
  if (
    typeof gqlData === "undefined" &&
    typeof gqlError === "undefined" &&
    hasGqlQuery
  ) {
    return <Loader />;
  }
  const item = gqlData ? gqlQueries?.item?.extract(gqlData) : undefined;
  if (item && updatable && !updatable((item as any) as T)) {
    return <Redirect to={`/dashboard/${dashboardPath}`} />;
  }
  return (
    <div className="w-full bg-white p-6 min-h-screen">
      <div className="w-full mx-auto">
        <h2 className="mb-5 text-3xl">{pageTitle}</h2>
        <form
          className="flex flex-col gap-2"
          onSubmit={useFormMethods.handleSubmit((data) =>
            submitAction(
              data,
              useFormMethods,
              setGeneralError,
              itemId,
              isEditForm
            )
          )}
        >
          <div className="w-full">
            <FormInputError
              message={
                generalError || errorMessage || depsErrorMessage || undefined
              }
            />
          </div>
          {renderForm(item, gqlData, useFormMethods, isEditForm)}

          <DashboardSubmit label={pageTitle} />
        </form>
      </div>
    </div>
  );
}

type DashboardUpdateFormProps<
  T,
  Schema extends z.ZodObject<any, any, any>,
  Query,
  QueryItem extends IdObj
> = Omit<
  DashboardCreateFormProps<T, Schema, Query, QueryItem>,
  "item" | "isEditForm"
> & {
  remoteUrl: string;
};

export function DashboardUpdateForm<
  T extends IdObj,
  Schema extends z.ZodObject<any, any, any>,
  Query,
  QueryItem extends IdObj
>({
  remoteUrl,
  ...props
}: DashboardUpdateFormProps<T, Schema, Query, QueryItem>) {
  type RouteParams = { item_id: string };
  const { params } = useRouteMatch<RouteParams>();
  return (
    <DashboardCreateForm
      {...props}
      itemId={+params.item_id}
      isEditForm={true}
    />
  );
}
