type Props = {
  label: string;
};

export default function DashboardSubmit({ label }: Props) {
  return (
    <input
      type="submit"
      className="text-xl laatu-bg-5 w-1/2 mx-auto rounded my-4 py-2 text-white"
      value={label}
    />
  );
}
