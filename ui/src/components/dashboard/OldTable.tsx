import TableIcon from "@/components/dashboard/TableIconColumn";
import { IdObj, PaginationOptions } from "@/domain/models/shared";
import React from "react";
import DeleteIcon from "@material-ui/icons/Delete";
import { useConfirm } from "material-ui-confirm";
import red from "@material-ui/core/colors/red";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";

export type TableFilters<T> = ((item: T) => boolean)[];

export type TableExtraColumns<T> = {
  header: string;
  component: (item: T) => React.ReactNode;
};

type TableProps<T> = {
  dashboardPath: string;
  items: T[];
  itemName: string;
  headers: string[];
  renderItem: (item: T) => React.ReactNode;
  updatable?: (item: T) => boolean;
  deletable?: {
    predicate: (item: T) => boolean;
    readableKey: keyof T;
    action: (item: T, readableKey: keyof T, itemId: number | string) => void;
  };
  paginate?: PaginationOptions;
  filters?: TableFilters<T>;
  extraColumns?: TableExtraColumns<T>[];
};

const paginateItems = <T,>(
  { pageSize, currentPage }: PaginationOptions,
  items: T[]
): T[] => {
  const offset = currentPage * pageSize;
  console.log("Page", {
    pageSize,
    currentPage,
    offset,
    finalPage: offset * currentPage + pageSize,
  });
  return items.slice(offset, offset + pageSize);
};

const filterItems = <T,>(filters: TableFilters<T>, items: T[]) => {
  return items.filter((item) =>
    filters.every((filter) => {
      return filter(item);
    })
  );
};

const applyIfExists = <T, A>(
  items: T[],
  arg: A | undefined,
  fn: (items: T[]) => T[]
): T[] => {
  return arg ? fn(items) : items;
};

const tableRowBackground = (idx: number): string => {
  return idx % 2 !== 0 ? "bg-gray-100" : "bg-white";
};
export default function DashboardTable<T extends IdObj>({
  headers,
  items,
  itemName,
  renderItem,
  updatable,
  deletable,
  dashboardPath,
  paginate,
  filters,
  extraColumns,
}: TableProps<T>) {
  const confirm = useConfirm();
  const renderedItems = applyIfExists(
    applyIfExists(items, filters, (items) => filterItems(filters!!, items)),
    paginate,
    (items) => paginateItems(paginate!!, items)
  );
  return (
    <table
      className="mt-4 table-auto w-full text-lg border-2 border-gray-100 text-center"
      cellPadding="5px"
    >
      <thead className="laatu-bg-1 text-white">
        <tr className="border-b-2 border-black">
          {headers.map((header, i) => {
            return (
              <td key={header + i.toString()} className="font-bold">
                {header}
              </td>
            );
          })}
          <td></td>
          <td></td>
          {extraColumns?.map((c) => (
            <td key={`extra-column-header-${c.header}`}>{c.header}</td>
          ))}
        </tr>
      </thead>
      <tbody>
        {renderedItems.map((item, i) => {
          return (
            <tr className={"border-b " + tableRowBackground(i)} key={item.id}>
              {renderItem(item)}
              <td>
                {(typeof updatable !== "function" || updatable(item)) && (
                  <TableIcon to={`/${dashboardPath}/${item.id}/edit`}>
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth={2}
                      d="M11 5H6a2 2 0 00-2 2v11a2 2 0 002 2h11a2 2 0 002-2v-5m-1.414-9.414a2 2 0 112.828 2.828L11.828 15H9v-2.828l8.586-8.586z"
                    />
                  </TableIcon>
                )}
              </td>
              <td>
                {deletable?.predicate(item) && (
                  <DeleteIcon
                    style={{ color: red[600] }}
                    onClick={() => {
                      confirm({
                        description: `Are you sure you want to delete the ${itemName} "${
                          item[deletable!!.readableKey]
                        }"?
                          `,
                      }).then(() => {
                        deletable!!.action(
                          item,
                          deletable!!.readableKey,
                          item.id
                        );
                      });
                    }}
                  />
                  /*<TableIcon
                         action={() =>
                         deletable!!.action(item, deletable!!.readableKey)
                         }
                         >
                         <path
                         strokeLinecap="round"
                         strokeLinejoin="round"
                         strokeWidth={2}
                         d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"
                         />
                         </TableIcon>*/
                )}
              </td>
              {extraColumns?.map((c, i) => (
                <React.Fragment key={`extra-column-${i}-${item.id}`}>
                  {c.component(item)}
                </React.Fragment>
              ))}
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}
