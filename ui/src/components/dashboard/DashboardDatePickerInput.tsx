import DashboardInput from "@/components/dashboard/DashboardInput";
import { jsDateParsers } from "@/utils/time";
import React from "react";
import DatePicker, { ReactDatePickerProps } from "react-datepicker";
import { Controller, UseFormReturn } from "react-hook-form";

type InputTypes = Date | string;

type DashboardDatePickerInputProps = {
  useFormMethods: UseFormReturn<Record<string, any>>;
  initialDate?: InputTypes;
  name: string;
  placeholder?: string;
  error?: string;
  hidePlaceholder?: boolean;
  onChange?: (date: Date | [Date, Date] | null) => void;
} & Omit<ReactDatePickerProps, "onChange">;

export function DashboardDatePickerInput({
  useFormMethods,
  name,
  placeholder,
  initialDate: initialDate_,
  error,
  hidePlaceholder = false,
  onChange,
  minDate,
  maxDate,
  startDate,
}: DashboardDatePickerInputProps) {
  const { control } = useFormMethods;
  const initialDate =
    (typeof initialDate_ === "string"
      ? jsDateParsers.fromIso(initialDate_)
      : initialDate_) || new Date();
  return (
    <DashboardInput
      name={name}
      placeholder={placeholder}
      error={error}
      hidePlaceholder={hidePlaceholder}
    >
      <Controller
        name={name}
        control={control}
        defaultValue={initialDate}
        render={({ field }) => (
          <DatePicker
            {...field}
            className="my-2 w-11/12 max-w-32 border-b-2 border-black"
            selected={field.value}
            minDate={minDate}
            maxDate={maxDate}
            startDate={startDate}
            onChange={(date) => {
              field.onChange(date);
              onChange?.(date);
            }}
          />
        )}
      />
    </DashboardInput>
  );
}

export default DashboardDatePickerInput;
