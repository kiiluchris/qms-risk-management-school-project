import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { AuthFacade } from "@/domain/ports/AuthFacade";
import { usePopper } from "react-popper";
import { clearLoginSession } from "@/storage/authSlice";

type DashboardHeaderProps = {
  email: string;
  organization: string;
  authService: AuthFacade;
};

export const DashboardHeader: React.FC<DashboardHeaderProps> = ({
  email,
  organization,
  authService,
}) => {
  const dispatch = useDispatch();
  const [hide, setHide] = useState(true);
  const [
    referenceElement,
    setReferenceElement,
  ] = useState<HTMLButtonElement | null>(null);
  const [popperElement, setPopperElement] = useState<HTMLDivElement | null>(
    null
  );
  const [arrowElement, setArrowElement] = useState<HTMLDivElement | null>(null);
  const { styles, attributes } = usePopper(referenceElement, popperElement, {
    modifiers: [{ name: "arrow", options: { element: arrowElement } }],
    placement: "left",
  });
  const tooltipClass = hide ? " hidden" : "";
  return (
    <div className="bg-white relative right-8 top-2 float-right">
      <button
        type="button"
        className="w-8"
        ref={setReferenceElement}
        onClick={() => setHide(!hide)}
      >
        <svg
          xmlns="http://www.w3.org/2000/svg"
          fill="none"
          viewBox="0 0 24 24"
          stroke="currentColor"
          className="cursor-pointer"
        >
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth={2}
            d="M5.121 17.804A13.937 13.937 0 0112 16c2.5 0 4.847.655 6.879 1.804M15 10a3 3 0 11-6 0 3 3 0 016 0zm6 2a9 9 0 11-18 0 9 9 0 0118 0z"
          />
        </svg>
      </button>
      <div
        id="tooltip"
        style={styles.popper}
        ref={setPopperElement}
        {...attributes.popper}
        className={
          "font-xl px-8 py-4 rounded-lg bg-white inline shadow" + tooltipClass
        }
      >
        <ul>
          <li>
            <b>{organization}</b>
            <p>{email}</p>
          </li>
          <li className="my-2">
            <hr />
          </li>
          <li>
            <button
              className="text-center"
              onClick={() => dispatch(clearLoginSession(authService))}
            >
              Logout
            </button>
          </li>
        </ul>
        <div ref={setArrowElement} style={styles.arrow} />
      </div>
    </div>
  );
};

export default DashboardHeader;
