import { buildGqlQuery, GqlQuery } from "@/domain/models/gql";
import { IdObj } from "@/domain/models/shared";
import {
  useGqlQuery,
  usePageTitle,
  useRefreshLoginSession,
} from "@/utils/hooks";
import Container from "@material-ui/core/Container";
import React, { useState } from "react";
import Loader from "@/components/shared/Loader";
import { Redirect, useRouteMatch } from "react-router-dom";
import { RolePermission } from "@/domain/models/role";

export type ItemViewProps<Query, QueryItem extends IdObj> = {
  gqlQueries: GqlQuery<Query, QueryItem>;
  pageTitle: string;
  itemName: string;
  dashboardPath: string;
  children?: React.ReactNode;
  render: (
    item: QueryItem,
    deps: Query,
    minimumRoleReadOnly?: RolePermission
  ) => React.ReactNode;
  minimumRoleReadOnly?: RolePermission;
};

export function ItemView<Query, QueryItem extends IdObj>(
  props: ItemViewProps<Query, QueryItem>
) {
  type RouteParams = { item_id: string };
  const { params } = useRouteMatch<RouteParams>();
  const itemId = +params.item_id;

  const {
    render,
    gqlQueries,
    pageTitle,
    itemName,
    dashboardPath,
    minimumRoleReadOnly,
  } = props;
  usePageTitle(pageTitle);
  const [generalError, setGeneralError] = useState("");
  const { gqlData, gqlError, hasGqlQuery } = useGqlQuery(gqlQueries, itemId);
  let depsErrorMessage = gqlError ? "Failed to load  dependencies " : "";
  useRefreshLoginSession(gqlError?.status === 401 ? gqlError : undefined, [
    buildGqlQuery(gqlQueries, itemId),
  ]);
  if (
    typeof gqlData === "undefined" &&
    typeof gqlError === "undefined" &&
    hasGqlQuery
  ) {
    return <Loader />;
  }
  const item = gqlData ? gqlQueries?.item?.extract(gqlData) : undefined;
  if (!item) {
    return <Redirect to={`/${dashboardPath}`} />;
  }
  console.log(item);
  return (
    <div className="w-full p-6  min-h-screen">
      <div className="w-full mx-auto"></div>
      <Container>
        <>{render(item, gqlData as Query, minimumRoleReadOnly)}</>
      </Container>
    </div>
  );
}

export default ItemView;
