import React from "react";
import FormInputError from "@/components/forms/FormInputError";
import { UseFormRegister } from "react-hook-form";

type DashboardInputProps = {
  type?: string;
  autocomplete?: string;
  placeholder?: string;
  disabled?: boolean;
  name: string;
  register?: UseFormRegister<Record<string, any>>;
  onChange?: React.ChangeEventHandler<HTMLInputElement>;
  error?: string;
  forceHalfSize?: boolean;
  hidePlaceholder?: boolean;
} & React.DetailedHTMLProps<
  React.InputHTMLAttributes<HTMLInputElement>,
  HTMLInputElement
>;

const wrapHiddenClassName = (hidden: boolean, className: string): string => {
  return hidden ? "" : className;
};

const DashboardInput: React.FC<DashboardInputProps> = ({
  placeholder,
  type: type_,
  name,
  error,
  register,
  value,
  defaultValue,
  children,
  forceHalfSize = false,
  className: className_,
  hidePlaceholder = false,
  onChange,
}) => {
  const hidden = type_ === "hidden";
  let className = "border-b-2 border-black block max-w-32 my-2";
  if (type_ !== "checkbox") {
    className += " w-11/12";
  }
  if (className_) {
    className += " " + className_;
  }
  const placeholder_ =
    placeholder ||
    name
      .split("_")
      .map((ch) => ch[0].toUpperCase() + ch.slice(1))
      .join(" ");
  const props = register ? register(name) : { name };

  return (
    <div
      className={wrapHiddenClassName(
        hidden,
        "w-full" + (forceHalfSize ? " md:w-1/2" : "")
      )}
    >
      {!hidePlaceholder && (
        <label htmlFor={name} className="text-bold text-xl block">
          {placeholder_}
        </label>
      )}
      {children || (
        <input
          type={type_}
          {...props}
          defaultValue={defaultValue}
          value={value}
          className={wrapHiddenClassName(hidden, className)}
          onChange={onChange}
        />
      )}
      <FormInputError message={error} />
    </div>
  );
};

export default DashboardInput;
