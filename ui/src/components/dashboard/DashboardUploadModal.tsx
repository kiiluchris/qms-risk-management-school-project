import { fetcher, fileFetcher } from "@/api/client";
import { Transition } from "@headlessui/react";
import React, { useState } from "react";
import { mutate } from "swr";
import {
  DropzoneArea,
  DropzoneDialogBase,
  FileObject,
} from "material-ui-dropzone";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import { canDisplayMinimumRole, RolePermission } from "@/domain/models/role";
import { useRootState } from "@/app/rootReducer";

export function useUploadModalState() {
  const initialUploadModalState = {
    hidden: true,
    minimumRoleReadOnly: undefined as undefined | RolePermission,
  };
  const [uploadModalState, setUploadModalState] = useState(
    initialUploadModalState
  );
  const hideUploadModal = () => setUploadModalState(initialUploadModalState);
  const showUploadModal = (minimumRoleReadOnly?: RolePermission) =>
    setUploadModalState({
      hidden: false,
      minimumRoleReadOnly,
    });

  return { showUploadModal, hideUploadModal, uploadModalState };
}
const downloadSample = (
  remoteUrl: string
): React.MouseEventHandler<HTMLButtonElement> => (e) => {
  e.preventDefault();
  fileFetcher("/sample-file" + remoteUrl + "-sample.xlsx")
    .then((r) => r.blob())
    .then((blob) => {
      const url = window.URL.createObjectURL(blob);
      const a = document.createElement("a");
      a.style.display = "none";
      a.href = url;
      a.download = remoteUrl.slice(1) + ".xlsx";
      document.body.appendChild(a);
      a.click();
      window.URL.revokeObjectURL(url);
    });
};
type UploadProps = {
  selectedFile?: File;
  remoteUrl: string;
  setUploadError: React.Dispatch<React.SetStateAction<string>>;
  setSuccessfulUpload: React.Dispatch<React.SetStateAction<true | undefined>>;
  callback_url?: string;
};
const uploadFile = ({
  selectedFile,
  setSuccessfulUpload,
  setUploadError,
  remoteUrl,
  callback_url,
}: UploadProps) => () => {
  if (!selectedFile) {
    setUploadError("You must select a file before you upload");
    return;
  }
  const formData = new FormData();

  if (callback_url) {
    formData.append("callback_url", callback_url);
  }
  formData.append("file", selectedFile, selectedFile.name);
  formData.append("submit", "submit");
  console.log(formData);
  fetcher(
    remoteUrl + "/import",
    {
      body: formData,
      method: "POST",
    },
    true
  )
    .then((r) => {
      setUploadError("");
      setSuccessfulUpload(true);
      mutate(remoteUrl);
    })
    .catch((e) => {
      console.log(e);
      setSuccessfulUpload(undefined);
      setUploadError(`Unsuccessful file upload: Badly formatted file given`);
    });
};

const dialogTitle = ({
  name,
  hide,
  successfulUpload,
  remoteUrl,
  uploadError,
  canUpload,
}: {
  name: string;
  hide: () => void;
  successfulUpload?: boolean;
  uploadError?: string;
  remoteUrl: string;
  canUpload: boolean;
}) => (
  <>
    <span>{name} Uploads</span>
    {/* <p className="text-sm text-gray-500 text-center">
        You may download a sample file showing the format used or upload your own
        excel file
        </p> */}
    {successfulUpload && (
      <p className="text-sm text-green-500">Successful upload</p>
    )}
    {uploadError && <p className="text-sm text-red-500">{uploadError}</p>}
    {!canUpload && (
      <p className="text-sm text-red-500">
        Your Account Does not have upload permissions
      </p>
    )}
    {/* <div className="text-center">
        <button
        onClick={downloadSample(remoteUrl)}
        className="text-blue-400 outline-none focus:outline-none background-transparent font-bold py-1 px-3"
        >
        Download Sample
        </button>
        </div>
        <p className="text-center text-gray-500">or</p> */}
    <IconButton
      style={{ right: "12px", top: "8px", position: "absolute" }}
      onClick={() => hide()}
    >
      <CloseIcon />
    </IconButton>
  </>
);
export const DashboardUploadModal: React.FC<{
  hidden: boolean;
  name: string;
  remoteUrl: string;
  hide: () => void;
  action: () => void;
  minimumRoleReadOnly?: RolePermission;
  callback_url?: string;
}> = ({
  hidden: isHidden,
  name,
  hide,
  action,
  remoteUrl,
  minimumRoleReadOnly,
  callback_url,
}) => {
  const { role: accountRole } = useRootState((state) => state.auth);
  const [selectedFile, setSelectedFile] = useState<File | undefined>();
  const [successfulUpload, setSuccessfulUpload] = useState<true | undefined>();
  const [uploadError, setUploadError] = useState("");
  const [fileObjects, setFileObjects] = useState([] as FileObject[]);
  const changeFile = (files: File[]) => {
    setSelectedFile(files![0]);
  };
  const changeFileEventHandler = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    changeFile((event.target.files! as any) as File[]);
  };
  const canUpload = canDisplayMinimumRole(minimumRoleReadOnly, accountRole);

  return (
    <DropzoneDialogBase
      dialogTitle={dialogTitle({
        name,
        hide,
        successfulUpload,
        remoteUrl,
        uploadError,
        canUpload,
      })}
      acceptedFiles={[
        "application/vnd.oasis.opendocument.spreadsheet",
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
      ]}
      filesLimit={canUpload ? 1 : 0}
      cancelButtonText={"cancel"}
      submitButtonText={"upload"}
      fileObjects={fileObjects}
      maxFileSize={5000000}
      open={!isHidden}
      onAdd={(newFileObjs) => {
        setFileObjects(newFileObjs);
      }}
      onDelete={(deleteFileObj) => {
        setFileObjects([]);
      }}
      onClose={() => hide()}
      onSave={() => {
        uploadFile({
          remoteUrl,
          setUploadError,
          setSuccessfulUpload,
          selectedFile: fileObjects?.[0]?.file,
          callback_url,
        })();
      }}
    />
  );

  return (
    <div
      className="fixed z-10 inset-0 w-full h-full overflow-y-auto grid justify-center content-center"
      //onClick={() => hide()}
    >
      <div>
        <div className="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
          <Transition
            show={!isHidden}
            enter="ease-out duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <div
              className="fixed inset-0 transition-opacity"
              aria-hidden={isHidden}
            >
              <div className="absolute inset-0 bg-gray-500 opacity-75"></div>
            </div>
          </Transition>

          <span
            className="hidden sm:inline-block sm:align-middle sm:h-screen"
            aria-hidden={isHidden}
          >
            &#8203;
          </span>
          <Transition
            show={!isHidden}
            enter="ease-out duration-300"
            enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
            enterTo="opacity-100 translate-y-0 sm:scale-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100 translate-y-0 sm:scale-100"
            leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
          >
            <div
              className="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full"
              role="dialog"
              aria-modal="true"
              aria-labelledby="modal-headline"
            >
              <div className="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                <div className="sm:flex sm:items-start">
                  <div className="mt-3 text-center sm:mt-0 sm:ml-4 sm:text-left">
                    <h3
                      className="text-lg leading-6 font-medium text-gray-900"
                      id="modal-headline"
                    >
                      {name} Uploads
                    </h3>
                    <div className="mt-2">
                      {uploadError && (
                        <p className="text-sm text-red-500">{uploadError}</p>
                      )}
                      <p className="text-sm text-gray-500">
                        You may download a sample file showing the format used
                        or upload your own excel file
                      </p>
                      {successfulUpload && (
                        <p className="text-sm text-green-500">
                          Successful upload
                        </p>
                      )}
                      <div className="flex flex-col items-center">
                        <div>
                          <button
                            onClick={downloadSample(remoteUrl)}
                            className="text-blue-400 outline-none focus:outline-none background-transparent font-bold py-1 px-3"
                          >
                            Download Sample
                          </button>
                        </div>
                        <p>or</p>
                        <div className="flex flex-col items-center">
                          <DropzoneArea onChange={changeFile} />
                          <input
                            type="file"
                            onChange={changeFileEventHandler}
                          />
                          <button
                            className="outline-none focus:outline-none laatu-bg-4 py-2 px-4 mt-4 text-white font-bold rounded"
                            onClick={uploadFile({
                              remoteUrl,
                              setSuccessfulUpload,
                              setUploadError,
                              selectedFile,
                            })}
                          >
                            Upload File
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
                <button
                  type="button"
                  className="mt-3 w-full inline-flex justify-center rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-base font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:mt-0 sm:ml-3 sm:w-auto sm:text-sm"
                  onClick={() => hide()}
                >
                  Cancel
                </button>
              </div>
            </div>
          </Transition>
        </div>
      </div>
    </div>
  );
};

export default DashboardUploadModal;
