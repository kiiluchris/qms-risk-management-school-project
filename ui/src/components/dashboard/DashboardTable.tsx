import { useRootState } from "@/app/rootReducer";
import TableIcon from "@/components/dashboard/TableIconColumn";
import { canDisplayMinimumRole, RolePermission } from "@/domain/models/role";
import { IdObj, PaginationOptions } from "@/domain/models/shared";
import red from "@material-ui/core/colors/red";
import IconButton from "@material-ui/core/IconButton";
import Paper from "@material-ui/core/Paper";
import {
  createStyles,
  lighten,
  makeStyles,
  Theme,
} from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Toolbar from "@material-ui/core/Toolbar";
import Tooltip from "@material-ui/core/Tooltip";
import Typography from "@material-ui/core/Typography";
import DeleteIcon from "@material-ui/icons/Delete";
import clsx from "clsx";
import { useConfirm } from "material-ui-confirm";
import Pagination from "material-ui-flat-pagination";
import React, { useState } from "react";

export type TableFilters<T> = ((item: T) => boolean)[];

export type TableExtraColumns<T> = {
  header: string;
  component: (item: T) => React.ReactNode;
};

type TableProps<T> = {
  dashboardPath: string;
  items: T[];
  itemName: string;
  headers: string[];
  renderItem: (item: T) => React.ReactNode;
  updatable?: (item: T) => boolean;
  deletable?: {
    predicate: (item: T) => boolean;
    readableKey: keyof T;
    action: (item: T, readableKey: keyof T, itemId: number | string) => void;
  };
  filters?: TableFilters<T>;
  extraColumns?: TableExtraColumns<T>[];
  minimumRoleReadOnly?: RolePermission;
};

const paginateItems = <T,>(
  { pageSize, currentPage }: PaginationOptions,
  items: T[]
): T[] => {
  const offset = currentPage * pageSize;
  return items.slice(offset, offset + pageSize);
};

const filterItems = <T,>(filters: TableFilters<T>, items: T[]) => {
  return items.filter((item) =>
    filters.every((filter) => {
      return filter(item);
    })
  );
};

const applyIfExists = <T, A>(
  items: T[],
  arg: A | undefined,
  fn: (items: T[]) => T[]
): T[] => {
  return arg ? fn(items) : items;
};

const tableRowBackground = (idx: number): string => {
  return idx % 2 !== 0 ? "bg-gray-100" : "bg-white";
};
const useToolbarStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      paddingLeft: theme.spacing(2),
      paddingRight: theme.spacing(1),
    },
    highlight:
      theme.palette.type === "light"
        ? {
            color: theme.palette.secondary.main,
            backgroundColor: lighten(theme.palette.secondary.light, 0.85),
          }
        : {
            color: theme.palette.text.primary,
            backgroundColor: theme.palette.secondary.dark,
          },
    title: {
      flex: "1 1 100%",
    },
  })
);
interface EnhancedTableToolbarProps {
  numSelected: number;
  itemName: string;
}

const EnhancedTableToolbar = (props: EnhancedTableToolbarProps) => {
  const classes = useToolbarStyles();
  const { numSelected, itemName } = props;

  return (
    <Toolbar
      className={clsx(classes.root, {
        [classes.highlight]: numSelected > 0,
      })}
    >
      {numSelected > 0 ? (
        <Typography
          className={classes.title}
          color="inherit"
          variant="subtitle1"
          component="div"
        >
          {numSelected} selected
        </Typography>
      ) : (
        <Typography
          className={classes.title}
          variant="h6"
          id="tableTitle"
          component="div"
        ></Typography>
      )}
      {numSelected > 0 && (
        <Tooltip title="Delete">
          <IconButton aria-label="delete">
            <DeleteIcon style={{ color: red[600] }} />
          </IconButton>
        </Tooltip>
      )}
    </Toolbar>
  );
};
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: "100%",
    },
    paper: {
      width: "100%",
      marginBottom: theme.spacing(2),
    },
    table: {
      minWidth: 750,
    },
    visuallyHidden: {
      border: 0,
      clip: "rect(0 0 0 0)",
      height: 1,
      margin: -1,
      overflow: "hidden",
      padding: 0,
      position: "absolute",
      top: 20,
      width: 1,
    },
  })
);
export default function DashboardTable<T extends IdObj>({
  headers,
  items,
  itemName,
  renderItem,
  updatable,
  deletable,
  dashboardPath,
  filters,
  extraColumns,
  minimumRoleReadOnly,
}: TableProps<T>) {
  const [currentPage, setCurrentPage] = useState(0);
  const { pageSize, accountRole } = useRootState((state) => ({
    pageSize: state.dashboard.pagination.pageSize,
    accountRole: state.auth.role,
  }));
  const paginate = { currentPage, pageSize };
  const confirm = useConfirm();
  const classes = useStyles();
  const [selected, setSelected] = React.useState<(string | number)[]>([]);
  const renderedItems = applyIfExists(
    applyIfExists(items, filters, (items) => filterItems(filters!!, items)),
    paginate,
    (items) => paginateItems(paginate, items)
  );
  const rowCount = items.length;
  const emptyRows =
    pageSize - Math.min(pageSize, rowCount - currentPage * pageSize);
  const numSelected = selected.length;
  const handleSelectAllClick = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (event.target.checked) {
      const newSelecteds = items.map((n) => n.id);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleClick = (
    event: React.MouseEvent<unknown>,
    name: string | number
  ) => {
    const selectedIndex = selected.indexOf(name);
    let newSelected: (string | number)[] = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }

    setSelected(newSelected);
  };
  const isSelected = (name: string | number) => selected.indexOf(name) !== -1;

  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        {/* <EnhancedTableToolbar numSelected={numSelected} itemName={itemName} /> */}
        <TableContainer>
          <Table className={classes.table} aria-label="simple table">
            <TableHead>
              <TableRow>
                {/* <TableCell padding="checkbox">
                 *   <Checkbox
                 *     indeterminate={numSelected > 0 && numSelected < rowCount}
                 *     checked={rowCount > 0 && numSelected === rowCount}
                 *     onChange={handleSelectAllClick}
                 *     inputProps={{ "aria-label": "select all rows" }}
                 *   />
                 * </TableCell> */}
                {headers.map((header, i) => {
                  return (
                    <TableCell
                      key={header + i.toString()}
                      className="font-bold"
                    >
                      {header}
                    </TableCell>
                  );
                })}

                {canDisplayMinimumRole(minimumRoleReadOnly, accountRole) && (
                  <>
                    <TableCell></TableCell>
                    <TableCell></TableCell>
                  </>
                )}
                {extraColumns?.map((c) => (
                  <TableCell key={`extra-column-header-${c.header}`}>
                    {c.header}
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {renderedItems.map((item, i) => {
                const isItemSelected = isSelected(item.id);
                return (
                  <TableRow
                    key={item.id}
                    selected={isItemSelected}
                    tabIndex={-1}
                    aria-checked={isItemSelected}
                    role="checkbox"
                    hover
                  >
                    {/* <TableCell padding="checkbox">
                        <Checkbox
                        onClick={(event) => handleClick(event, item.id)}
                        checked={isItemSelected}
                        inputProps={{ "aria-labelledby": `row-checkbox-${i}` }}
                        />
                        </TableCell> */}
                    {renderItem(item)}
                    {canDisplayMinimumRole(
                      minimumRoleReadOnly,
                      accountRole
                    ) && (
                      <>
                        <TableCell>
                          {(typeof updatable !== "function" ||
                            updatable(item)) && (
                            <TableIcon to={`/${dashboardPath}/${item.id}/edit`}>
                              <path
                                strokeLinecap="round"
                                strokeLinejoin="round"
                                strokeWidth={2}
                                d="M11 5H6a2 2 0 00-2 2v11a2 2 0 002 2h11a2 2 0 002-2v-5m-1.414-9.414a2 2 0 112.828 2.828L11.828 15H9v-2.828l8.586-8.586z"
                              />
                            </TableIcon>
                          )}
                        </TableCell>
                        <TableCell>
                          {deletable?.predicate(item) && (
                            <DeleteIcon
                              style={{ color: red[600] }}
                              onClick={() => {
                                confirm({
                                  description: `Are you sure you want to delete the ${itemName} "${
                                    item[deletable!!.readableKey]
                                  }"?
                          `,
                                }).then(() => {
                                  deletable!!.action(
                                    item,
                                    deletable!!.readableKey,
                                    item.id
                                  );
                                });
                              }}
                            />
                          )}
                        </TableCell>
                      </>
                    )}
                    {extraColumns?.map((c, i) => (
                      <React.Fragment key={`extra-column-${i}-${item.id}`}>
                        {c.component(item)}
                      </React.Fragment>
                    ))}
                  </TableRow>
                );
              })}
              {/* {emptyRows > 0 && (
                  <TableRow style={{ height: 53 * emptyRows }}>
                  <TableCell colSpan={headers.length + 2} />
                  </TableRow>
                  )} */}
            </TableBody>
          </Table>
        </TableContainer>
        <Pagination
          limit={pageSize}
          offset={currentPage * pageSize}
          total={(items || []).length}
          onClick={(e, nextOffset) => {
            const nextPage = Math.floor(nextOffset / pageSize);
            setCurrentPage(nextPage);
          }}
          classes={{
            root: "flex mx-auto my-4 justify-center",
          }}
        />
      </Paper>
    </div>
  );
}
