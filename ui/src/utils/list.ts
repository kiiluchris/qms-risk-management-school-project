export function filterMap<X, Y>(xs: X[], fn: (x: X) => Y | null): Y[] {
  const ys = [];
  for (const x of xs) {
    const y = fn(x);
    if (y !== null) {
      ys.push(y);
    }
  }

  return ys;
}

export function range(start: number, end?: number) {
  const result = [];
  let from_: number, to_: number;
  if (typeof end === "undefined") {
    from_ = 0;
    to_ = start;
  } else {
    from_ = start;
    to_ = end;
  }
  for (; from_ < to_; from_++) {
    result.push(from_);
  }
  return result;
}

export function partition<T>(
  predicate: (x: T) => boolean,
  xs: T[]
): [T[], T[]] {
  const [matching, notMatching]: [T[], T[]] = [[], []];
  for (const x of xs) {
    if (predicate(x)) {
      matching.push(x);
    } else {
      notMatching.push(x);
    }
  }
  return [matching, notMatching];
}
