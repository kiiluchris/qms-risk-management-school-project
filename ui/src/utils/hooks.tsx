import Alert, { Color } from "@material-ui/lab/Alert";
import Snackbar from "@material-ui/core/Snackbar";
import ApiContext from "@/api/api";
import { gqlFetcher } from "@/api/client";
import { FormSubmitError } from "@/domain/models/errors";
import { buildGqlQuery, GqlQuery } from "@/domain/models/gql";
import { refreshAccessToken } from "@/storage/authSlice";
import { setPageTitle } from "@/storage/dashboardSlice";
import { useContext, useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useLocation } from "react-router-dom";
import useSWR from "swr";

export const useToggle = (
  initial: boolean
): [boolean, () => void, () => void] => {
  const [value, setValue] = useState(initial);
  const toggle = (v: boolean) => () => setValue(v);
  return [value, toggle(true), toggle(false)];
};

export function useQuery() {
  return new URLSearchParams(useLocation().search);
}

export function usePageTitle(title: string) {
  const dispatch = useDispatch();
  useEffect(() => {
    document.title = title;
    dispatch(setPageTitle(title));
    // eslint-disable-next-line
  }, []);
}

export function useRefreshLoginSession(
  error: Error | FormSubmitError | undefined,
  urlsToRefresh: string[] = []
) {
  const dispatch = useDispatch();
  const { authService } = useContext(ApiContext);
  useEffect(() => {
    console.log("Refreshing Token");
    if (!(error instanceof Error) && error?.status === 401) {
      dispatch(refreshAccessToken(authService, error.messages, urlsToRefresh));
    }
    // eslint-disable-next-line
  }, [error]);
}

type GqlError = {
  message: string;
  path: string[];
};

type GqlErrorObj = {
  [key: string]: GqlErrorObj | string;
};

function mergeGqlError(...objects: GqlErrorObj[]) {
  const isObject = (obj: GqlErrorObj | string): obj is GqlErrorObj =>
    typeof obj === "object";
  return objects.reduce((prev, next) => {
    Object.keys(next).forEach((key) => {
      const pVal = prev[key];
      const nVal = next[key];
      if (isObject(pVal) && isObject(nVal)) {
        prev[key] = mergeGqlError(pVal, nVal);
      } else {
        prev[key] = nVal;
      }
    });
    return prev;
  }, {});
}

export function useGqlQuery<T, V>(
  query: GqlQuery<T, V>,
  itemId?: number | undefined
) {
  const queryStr = query ? buildGqlQuery(query, itemId) : "";
  const [state, setState] = useState<{ data?: T; error?: any }>({});
  /* const {} = useSWR(que);
   * useEffect(() => {
   *   queryStr
   *     ? gqlFetcher(queryStr)
   *         .then((d) => {
   *           setState({ data: d });
   *         })
   *         .catch((e) => {
   *           setState({ error: e });
   *         })
   *     : Promise.resolve(undefined);
   * }, [queryStr]); */
  /* const { data, error } = state; */
  const { data, error } = useSWR<T>(queryStr, (q) =>
    queryStr ? gqlFetcher(q) : Promise.resolve(undefined)
  );
  let gqlData = data,
    gqlError = error;
  if (error) {
    console.error("GQL: Error", error);
  }
  if (error?.status === 200) {
    gqlData = error.data;
    gqlError = (error.errors as GqlError[]).reduce((acc, err) => {
      return mergeGqlError(
        acc,
        err.path.reduceRight((acc, path) => {
          return {
            [path]: acc,
          };
        }, err.message as any) as GqlErrorObj
      );
    }, {} as GqlErrorObj);
  }

  return {
    gqlData: gqlData,
    gqlError: gqlError,
    hasGqlQuery: !!queryStr,
  };
}

export const useSnackbar = (hideDuration: number = 6000) => {
  const [snackbar, setSnackbarState] = useState<{
    open: boolean;
    message: string;
    severity: Color;
  }>({
    open: false,
    severity: "info",
    message: "",
  });
  const handleClose = () => {
    setSnackbarState({ open: false, severity: "info", message: "" });
  };
  const MySnackbar = () => (
    <Snackbar
      open={snackbar.open}
      autoHideDuration={hideDuration}
      onClose={handleClose}
    >
      <Alert onClose={handleClose} severity={snackbar.severity}>
        {snackbar.message}
      </Alert>
    </Snackbar>
  );

  return {
    setSnackbarError: (message: string) =>
      setSnackbarState({
        message,
        severity: "error",
        open: true,
      }),
    setSnackbarSuccess: (message: string) =>
      setSnackbarState({
        message,
        severity: "success",
        open: true,
      }),
    closeSnackbar: handleClose,
    Snackbar: MySnackbar,
  };
};
