import parseISO from "date-fns/parseISO";
import formatISO from "date-fns/formatISO";
import format from "date-fns/format";
import parse from "date-fns/parse";

export const getCurrentTimezone = () => {
  return Intl.DateTimeFormat().resolvedOptions().timeZone;
};

export const jsDateFormatters = {
  toIsoDateTime(date: Date) {
    return formatISO(date);
  },
  toIsoDate(date: Date) {
    return format(date, "y-MM-dd");
  },
  format(date: Date, formatStr: string = "dd-MM-y") {
    return format(date, formatStr);
  },
};

export const jsDateParsers = {
  fromIso(date: string) {
    return parseISO(date);
  },
  parse(date: string, formatStr: string = "dd-MM-y") {
    return parse(date, formatStr, new Date());
  },
};
