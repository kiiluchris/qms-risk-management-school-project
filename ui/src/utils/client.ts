export type ReadMethod = "HEAD" | "GET";
export type WriteMethod = "POST" | "PUT" | "PATCH" | "DELETE";
export type Method = ReadMethod & WriteMethod;
export type HeadersT = Headers | Record<string, string>;

export type RequestOptions = {
  headers?: Headers | Record<string, string>;
  authenticate?: boolean;
};

export type WriteRequestOptions = {} & RequestOptions;

export type ReadRequestOptions = {
  query?: URLSearchParams;
} & RequestOptions;

type Tokens = {
  refreshToken: string;
  accessToken: string;
};

class ApiClient {
  baseUrl: string;
  baseHeaders: Headers;

  private tokenStorageKey = "tokens";

  constructor(baseUrl: string, headers: HeadersT = {}) {
    this.baseUrl = baseUrl;
    this.baseHeaders = new Headers(headers);
  }

  setAuthTokens(accessToken: string, refreshToken: string) {
    localStorage.setItem(
      this.tokenStorageKey,
      JSON.stringify({
        accessToken,
        refreshToken,
      })
    );
  }

  retrieveTokens(): Tokens | null {
    const token = localStorage.getItem(this.tokenStorageKey);
    return token ? (JSON.parse(token) as Tokens) : null;
  }

  deleteTokens() {
    localStorage.removeItem(this.tokenStorageKey);
  }

  private async headers(
    otherHeaders: HeadersT = {},
    authenticate: boolean = false
  ): Promise<Headers> {
    const h = new Headers(otherHeaders);
    this.baseHeaders.forEach((value, key) => h.set(key, value));
    if (authenticate) {
      const tokens = this.retrieveTokens();
      if (tokens) {
        h.set("Authorization", `Bearer ${tokens.accessToken}`);
      }
    }

    return h;
  }

  private async formatResponse<T>(res: Response) {
    const headers = res.headers;
    const body = await res.json();
    if (!res.ok) throw { ...body, status: res.status, headers };
    return {
      headers,
      data: body as T,
    };
  }

  private async readRequest<T>(
    path: string,
    method: ReadMethod,
    options: ReadRequestOptions = {}
  ) {
    const url = new URL(this.baseUrl + path);
    options.query && (url.search = options.query.toString());
    const headers = await this.headers(options.headers, options.authenticate);
    return await fetch(url.toString(), {
      method,
      headers: headers,
    }).then((r) => this.formatResponse<T>(r));
  }

  private async writeRequest<T>(
    path: string,
    method: WriteMethod,
    body: any,
    options: WriteRequestOptions = {}
  ) {
    const url = this.baseUrl + path;
    const headers = await this.headers(options.headers, options.authenticate);
    return fetch(url, {
      method,
      headers: headers,
      body: JSON.stringify(body),
    }).then((r) => this.formatResponse<T>(r));
  }

  head<T>(path: string, options: ReadRequestOptions = {}) {
    return this.readRequest<T>(path, "HEAD", options);
  }

  get<T>(path: string, options: ReadRequestOptions = {}) {
    return this.readRequest<T>(path, "GET", options);
  }

  post<T>(path: string, body: any, options: WriteRequestOptions = {}) {
    return this.writeRequest<T>(path, "POST", body, options);
  }

  put<T>(path: string, body: any, options: WriteRequestOptions = {}) {
    return this.writeRequest<T>(path, "PUT", body, options);
  }

  patch<T>(path: string, body: any, options: WriteRequestOptions = {}) {
    return this.writeRequest<T>(path, "PATCH", body, options);
  }

  delete<T>(path: string, options: WriteRequestOptions = {}) {
    return this.writeRequest<T>(path, "DELETE", "", options);
  }

  nest(path: string) {
    return new ApiClient(this.baseUrl + path, this.baseHeaders);
  }
}

export type ApiClientT = InstanceType<typeof ApiClient>;

export const makeClient = (url: string, headers: HeadersT = {}) => {
  return new ApiClient(url, headers);
};

export default makeClient;
