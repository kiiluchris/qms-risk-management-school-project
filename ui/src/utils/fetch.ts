import { rootClient } from "@/api/client";
import { GraphQLClient } from "graphql-request";

export const mkFetcher = (
  rootUrl: string,
  defaultHeaders: Record<string, string>
) => async (
  url: string,
  options: RequestInit = {},
  isMultiPart: boolean = false
) => {
  const tokens = rootClient.retrieveTokens();
  const headers: Record<string, string> = {
    ...defaultHeaders,
    ...(options.headers as Record<string, string>),
  };
  if (tokens) {
    headers["Authorization"] = `Bearer ${tokens.accessToken}`;
  }
  if (isMultiPart) {
    delete headers["Content-Type"];
  }
  const r = await fetch(rootUrl + url, {
    ...options,
    headers: headers,
  });
  return r;
};

export const mkJsonFetcher = (
  rootUrl: string,
  defaultHeaders: Record<string, string>
) => {
  const fetcher = mkFetcher(rootUrl, defaultHeaders);
  return async (
    url: string,
    options: RequestInit = {},
    isMultiPart: boolean = false
  ) => {
    const r = await fetcher(url, options, isMultiPart);
    const data = await r.json();
    if (!r.ok) throw { ...data, status: r.status, headers: r.headers };
    return data;
  };
};

export const mkFileFetcher = (
  rootUrl: string,
  defaultHeaders: Record<string, string>
) => {
  const fetcher = mkFetcher(rootUrl, defaultHeaders);
  return async (url: string, options: RequestInit = {}) => {
    return await fetcher(url, options);
  };
};

export const mkGqlFetcher = (
  rootUrl: string,
  defaultHeaders: Record<string, string>
) => async (query: string, variables: Record<string, string> = {}) => {
  const tokens = rootClient.retrieveTokens();
  const headers: Record<string, string> = {
    ...defaultHeaders,
    // ...(options.headers as Record<string, string>),
  };
  if (tokens) {
    headers["Authorization"] = `Bearer ${tokens.accessToken}`;
  }
  const graphQLClient = new GraphQLClient(rootUrl, {
    headers,
  });
  try {
    return await graphQLClient.request(query, variables);
  } catch (e) {
    throw e.response;
  }
};
