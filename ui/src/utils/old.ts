import { rootClient } from "@/api/client";
import { useState, useEffect } from "react";

const useItem = <T>(url: string) => {
  const [item, setItem] = useState({
    item: null as T | null,
    error: null as null | {
      name: string;
      messages: string[][];
      status: number;
    },
  });
  useEffect(() => {
    rootClient.get(url, { authenticate: true }).then(
      ({ data }) => {
        setItem({
          item: data as T,
          error: null,
        });
      },
      (e) => {
        setItem({
          item: null,
          error:
            e instanceof Error
              ? {
                  name: "Network Error",
                  messages: [],
                  status: -1,
                }
              : {
                  name: e.error,
                  messages: e.messages,
                  status: e.status,
                },
        });
      }
    );
  }, []);

  return item;
};
