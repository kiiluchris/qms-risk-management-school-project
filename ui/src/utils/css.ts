import { filterMap } from "./list";

export function joinCssModuleClassnames(
  cssModule: {
    [key: string]: string;
  },
  expectedClasses: Set<string>,
) : string {
  return filterMap(
    Object.entries(cssModule),
    ([cls, actualClass]) => {
      return expectedClasses.has(cls)
        ? actualClass
        : null;
    }
  ).join(" ");
}
