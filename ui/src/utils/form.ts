import { IdObj, RelatedItem } from "@/domain/models/shared";
import { userSchema } from "@/domain/models/user";
import { userFullName } from "@/views/app/main/risk-graph-utils";
import * as z from "zod";

export const zodReactSelectOption = z.object({
  value: z.number().or(z.string()),
  label: z.string(),
  __isNew__: z.boolean().optional(),
});

export const zodReactSelectOptionString = z.object({
  value: z.string(),
  label: z.string(),
});

export const zodReactSelectOptionNumber = z.object({
  value: z.number(),
  label: z.string(),
});

export type ReactSelectOption = z.infer<typeof zodReactSelectOption>;

export const reactSelectOptionToRelatedItem = (
  item: ReactSelectOption
): RelatedItem => {
  return { id: item.value, value: item.label };
};

export const reactSelectOptionToRelatedItemOpt = (
  item: ReactSelectOption | undefined
): RelatedItem | undefined => {
  return item ? reactSelectOptionToRelatedItem(item) : undefined;
};

export const toReactSelectOption = <T extends IdObj>(
  item: T | undefined,
  display: (t: T) => string
): ReactSelectOption | undefined => {
  return typeof item !== "undefined"
    ? { value: item.id, label: display(item) }
    : undefined;
};

export const toRelatedItem = <T extends IdObj>(
  item: T | undefined,
  display: (t: T) => string
): RelatedItem | undefined => {
  return typeof item !== "undefined" && item !== null
    ? { id: item.id, value: display(item) }
    : undefined;
};

export const defaultFetchedSelectValue = <T>(
  items: T[] | undefined,
  mapper: (item: T) => ReactSelectOption,
  itemRelatedToParent?: RelatedItem
) => {
  return itemRelatedToParent
    ? { label: itemRelatedToParent.value, value: itemRelatedToParent.id }
    : items?.[0]
    ? mapper(items[0])
    : undefined;
};

export const genericSchema = z
  .object({
    id: z.number(),
    name: z.string(),
  })
  .nonstrict();

export const genericSchemaToRelatedItem = (
  t: z.infer<typeof genericSchema>
): RelatedItem => {
  return {
    id: t.id,
    value: t.name,
  };
};

export const userSchemaToRelatedItem = (
  t: z.infer<typeof userSchema>
): RelatedItem => {
  return {
    id: t.id,
    value: userFullName(t),
  };
};
