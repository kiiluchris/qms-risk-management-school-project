import { isGeLength } from "./String";
import { Either, getValidation, left, map, right } from "fp-ts/lib/Either";
import { sequenceT } from "fp-ts/lib/Apply";
import { NonEmptyArray, getSemigroup } from "fp-ts/lib/NonEmptyArray";
import { pipe } from "fp-ts/lib/function";

interface PasswordBrand {
  readonly Password: unique symbol;
}

export type Password = string & PasswordBrand;

function minLengthV(s: string): Either<NonEmptyArray<string>, Password> {
  const minLength = 8;
  const isAtMinimumValidLength = isGeLength(minLength);

  return isAtMinimumValidLength(s)
    ? right(s as Password)
    : left([`Password needs to be at least ${minLength} characters long`]);
}

function hasCapitalV(s: string): Either<NonEmptyArray<string>, Password> {
  const hasCapital = /[A-Z]/.test(s);
  return hasCapital
    ? right(s as Password)
    : left(["Password should have at least one uppercase letter"]);
}

function hasLowerV(s: string): Either<NonEmptyArray<string>, Password> {
  const hasLower = /[a-z]/.test(s);
  return hasLower
    ? right(s as Password)
    : left(["Password should have at least one lowercase letter"]);
}

function hasSpecialV(s: string): Either<NonEmptyArray<string>, Password> {
  const hasLower = /[~`! @#$%^&*()_-+={[}]|\:;"'<,>.?\/]/.test(s);
  return hasLower
    ? right(s as Password)
    : left([
        `Password should have at least one special letter of ~\`! @#$%^&*()_-+={[}]|\:;"'<,>.?/`,
      ]);
}

export function makePassword(
  s: string
): Either<NonEmptyArray<string>, Password> {
  const validationApplicative = getValidation(getSemigroup<string>());
  return pipe(
    sequenceT(validationApplicative)(
      minLengthV(s),
      hasCapitalV(s),
      hasLowerV(s),
      hasSpecialV(s)
    ),
    map(() => s as Password)
  );
}
