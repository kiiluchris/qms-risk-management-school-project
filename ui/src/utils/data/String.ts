export function isOfLength(n: number) {
  return (s: string) => s.length === n;
}

export function isGtLength(n: number) {
  return (s: string) => s.length > n;
}

export function isLtLength(n: number) {
  return (s: string) => s.length < n;
}

export function isGeLength(n: number) {
  return (s: string) => s.length >= n;
}

export function isLeLength(n: number) {
  return (s: string) => s.length <= n;
}

export const isEmptyString = isOfLength(0);
