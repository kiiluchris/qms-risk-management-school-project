import { Option, some, none } from "fp-ts/lib/Option";

export interface NonEmptyStringBrand {
  readonly NonEmptyString: unique symbol;
}

export type NonEmptyString = string & NonEmptyStringBrand;

export function isNonEmptyString(s: string): s is NonEmptyString {
  return s.length > 0;
}

export function makeNonEmptyString(s: string): Option<NonEmptyString> {
  return isNonEmptyString(s) ? some(s) : none;
}
