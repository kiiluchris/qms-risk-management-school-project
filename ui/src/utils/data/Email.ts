import { sequenceT } from "fp-ts/lib/Apply";
import { Either, getValidation, left, map, right } from "fp-ts/lib/Either";
import { pipe } from "fp-ts/lib/function";
import { getSemigroup, NonEmptyArray } from "fp-ts/lib/NonEmptyArray";
import { isLeLength } from "./String";

const applicativeValidation = getValidation(getSemigroup<string>());

interface EmailBrand {
  readonly Email: unique symbol;
}

export type Email = string & EmailBrand;

const noConsecutivePeriods = (s: string) => !/\.\./.test(s);
const hasValidPrefixCharacters = (s: string) => {
  return /^[a-zA-Z0-9!#$%&'*+-/=?^_`{|}~]+$/.test(s);
};
const startsWithAlphabetic = (s: string) => {
  return /^[a-zA-B]/.test(s);
};
const hasValidSuffixCharacters = (s: string) => {
  return /^([a-zA-B0-9]-?)+$/.test(s);
};
const hasValidEnd = (s: string) => !s.endsWith("-");

function consecutivePeriodV(
  s: string,
  partname: string
): Either<NonEmptyArray<string>, Email> {
  return noConsecutivePeriods(s)
    ? right(s as Email)
    : left([`Email address ${partname} must not contain consecutive periods`]);
}

function startsWithAlphabeticV(
  s: string,
  partname: string
): Either<NonEmptyArray<string>, Email> {
  return startsWithAlphabetic(s)
    ? right(s as Email)
    : left([
        `Email address ${partname} must start with an alphabetic character`,
      ]);
}
function hasValidEndV(
  s: string,
  partname: string
): Either<NonEmptyArray<string>, Email> {
  return hasValidEnd(s)
    ? right(s as Email)
    : left([`Email address ${partname} must not end with a hyphen`]);
}

function hasValidPrefixCharactersV(
  s: string
): Either<NonEmptyArray<string>, Email> {
  return hasValidPrefixCharacters(s)
    ? right(s as Email)
    : left([
        "Email user only accepts alphanumeric characters and !#$%&'*+-/=?^_`{|}~",
      ]);
}

function hasValidSuffixCharactersV(
  s: string,
  partname: string
): Either<NonEmptyArray<string>, Email> {
  return hasValidSuffixCharacters(s)
    ? right(s as Email)
    : left([
        `Email subdomain ${partname} should only contain alphanumeric characters`,
      ]);
}

function hasValidSuffixV(s: string): Either<NonEmptyArray<string>, Email> {
  const [either0, ...eithers] = s.split(".").map((subdomain) => {
    return sequenceT(applicativeValidation)(
      startsWithAlphabeticV(subdomain, `subdomain "${subdomain}"`),
      hasValidSuffixCharactersV(subdomain, subdomain),
      hasValidEndV(subdomain, `subdomain "${subdomain}"`)
    );
  });

  return pipe(
    sequenceT(applicativeValidation)(either0, ...eithers),
    map(() => s as Email)
  );
}

const maxPrefixLength = 64;
function maxPrefixLengthV(s: string): Either<NonEmptyArray<string>, Email> {
  return isLeLength(maxPrefixLength)(s)
    ? right(s as Email)
    : left([
        `Email user needs is at maximum ${maxPrefixLength} characters long`,
      ]);
}

const maxSuffixLength = 255;

function maxSuffixLengthV(s: string): Either<NonEmptyArray<string>, Email> {
  return isLeLength(maxSuffixLength)(s)
    ? right(s as Email)
    : left([
        `Email user needs is at maximum ${maxSuffixLength} characters long`,
      ]);
}

export function makeEmail(s: string): Either<NonEmptyArray<string>, Email> {
  const [local, domain] = s.split("@");

  return pipe(
    sequenceT(applicativeValidation)(
      consecutivePeriodV(local, "user"),
      consecutivePeriodV(domain, "domain"),
      startsWithAlphabeticV(local, "user"),
      hasValidPrefixCharactersV(local),
      hasValidSuffixV(domain),
      maxPrefixLengthV(local),
      maxSuffixLengthV(domain)
    ),
    map(() => s as Email)
  );
}
