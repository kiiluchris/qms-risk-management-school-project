import { some, none, Option } from "fp-ts/lib/Option";
import { isGeLength, isLeLength } from "./String";

interface OrganizationDomainBrand {
  readonly OrganizationDomain: unique symbol;
}

export type OrganizationDomain = string & OrganizationDomainBrand;

const minLength = 8;
const maxLength = 16;

export function isOrganizationDomain(s: string): s is OrganizationDomain {
  return isGeLength(minLength)(s) && isLeLength(maxLength)(s);
}

export function makeOrganizationDomain(s: string): Option<OrganizationDomain> {
  return isOrganizationDomain(s) ? some(s) : none;
}
