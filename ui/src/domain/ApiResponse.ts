import { Either } from "fp-ts/lib/Either";
import { DecodeError } from "io-ts/lib/DecodeError";
import { FreeSemigroup } from "io-ts/lib/FreeSemigroup";

export type ApiResponse<T> = Either<FreeSemigroup<DecodeError<string>>, T>;
