import { RelatedItem } from "@/domain/models/shared";
import { GqlRole } from "@/domain/models/role";
import { GqlQuery } from "./gql";
import * as z from "zod";

export type User = {
  first_name: string;
  last_name: string;
  email: string;
  phone: string;
  role: RelatedItem;
  callback_url: string;
};

export type UserAccount = Omit<User, "role" | "callback_url" | "email"> & {
  use_sms: boolean;
};

export type UserWithPassword = {
  password: string;
} & User;

export type DashboardUser = {
  id: number;
  first_name: string;
  last_name: string;
  email: string;
  role: RelatedItem;
  phone: string;
  use_sms?: boolean;
};

export type GqlUser = {
  id: number;
  first_name: string;
  last_name: string;
  email: string;
  role: GqlRole;
  phone: string;
  use_sms: boolean;
};

export type GqlUserWithDeps = {
  user: GqlUser;
  roles: GqlRole[];
};

export type GqlUserToSelect = Pick<
  GqlUser,
  "id" | "first_name" | "last_name" | "role"
>;

export const gqlQuery: GqlQuery<GqlUserWithDeps, GqlUser> = {
  item: {
    extract: ({ user }) => user,
    query: (id) => `
      user(id: ${id}) {
        id
        first_name
        last_name
        email
        phone
        use_sms
        role {
          name
          id
        }
      }
    `,
  },
  dependencies: `
      roles {
        id
        name
      }
  `,
};

export type GqlAccountWithDeps = {
  account: GqlUser;
  roles: GqlRole[];
};

export const gqlAccountQuery: GqlQuery<GqlAccountWithDeps, undefined> = {
  dependencies: `
      account {
        id
        first_name
        last_name
        email
        phone
        use_sms
        role {
          name
          id
        }
      }
      roles {
        id
        name
      }
  `,
};

export type T = typeof gqlQuery;

export const userSchema = z
  .object({
    id: z.number(),
    first_name: z.string(),
    last_name: z.string(),
  })
  .nonstrict();
