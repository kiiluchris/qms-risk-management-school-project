import { IdObj, RelatedItem } from "@/domain/models/shared";
import { GqlQuery } from "./gql";
import { GqlUserToSelect } from "./user";

export type TaskTeam = {
  name: string;
  leader: RelatedItem;
  members: RelatedItem[];
};

export type TaskTeamWithId = TaskTeam & IdObj;

export type GqlTaskTeam = {
  name: string;
  leader: GqlUserToSelect;
  members: GqlUserToSelect[];
} & IdObj;

export type GqlTaskTeamWithDeps = {
  task_team: GqlTaskTeam;
  users: GqlUserToSelect[];
};

export const gqlQuery: GqlQuery<GqlTaskTeamWithDeps, GqlTaskTeam> = {
  item: {
    extract: ({ task_team }) => task_team,
    query: (id) => `
      task_team(id: ${id}){
        id
        name
        leader {
          id
          first_name
          last_name
        }
        members {
          id
          first_name
          last_name
        }
      }
  `,
  },
  dependencies: `
    users {
      id
      first_name
      last_name
      role {
        id
        name
      }
    }
  `,
};

// possible_leaders {
//   id
//   first_name
//   last_name
// }
// possible_team_members {
//   id
//   first_name
//   last_name
// }
