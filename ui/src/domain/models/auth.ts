import { RolePermission } from "@/domain/models/role";

export type LoginTokens = {
  access_token: string;
  refresh_token: string;
  role: RolePermission;
};

export type AuthTokens = {
  accessToken: string;
  refreshToken: string;
};

export const loginTokensFromCamelCase = (
  tokens: AuthTokens
): Omit<LoginTokens, "role"> => {
  return {
    access_token: tokens.accessToken,
    refresh_token: tokens.refreshToken,
  };
};
export const loginTokensToCamelCase = (tokens: LoginTokens): AuthTokens => {
  return {
    accessToken: tokens.access_token,
    refreshToken: tokens.refresh_token,
  };
};

export type ResetPasswordUser = {
  organization: string;
  email: string;
  callback_url: string;
};

export type ResetPasswordDetails = {
  organization: string;
  email: string;
  password: string;
  token: string;
};
