export type GqlQuery<T, V> = {
  item?: {
    query: (itemId: number) => string;
    extract: (data: T) => V;
  };
  dependencies?: string;
};

export function buildGqlQuery<T, V>(
  { item, dependencies }: GqlQuery<T, V>,
  itemId?: number
): string {
  if (item && itemId) {
    return `{
      ${item.query(itemId)}
      ${dependencies || ""}
    }`;
  } else if (dependencies) {
    return `{ ${dependencies} }`;
  } else {
    return "";
  }
}
