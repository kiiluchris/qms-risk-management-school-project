import { GqlQuery } from "./gql";
import { IdObj } from "./shared";

export type ProcessOutput = {
  name: string;
};

export type ProcessOutputWithId = ProcessOutput & IdObj;

export type GqlProcessOutput = ProcessOutputWithId;

export type GqlProcessOutputToSelect = GqlProcessOutput;

export type GqlProcessOutputWithDeps = {
  process_output: GqlProcessOutput;
};

export const gqlQuery: GqlQuery<GqlProcessOutputWithDeps, GqlProcessOutput> = {
  item: {
    extract: ({ process_output }) => process_output,
    query: (id) => `
      process_output(id: ${id}) {
        id
        name
      }
    `,
  },
};
