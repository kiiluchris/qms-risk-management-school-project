import { IdObj, MonitoringSchedule, RelatedItem } from "@/domain/models/shared";
import { GqlQuery } from "./gql";
import { GqlObjectiveToSelect } from "./objective";
import { GqlProcessValue } from "./process-value";
import { ProcessNewTask } from "./task";
import { GqlTaskTeam } from "./task-team";
import { GqlUserToSelect } from "./user";

export type Process = {
  name: string;
  person_responsible?: RelatedItem;
  team_responsible?: RelatedItem;
  tasks: ProcessNewTask[];
  objective: RelatedItem;
  monitoring_schedule?: string;
  inputs: RelatedItem[];
  outputs: RelatedItem[];
};

export type ProcessWithId = Process & IdObj;

export type GqlProcess = {
  name: string;
  person_responsible?: GqlUserToSelect;
  team_responsible: GqlTaskTeam;
  tasks: Omit<GqlTask, "process">[];
  objective: GqlObjectiveToSelect;
  monitoring_schedule?: MonitoringSchedule;
  inputs: RelatedItem[];
  outputs: RelatedItem[];
  potential_risks: RelatedItem[];
  known_risks: RelatedItem[];
} & IdObj;

export type GqlProcessToSelect = Pick<GqlProcess, "id" | "name" | "objective">;

export type GqlProcessWithDeps = {
  process: GqlProcess;
  objectives: GqlObjectiveToSelect[];
  users: GqlUserToSelect[];
  task_teams: GqlTaskTeam[];
  process_values: GqlProcessValue[];
};

export const gqlQuery: GqlQuery<GqlProcessWithDeps, GqlProcess> = {
  item: {
    extract: ({ process }) => process,
    query: (itemId) => `
      process(id: ${itemId}) {
        id
        name
        person_responsible {
          id
          first_name
          last_name
        }
        tasks {
          id
          name
          due_by
          is_complete
        }
        objective {
          id
          name
          target_date
        }
        monitoring_schedule
        inputs {
          id
          value
        }
        outputs {
          id
          value
        }
        known_risks {
          id
          value
        }
        potential_risks {
          id
          value
        }
        team_responsible {
          id
          name
        }
      }
  `,
  },
  dependencies: `
    objectives {
      id
      name
    }
    users {
      id
      first_name
      last_name
    }
    task_teams {
      id
      name
    }
    process_values {
      id
      name
    }
  `,
};

export type GqlTask = {
  name: string;
  is_complete: boolean;
  due_by: string;
  process: GqlProcessToSelect;
  user_owner?: GqlUserToSelect;
} & IdObj;

export type GqlTaskWithDeps = {
  task: GqlTask;
  processes: GqlProcessToSelect[];
  users: GqlUserToSelect[];
};

export const gqlTaskQuery: GqlQuery<GqlTaskWithDeps, GqlTask> = {
  item: {
    extract: ({ task }) => task,
    query: (id) => `
        task(id: ${id}) {
          id
          name
          is_complete
          due_by
          process {
            id
            name
            objective {
              id
              name
              target_date
            }
          }
          user_owner {
            id
            first_name
            last_name
          }
        }
  `,
  },
  dependencies: `
    processes {
      id
      name
      objective {
        id
        name
        target_date
      }
    }
    users {
      id
      first_name
      last_name
    }
  `,
};
