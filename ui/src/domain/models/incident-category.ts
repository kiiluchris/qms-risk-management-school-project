import { GqlQuery } from "./gql";
import { IdObj } from "./shared";

export type RiskCategory = {
  name: string;
};

export type RiskCategoryWithId = RiskCategory & IdObj;

export type GqlRiskCategory = RiskCategoryWithId;
export type GqlRiskCategoryToSelect = GqlRiskCategory;

export type GqlRiskCategoryWithDeps = {
  incident: GqlRiskCategory;
};

export const gqlQuery: GqlQuery<GqlRiskCategoryWithDeps, GqlRiskCategory> = {
  item: {
    extract: ({ incident }) => incident,
    query: (id) => `
      risk_category(id: $id) {
        id
        name

      }
    `,
  },
};
