import { GqlQuery } from "./gql";

export type RolePermission = "Admin" | "Manager" | "Employee";

export type Role = {
  id: number;
  name: string;
};

export type GqlRole = Role;

export type GqlRoleWithDeps = {
  role: GqlRole;
};

export const gqlQuery: GqlQuery<GqlRoleWithDeps, GqlRole> = {
  item: {
    extract: ({ role }) => role,
    query: (itemId) => `
      role(id: ${itemId}){
        id
        name
      }
    `,
  },
};

export const canDisplayMinimumRole = (
  minimumRole: RolePermission | undefined,
  accountRole: RolePermission
): boolean => {
  return (
    !minimumRole ||
    accountRole === "Admin" ||
    (accountRole === "Manager" && minimumRole === "Employee")
  );
};
