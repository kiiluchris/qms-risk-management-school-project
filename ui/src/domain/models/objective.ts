import { GqlQuery } from "./gql";
import { IdObj, RelatedItem } from "./shared";
import { GqlUserToSelect } from "./user";

export type Objective = {
  name: string;
  kpi: string;
  target_date: string;
  current_status: string;
  description: string;
  person_responsible: RelatedItem;
  outputs?: RelatedItem[];
};

export type ObjectiveWithId = Objective & IdObj;
export type GqlObjective = {
  name: string;
  kpi: string;
  target_date: string;
  current_status: "Open" | "Closed";
  description: string;
  person_responsible: GqlUserToSelect;
  processes: { name: string; team_responsible: { name: string } }[];
  // outputs?: RelatedItem[];
} & IdObj;

export type GqlObjectiveToSelect = Pick<
  GqlObjective,
  "id" | "name" | "target_date"
>;

export type GqlObjectiveWithDeps = {
  objective: GqlObjective;
  users: GqlUserToSelect[];
};

export const gqlQuery: GqlQuery<GqlObjectiveWithDeps, GqlObjective> = {
  item: {
    extract: ({ objective }) => objective,
    query: (itemId) => `
      objective(id: ${itemId}){
        id
        name
        kpi
        target_date
        current_status
        description
        person_responsible {
          id
          first_name
          last_name
        }
        processes {
          name
          team_responsible {
            name
          }
        }
      }
  `,
  },
  dependencies: `
    users {
      id
      first_name
      last_name
    }
`,
};
