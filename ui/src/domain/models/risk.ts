import { GqlCapa } from "./capa";
import { GqlQuery } from "./gql";
import { GqlRiskCategoryToSelect } from "./incident-category";
import { GqlProcessToSelect } from "./process";
import { GqlProcessValueToSelect } from "./process-value";
import { IdObj, RelatedItem } from "./shared";
import { GqlUserToSelect } from "./user";

export type Risk = {
  name: string;
  description: string;
  category: RelatedItem;
  likelihood: number;
  recommended_actions: string;
  impact: number;
  affected_processes: RelatedItem[];
  created_by?: RelatedItem;
  is_finalized: boolean;
  risk_owner: RelatedItem;
  current_status: string;
  // is_positive: boolean;
  // affected_inputs: RelatedItem[];
  // affected_outputs: RelatedItem[];
  // affected_values: RelatedItem[];
};

export type RiskWithId = Risk & IdObj;

export type GqlRisk = {
  name: string;
  description: string;
  recommended_actions: string;
  category: GqlRiskCategoryToSelect;
  likelihood: number;
  is_finalized: boolean;
  impact: number;
  created_by?: GqlUserToSelect;
  affected_processes: GqlProcessToSelect[];
  preventive_capas: Pick<GqlCapa, "name" | "target_effective_date">[];
  suggested_capas: Pick<GqlCapa, "name" | "target_effective_date">[];
  risk_owner?: GqlUserToSelect;
  current_status: string;
  // is_positive: boolean;
  // affected_inputs: GqlProcessInputToSelect[];
  // affected_outputs: GqlProcessOutputToSelect[];
  // affected_values: GqlProcessValueToSelect[];
} & IdObj;

export type GqlRiskToSelect = Pick<GqlRisk, "id" | "name">;

export type GqlRiskWithDeps = {
  risk: GqlRisk;
  users: GqlUserToSelect[];
  process_values: GqlProcessValueToSelect[];
  processes: GqlProcessToSelect[];
};

export const gqlQuery: GqlQuery<GqlRiskWithDeps, GqlRisk> = {
  item: {
    extract: ({ risk }) => risk,
    query: (id) => `
      risk(id: ${id}) {
        id
        name
        description
        recommended_actions
        is_finalized
        likelihood
        impact
        current_status
        category {
          id
          name
        }
        affected_processes {
          id
          name
          objective {
             target_date
          }
        }
        preventive_capas {
          name
          target_effective_date
        }
        suggested_capas {
          name
          target_effective_date
        }
      }
  `,
  },
  dependencies: `
    users {
      id
      first_name
      last_name
      role {
        id
        name
      }
    }
    processes {
      id
      name
      objective {
          target_date
      }
    }
  `,
};
