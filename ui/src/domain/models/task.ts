import { IdObj, RelatedItem } from "@/domain/models/shared";
import { GqlQuery } from "./gql";

export type NewTask = {
  name: string;
  is_complete: boolean;
  due_by?: string;
  user_owner?: RelatedItem;
  // person_responsible: RelatedItem;
  // group_responsible: RelatedItem;
};

type TaskProcess = {
  process: RelatedItem;
};
export type Task = TaskProcess & NewTask;

export type ProcessNewTask = NewTask & Partial<TaskProcess> & Partial<IdObj>;

export type TaskWithId = Task & IdObj;
