import { IdObj, RelatedItem } from "@/domain/models/shared";
import { GqlQuery } from "./gql";
import { GqlRiskCategoryToSelect } from "./incident-category";
import { GqlProcessToSelect } from "./process";
import { GqlRiskToSelect } from "./risk";
import { GqlTaskTeam } from "./task-team";
import { GqlUserToSelect } from "./user";

export type Incident = {
  name: string;
  risk?: RelatedItem;
  reported_by: RelatedItem;
  location: string;
  // department: RelatedItem;
  // branch: RelatedItem;
  date_occurred: string;
  lag_time: string;
  // TODO Figure out whether it's more appropriate to use process
  //process: RelatedItem;
  // activity: string;
  // activity_details: string;
  damage: string;
  category: RelatedItem;
  person_responsible: RelatedItem;
  first_aid_provided_by?: RelatedItem;
  affected_processes: RelatedItem[];
  // first_aid_provided?: {
  //   details: string;
  //   provided_by: RelatedItem;
  // };
};

export type IncidentWithId = Incident & IdObj;

export type GqlIncident = {
  name: string;
  risk: GqlRiskToSelect;
  reported_by: GqlUserToSelect;
  location: string;
  // department: RelatedItem;
  // branch: RelatedItem;
  date_occurred: string;
  lag_time: string;
  // TODO Figure out whether it's more appropriate to use process
  //process: RelatedItem;
  // activity: string;
  // activity_details: string;
  damage: string;
  category: GqlRiskCategoryToSelect;
  person_responsible: GqlUserToSelect;
  first_aid_provided_by?: GqlUserToSelect;
  affected_processes: (Omit<GqlProcessToSelect, "objective"> & {
    team_responsible: Pick<GqlTaskTeam, "name">;
  })[];
} & IdObj;

export type GqlIncidentToSelect = Pick<GqlIncident, "id" | "name">;

export type GqlIncidentWithDeps = {
  incident: GqlIncident;
  users: GqlUserToSelect[];
  risks: GqlRiskToSelect[];
  risk_categories: GqlRiskCategoryToSelect[];
  processes: Omit<GqlProcessToSelect, "objective">[];
};

export const gqlQuery: GqlQuery<GqlIncidentWithDeps, GqlIncident> = {
  item: {
    extract: ({ incident }) => incident,
    query: (itemId) => `
      incident(id: ${itemId}){
        id
        name
        date_occurred
        risk {
          id
          name
        }
        reported_by {
          id
          first_name
          last_name
        }
        location
        lag_time
        damage
        category {
          id
          name
        }
        person_responsible {
          id
          first_name
          last_name
        }
        first_aid_provided_by {
          id
          first_name
          last_name
        }
        affected_processes {
          id
          name
          team_responsible {
            name
          }
        }
      }
  `,
  },
  dependencies: `
    risks {
      id
      name
    }
    users {
      id
      first_name
      last_name
    }
    risk_categories {
      id
      name
    }
    processes {
      id
      name
    }
  `,
};
