import { GqlQuery } from "./gql";
import { IdObj } from "./shared";

export type Unit = {
  name: string;
};

export type UnitWithId = Unit & IdObj;

export type GqlUnit = UnitWithId;
export type GqlUnitToSelect = GqlUnit;

export type GqlUnitWithDeps = {
  unit: GqlUnit;
};

export const gqlQuery: GqlQuery<GqlUnitWithDeps, GqlUnit> = {
  item: {
    extract: ({ unit }) => unit,
    query: (itemId) => `
      unit(id: ${itemId}){
        id
        name
      }
    `,
  },
};
