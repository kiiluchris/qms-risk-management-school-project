import { GqlQuery } from "./gql";

export type Settings = {
  name: string;
  domain: string;
  api_token: string;
  risk_likelihood_weight: number;
  risk_impact_weight: number;
  ml_model: string;
};

export type SettingsUpdate = Omit<Settings, "api_token" | "domain">;

export type GqlSettings = {
  settings: Settings;
  ml_models: string[];
};

export const gqlQuery: GqlQuery<GqlSettings, undefined> = {
  dependencies: `
    settings {
      name
      domain
      api_token
      risk_likelihood_weight
      risk_impact_weight
      ml_model
    }
    ml_models
  `,
};
