export interface IdObj {
  id: number | string;
}

export interface ToString {
  toString: () => string;
}

export type PaginationOptions = {
  pageSize: number;
  currentPage: number;
};

export type RelatedItem = {
  id: string | number;
  value: string;
  readable?: string;
};

export type MonitoringSchedule =
  | "Daily"
  | "Weekly"
  | "Monthly"
  | "Quarterly"
  | "Biannually"
  | "Annually";
