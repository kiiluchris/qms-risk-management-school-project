// import { Email } from "./Email";
// import { OrganizationDomain } from "./OrganizationDomain";
// import { Password } from "./Password";

// export type LoginCredentials = {
//   organization: OrganizationDomain;
//   email: Email;
//   password: Password;
// };

export type LoginCredentials = {
  organization: string;
  email: string;
  password: string;
  token: string;
};
