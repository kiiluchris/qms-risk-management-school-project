// import { NonEmptyString } from "./NonEmptyString";
// import { OrganizationDomain } from "./OrganizationDomain";
// import { Password } from "./Password";
// import { Email } from "./Email";
// // import { PhoneNumber } from "./PhoneNumber";

// export type OrganizationDetails = {
//   name: NonEmptyString;
//   domain: OrganizationDomain;
//   firstName: NonEmptyString;
//   lastName: NonEmptyString;
//   password: Password;
//   email: Email;
//   // phone: PhoneNumber;
// };

export type OrganizationRegistrationDetails = {
  name: string;
  domain: string;
  first_name: string;
  last_name: string;
  password: string;
  email: string;
  phone: string;
  callback_url: string;
};
