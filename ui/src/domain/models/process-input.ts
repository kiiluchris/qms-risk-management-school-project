import { GqlQuery } from "./gql";
import { GqlRisk, GqlRiskToSelect } from "./risk";
import { IdObj, RelatedItem } from "./shared";
import { GqlUnit, GqlUnitToSelect } from "./unit";

export type ProcessInput = {
  name: string;
  value: number;
  unit: RelatedItem;
  // valueType: "Binary" | "Numeric" | "Floating"
};

export type ProcessInputWithId = ProcessInput & IdObj;

export type GqlProcessInput = {
  name: string;
  value: number;
  unit: GqlUnitToSelect[];
  possible_risks: GqlRiskToSelect;
} & IdObj;

export type GqlProcessInputToSelect = GqlProcessInput;

export type GqlProcessInputWithDeps = {
  process_input: GqlProcessInput;
};

export const gqlQuery: GqlQuery<GqlProcessInputWithDeps, GqlProcessInput> = {
  item: {
    extract: ({ process_input }) => process_input,
    query: (id) => `
      process_input(id: ${id}) {
        id
        name
      }
    `,
  },
};
