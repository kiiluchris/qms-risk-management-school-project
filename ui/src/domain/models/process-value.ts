import { GqlQuery } from "./gql";
import { GqlProcessToSelect } from "./process";
import { GqlRiskToSelect } from "./risk";
import { IdObj, RelatedItem } from "./shared";
import { GqlUnitToSelect } from "./unit";

export type ProcessValue = {
  name: string;
  actual_value?: number;
  target_value: number;
  unit?: RelatedItem;
  // valueType: "Binary" | "Numeric" | "Floating"
};

export type ProcessValueWithId = ProcessValue & IdObj;

export type NewProcessValue = ProcessValue & Partial<IdObj>;

type GqlProcessValueNoId = {
  name: string;
  // target_value: number;
  // actual_value: number;
  // unit?: GqlUnitToSelect;
  // unit: string;
  possible_risks?: GqlRiskToSelect[];
  input_processes?: GqlProcessToSelect[];
  output_processes?: GqlProcessToSelect[];
};
export type GqlProcessValue = GqlProcessValueNoId & IdObj;
export type GqlProcessValueOfProcess = GqlProcessValueNoId & Partial<IdObj>;

export type GqlProcessValueToSelect = Pick<GqlProcessValue, "name" | "id">;

export type GqlProcessValueWithDeps = {
  process_value: GqlProcessValue;
  risks: GqlRiskToSelect[];
  processes: GqlProcessToSelect[];
};

export const gqlProcessValueQuery: GqlQuery<
  GqlProcessValueWithDeps,
  GqlProcessValue
> = {
  item: {
    extract: ({ process_value }) => process_value,
    query: (id) => `
      process_value(id: ${id}) {
        id
        name
      }
    `,
  },
  dependencies: `
      risks {
        id
        name
      }
      processes {
        id
        name
      }
    `,
};
