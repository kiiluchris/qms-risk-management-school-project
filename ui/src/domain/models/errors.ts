export type FormSubmitError = {
  messages: string[][];
  error: string;
  headers: Record<string, string>;
  status: number;
};

export type GqlError = { status: number; errors: { message: string }[] };
