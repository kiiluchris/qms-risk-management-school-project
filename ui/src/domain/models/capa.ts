import { IdObj, RelatedItem } from "@/domain/models/shared";
import { GqlQuery } from "./gql";
import { GqlIncidentToSelect } from "./incident";
import { GqlRiskCategoryToSelect } from "./incident-category";
import { GqlRiskToSelect } from "./risk";
import { GqlUserToSelect } from "./user";

export type CapaRating = "Low" | "Moderate" | "High";

export type CapaClassification = "Corrective" | "Preventive" | "Both";

export type Capa = {
  name: string;
  action_type: CapaClassification;
  description: string;
  measures_taken: string;
  date_created: string;
  // reference_id: string;
  category: RelatedItem;
  // source: RelatedItem; // How the CA came up e.g.
  rating: CapaRating;
  target_effective_date: string;
  comments: string;
  // proposed_actions: string;
  // root_cause_analysis: EmployeeResponsibility;
  approved_by?: RelatedItem;
  prepared_by: RelatedItem;
  incident?: RelatedItem;
  risks_to_prevent?: RelatedItem[];
};

export type CapaWithId = Capa & IdObj;

export type GqlCapa = {
  name: string;
  action_type: CapaClassification;
  category: GqlRiskCategoryToSelect;
  rating: CapaRating;
  description: string;
  // reference_id: string;
  date_created: string;
  target_effective_date: string;
  // source: RelatedItem; // How the CA came up e.g.
  measures_taken: string;
  // proposed_actions: string;
  comments: string;
  // root_cause_analysis: EmployeeResponsibility;
  approved_by?: GqlUserToSelect;
  prepared_by: GqlUserToSelect;
  incident?: GqlIncidentToSelect;
  risks_to_prevent: GqlRiskToSelect[];
} & IdObj;

export type GqlCapaWithDeps = {
  capa: GqlCapa;
  users: GqlUserToSelect[];
  risk_categories: GqlRiskCategoryToSelect[];
  incidents: GqlIncidentToSelect[];
  risks: GqlRiskToSelect[];
};

export const gqlQuery: GqlQuery<GqlCapaWithDeps, GqlCapa> = {
  item: {
    extract: ({ capa }) => capa,
    query: (itemId: number) => `
      capa(id: ${itemId}){
      id
        name
        action_type
        description
        measures_taken
        date_created
        category {
          id
          name
        }
        rating
        target_effective_date
        comments
        approved_by {
          id
          first_name
          last_name
        }
        prepared_by {
          id
          first_name
          last_name
        }
        incident {
          id
          name
        }
        risks_to_prevent {
          id
          name
        }
      }
    `,
  },
  dependencies: `
    users {
      id
      first_name
      last_name
    }
    incidents {
      id
      name
    }
    risk_categories {
      id
      name
    }
    risks {
      id
      name
    }
  `,
};
