import { User, UserWithPassword } from "@/domain/models/user";

export interface UserFacade {
  getUsers(): Promise<User[]>;
  createUser(user: UserWithPassword): Promise<number>;
  deleteUser(userId: number | string): Promise<number>;
  editUser(
    userId: number | string,
    user: Partial<UserWithPassword>
  ): Promise<number>;
  getUser(userId: number | string): Promise<User>;
}
