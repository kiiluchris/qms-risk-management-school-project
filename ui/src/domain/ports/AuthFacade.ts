import { LoginCredentials } from "@/domain/models/auth/LoginCredentials";
import { OrganizationRegistrationDetails } from "@/domain/models/auth/OrganizationRegistrationDetails";
import { Either } from "fp-ts/lib/Either";
import {
  LoginTokens,
  ResetPasswordDetails,
  ResetPasswordUser,
} from "../models/auth";

export interface AuthFacade {
  verifyOrganizationExists: (org: string) => Promise<boolean>;
  registerOrganizationAccount: (
    accountDetails: OrganizationRegistrationDetails
  ) => Promise<boolean>;
  signIn: (credentials: LoginCredentials) => Promise<LoginTokens>;
  refreshAccessToken: () => Promise<LoginTokens | null>;
  setAuthTokens: (accessToken: string, refreshToken: string) => void;
  deleteAuthTokens: () => void;
  requestPasswordReset: (data: ResetPasswordUser) => Promise<boolean>;
  resetPassword: (data: ResetPasswordDetails) => Promise<boolean>;
  logOut: () => Promise<boolean>;
}
