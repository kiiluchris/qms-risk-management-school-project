import { ApiClientT } from "@/utils/client";

export type DashboardCrudFacade<T> = {
  create(item: T): Promise<number>;
  update(itemId: number | string, item: T): Promise<number>;
  delete(itemId: number | string): Promise<number>;
};

export const makeDashboardCrudService = <T>(
  apiClient: ApiClientT
): DashboardCrudFacade<T> => {
  return {
    create(item) {
      return apiClient
        .post("/", item, { authenticate: true })
        .then(({ data }) => {
          return data as number;
        });
    },
    update(itemId: number | string, item: T) {
      return apiClient
        .put(`/${itemId}`, item, { authenticate: true })
        .then(({ data }) => {
          return data as number;
        });
    },
    delete(itemId) {
      return apiClient
        .delete(`/${itemId}`, { authenticate: true })
        .then(({ data }) => {
          return data as number;
        });
    },
  };
};
