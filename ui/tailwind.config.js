module.exports = {
  purge: {
    content: ["./src/**/*.{js,jsx,ts,tsx}", "./public/index.html"],
    safelist: [/^laatu-(bg|txt)-\d+$/, /^laatu-[^-]+-theme$/],
  },
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      left: {
        // '1/4': '25%',
      },
      boxShadow: {
        sidebar: "rgba(0, 0, 0, 0.3) 100px 0px 0px 0px",
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
