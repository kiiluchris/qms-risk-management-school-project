{ pkgs ? import <nixpkgs> { } }:
let unstable = import <nixos-unstable> { config = { allowUnfree = true; }; };
in with pkgs;
pkgs.mkShell {
  buildInputs = [ python39 unstable.python39Packages.poetry ];
  shellHook = ''
    # fixes libstdc++ issues and libgl.so issues
    export LD_LIBRARY_PATH=${stdenv.cc.cc.lib}/lib/:/run/opengl-driver/lib/
  '';
}
