from domain.user.ports.storage import UserDatabase
from typing import Union

from injector import Module, provider, singleton
from sqlalchemy.orm import scoped_session

from domain.objective.ports.storage import ObjectiveDatabase
from domain.objective.facade import ObjectiveFacadeT, ObjectiveFacade
from domain.objective.infrastructure.database import ObjectiveDatabaseAdapter


class ObjectiveModule(Module):
    @provider
    @singleton
    def objective_db(
        self, db: scoped_session, user_db: UserDatabase
    ) -> ObjectiveDatabase:
        return ObjectiveDatabaseAdapter(db, user_db)

    @provider
    @singleton
    def objective_facade(self, objective_db: ObjectiveDatabase) -> ObjectiveFacadeT:
        return ObjectiveFacade(objective_db)
