#!/usr/bin/env python3

from datetime import date
from typing import cast
from domain.shared.models import ObjectiveStatus
import pandas
from application.utils.request import AppContext
from flask import Blueprint, Flask, Request, jsonify
from sqlalchemy.orm import scoped_session

from domain.auth.ports.utils import Jwt
from domain.auth.models import AuthUser
from domain.objective.models import (
    NewObjective,
    Objective,
    ObjectiveImport,
    ObjectiveQueryParams,
    ObjectiveUpdate,
)
from application.utils.middlewares import get_upload_file, login_required, parse_request
from domain.objective.facade import ObjectiveFacadeT
from application.utils.response import error_response


objective_blueprint = Blueprint("objectives", __name__)


@objective_blueprint.route("")
@login_required()
def get_all(
    request: Request,
    ctx: AppContext,
):
    query_params = ObjectiveQueryParams()
    objectives = ctx.objective_facade.get_objectives(ctx.auth_user, query_params)

    return jsonify([o.to_dict() for o in objectives])


@objective_blueprint.route("", methods=["POST"])
@login_required()
@parse_request(NewObjective, "objective")
def create(
    objective,
    request: Request,
    ctx: AppContext,
):
    objective_id = ctx.objective_facade.create_objective(ctx.auth_user, objective)

    return jsonify(objective_id)


@objective_blueprint.route("/<int:objective_id>")
@login_required()
def get_one(
    objective_id,
    request: Request,
    ctx: AppContext,
):
    objective = ctx.objective_facade.get_objective(ctx.auth_user, objective_id)

    return (
        jsonify(objective.to_dict())
        if objective
        else error_response(404, "Objective does not exist", [])
    )


@objective_blueprint.route("/<int:objective_id>", methods=["PUT"])
@login_required()
@parse_request(ObjectiveUpdate, "objective")
def update(
    objective_id,
    objective,
    ctx: AppContext,
    request: Request,
):
    objective_id = ctx.objective_facade.update_objective(
        ctx.auth_user, objective_id, objective
    )

    return jsonify(objective_id)


@objective_blueprint.route("/<int:objective_id>", methods=["DELETE"])
@login_required()
def delete_(
    objective_id,
    request: Request,
    ctx: AppContext,
):
    objective = ctx.objective_facade.delete_objective(ctx.auth_user, objective_id)

    return (
        jsonify(objective)
        if objective
        else error_response(404, "Objective does not exist", [])
    )


@objective_blueprint.route("/import", methods=["POST"])
@login_required()
@get_upload_file
def import_objectives(
    file_,
    request: Request,
    ctx: AppContext,
):
    df = pandas.read_excel(
        file_,
        usecols=[
            "Name",
            "Key Performance Indicator",
            "Current Status",
            "Target Date of Completion",
            "Person Responsible",
        ],
    )
    objectives = [
        ObjectiveImport(
            name=name.strip(),
            description="",
            kpi=kpi.strip(),
            current_status=ObjectiveStatus[sta.strip()],
            target_date=target_date.date(),
            person_responsible=person_res.strip(),
        )
        for name, kpi, sta, target_date, person_res in df.values.tolist()
    ]
    objective_ids = ctx.objective_facade.import_objectives(
        cast(AuthUser, ctx.auth_user), objectives
    )

    return jsonify(objective_ids)
