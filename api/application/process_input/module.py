from typing import Union

from injector import Module, provider, singleton
from sqlalchemy.orm import scoped_session

from domain.process_input.ports.storage import ProcessInputDatabase
from domain.process_input.facade import ProcessInputFacadeT, ProcessInputFacade
from domain.process_input.infrastructure.database import ProcessInputDatabaseAdapter


class ProcessInputModule(Module):
    @provider
    @singleton
    def process_input_db(
        self,
        db: scoped_session,
    ) -> ProcessInputDatabase:
        return ProcessInputDatabaseAdapter(db)

    @provider
    @singleton
    def process_input_facade(self, process_input_db: ProcessInputDatabase) -> ProcessInputFacadeT:
        return ProcessInputFacade(process_input_db)