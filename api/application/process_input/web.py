#!/usr/bin/env python3

from flask import Blueprint, Flask, Request, jsonify
from sqlalchemy.orm import scoped_session

from application.utils.request import AppContext
from domain.process_input.models import (
    ProcessInput,
    ProcessInputQueryParams,
    ProcessInputUpdate,
    NewProcessInput
)
from application.utils.middlewares import login_required, parse_request
from domain.process_input.facade import ProcessInputFacadeT
from application.utils.response import error_response


process_input_blueprint = Blueprint("process_inputs", __name__)


@process_input_blueprint.route("")
@login_required()
def get_all(
    request: Request,
    ctx: AppContext,
):
    query_params = ProcessInputQueryParams()
    process_inputs = ctx.process_input_facade.get_process_inputs(ctx.auth_user, query_params)

    return jsonify([process_input.to_dict() for process_input in process_inputs])


@process_input_blueprint.route("", methods=["POST"])
@login_required()
@parse_request(NewProcessInput, "process_input")
def create(
    process_input,
    request: Request,
    ctx: AppContext,
):
    process_input_id = ctx.process_input_facade.create_process_input(ctx.auth_user, process_input)

    return jsonify(process_input_id)


@process_input_blueprint.route("/<int:process_input_id>")
@login_required()
def get_one(
    process_input_id,
    request: Request,
    ctx: AppContext,
):
    process_input = ctx.process_input_facade.get_process_input(ctx.auth_user, process_input_id)

    return jsonify(process_input.to_dict()) if process_input else error_response(404, "ProcessInput does not exist", [])


@process_input_blueprint.route("/<int:process_input_id>", methods=["PUT"])
@login_required()
@parse_request(ProcessInputUpdate, "process_input")
def update(
    process_input_id,
    process_input,
    request: Request,
    ctx: AppContext,
):
    process_input_id = ctx.process_input_facade.update_process_input(ctx.auth_user, process_input_id, process_input)

    return jsonify(process_input_id)


@process_input_blueprint.route("/<int:process_input_id>", methods=["DELETE"])
@login_required()
def delete_(
    process_input_id,
    request: Request,
    ctx: AppContext,
):
    process_input = ctx.process_input_facade.delete_process_input(ctx.auth_user, process_input_id)

    return jsonify(process_input) if process_input else error_response(404, "ProcessInput does not exist", [])


@process_input_blueprint.route("/import", methods=["POST"])
@login_required()
def import_process_inputs(
    request: Request,
    ctx: AppContext,
):
    process_inputs = []
    process_input_ids = ctx.process_input_facade.create_process_inputs(ctx.auth_user, process_inputs)

    return jsonify(process_input_ids)