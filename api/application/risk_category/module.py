from typing import Union

from injector import Module, provider, singleton
from sqlalchemy.orm import scoped_session

from domain.risk_category.ports.storage import RiskCategoryDatabase
from domain.risk_category.facade import RiskCategoryFacadeT, RiskCategoryFacade
from domain.risk_category.infrastructure.database import RiskCategoryDatabaseAdapter


class RiskCategoryModule(Module):
    @provider
    @singleton
    def risk_category_db(
        self,
        db: scoped_session,
    ) -> RiskCategoryDatabase:
        return RiskCategoryDatabaseAdapter(db)

    @provider
    @singleton
    def risk_category_facade(self, risk_category_db: RiskCategoryDatabase) -> RiskCategoryFacadeT:
        return RiskCategoryFacade(risk_category_db)