#!/usr/bin/env python3

from flask import Blueprint, Flask, Request, jsonify
from sqlalchemy.orm import scoped_session

from application.utils.request import AppContext
from domain.risk_category.models import (
    RiskCategory,
    RiskCategoryQueryParams,
    RiskCategoryUpdate,
    NewRiskCategory
)
from application.utils.middlewares import login_required, parse_request
from domain.risk_category.facade import RiskCategoryFacadeT
from application.utils.response import error_response


risk_category_blueprint = Blueprint("risk_categories", __name__)


@risk_category_blueprint.route("")
@login_required()
def get_all(
    request: Request,
    ctx: AppContext,
):
    query_params = RiskCategoryQueryParams()
    risk_categories = ctx.risk_category_facade.get_risk_categories(ctx.auth_user, query_params)

    return jsonify([risk_category.to_dict() for risk_category in risk_categories])


@risk_category_blueprint.route("", methods=["POST"])
@login_required()
@parse_request(NewRiskCategory, "risk_category")
def create(
    risk_category,
    request: Request,
    ctx: AppContext,
):
    risk_category_id = ctx.risk_category_facade.create_risk_category(ctx.auth_user, risk_category)

    return jsonify(risk_category_id)


@risk_category_blueprint.route("/<int:risk_category_id>")
@login_required()
def get_one(
    risk_category_id,
    request: Request,
    ctx: AppContext,
):
    risk_category = ctx.risk_category_facade.get_risk_category(ctx.auth_user, risk_category_id)

    return jsonify(risk_category.to_dict()) if risk_category else error_response(404, "RiskCategory does not exist", [])


@risk_category_blueprint.route("/<int:risk_category_id>", methods=["PUT"])
@login_required()
@parse_request(RiskCategoryUpdate, "risk_category")
def update(
    risk_category_id,
    risk_category,
    request: Request,
    ctx: AppContext,
):
    risk_category_id = ctx.risk_category_facade.update_risk_category(ctx.auth_user, risk_category_id, risk_category)

    return jsonify(risk_category_id)


@risk_category_blueprint.route("/<int:risk_category_id>", methods=["DELETE"])
@login_required()
def delete_(
    risk_category_id,
    request: Request,
    ctx: AppContext,
):
    risk_category = ctx.risk_category_facade.delete_risk_category(ctx.auth_user, risk_category_id)

    return jsonify(risk_category) if risk_category else error_response(404, "RiskCategory does not exist", [])


@risk_category_blueprint.route("/import", methods=["POST"])
@login_required()
def import_risk_categories(
    request: Request,
    ctx: AppContext,
):
    risk_categories = []
    risk_category_ids = ctx.risk_category_facade.create_risk_categories(ctx.auth_user, risk_categories)

    return jsonify(risk_category_ids)