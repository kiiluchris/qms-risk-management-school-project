from typing import Union

from injector import Module, provider, singleton
from sqlalchemy.orm import scoped_session

from domain.auth.ports.utils import PasswordEncryption, Jwt
from domain.auth.infrastructure.password import PasslibEncrypter
from domain.auth.infrastructure.jwt import JwtEncoder
from domain.auth.facade import AuthFacadeT, AuthFacade
from domain.auth.infrastructure.database import AuthDatabaseAdapter
from domain.auth.ports.outgoing import AuthDatabase
from domain.email.ports import EmailClient


class AuthModule(Module):
    @provider
    @singleton
    def password_encrypter(self) -> PasswordEncryption:
        return PasslibEncrypter()

    @provider
    @singleton
    def jwt_encoding(self) -> Jwt:
        return JwtEncoder()

    @provider
    @singleton
    def auth_db(
        self,
        db: scoped_session,
        password_encrypter: PasswordEncryption,
        jwt_encoding: Jwt,
    ) -> AuthDatabase:
        return AuthDatabaseAdapter(db, password_encrypter, jwt_encoding)

    @provider
    @singleton
    def auth_facade(
        self,
        auth_db: AuthDatabase,
        password_encrypter: PasswordEncryption,
        jwt_encoding: Jwt,
        email: EmailClient,
    ) -> AuthFacadeT:
        return AuthFacade(auth_db, password_encrypter, jwt_encoding, email)
