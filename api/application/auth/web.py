from typing import cast
from application.utils.request import AppContext
from flask import Blueprint, Flask, Request, jsonify
from injector import inject
from sqlalchemy.orm import scoped_session

from application.utils.middlewares import parse_request, login_required
from application.utils.response import error_response
from domain.auth.facade import AuthFacadeT
from domain.auth.infrastructure.database import AuthDatabaseAdapter
from domain.auth.ports.utils import Jwt
from domain.auth.models import (
    AccountRegistrationDetails,
    AuthUser,
    LoginCredentials,
    ResetPasswordDetails,
    ResetPasswordUser,
)
from domain.auth.models.errors import (
    ExpiredToken,
    InvalidPassword,
    LoginUserDoesNotExist,
    TokenDoesNotExist,
    SessionDoesNotExist,
)

auth_blueprint = Blueprint("auth", __name__)


@auth_blueprint.route("/organization/verify", methods=["POST"])
def verify_organization_exists(
    request: Request, auth_facade: AuthFacadeT, ctx: AppContext
):
    organization_domain = request.json
    organization_exists = auth_facade.verify_organization_exists(organization_domain)

    return jsonify(organization_exists)


@auth_blueprint.route("/register", methods=("POST",))
@parse_request(AccountRegistrationDetails, "details")
def register(details, request: Request, auth_facade: AuthFacadeT, ctx: AppContext):
    organization_id = auth_facade.register_account(details)
    return (
        (jsonify(organization_id), 201)
        if organization_id != -1
        else (
            jsonify(
                {
                    "error": "Invalid organization",
                    "messages": [["Organization already exists", "name"]],
                }
            ),
            409,
        )
    )


@auth_blueprint.route("/login", methods=("POST",))
@parse_request(LoginCredentials, "credentials")
def login(credentials, request: Request, auth_facade: AuthFacadeT, ctx: AppContext):
    try:
        print(credentials)
        session = auth_facade.sign_in(credentials)
        return jsonify(
            {
                "access_token": session.access_token,
                "refresh_token": session.refresh_token,
                "role": session.role,
            }
        )
    except LoginUserDoesNotExist as e:
        return error_response(400, "Invalid user", [[e.message, "email"]])

    except InvalidPassword as e:
        return error_response(400, "Invalid Password", [[str(e), "password"]])


@auth_blueprint.route("/refresh", methods=("POST",))
def refresh_access_token(request: Request, auth_facade: AuthFacadeT, ctx: AppContext):
    try:
        tokens = auth_facade.refresh_session(request.json)
        return jsonify(tokens)
    except ExpiredToken:
        return error_response(401, "Refresh token expired", [["expired"]])
    except TokenDoesNotExist:
        return error_response(401, "Token does not exist", [["not_found"]])


@auth_blueprint.route("/reset-password", methods=("POST",))
@parse_request(ResetPasswordUser, "details")
def request_password_reset(
    details, request: Request, auth_facade: AuthFacadeT, ctx: AppContext
):
    try:
        auth_facade.request_password_reset(details)
        return jsonify(True)
    except LoginUserDoesNotExist as e:
        return error_response(400, "Invalid user", [[e.message, "email"]])


@auth_blueprint.route("/reset-password/confirm", methods=("POST",))
@parse_request(ResetPasswordDetails, "details")
def confirm_password_reset(
    details, request: Request, auth_facade: AuthFacadeT, ctx: AppContext
):
    try:
        auth_facade.reset_password(details)
        return jsonify(True)
    except LoginUserDoesNotExist as e:
        return error_response(400, "Invalid user", [[e.message, "email"]])
    except SessionDoesNotExist:
        return error_response(400, "Session does not exist", [["Bad token", "token"]])


@auth_blueprint.route("/logout", methods=("POST",))
@login_required
def logout(
    request: Request,
    auth_facade: AuthFacadeT,
    jwt_encoding: Jwt,
    db_session: scoped_session,
    ctx: AppContext,
):
    try:
        auth_facade.sign_out(cast(AuthUser, ctx.auth_user))
        return jsonify(True)
    except SessionDoesNotExist:
        return error_response(400, "Session does not exist", [["Bad token", "token"]])
