#!/usr/bin/env python3

from domain.shared.models import MonitoringSchedule
from typing import cast
from application.utils.request import AppContext
from flask import Blueprint, Flask, Request, jsonify
from sqlalchemy.orm import scoped_session
import pandas

from domain.auth.ports.utils import Jwt
from domain.auth.models import AuthUser
from domain.process.models import (
    Process,
    ProcessQueryParams,
    ProcessUpdate,
    NewProcess,
    ProcessImport,
)
from application.utils.middlewares import get_upload_file, login_required, parse_request
from domain.process.facade import ProcessFacadeT
from application.utils.response import error_response


process_blueprint = Blueprint("processes", __name__)


@process_blueprint.route("")
@login_required()
def get_all(
    request: Request,
    ctx: AppContext,
):
    query_params = ProcessQueryParams()
    processes = ctx.process_facade.get_processes(
        cast(AuthUser, ctx.auth_user), query_params
    )

    return jsonify([process.to_dict() for process in processes])


@process_blueprint.route("", methods=["POST"])
@login_required()
@parse_request(NewProcess, "process")
def create(
    process,
    request: Request,
    ctx: AppContext,
):
    process_id = ctx.process_facade.create_process(
        cast(AuthUser, ctx.auth_user), process
    )

    return jsonify(process_id)


@process_blueprint.route("/<int:process_id>")
@login_required()
def get_one(
    process_id,
    request: Request,
    ctx: AppContext,
):
    process = ctx.process_facade.get_process(cast(AuthUser, ctx.auth_user), process_id)

    return (
        jsonify(process.to_dict())
        if process
        else error_response(404, "Process does not exist", [])
    )


@process_blueprint.route("/<int:process_id>", methods=["PUT"])
@login_required()
@parse_request(ProcessUpdate, "process")
def update(
    process_id,
    process,
    request: Request,
    ctx: AppContext,
):
    process_id = ctx.process_facade.update_process(
        cast(AuthUser, ctx.auth_user), process_id, process
    )

    return jsonify(process_id)


@process_blueprint.route("/<int:process_id>", methods=["DELETE"])
@login_required()
def delete_(
    process_id,
    request: Request,
    ctx: AppContext,
):
    process = ctx.process_facade.delete_process(
        cast(AuthUser, ctx.auth_user), process_id
    )

    return (
        jsonify(process)
        if process
        else error_response(404, "Process does not exist", [])
    )


@process_blueprint.route("/import", methods=["POST"])
@login_required()
@get_upload_file
def import_processes(
    file_,
    request: Request,
    ctx: AppContext,
):
    df = pandas.read_excel(
        file_,
        usecols=[
            "Name",
            "Monitoring Schedule",
            "Objective",
            "Team Responsible",
            "Inputs",
            "Outputs",
        ],
        keep_default_na=False,
    )
    processes = [
        ProcessImport(
            name=name.strip(),
            monitoring_schedule=MonitoringSchedule[schedule]
            if schedule in MonitoringSchedule.__members__
            else None,
            objective=obj.strip(),
            team_responsible=team_responsible.strip(),
            inputs=[line.strip() for line in inputs.strip().split("\n")],
            outputs=[line.strip() for line in outputs.strip().split("\n")],
        )
        for name, schedule, obj, team_responsible, inputs, outputs in df.values.tolist()
    ]
    process_ids = ctx.process_facade.import_processes(
        cast(AuthUser, ctx.auth_user), processes
    )

    return jsonify(process_ids)
