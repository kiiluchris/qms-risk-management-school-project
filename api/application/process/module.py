from domain.sms.ports import SmsClient
from domain.email.ports import EmailClient
from typing import Union

from domain.user.ports.storage import UserDatabase
from injector import Module, provider, singleton
from sqlalchemy.orm import scoped_session

from domain.process.ports.storage import ProcessDatabase
from domain.objective.ports.storage import ObjectiveDatabase
from domain.task_team.ports.storage import TaskTeamDatabase
from domain.process_value.ports.storage import ProcessValueDatabase
from domain.task.ports.storage import TaskDatabase
from domain.process.facade import ProcessFacadeT, ProcessFacade
from domain.process.infrastructure.database import ProcessDatabaseAdapter


class ProcessModule(Module):
    @provider
    @singleton
    def process_db(
        self,
        db: scoped_session,
        task_db: TaskDatabase,
        process_value_db: ProcessValueDatabase,
        user_db: UserDatabase,
        objective_db: ObjectiveDatabase,
        task_team_db: TaskTeamDatabase,
    ) -> ProcessDatabase:
        return ProcessDatabaseAdapter(
            db, task_db, process_value_db, user_db, objective_db, task_team_db
        )

    @provider
    @singleton
    def process_facade(
        self, process_db: ProcessDatabase, email: EmailClient, sms: SmsClient
    ) -> ProcessFacadeT:
        return ProcessFacade(process_db, email, sms)
