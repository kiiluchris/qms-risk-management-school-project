#!/usr/bin/env python3

from typing import cast
from application.utils.request import AppContext
from flask import Blueprint, Flask, Request, jsonify
from sqlalchemy.orm import scoped_session

from domain.auth.ports.utils import Jwt
from domain.auth.models import AuthUser
from domain.role.models import Role
from application.utils.middlewares import login_required, parse_request
from domain.role.facade import RoleFacadeT
from application.utils.response import error_response


role_blueprint = Blueprint("roles", __name__)


@role_blueprint.route("")
@login_required(allows_external=False)
def get_all(
    request: Request,
    ctx: AppContext,
):
    roles = ctx.role_facade.get_roles(cast(AuthUser, ctx.auth_user))

    return jsonify(roles)


@role_blueprint.route("", methods=["POST"])
@login_required(allows_external=False)
@parse_request(Role, "role")
def create(
    role,
    request: Request,
    ctx: AppContext,
):
    role_id = ctx.role_facade.create_role(cast(AuthUser, ctx.auth_user), role)

    return jsonify(role_id)


@role_blueprint.route("/<int:role_id>")
@login_required(allows_external=False)
def get_one(
    role_id,
    request: Request,
    ctx: AppContext,
):
    role = ctx.role_facade.get_role(cast(AuthUser, ctx.auth_user), role_id)

    return jsonify(role) if role else error_response(404, "Role does not exist", [])


@role_blueprint.route("/<int:role_id>", methods=["PUT"])
@login_required(allows_external=False)
@parse_request(Role, "role")
def update(
    role_id,
    role,
    request: Request,
    ctx: AppContext,
):
    role_id = ctx.role_facade.update_role(cast(AuthUser, ctx.auth_user), role_id, role)

    return jsonify(role_id)


@role_blueprint.route("/<int:role_id>", methods=["DELETE"])
@login_required(allows_external=False)
def delete_(
    role_id,
    request: Request,
    ctx: AppContext,
):
    role = ctx.role_facade.delete_role(cast(AuthUser, ctx.auth_user), role_id)

    return jsonify(role) if role else error_response(404, "Role does not exist", [])
