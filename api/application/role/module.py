from typing import Union

from injector import Module, provider, singleton
from sqlalchemy.orm import scoped_session

from domain.role.ports.storage import RoleDatabase
from domain.role.facade import RoleFacadeT, RoleFacade
from domain.role.infrastructure.database import RoleDatabaseAdapter


class RoleModule(Module):
    @provider
    @singleton
    def role_db(
        self,
        db: scoped_session,
    ) -> RoleDatabase:
        return RoleDatabaseAdapter(db)

    @provider
    @singleton
    def role_facade(self, role_db: RoleDatabase) -> RoleFacadeT:
        return RoleFacade(role_db)
