import graphene
from graphene import relay, Int
from graphene_sqlalchemy import (
    SQLAlchemyObjectType,
    SQLAlchemyConnectionField,
)
from graphene_sqlalchemy.converter import convert_sqlalchemy_type
from flask import Flask
from flask_graphql import GraphQLView
from sqlalchemy import Integer, SmallInteger

from infrastructure.models import *


@convert_sqlalchemy_type.register(SmallInteger)
@convert_sqlalchemy_type.register(Integer)
def convert_column_to_int_or_id(type, column, registry=None):
    return Int


class Organization(SQLAlchemyObjectType):
    class Meta:
        model = OrganizationModel
        interfaces = (relay.Node,)


class Role(SQLAlchemyObjectType):
    class Meta:
        model = RoleModel
        interfaces = (relay.Node,)


class User(SQLAlchemyObjectType):
    class Meta:
        model = UserModel
        interfaces = (relay.Node,)


class Session(SQLAlchemyObjectType):
    class Meta:
        model = SessionModel
        interfaces = (relay.Node,)


class Unit(SQLAlchemyObjectType):
    class Meta:
        model = UnitModel
        interfaces = (relay.Node,)


class UnitType(SQLAlchemyObjectType):
    class Meta:
        model = UnitTypeModel
        interfaces = (relay.Node,)


class Risk(SQLAlchemyObjectType):
    class Meta:
        model = RiskModel
        interfaces = (relay.Node,)


class Capa(SQLAlchemyObjectType):
    class Meta:
        model = CapaModel
        interfaces = (relay.Node,)


class Incident(SQLAlchemyObjectType):
    class Meta:
        model = IncidentModel
        interfaces = (relay.Node,)


class TaskTeam(SQLAlchemyObjectType):
    class Meta:
        model = TaskTeamModel
        interfaces = (relay.Node,)


class Objective(SQLAlchemyObjectType):
    class Meta:
        model = ObjectiveModel
        interfaces = (relay.Node,)


class Process(SQLAlchemyObjectType):
    class Meta:
        model = ProcessModel
        interfaces = (relay.Node,)


class Task(SQLAlchemyObjectType):
    class Meta:
        model = TaskModel
        interfaces = (relay.Node,)


class SubTask(SQLAlchemyObjectType):
    class Meta:
        model = SubTaskModel
        interfaces = (relay.Node,)


class ProcessInput(SQLAlchemyObjectType):
    class Meta:
        model = ProcessInputModel
        interfaces = (relay.Node,)


# class ProcessInputSource(SQLAlchemyObjectType):
#     class Meta:
#         model = ProcessInputSourceModel
#         interfaces = (relay.Node,)


class ProcessOutput(SQLAlchemyObjectType):
    class Meta:
        model = ProcessOutputModel
        interfaces = (relay.Node,)


# class ProcessOutputReceiver(SQLAlchemyObjectType):
#     class Meta:
#         model = ProcessOutputReceiverModel
#         interfaces = (relay.Node,)


class Query(graphene.ObjectType):
    node = relay.Node.Field()
    # # Allows sorting over multiple columns, by default over the primary key
    # all_employees = SQLAlchemyConnectionField(User.connection)
    # # Disable sorting over this field
    # all_departments = SQLAlchemyConnectionField(Role.connection, sort=None)
    # all_process_outputs = SQLAlchemyConnectionField(ProcessOutput.connection)

    organizations = SQLAlchemyConnectionField(Organization.connection)
    roles = SQLAlchemyConnectionField(Role.connection)
    users = SQLAlchemyConnectionField(User.connection)
    sessions = SQLAlchemyConnectionField(Session.connection)
    units = SQLAlchemyConnectionField(Unit.connection)
    unit_types = SQLAlchemyConnectionField(UnitType.connection)
    risks = SQLAlchemyConnectionField(Risk.connection)
    capas = SQLAlchemyConnectionField(Capa.connection)
    incidents = SQLAlchemyConnectionField(Incident.connection)
    task_teams = SQLAlchemyConnectionField(TaskTeam.connection)
    objectives = SQLAlchemyConnectionField(Objective.connection)
    processes = SQLAlchemyConnectionField(Process.connection)
    tasks = SQLAlchemyConnectionField(Task.connection)
    sub_tasks = SQLAlchemyConnectionField(SubTask.connection)
    process_inputs = SQLAlchemyConnectionField(ProcessInput.connection)
    # process_input_sources = SQLAlchemyConnectionField(ProcessInputSource.connection)
    process_outputs = SQLAlchemyConnectionField(ProcessOutput.connection)
    # process_output_receivers = SQLAlchemyConnectionField(
    #     ProcessOutputReceiver.connection
    # )


schema = graphene.Schema(query=Query)


def setup_graphene(app: Flask):
    app.add_url_rule(
        "/graphql",
        view_func=GraphQLView.as_view(
            "graphql", schema=schema, graphiql=True  # for having the GraphiQL interface
        ),
    )
