import dataclasses
from domain.process.models import ProcessQueryParams
from domain.user.models import UserQueryParams
from domain.task.models import TaskQueryParams
from domain.task_team.models import TaskTeamQueryParams
from domain.objective.models import ObjectiveQueryParams
from domain.incident.models import IncidentQueryParams
from domain.risk.models import RiskQueryParams
from domain.capa.models import CapaQueryParams
from domain.process_input.models import ProcessInputQueryParams
from domain.process_output.models import ProcessOutputQueryParams
from domain.process_value.models import ProcessValueQueryParams
from domain.risk_category.models import RiskCategoryQueryParams
from domain.objective.models import ObjectiveQueryParams
from functools import wraps
from sqlalchemy.orm import scoped_session
from domain.auth.ports.utils import Jwt
from application.utils.request import AppContext
from dataclasses import dataclass
from os import path
from ariadne import (
    load_schema_from_path,
    ObjectType,
    graphql_sync,
    make_executable_schema,
    EnumType,
    ScalarType,
)
from ariadne.constants import PLAYGROUND_HTML
from ariadne.types import Resolver
from graphql.type import GraphQLResolveInfo
from flask import Flask, Request, jsonify
from domain.auth.models import AuthUser
from domain.shared.models import (
    CapaRating,
    CapaClassification,
    MonitoringSchedule,
    ObjectiveStatus,
    RiskStatus,
)
from application.utils.middlewares import login_required
from typing import Optional, cast


def attr_null_check(prop: str):
    def wrapper(f) -> Resolver:
        @wraps(f)
        def go(obj, info, **kwargs):
            if getattr(obj, prop, None) is None:
                return None
            return f(obj, info, **kwargs)

        return go

    return wrapper


type_defs = load_schema_from_path(path.join(path.dirname(__file__), "schema.graphql"))
date_scalar = ScalarType("Date")
datetime_scalar = ScalarType("DateTime")
model_analysis_scalar = ScalarType("ItemAnalysis")
monitoring_schedule = EnumType("MonitoringSchedule", MonitoringSchedule)
objective_status = EnumType("ObjectiveStatus", ObjectiveStatus)
capa_type = EnumType("CapaClassification", CapaClassification)
capa_rating = EnumType("CapaRating", CapaRating)
risk_status = EnumType("RiskStatus", RiskStatus)
query = ObjectType("Query")
user_query = ObjectType("User")
objective_query = ObjectType("Objective")
process_query = ObjectType("Process")
task_query = ObjectType("Task")
incident_query = ObjectType("Incident")
capa_query = ObjectType("Capa")
risk_query = ObjectType("Risk")
process_input_query = ObjectType("ProcessInput")
process_output_query = ObjectType("ProcessOutput")
process_value_query = ObjectType("ProcessValue")
task_team_query = ObjectType("TaskTeam")
risk_category_query = ObjectType("RiskCategory")


@date_scalar.serializer
def date_serializer(value):
    return value.isoformat()


@datetime_scalar.serializer
def datetime_serializer(value):
    return value.isoformat()


@query.field("role")
def resolve_role(_, info: GraphQLResolveInfo, *, id):
    ctx: AppContext = info.context
    return ctx.role_facade.get_role(cast(AuthUser, ctx.auth_user), id)


@query.field("roles")
def resolve_roles(_, info):
    ctx: AppContext = info.context
    return ctx.role_facade.get_roles(cast(AuthUser, ctx.auth_user))


@query.field("user")
def resolve_user(_, info, *, id):
    ctx: AppContext = info.context
    return ctx.user_facade.get_user(cast(AuthUser, ctx.auth_user), id)


@query.field("account")
def resolve_account(_, info):
    user_id = cast(AuthUser, info.context.auth_user).user_id
    return resolve_user(None, info, id=user_id)


@query.field("users")
def resolve_users(_, info, *, team_id=None, is_manager=None, has_team=None):
    ctx: AppContext = info.context
    return ctx.user_facade.get_users(
        cast(AuthUser, ctx.auth_user),
        UserQueryParams(team_id=team_id, is_manager=is_manager, has_team=has_team),
    )


@user_query.field("role")
def resolve_user_role(obj, info):
    return resolve_role(None, info, id=obj.role.id)


@query.field("objective")
def resolve_objective(_, info, *, id):
    ctx: AppContext = info.context
    return ctx.objective_facade.get_objective(cast(AuthUser, ctx.auth_user), id)


@query.field("objectives")
def resolve_objectives(_, info):
    ctx: AppContext = info.context
    return ctx.objective_facade.get_objectives(
        cast(AuthUser, ctx.auth_user), ObjectiveQueryParams()
    )


@objective_query.field("person_responsible")
@process_query.field("person_responsible")
@attr_null_check("person_responsible")
def resolve_objective_person_responsible(obj, info):
    return resolve_user(None, info, id=obj.person_responsible.id)


@objective_query.field("processes")
def resolve_objective_processes(obj, info):
    return resolve_processes(None, info, objective_id=obj.id)


@query.field("process")
def resolve_process(_, info, *, id):
    ctx: AppContext = info.context
    return ctx.process_facade.get_process(cast(AuthUser, ctx.auth_user), id)


@query.field("processes")
def resolve_processes(
    _,
    info,
    *,
    input_value_id=None,
    output_value_id=None,
    incident_id=None,
    risk_id=None,
    objective_id=None
):
    ctx: AppContext = info.context
    return ctx.process_facade.get_processes(
        cast(AuthUser, ctx.auth_user),
        ProcessQueryParams(
            input_value_id=input_value_id,
            output_value_id=output_value_id,
            incident_id=incident_id,
            risk_id=risk_id,
            objective_id=objective_id,
        ),
    )


@process_query.field("objective")
@attr_null_check("objective")
def resolve_process_objective(obj, info):
    return resolve_objective(None, info, id=obj.objective.id)


# @process_query.field("inputs")
# def resolve_process_inputs_(obj, info):
#     return resolve_process_values(None, info, input_process_id=obj.id)


# @process_query.field("outputs")
# def resolve_process_outputs_(obj, info):
#     return resolve_process_values(None, info, output_process_id=obj.id)


@process_query.field("potential_risks")
def process_potential_risks(obj, info):
    ctx: AppContext = info.context
    return ctx.process_facade.possible_risks_for_process_inputs(
        cast(AuthUser, ctx.auth_user), obj.id
    )


@process_query.field("known_risks")
def process_known_risks(obj, info):
    ctx: AppContext = info.context
    return ctx.process_facade.known_risks_for_process(
        cast(AuthUser, ctx.auth_user), obj.id
    )


@process_query.field("team_responsible")
def resolve_process_team_responsible(obj, info):
    return resolve_task_team(None, info, id=obj.team_responsible.id)


@query.field("task")
def resolve_task(_, info, *, id):
    ctx: AppContext = info.context
    return ctx.task_facade.get_task(cast(AuthUser, ctx.auth_user), id)


@query.field("tasks")
def resolve_tasks(
    _, info, *, pending: Optional[bool] = None, current_user: Optional[bool] = None
):
    ctx: AppContext = info.context
    auth_user = cast(AuthUser, ctx.auth_user)
    qp = TaskQueryParams(is_complete=not pending if pending is not None else None)
    if current_user is not None:
        qp = dataclasses.replace(qp, user_id=auth_user.user_id)
    return ctx.task_facade.get_tasks(auth_user, qp)


@task_query.field("user_owner")
@attr_null_check("user_owner")
def resolver_task_user_owner(obj, info):
    return resolve_user(None, info, id=obj.user_owner.id)


@task_query.field("process")
@attr_null_check("process")
def resolve_task_process(obj, info):
    return resolve_process(None, info, id=obj.process.id)


@query.field("process_value")
def resolve_process_value(_, info, *, id):
    ctx: AppContext = info.context
    return ctx.process_value_facade.get_process_value(cast(AuthUser, ctx.auth_user), id)


@query.field("process_values")
def resolve_process_values(
    _, info, *, risk_id=None, input_process_id=None, output_process_id=None
):
    ctx: AppContext = info.context
    return ctx.process_value_facade.get_process_values(
        cast(AuthUser, ctx.auth_user),
        ProcessValueQueryParams(
            risk_id=risk_id,
            input_process_id=input_process_id,
            output_process_id=output_process_id,
        ),
    )


@process_value_query.field("possible_risks")
def resolve_process_value_possible_risks(obj, info):
    return resolve_risks(None, info, process_value_id=obj.id)


@process_value_query.field("input_processes")
def resolve_process_value_input_processes(obj, info):
    return resolve_processes(None, info, input_value_id=obj.id)


@process_value_query.field("output_processes")
def resolve_process_value_output_processes(obj, info):
    return resolve_processes(None, info, output_value_id=obj.id)


@query.field("process_input")
def resolve_process_input(_, info, *, id):
    ctx: AppContext = info.context
    return ctx.process_input_facade.get_process_input(cast(AuthUser, ctx.auth_user), id)


@query.field("process_inputs")
def resolve_process_inputs(_, info, *, risk_id=None):
    ctx: AppContext = info.context
    return ctx.process_input_facade.get_process_inputs(
        cast(AuthUser, ctx.auth_user), ProcessInputQueryParams(risk_id=risk_id)
    )


@query.field("process_output")
def resolve_process_output(_, info, *, id):
    ctx: AppContext = info.context
    return ctx.process_output_facade.get_process_output(
        cast(AuthUser, ctx.auth_user), id
    )


@query.field("process_outputs")
def resolve_process_outputs(_, info, *, risk_id=None):
    ctx: AppContext = info.context
    return ctx.process_output_facade.get_process_outputs(
        cast(AuthUser, ctx.auth_user), ProcessOutputQueryParams(risk_id=risk_id)
    )


@query.field("risk")
def resolve_risk(_, info, *, id):
    ctx: AppContext = info.context
    return ctx.risk_facade.get_risk(cast(AuthUser, ctx.auth_user), id)


@query.field("risks")
def resolve_risks(_, info, *, process_value_id=None, capa_id=None):
    ctx: AppContext = info.context
    return ctx.risk_facade.get_risks(
        cast(AuthUser, ctx.auth_user),
        RiskQueryParams(process_value_id=process_value_id, capa_id=capa_id),
    )


@risk_query.field("affected_processes")
def resolve_risk_affected_processes(obj, info):
    return resolve_processes(None, info, risk_id=obj.id)


@risk_query.field("category")
def resolve_risk_category_of_risk(obj, info):
    return resolve_risk_category(None, info, id=obj.category.id)


@risk_query.field("preventive_capas")
def resolve_risk_preventive_capas(obj, info):
    return resolve_capas(None, info, risk_id=obj.id)


@risk_query.field("suggested_capas")
def resolve_risk_suggested_capas(obj, info):
    ctx: AppContext = info.context
    return ctx.risk_facade.get_suggested_capas(cast(AuthUser, ctx.auth_user), obj)


# @risk_query.field("affected_inputs")
# def resolve_risk_affected_inputs(obj, info):
#     return resolve_process_inputs(None, info, risk_id=obj.id)


# @risk_query.field("affected_outputs")
# def resolve_risk_affected_outputs(obj, info):
#     return resolve_process_outputs(None, info, risk_id=obj.id)


# @risk_query.field("affected_values")
# def resolve_risk_affected_values(obj, info):
#     return resolve_process_values(None, info, risk_id=obj.id)


@query.field("risk_category")
def resolve_risk_category(_, info, *, id):
    ctx: AppContext = info.context
    return ctx.risk_category_facade.get_risk_category(cast(AuthUser, ctx.auth_user), id)


@query.field("risk_categories")
def resolve_risk_categories(_, info):
    ctx: AppContext = info.context
    return ctx.risk_category_facade.get_risk_categories(
        cast(AuthUser, ctx.auth_user), RiskCategoryQueryParams()
    )


@query.field("incident")
def resolve_incident(_, info, *, id):
    ctx: AppContext = info.context
    return ctx.incident_facade.get_incident(cast(AuthUser, ctx.auth_user), id)


@query.field("incidents")
def resolve_incidents(_, info):
    ctx: AppContext = info.context
    return ctx.incident_facade.get_incidents(
        cast(AuthUser, ctx.auth_user), IncidentQueryParams()
    )


@incident_query.field("risk")
@attr_null_check("risk")
def resolve_incident_risk(obj, info):
    return resolve_risk(None, info, id=obj.risk.id)


@incident_query.field("reported_by")
@attr_null_check("reported_by")
def resolve_incident_reported_by(obj, info):
    return resolve_user(None, info, id=obj.reported_by.id)


@incident_query.field("person_responsible")
@attr_null_check("person_responsible")
def resolve_incident_person_responsible(obj, info):
    return resolve_user(None, info, id=obj.person_responsible.id)


@incident_query.field("first_aid_provided_by")
@attr_null_check("first_aid_provided_by")
def resolve_incident_first_aid_provided_by(obj, info):
    return resolve_user(None, info, id=obj.first_aid_provided_by.id)


@incident_query.field("affected_processes")
@attr_null_check("affected_processes")
def resolve_incident_affected_processes(obj, info):
    return resolve_processes(None, info, incident_id=obj.id)


@incident_query.field("category")
@capa_query.field("category")
@attr_null_check("category")
def resolve_risk_category_(obj, info):
    return resolve_risk_category(None, info, id=obj.category.id)


@query.field("capa")
def resolve_capa(_, info, *, id):
    ctx: AppContext = info.context
    return ctx.capa_facade.get_capa(cast(AuthUser, ctx.auth_user), id)


@query.field("capas")
def resolve_capas(_, info, *, risk_id=None):
    ctx: AppContext = info.context
    return ctx.capa_facade.get_capas(
        cast(AuthUser, ctx.auth_user), CapaQueryParams(risk_id=risk_id)
    )


@capa_query.field("approved_by")
@attr_null_check("approved_by")
def resolve_capa_approved_by(obj, info):
    return resolve_user(None, info, id=obj.approved_by.id)


@capa_query.field("prepared_by")
@attr_null_check("prepared_by")
def resolve_capa_prepared_by(obj, info):
    return resolve_user(None, info, id=obj.prepared_by.id)


@capa_query.field("incident")
@attr_null_check("incident_id")
def resolve_capa_incident(obj, info):
    return resolve_incident(None, info, id=obj.incident.id)


@capa_query.field("risks_to_prevent")
def resolve_capa_risks_to_prevent(obj, info):
    return resolve_risks(None, info, capa_id=obj.id)


@query.field("task_team")
def resolve_task_team(_, info, *, id):
    ctx: AppContext = info.context
    return ctx.task_team_facade.get_task_team(cast(AuthUser, ctx.auth_user), id)


@query.field("task_teams")
def resolve_task_teams(_, info):
    ctx: AppContext = info.context
    return ctx.task_team_facade.get_task_teams(
        cast(AuthUser, ctx.auth_user), TaskTeamQueryParams()
    )


@task_team_query.field("members")
def resolve_task_team_members(obj, info):
    return resolve_users(None, info, team_id=obj.id)


@task_team_query.field("leader")
def resolve_task_team_leader(obj, info):
    return resolve_user(None, info, id=obj.leader.id)


@query.field("model_metrics")
def resolve_model_metrics(_, info):
    ctx: AppContext = info.context
    return ctx.risk_facade.get_modelling_data(cast(AuthUser, ctx.auth_user))


@query.field("risk_graph_groups")
def resolve_risk_graph_groups(_, info):
    ctx: AppContext = info.context
    return ctx.risk_facade.get_dashboard_risk_data(cast(AuthUser, ctx.auth_user))


@query.field("settings")
def resolve_settings(_, info):
    ctx: AppContext = info.context
    return ctx.settings_facade.get_settings(cast(AuthUser, ctx.auth_user))


@query.field("ml_models")
def resolve_ml_models(_, info):
    ctx: AppContext = info.context
    return ctx.risk_facade.get_ml_models(cast(AuthUser, ctx.auth_user))


@query.field("reports")
def resolve_reports(_, info):
    ctx: AppContext = info.context
    return ctx.report_facade.get_reports(cast(AuthUser, ctx.auth_user))


@query.field("static_model_analysis")
def resolve_static_model_analysis(_, info):
    ctx: AppContext = info.context
    return ctx.report_facade.static_model_analysis(cast(AuthUser, ctx.auth_user))


@query.field("possible_leaders")
def resolve_possible_leaders(_, info):
    return resolve_users(None, info, is_manager=True, has_team=False)


schema = make_executable_schema(
    type_defs,
    monitoring_schedule,
    objective_status,
    capa_type,
    capa_rating,
    risk_status,
    date_scalar,
    datetime_scalar,
    model_analysis_scalar,
    query,
    user_query,
    objective_query,
    process_query,
    task_query,
    incident_query,
    capa_query,
    risk_query,
    process_input_query,
    process_output_query,
    process_value_query,
    task_team_query,
    risk_category_query,
)


def setup_graphql(app: Flask):
    @app.route("/graphql", methods=["GET"])
    def graphql_playground():
        # On GET request serve GraphQL Playground
        # You don't need to provide Playground if you don't want to
        # but keep on mind this will not prohibit clients from
        # exploring your API using desktop GraphQL Playground app.
        return PLAYGROUND_HTML, 200

    @app.route("/graphql", methods=["POST"])
    @login_required()
    def graphql_server(request: Request, ctx: AppContext):
        # GraphQL queries are always sent as POST
        data = request.get_json()

        # Note: Passing the request to the context is optional.
        # In Flask, the current request is always accessible as flask.request
        success, result = graphql_sync(
            schema,
            data,
            context_value=ctx,
            debug=app.debug,
        )

        status_code = 200 if success else 400
        return jsonify(result), status_code
