from dataclasses import dataclass
from typing import Optional

from flask.wrappers import Request
from injector import Module, provider, singleton
from sqlalchemy.orm import scoped_session

from domain.auth.models import AuthUser
from domain.auth.ports.utils import Jwt
from domain.role.facade import RoleFacadeT
from domain.user.facade import UserFacadeT
from domain.objective.facade import ObjectiveFacadeT
from domain.process.facade import ProcessFacadeT
from domain.task.facade import TaskFacadeT
from domain.incident.facade import IncidentFacadeT
from domain.risk_category.facade import RiskCategoryFacadeT
from domain.capa.facade import CapaFacadeT
from domain.task_team.facade import TaskTeamFacadeT
from domain.process_input.facade import ProcessInputFacadeT
from domain.process_output.facade import ProcessOutputFacadeT
from domain.process_value.facade import ProcessValueFacadeT
from domain.risk.facade import RiskFacadeT
from domain.settings.facade import SettingsFacadeT
from domain.report.facade import ReportFacadeT


@dataclass(frozen=True)
class AppContext:
    user_facade: UserFacadeT
    role_facade: RoleFacadeT
    objective_facade: ObjectiveFacadeT
    task_team_facade: TaskTeamFacadeT
    task_facade: TaskFacadeT
    process_facade: ProcessFacadeT
    incident_facade: IncidentFacadeT
    capa_facade: CapaFacadeT
    risk_facade: RiskFacadeT
    settings_facade: SettingsFacadeT
    process_value_facade: ProcessValueFacadeT
    process_output_facade: ProcessOutputFacadeT
    process_input_facade: ProcessInputFacadeT
    risk_category_facade: RiskCategoryFacadeT
    report_facade: ReportFacadeT
    jwt_encoding: Jwt
    db_session: scoped_session
    auth_user: Optional[AuthUser] = None


class AppContextModule(Module):
    @provider
    @singleton
    def app_context(
        self,
        user_facade: UserFacadeT,
        role_facade: RoleFacadeT,
        objective_facade: ObjectiveFacadeT,
        task_facade: TaskFacadeT,
        task_team_facade: TaskTeamFacadeT,
        process_facade: ProcessFacadeT,
        jwt_encoding: Jwt,
        db_session: scoped_session,
        incident_facade: IncidentFacadeT,
        capa_facade: CapaFacadeT,
        risk_facade: RiskFacadeT,
        settings_facade: SettingsFacadeT,
        process_output_facade: ProcessOutputFacadeT,
        process_value_facade: ProcessValueFacadeT,
        process_input_facade: ProcessInputFacadeT,
        risk_category_facade: RiskCategoryFacadeT,
        report_facade: ReportFacadeT,
    ) -> AppContext:
        return AppContext(
            user_facade=user_facade,
            role_facade=role_facade,
            report_facade=report_facade,
            jwt_encoding=jwt_encoding,
            db_session=db_session,
            objective_facade=objective_facade,
            task_facade=task_facade,
            process_facade=process_facade,
            task_team_facade=task_team_facade,
            incident_facade=incident_facade,
            risk_category_facade=risk_category_facade,
            capa_facade=capa_facade,
            risk_facade=risk_facade,
            settings_facade=settings_facade,
            process_value_facade=process_value_facade,
            process_output_facade=process_output_facade,
            process_input_facade=process_input_facade,
        )
