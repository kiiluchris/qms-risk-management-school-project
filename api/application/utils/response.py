from typing import List

from flask import jsonify
from flask.wrappers import Response


def error_response(
    status: int, error: str, messages: List[List[str]]
) -> tuple[Response, int]:
    return jsonify(dict(error=error, messages=messages)), status
