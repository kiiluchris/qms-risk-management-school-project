#!/usr/bin/env python3

from application.utils.functions import allowed_file
from application.utils.request import AppContext
from functools import wraps, partial
from typing import Optional, Tuple, TypeVar, Callable, List, cast, Callable, Any

from sqlalchemy.orm import scoped_session
from flask import Request
import dataclasses

from domain.auth.ports.utils import Jwt
from infrastructure.models import OrganizationModel, SessionModel, UserModel
from .response import error_response
from .protocols import IsDataclass
from domain.auth.models import AuthUser


def non_optional_fields(klass: IsDataclass):
    class_fields = dataclasses.fields(klass)
    for field in class_fields:
        if not (
            hasattr(field.type, "__args__")
            and len(field.type.__args__) == 2
            and field.type.__args__[-1] is type(None)
        ):
            # Check if exactly two arguments exists and one of them are None type
            yield field.name


def find_missing_json_keys(datacls: IsDataclass, data):
    if not data:
        data = {}
    return [field for field in non_optional_fields(datacls) if field not in data]


T = TypeVar("T")


def list_partition(
    predicate: Callable[[T], bool], xs: List[T]
) -> Tuple[List[T], List[T]]:
    matching, not_matching = [], []
    for x in xs:
        if predicate(x):
            matching.append(x)
        else:
            not_matching.append(x)
    return matching, not_matching


def parse_request(datacls: IsDataclass, name: str, is_list: bool = False):
    def wrapper(f):
        @wraps(f)
        def handler(request: Request, *args, **kwargs):
            body = request.json
            if not is_list:  # Parse single item
                missing_keys = find_missing_json_keys(datacls, body)
                # print(body, datacls, missing_keys)
                if missing_keys:
                    return error_response(
                        400,
                        "Missing keys in request",
                        [[f'"{key}" is required', key] for key in missing_keys],
                    )
                data = datacls.from_dict(body, infer_missing=True)
                kwargs[name] = data
                return f(request=request, *args, **kwargs)
            else:
                valid_items, invalid_items = list_partition(
                    lambda item: not find_missing_json_keys(datacls, item),
                    body,
                )
                if not valid_items:
                    return error_response(
                        400,
                        "Items are badly formatted, none match the expected format",
                        [
                            ['"{key}" is required', key]
                            for key in find_missing_json_keys(
                                cast(IsDataclass, invalid_items[0])
                            )
                        ],
                    )
                kwargs[name] = [
                    datacls.from_dict(item, infer_missing=True) for item in valid_items
                ]
                return f(request=request, *args, **kwargs)

        return handler

    return wrapper


def login_required(
    f: Optional[Callable[..., Any]] = None,
    *,
    role: Optional[str] = None,
    allows_external: bool = True,
) -> Callable[..., Any]:
    if not f:
        return partial(login_required, role=role)

    @wraps(f)
    def handler(request: Request, *args, ctx: AppContext, **kwargs):
        # print("Login Required: role = ", role)

        token = request.headers.get("Authorization", None)
        # print("T", token)
        if not token:
            return error_response(401, "Request is unauthorized", [["unauthorized"]])

        if token.startswith("Bearer "):
            token = token.replace("Bearer ", "")
            try:
                auth_info = ctx.jwt_encoding.parse_token(token)
                print("Parsed Token", auth_info)
                if not auth_info:
                    return error_response(
                        401, "Request token is expired", [["expired"]]
                    )
            except:
                return error_response(401, "Request token is invalid", [["invalid"]])
            # print("AI", auth_info)
            result = (
                ctx.db_session.query(UserModel, OrganizationModel)
                .join(OrganizationModel)
                .join(SessionModel)
                .filter(
                    UserModel.email == auth_info["e"],
                    OrganizationModel.domain == auth_info["o"],
                    SessionModel.access_token == token,
                    UserModel.is_verified == True,
                )
                .first()
            )
            if not result:
                return error_response(
                    401, "Session does not exist", [["does_not_exist"]]
                )
            user, organization = result
            auth_user = AuthUser(
                email=auth_info["e"],
                organization=auth_info["o"],
                user_id=user.id,
                organization_id=organization.id,
            )
            # print(user, organization)

            return f(
                request=request,
                ctx=dataclasses.replace(ctx, auth_user=auth_user),
                *args,
                **kwargs,
            )

        elif token.startswith("Token ") and allows_external:
            token = token.replace("Token ", "")
            try:
                organization = (
                    ctx.db_session.query(OrganizationModel)
                    .filter(OrganizationModel.api_token == token)
                    .first()
                )
                print("ORG", organization)
                if not organization:
                    return error_response(
                        401, "Organization does not exist", [["does_not_exist"]]
                    )
                secret = organization.api_token_secret
                auth_info = ctx.jwt_encoding.parse_token(token, secret=secret)
                print("Parsed Token", auth_info)
                if not auth_info:
                    return error_response(
                        401, "Request token is expired", [["expired"]]
                    )
            except Exception as e:
                print(e)
                return error_response(401, "Request token is invalid", [["invalid"]])
            auth_user = AuthUser(
                email="",
                organization=auth_info["o"],
                user_id=-1,
                organization_id=organization.id,
            )
            # print(user, organization)

            return f(
                request=request,
                ctx=dataclasses.replace(ctx, auth_user=auth_user),
                *args,
                **kwargs,
            )
        else:
            return error_response(401, "Wrong authentication type", [["auth_type"]])

    return handler


admin_login_required = login_required(role="Admin")


def get_upload_file(f):
    @wraps(f)
    def handler(request: Request, *args, **kwargs):
        file_ = request.files.get("file")
        if not file_:
            return error_response(400, "Filepart not provided", [])
        if file_.filename == "":
            return error_response(400, "No file provided", [])
        if not (file_ and allowed_file(file_.filename)):
            return error_response(400, "Invalid file type", [])
        return f(request=request, file_=file_, *args, **kwargs)

    return handler
