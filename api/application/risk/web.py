#!/usr/bin/env python3

import pandas
from typing import cast
from flask import Blueprint, Flask, Request, jsonify
from sqlalchemy.orm import scoped_session

from application.utils.request import AppContext
from domain.risk.models import (
    Risk,
    RiskQueryParams,
    RiskUpdate,
    NewRisk,
    RiskImport,
)
from domain.auth.models import AuthUser
from application.utils.middlewares import get_upload_file, login_required, parse_request
from domain.risk.facade import RiskFacadeT
from application.utils.response import error_response


risk_blueprint = Blueprint("risks", __name__)


@risk_blueprint.route("")
@login_required()
def get_all(
    request: Request,
    ctx: AppContext,
):
    query_params = RiskQueryParams()
    risks = ctx.risk_facade.get_risks(cast(AuthUser, ctx.auth_user), query_params)

    return jsonify([risk.to_dict() for risk in risks])


@risk_blueprint.route("", methods=["POST"])
@login_required()
@parse_request(NewRisk, "risk")
def create(
    risk,
    request: Request,
    ctx: AppContext,
):
    risk_id = ctx.risk_facade.create_risk(cast(AuthUser, ctx.auth_user), risk)

    return jsonify(risk_id)


@risk_blueprint.route("/<int:risk_id>")
@login_required()
def get_one(
    risk_id,
    request: Request,
    ctx: AppContext,
):
    risk = ctx.risk_facade.get_risk(cast(AuthUser, ctx.auth_user), risk_id)

    return (
        jsonify(risk.to_dict())
        if risk
        else error_response(404, "Risk does not exist", [])
    )


@risk_blueprint.route("/<int:risk_id>", methods=["PUT"])
@login_required()
@parse_request(RiskUpdate, "risk")
def update(
    risk_id,
    risk,
    request: Request,
    ctx: AppContext,
):
    risk_id = ctx.risk_facade.update_risk(cast(AuthUser, ctx.auth_user), risk_id, risk)

    return jsonify(risk_id)


@risk_blueprint.route("/<int:risk_id>", methods=["DELETE"])
@login_required()
def delete_(
    risk_id,
    request: Request,
    ctx: AppContext,
):
    risk = ctx.risk_facade.delete_risk(cast(AuthUser, ctx.auth_user), risk_id)

    return jsonify(risk) if risk else error_response(404, "Risk does not exist", [])


@risk_blueprint.route("/import", methods=["POST"])
@login_required()
@get_upload_file
def import_risks(
    file_,
    request: Request,
    ctx: AppContext,
):
    df = pandas.read_excel(
        file_,
        usecols=[
            "Category",
            "Risk Owner",
            "Likelihood",
            "Impact",
            "Name",
            "Description",
            "Actions",
            "Affected Processes",
        ],
        keep_default_na=False,
    )
    risks = [
        RiskImport(
            category=categ.strip(),
            likelihood=int(likeli),
            impact=int(impact),
            name=name.strip(),
            description=desc.strip(),
            affected_processes=[proc.strip() for proc in affected.split("\n")],
            recommended_actions=act.strip(),
            risk_owner=own.strip(),
        )
        for categ, own, likeli, impact, name, desc, act, affected in df.values.tolist()
    ]
    risk_ids = ctx.risk_facade.import_risks(cast(AuthUser, ctx.auth_user), risks)

    return jsonify(risk_ids)
