from domain.user.ports.storage import UserDatabase
from domain.settings.ports.storage import SettingsDatabase
from domain.process.ports.storage import ProcessDatabase
from typing import Union

from injector import Module, provider, singleton
from sqlalchemy.orm import scoped_session

from domain.risk.ports.storage import RiskDatabase
from domain.risk.facade import RiskFacadeT, RiskFacade
from domain.risk.infrastructure.database import RiskDatabaseAdapter


class RiskModule(Module):
    @provider
    @singleton
    def risk_db(
        self,
        db: scoped_session,
        process_db: ProcessDatabase,
        setting_db: SettingsDatabase,
        user_db: UserDatabase,
    ) -> RiskDatabase:
        return RiskDatabaseAdapter(db, process_db, setting_db, user_db)

    @provider
    @singleton
    def risk_facade(self, risk_db: RiskDatabase) -> RiskFacadeT:
        return RiskFacade(risk_db)
