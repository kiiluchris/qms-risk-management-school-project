#!/usr/bin/env python3

#!/usr/bin/env python3


from typing import Union

from injector import Module, provider, singleton
from sqlalchemy.orm import scoped_session

from domain.auth.ports.utils import Jwt
from domain.report.ports.storage import ReportDatabase
from domain.report.facade import ReportFacadeT, ReportFacade
from domain.report.infrastructure.database import ReportDatabaseAdapter


class ReportModule(Module):
    @provider
    @singleton
    def report_db(
        self,
        db: scoped_session,
        jwt_encoding: Jwt,
    ) -> ReportDatabase:
        return ReportDatabaseAdapter(db)

    @provider
    @singleton
    def report_facade(self, report_db: ReportDatabase) -> ReportFacadeT:
        return ReportFacade(report_db)
