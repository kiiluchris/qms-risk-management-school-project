from typing import Union

from injector import Module, provider, singleton
from sqlalchemy.orm import scoped_session

from domain.process_output.ports.storage import ProcessOutputDatabase
from domain.process_output.facade import ProcessOutputFacadeT, ProcessOutputFacade
from domain.process_output.infrastructure.database import ProcessOutputDatabaseAdapter


class ProcessOutputModule(Module):
    @provider
    @singleton
    def process_output_db(
        self,
        db: scoped_session,
    ) -> ProcessOutputDatabase:
        return ProcessOutputDatabaseAdapter(db)

    @provider
    @singleton
    def process_output_facade(self, process_output_db: ProcessOutputDatabase) -> ProcessOutputFacadeT:
        return ProcessOutputFacade(process_output_db)