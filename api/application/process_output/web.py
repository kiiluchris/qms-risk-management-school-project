#!/usr/bin/env python3

from flask import Blueprint, Flask, Request, jsonify
from sqlalchemy.orm import scoped_session

from application.utils.request import AppContext
from domain.process_output.models import (
    ProcessOutput,
    ProcessOutputQueryParams,
    ProcessOutputUpdate,
    NewProcessOutput
)
from application.utils.middlewares import login_required, parse_request
from domain.process_output.facade import ProcessOutputFacadeT
from application.utils.response import error_response


process_output_blueprint = Blueprint("process_outputs", __name__)


@process_output_blueprint.route("")
@login_required()
def get_all(
    request: Request,
    ctx: AppContext,
):
    query_params = ProcessOutputQueryParams()
    process_outputs = ctx.process_output_facade.get_process_outputs(ctx.auth_user, query_params)

    return jsonify([process_output.to_dict() for process_output in process_outputs])


@process_output_blueprint.route("", methods=["POST"])
@login_required()
@parse_request(NewProcessOutput, "process_output")
def create(
    process_output,
    request: Request,
    ctx: AppContext,
):
    process_output_id = ctx.process_output_facade.create_process_output(ctx.auth_user, process_output)

    return jsonify(process_output_id)


@process_output_blueprint.route("/<int:process_output_id>")
@login_required()
def get_one(
    process_output_id,
    request: Request,
    ctx: AppContext,
):
    process_output = ctx.process_output_facade.get_process_output(ctx.auth_user, process_output_id)

    return jsonify(process_output.to_dict()) if process_output else error_response(404, "ProcessOutput does not exist", [])


@process_output_blueprint.route("/<int:process_output_id>", methods=["PUT"])
@login_required()
@parse_request(ProcessOutputUpdate, "process_output")
def update(
    process_output_id,
    process_output,
    request: Request,
    ctx: AppContext,
):
    process_output_id = ctx.process_output_facade.update_process_output(ctx.auth_user, process_output_id, process_output)

    return jsonify(process_output_id)


@process_output_blueprint.route("/<int:process_output_id>", methods=["DELETE"])
@login_required()
def delete_(
    process_output_id,
    request: Request,
    ctx: AppContext,
):
    process_output = ctx.process_output_facade.delete_process_output(ctx.auth_user, process_output_id)

    return jsonify(process_output) if process_output else error_response(404, "ProcessOutput does not exist", [])


@process_output_blueprint.route("/import", methods=["POST"])
@login_required()
def import_process_outputs(
    request: Request,
    ctx: AppContext,
):
    process_outputs = []
    process_output_ids = ctx.process_output_facade.create_process_outputs(ctx.auth_user, process_outputs)

    return jsonify(process_output_ids)