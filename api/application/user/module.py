from typing import Union

from injector import Module, provider, singleton
from sqlalchemy.orm import scoped_session

from domain.user.ports.storage import UserDatabase
from domain.user.facade import UserFacadeT, UserFacade
from domain.user.infrastructure.database import UserDatabaseAdapter
from domain.email.ports import EmailClient


class UserModule(Module):
    @provider
    @singleton
    def user_db(
        self,
        db: scoped_session,
    ) -> UserDatabase:
        return UserDatabaseAdapter(db)

    @provider
    @singleton
    def user_facade(self, user_db: UserDatabase, mail: EmailClient) -> UserFacadeT:
        return UserFacade(user_db, mail)
