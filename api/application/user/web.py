from application.utils.functions import allowed_file
from typing import cast

from application.utils.request import AppContext
from flask import Blueprint, Flask, Request, jsonify
from sqlalchemy.orm import scoped_session

import pandas as pd

from domain.auth.ports.utils import Jwt
from domain.auth.models import AuthUser
from domain.user.models import (
    ImportUser,
    User,
    RequestUser,
    UserAccount,
    UserQueryParams,
)
from application.utils.middlewares import get_upload_file, login_required, parse_request
from domain.user.facade import UserFacadeT
from application.utils.response import error_response

user_blueprint = Blueprint("users", __name__)


@user_blueprint.route("")
@login_required(allows_external=False)
def get_all(
    request: Request,
    ctx: AppContext,
):
    users = ctx.user_facade.get_users(cast(AuthUser, ctx.auth_user), UserQueryParams())

    return jsonify(users)


@user_blueprint.route("", methods=["POST"])
@parse_request(RequestUser, "user")
@login_required(allows_external=False)
def create(
    user,
    request: Request,
    ctx: AppContext,
):
    users = ctx.user_facade.create_user(cast(AuthUser, ctx.auth_user), user)

    return jsonify(users)


@user_blueprint.route("<int:user_id>")
@login_required(allows_external=False)
def get_one(
    user_id,
    request: Request,
    ctx: AppContext,
):
    user = ctx.user_facade.get_user(cast(AuthUser, ctx.auth_user), user_id)

    return jsonify(user) if user else error_response(404, "User does not exist", [])


@user_blueprint.route("/<int:user_id>", methods=["PUT"])
@login_required(allows_external=False)
@parse_request(RequestUser, "user")
def update(
    user_id,
    user,
    request: Request,
    ctx: AppContext,
):
    user_id = ctx.user_facade.update_user(cast(AuthUser, ctx.auth_user), user_id, user)

    return jsonify(user_id)


@user_blueprint.route("/account", methods=["PUT"])
@login_required(allows_external=False)
@parse_request(UserAccount, "user")
def update_account(
    user,
    request: Request,
    ctx: AppContext,
):
    user_id = ctx.user_facade.update_account(cast(AuthUser, ctx.auth_user), user)

    return jsonify(user_id)


@user_blueprint.route("/<int:user_id>", methods=["DELETE"])
@login_required(allows_external=False)
def delete_(
    user_id,
    request: Request,
    ctx: AppContext,
):
    user = ctx.user_facade.delete_user(cast(AuthUser, ctx.auth_user), user_id)

    return jsonify(user) if user else error_response(404, "User does not exist", [])


@user_blueprint.route("/import", methods=["POST"])
@login_required(allows_external=False)
@get_upload_file
def import_users(
    file_,
    request: Request,
    ctx: AppContext,
):
    cb_url = request.form["callback_url"]
    # filename = secure_filena(file_.filename)
    df = pd.read_excel(
        file_, usecols=["First Name", "Last Name", "Email", "Phone", "Role"]
    )
    users = [
        ImportUser(
            first_name=fname, last_name=lname, email=email, phone=phone, role=role
        )
        for fname, lname, email, phone, role in df.values.tolist()
    ]
    rows_created = ctx.user_facade.import_users(
        cast(AuthUser, ctx.auth_user), cb_url, users
    )

    return jsonify(rows_created)
