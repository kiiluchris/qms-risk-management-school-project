from application.report.module import ReportModule
from domain.sms.ports import SmsClient
from domain.sms.infrastructure.at_sms_client import AfricasTalkingSmsClient
import os
from application.utils.request import AppContext, AppContextModule
from uuid import uuid4
from domain.user.facade import UserFacadeT
from domain.role.facade import RoleFacadeT
from domain.objective.facade import ObjectiveFacadeT
from domain.process.facade import ProcessFacadeT
from domain.task.facade import TaskFacadeT
from domain.auth.models import AuthUser
import logging

from flask import Flask, _app_ctx_stack, Request, jsonify
from flask.helpers import send_from_directory
from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView
from flask_wtf.csrf import CSRFProtect
from flask_cors import CORS
from flask_injector import FlaskInjector
from injector import Module, provider, singleton
from sqlalchemy.orm import scoped_session

from application.auth.web import auth_blueprint
from application.auth.module import AuthModule
from application.settings.module import SettingsModule
from application.settings.web import setting_blueprint
from application.user.web import user_blueprint
from application.user.module import UserModule
from application.role.web import role_blueprint
from application.role.module import RoleModule
from application.incident.web import incident_blueprint
from application.incident.module import IncidentModule
from application.risk_category.web import risk_category_blueprint
from application.risk_category.module import RiskCategoryModule
from application.task.web import task_blueprint
from application.task.module import TaskModule
from application.task_team.web import task_team_blueprint
from application.task_team.module import TaskTeamModule
from application.capa.web import capa_blueprint
from application.capa.module import CapaModule
from application.process.web import process_blueprint
from application.process.module import ProcessModule
from application.objective.web import objective_blueprint
from application.objective.module import ObjectiveModule
from application.risk.module import RiskModule
from application.risk.web import risk_blueprint
from application.process_output.module import ProcessOutputModule
from application.process_output.web import process_output_blueprint
from application.process_value.module import ProcessValueModule
from application.process_value.web import process_value_blueprint
from application.process_input.module import ProcessInputModule
from application.process_input.web import process_input_blueprint
from infrastructure.database import Base, LocalSession, engine
from infrastructure.models import *
from infrastructure.models import task_team_user_assoc
from domain.email.ports import EmailClient
from infrastructure.email.sendgrid_client import SendGridEmail
from domain.auth.ports.utils import Jwt
from application.graphql.ariadne import setup_graphql
from application.utils.middlewares import login_required


class MyModule(Module):
    def configure(self, binder):
        pass

    @provider
    @singleton
    def db_session(self, _app: Flask) -> scoped_session:
        return scoped_session(LocalSession, scopefunc=_app_ctx_stack.__ident_func__)

    @provider
    @singleton
    def email_client(self, jwt: Jwt) -> EmailClient:
        return SendGridEmail(jwt)

    @provider
    @singleton
    def sms_client(self) -> SmsClient:
        return AfricasTalkingSmsClient()


def create_app(config_filename: str) -> Flask:
    app = Flask(__name__)
    app.url_map.strict_slashes = False

    Base.metadata.create_all(bind=engine)

    app.config.update(SECRET_KEY=str(uuid4()), UPLOAD_DIR="media")

    # csrf = CSRFProtect()
    # csrf.init_app(app)
    logging.getLogger("flask_cors").level = logging.DEBUG

    CORS(app)

    @app.route("/sample-file/<path:filename>", methods=["GET"])
    @login_required()
    def sample_import_file(
        filename,
        request: Request,
        ctx: AppContext,
    ):
        uploads = os.path.join(app.root_path, app.config["UPLOAD_DIR"])
        return send_from_directory(uploads, filename)

    setup_graphql(app)
    app.register_blueprint(auth_blueprint, url_prefix="/auth")
    app.register_blueprint(user_blueprint, url_prefix="/users")
    app.register_blueprint(role_blueprint, url_prefix="/roles")
    app.register_blueprint(incident_blueprint, url_prefix="/incidents")
    app.register_blueprint(risk_category_blueprint, url_prefix="/incident-categories")
    app.register_blueprint(capa_blueprint, url_prefix="/capas")
    app.register_blueprint(task_blueprint, url_prefix="/tasks")
    app.register_blueprint(task_team_blueprint, url_prefix="/task-teams")
    app.register_blueprint(process_blueprint, url_prefix="/processes")
    app.register_blueprint(risk_blueprint, url_prefix="/risks")
    app.register_blueprint(process_input_blueprint, url_prefix="/process-inputs")
    app.register_blueprint(process_output_blueprint, url_prefix="/process-outputs")
    app.register_blueprint(process_value_blueprint, url_prefix="/process-values")
    app.register_blueprint(objective_blueprint, url_prefix="/objectives")
    app.register_blueprint(setting_blueprint, url_prefix="/settings")

    flask_injector = FlaskInjector(
        app,
        modules=[
            MyModule,
            AppContextModule,
            AuthModule,
            UserModule,
            RoleModule,
            ReportModule,
            IncidentModule,
            ProcessModule,
            TaskModule,
            CapaModule,
            ObjectiveModule,
            TaskTeamModule,
            RiskModule,
            ProcessInputModule,
            ProcessOutputModule,
            ProcessValueModule,
            RiskCategoryModule,
            SettingsModule,
        ],
    )
    db = flask_injector.injector.get(scoped_session)

    admin = Admin(app, name="laatu", template_mode="bootstrap3")
    models = [
        UserModel,
        OrganizationModel,
        SessionModel,
        RoleModel,
        UnitModel,
        ProcessModel,
        TaskModel,
        SubTaskModel,
        ProcessInputModel,
        ProcessOutputModel,
        ProcessValueModel,
        UnitTypeModel,
        CapaModel,
        IncidentModel,
        ObjectiveModel,
        TaskTeamModel,
        RiskCategoryModel,
        RiskModel,
        RiskClassificationDataModel,
        # ProcessInputSourceModel,
        # ProcessOutputReceiverModel,
    ]
    for m in models:
        admin.add_view(ModelView(m, db))

    return app


if __name__ == "__main__":
    create_app("dev").run(debug=True, use_reloader=True)
