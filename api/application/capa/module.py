from domain.sms.ports import SmsClient
from domain.email.ports import EmailClient
from domain.risk.ports.storage import RiskDatabase
from domain.user.ports.storage import UserDatabase
from domain.incident.ports.storage import IncidentDatabase
from typing import Union

from injector import Module, provider, singleton
from sqlalchemy.orm import scoped_session

from domain.capa.ports.storage import CapaDatabase
from domain.capa.facade import CapaFacadeT, CapaFacade
from domain.capa.infrastructure.database import CapaDatabaseAdapter


class CapaModule(Module):
    @provider
    @singleton
    def capa_db(
        self,
        db: scoped_session,
        incident_db: IncidentDatabase,
        user_db: UserDatabase,
        risk_db: RiskDatabase,
    ) -> CapaDatabase:
        return CapaDatabaseAdapter(db, incident_db, user_db, risk_db)

    @provider
    @singleton
    def capa_facade(
        self, capa_db: CapaDatabase, email: EmailClient, sms: SmsClient
    ) -> CapaFacadeT:
        return CapaFacade(capa_db, email, sms)
