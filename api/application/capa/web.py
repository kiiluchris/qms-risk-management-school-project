#!/usr/bin/env python3
from domain.shared.models import CapaClassification, CapaRating
from typing import cast

from flask import Blueprint, Flask, Request, jsonify
from sqlalchemy.orm import scoped_session
import pandas

from application.utils.request import AppContext
from domain.capa.models import (
    Capa,
    CapaPartial,
    CapaQueryParams,
    CapaUpdate,
    NewCapa,
    CapaImport,
)
from domain.auth.models import AuthUser
from application.utils.middlewares import get_upload_file, login_required, parse_request
from domain.capa.facade import CapaFacadeT
from application.utils.response import error_response


capa_blueprint = Blueprint("capas", __name__)


@capa_blueprint.route("")
@login_required()
def get_all(
    request: Request,
    ctx: AppContext,
):
    query_params = CapaQueryParams()
    capas = ctx.capa_facade.get_capas(cast(AuthUser, ctx.auth_user), query_params)

    return jsonify([capa.to_dict() for capa in capas])


@capa_blueprint.route("", methods=["POST"])
@login_required()
@parse_request(NewCapa, "capa")
def create(
    capa,
    request: Request,
    ctx: AppContext,
):
    capa_id = ctx.capa_facade.create_capa(cast(AuthUser, ctx.auth_user), capa)

    return jsonify(capa_id)


@capa_blueprint.route("/<int:capa_id>")
@login_required()
def get_one(
    capa_id,
    request: Request,
    ctx: AppContext,
):
    capa = ctx.capa_facade.get_capa(cast(AuthUser, ctx.auth_user), capa_id)

    return (
        jsonify(capa.to_dict())
        if capa
        else error_response(404, "Capa does not exist", [])
    )


@capa_blueprint.route("/<int:capa_id>", methods=["PUT"])
@login_required()
@parse_request(CapaUpdate, "capa")
def update(
    capa_id,
    capa,
    request: Request,
    ctx: AppContext,
):
    capa_id = ctx.capa_facade.update_capa(cast(AuthUser, ctx.auth_user), capa_id, capa)

    return jsonify(capa_id)


@capa_blueprint.route("/<int:capa_id>", methods=["DELETE"])
@login_required()
def delete_(
    capa_id,
    request: Request,
    ctx: AppContext,
):
    capa = ctx.capa_facade.delete_capa(cast(AuthUser, ctx.auth_user), capa_id)

    return jsonify(capa) if capa else error_response(404, "Capa does not exist", [])


@capa_blueprint.route("/import", methods=["POST"])
@login_required()
@get_upload_file
def import_capas(
    file_,
    request: Request,
    ctx: AppContext,
):
    df = pandas.read_excel(
        file_,
        usecols=[
            "Name",
            "Rating",
            "Action Type",
            "Category",
            "Incident",
            "Risks",
            "Date Created",
            "Target Effective Date",
            "Description",
            "Measures Taken",
            "Extra Comments",
            "Prepared By",
            "Approved By",
        ],
        keep_default_na=False,
    )
    capas = [
        CapaImport(
            name=name,
            rating=CapaRating[rating],
            action_type=CapaClassification[action_type],
            category=category,
            date_created=print("ASD", date_created) or date_created.date(),
            target_effective_date=target_date.date(),
            description=description,
            measures_taken=measures,
            comments=comments,
            prepared_by=prepared_by,
            approved_by=approved_by,
            incident=incident,
            risks_to_prevent=[proc.strip() for proc in affected.split("\n")],
        )
        for name, rating, action_type, category, incident, affected, date_created, target_date, description, measures, comments, prepared_by, approved_by in df.values.tolist()
    ]
    capa_ids = ctx.capa_facade.import_capas(cast(AuthUser, ctx.auth_user), capas)

    return jsonify(capa_ids)
