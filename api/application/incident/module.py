from domain.sms.ports import SmsClient
from domain.email.ports import EmailClient
from domain.risk.ports.storage import RiskDatabase
from domain.process.ports.storage import ProcessDatabase
from domain.user.ports.storage import UserDatabase
from typing import Union

from injector import Module, provider, singleton
from sqlalchemy.orm import scoped_session

from domain.incident.ports.storage import IncidentDatabase
from domain.incident.facade import IncidentFacadeT, IncidentFacade
from domain.incident.infrastructure.database import IncidentDatabaseAdapter


class IncidentModule(Module):
    @provider
    @singleton
    def incident_db(
        self,
        db: scoped_session,
        user_db: UserDatabase,
        process_db: ProcessDatabase,
        risk_db: RiskDatabase,
    ) -> IncidentDatabase:
        return IncidentDatabaseAdapter(db, user_db, process_db, risk_db)

    @provider
    @singleton
    def incident_facade(
        self, incident_db: IncidentDatabase, email: EmailClient, sms: SmsClient
    ) -> IncidentFacadeT:
        return IncidentFacade(incident_db, email, sms)
