#!/usr/bin/env python3

from typing import cast

from flask import Blueprint, Flask, Request, jsonify
import pandas
from sqlalchemy.orm import scoped_session

from application.utils.request import AppContext
from domain.auth.models import AuthUser
from domain.incident.models import (
    Incident,
    IncidentQueryParams,
    IncidentUpdate,
    NewIncident,
    IncidentImport,
)
from application.utils.middlewares import get_upload_file, login_required, parse_request
from domain.incident.facade import IncidentFacadeT
from application.utils.response import error_response


incident_blueprint = Blueprint("incidents", __name__)


@incident_blueprint.route("")
@login_required()
def get_all(
    request: Request,
    ctx: AppContext,
):
    query_params = IncidentQueryParams()
    incidents = ctx.incident_facade.get_incidents(
        cast(AuthUser, ctx.auth_user), query_params
    )

    return jsonify([incident.to_dict() for incident in incidents])


@incident_blueprint.route("", methods=["POST"])
@login_required()
@parse_request(NewIncident, "incident")
def create(
    incident,
    request: Request,
    ctx: AppContext,
):
    incident_id = ctx.incident_facade.create_incident(
        cast(AuthUser, ctx.auth_user), incident
    )

    return jsonify(incident_id)


@incident_blueprint.route("/<int:incident_id>")
@login_required()
def get_one(
    incident_id,
    request: Request,
    ctx: AppContext,
):
    incident = ctx.incident_facade.get_incident(
        cast(AuthUser, ctx.auth_user), incident_id
    )

    return (
        jsonify(incident.to_dict())
        if incident
        else error_response(404, "Incident does not exist", [])
    )


@incident_blueprint.route("/<int:incident_id>", methods=["PUT"])
@login_required()
@parse_request(IncidentUpdate, "incident")
def update(
    incident_id,
    incident,
    request: Request,
    ctx: AppContext,
):
    incident_id = ctx.incident_facade.update_incident(
        cast(AuthUser, ctx.auth_user), incident_id, incident
    )

    return jsonify(incident_id)


@incident_blueprint.route("/<int:incident_id>", methods=["DELETE"])
@login_required()
def delete_(
    incident_id,
    request: Request,
    ctx: AppContext,
):
    incident = ctx.incident_facade.delete_incident(
        cast(AuthUser, ctx.auth_user), incident_id
    )

    return (
        jsonify(incident)
        if incident
        else error_response(404, "Incident does not exist", [])
    )


@incident_blueprint.route("/import", methods=["POST"])
@login_required()
@get_upload_file
def import_incidents(
    file_,
    request: Request,
    ctx: AppContext,
):
    df = pandas.read_excel(
        file_,
        usecols=[
            "Name",
            "Damage",
            "Date Occurred",
            "Location",
            "Lag Time",
            "Category",
            "Affected Processes",
            "Reported By",
            "Person Responsible",
            "First Aid Provided By",
        ],
        keep_default_na=False,
    )
    incidents = [
        IncidentImport(
            name=name,
            damage=damage,
            date_occurred=date_occurred.date(),
            location=location,
            lag_time=lag_time,
            category=category,
            affected_processes=affected_processes,
            reported_by=reported_by,
            person_responsible=person_responsible,
            first_aid_provided_by=first_aid_by,
        )
        for name, damage, date_occurred, location, lag_time, category, affected_processes, reported_by, person_responsible, first_aid_by in df.values.tolist()
    ]
    incident_ids = ctx.incident_facade.import_incidents(
        cast(AuthUser, ctx.auth_user), incidents
    )

    return jsonify(incident_ids)
