#!/usr/bin/env python3

import pandas
from typing import cast
from flask import Blueprint, Flask, Request, jsonify
from sqlalchemy.orm import scoped_session

from application.utils.request import AppContext
from domain.risk.models import (
    Risk,
    RiskQueryParams,
    RiskUpdate,
    NewRisk,
    RiskImport,
)
from domain.auth.models import AuthUser
from application.utils.middlewares import get_upload_file, login_required, parse_request
from domain.risk.facade import RiskFacadeT
from application.utils.response import error_response
from domain.settings.models import SettingsUpdate


setting_blueprint = Blueprint("setting", __name__)


@setting_blueprint.route("/", methods=["GET"])
@login_required
def get_settings(request: Request, ctx: AppContext):
    return ctx.settings_facade.get_settings(cast(AuthUser, ctx.auth_user))


@setting_blueprint.route("/", methods=["PUT"])
@login_required
@parse_request(SettingsUpdate, "setting")
def update_settings(setting, request: Request, ctx: AppContext):
    id_ = ctx.settings_facade.update_settings(cast(AuthUser, ctx.auth_user), setting)
    return (
        jsonify(id_)
        if id_ != -1
        else error_response(404, "Organization not found", [["not_found"]])
    )


@setting_blueprint.route("/generate-token", methods=["POST"])
@login_required
def generate_token(request: Request, ctx: AppContext):
    token = ctx.settings_facade.generate_api_token(cast(AuthUser, ctx.auth_user))
    return (
        jsonify(token)
        if token
        else error_response(404, "Organization not found", [["not_found"]])
    )


@setting_blueprint.route("/", methods=["DELETE"])
@login_required
def delete_(request: Request, ctx: AppContext):
    id_ = ctx.settings_facade.delete_account(cast(AuthUser, ctx.auth_user))
    return (
        jsonify(id_)
        if id_ != -1
        else error_response(404, "Organization not found", [["not_found"]])
    )
