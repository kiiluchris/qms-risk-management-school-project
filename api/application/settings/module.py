#!/usr/bin/env python3


from typing import Union

from injector import Module, provider, singleton
from sqlalchemy.orm import scoped_session

from domain.auth.ports.utils import Jwt
from domain.settings.ports.storage import SettingsDatabase
from domain.settings.facade import SettingsFacadeT, SettingsFacade
from domain.settings.infrastructure.database import SettingsDatabaseAdapter


class SettingsModule(Module):
    @provider
    @singleton
    def settings_db(
        self,
        db: scoped_session,
        jwt_encoding: Jwt,
    ) -> SettingsDatabase:
        return SettingsDatabaseAdapter(db, jwt_encoding)

    @provider
    @singleton
    def settings_facade(self, setting_db: SettingsDatabase) -> SettingsFacadeT:
        return SettingsFacade(setting_db)
