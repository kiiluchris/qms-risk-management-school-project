#!/usr/bin/env python3

from flask import Blueprint, Flask, Request, jsonify
from sqlalchemy.orm import scoped_session

from application.utils.request import AppContext
from domain.process_value.models import (
    ProcessValue,
    ProcessValueQueryParams,
    ProcessValueUpdate,
    NewProcessValue
)
from application.utils.middlewares import login_required, parse_request
from domain.process_value.facade import ProcessValueFacadeT
from application.utils.response import error_response


process_value_blueprint = Blueprint("process_values", __name__)


@process_value_blueprint.route("")
@login_required()
def get_all(
    request: Request,
    ctx: AppContext,
):
    query_params = ProcessValueQueryParams()
    process_values = ctx.process_value_facade.get_process_values(ctx.auth_user, query_params)

    return jsonify([process_value.to_dict() for process_value in process_values])


@process_value_blueprint.route("", methods=["POST"])
@login_required()
@parse_request(NewProcessValue, "process_value")
def create(
    process_value,
    request: Request,
    ctx: AppContext,
):
    process_value_id = ctx.process_value_facade.create_process_value(ctx.auth_user, process_value)

    return jsonify(process_value_id)


@process_value_blueprint.route("/<int:process_value_id>")
@login_required()
def get_one(
    process_value_id,
    request: Request,
    ctx: AppContext,
):
    process_value = ctx.process_value_facade.get_process_value(ctx.auth_user, process_value_id)

    return jsonify(process_value.to_dict()) if process_value else error_response(404, "ProcessValue does not exist", [])


@process_value_blueprint.route("/<int:process_value_id>", methods=["PUT"])
@login_required()
@parse_request(ProcessValueUpdate, "process_value")
def update(
    process_value_id,
    process_value,
    request: Request,
    ctx: AppContext,
):
    process_value_id = ctx.process_value_facade.update_process_value(ctx.auth_user, process_value_id, process_value)

    return jsonify(process_value_id)


@process_value_blueprint.route("/<int:process_value_id>", methods=["DELETE"])
@login_required()
def delete_(
    process_value_id,
    request: Request,
    ctx: AppContext,
):
    process_value = ctx.process_value_facade.delete_process_value(ctx.auth_user, process_value_id)

    return jsonify(process_value) if process_value else error_response(404, "ProcessValue does not exist", [])


@process_value_blueprint.route("/import", methods=["POST"])
@login_required()
def import_process_values(
    request: Request,
    ctx: AppContext,
):
    process_values = []
    process_value_ids = ctx.process_value_facade.create_process_values(ctx.auth_user, process_values)

    return jsonify(process_value_ids)