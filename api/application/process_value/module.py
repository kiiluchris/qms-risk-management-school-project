from typing import Union

from injector import Module, provider, singleton
from sqlalchemy.orm import scoped_session

from domain.process_value.ports.storage import ProcessValueDatabase
from domain.process_value.facade import ProcessValueFacadeT, ProcessValueFacade
from domain.process_value.infrastructure.database import ProcessValueDatabaseAdapter


class ProcessValueModule(Module):
    @provider
    @singleton
    def process_value_db(
        self,
        db: scoped_session,
    ) -> ProcessValueDatabase:
        return ProcessValueDatabaseAdapter(db)

    @provider
    @singleton
    def process_value_facade(self, process_value_db: ProcessValueDatabase) -> ProcessValueFacadeT:
        return ProcessValueFacade(process_value_db)