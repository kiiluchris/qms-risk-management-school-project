from domain.user.ports.storage import UserDatabase
from typing import Union

from injector import Module, provider, singleton
from sqlalchemy.orm import scoped_session

from domain.task_team.ports.storage import TaskTeamDatabase
from domain.task_team.facade import TaskTeamFacadeT, TaskTeamFacade
from domain.task_team.infrastructure.database import TaskTeamDatabaseAdapter


class TaskTeamModule(Module):
    @provider
    @singleton
    def task_team_db(
        self, db: scoped_session, user_db: UserDatabase
    ) -> TaskTeamDatabase:
        return TaskTeamDatabaseAdapter(db, user_db)

    @provider
    @singleton
    def task_team_facade(self, task_team_db: TaskTeamDatabase) -> TaskTeamFacadeT:
        return TaskTeamFacade(task_team_db)
