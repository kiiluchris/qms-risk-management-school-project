#!/usr/bin/env python3

from typing import cast
from flask import Blueprint, Flask, Request, jsonify
import pandas
from sqlalchemy.orm import scoped_session

from application.utils.request import AppContext
from domain.task_team.models import (
    TaskTeam,
    TaskTeamImport,
    TaskTeamQueryParams,
    TaskTeamUpdate,
    NewTaskTeam,
)
from domain.auth.models import AuthUser
from application.utils.middlewares import get_upload_file, login_required, parse_request
from domain.task_team.facade import TaskTeamFacadeT
from application.utils.response import error_response


task_team_blueprint = Blueprint("task_teams", __name__)


@task_team_blueprint.route("")
@login_required()
def get_all(
    request: Request,
    ctx: AppContext,
):
    query_params = TaskTeamQueryParams()
    task_teams = ctx.task_team_facade.get_task_teams(
        cast(AuthUser, ctx.auth_user), query_params
    )

    return jsonify([task_team.to_dict() for task_team in task_teams])


@task_team_blueprint.route("", methods=["POST"])
@login_required()
@parse_request(NewTaskTeam, "task_team")
def create(
    task_team,
    request: Request,
    ctx: AppContext,
):
    task_team_id = ctx.task_team_facade.create_task_team(
        cast(AuthUser, ctx.auth_user), task_team
    )

    return jsonify(task_team_id)


@task_team_blueprint.route("/<int:task_team_id>")
@login_required()
def get_one(
    task_team_id,
    request: Request,
    ctx: AppContext,
):
    task_team = ctx.task_team_facade.get_task_team(
        cast(AuthUser, ctx.auth_user), task_team_id
    )

    return (
        jsonify(task_team.to_dict())
        if task_team
        else error_response(404, "TaskTeam does not exist", [])
    )


@task_team_blueprint.route("/<int:task_team_id>", methods=["PUT"])
@login_required()
@parse_request(TaskTeamUpdate, "task_team")
def update(
    task_team_id,
    task_team,
    request: Request,
    ctx: AppContext,
):
    task_team_id = ctx.task_team_facade.update_task_team(
        cast(AuthUser, ctx.auth_user), task_team_id, task_team
    )

    return jsonify(task_team_id)


@task_team_blueprint.route("/<int:task_team_id>", methods=["DELETE"])
@login_required()
def delete_(
    task_team_id,
    request: Request,
    ctx: AppContext,
):
    task_team = ctx.task_team_facade.delete_task_team(
        cast(AuthUser, ctx.auth_user), task_team_id
    )

    return (
        jsonify(task_team)
        if task_team
        else error_response(404, "TaskTeam does not exist", [])
    )


@task_team_blueprint.route("/import", methods=["POST"])
@login_required()
@get_upload_file
def import_task_teams(
    file_,
    request: Request,
    ctx: AppContext,
):
    df = pandas.read_excel(
        file_, usecols=["Name", "Leader", "Members"], keep_default_na=False
    )
    task_teams = [
        TaskTeamImport(
            name=team_name.strip(),
            leader=leader.strip(),
            members=[member.strip() for member in members.split("\n")],
        )
        for team_name, leader, members in df.values.tolist()
    ]
    task_team_ids = ctx.task_team_facade.import_task_teams(
        cast(AuthUser, ctx.auth_user), task_teams
    )

    return jsonify(task_team_ids)
