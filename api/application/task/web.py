#!/usr/bin/env python3

from typing import cast
import pandas
from application.utils.request import AppContext
from flask import Blueprint, Flask, Request, jsonify
from sqlalchemy.orm import scoped_session

from domain.auth.ports.utils import Jwt
from domain.auth.models import AuthUser
from domain.task.models import Task, TaskQueryParams, TaskUpdate, NewTask, TaskImport
from application.utils.middlewares import get_upload_file, login_required, parse_request
from domain.task.facade import TaskFacadeT
from application.utils.response import error_response


task_blueprint = Blueprint("tasks", __name__)


@task_blueprint.route("")
@login_required()
def get_all(
    request: Request,
    ctx: AppContext,
):
    try:
        process_id = request.args.get("process_id", type=int)
    except Exception:
        process_id = None
    query_params = TaskQueryParams(process_id=process_id)
    tasks = ctx.task_facade.get_tasks(cast(AuthUser, ctx.auth_user), query_params)

    return jsonify([task.to_dict() for task in tasks])


@task_blueprint.route("", methods=["POST"])
@login_required()
@parse_request(NewTask, "task")
def create(
    task,
    request: Request,
    ctx: AppContext,
):
    task_id = ctx.task_facade.create_task(cast(AuthUser, ctx.auth_user), task)

    return jsonify(task_id)


@task_blueprint.route("/<int:task_id>")
@login_required()
def get_one(
    task_id,
    request: Request,
    ctx: AppContext,
):
    task = ctx.task_facade.get_task(cast(AuthUser, ctx.auth_user), task_id)

    return (
        jsonify(task.to_dict())
        if task
        else error_response(404, "Task does not exist", [])
    )


@task_blueprint.route("/<int:task_id>/toggle", methods=["PUT"])
@login_required()
def complete_task(
    task_id,
    request: Request,
    ctx: AppContext,
):
    task_id = ctx.task_facade.complete_task(cast(AuthUser, ctx.auth_user), task_id)

    return jsonify(task_id)


@task_blueprint.route("/<int:task_id>", methods=["PUT"])
@login_required()
@parse_request(TaskUpdate, "task")
def update(
    task_id,
    task,
    request: Request,
    ctx: AppContext,
):
    task_id = ctx.task_facade.update_task(cast(AuthUser, ctx.auth_user), task_id, task)

    return jsonify(task_id)


@task_blueprint.route("/<int:task_id>", methods=["DELETE"])
@login_required()
def delete_(
    task_id,
    request: Request,
    ctx: AppContext,
):
    task = ctx.task_facade.delete_task(cast(AuthUser, ctx.auth_user), task_id)

    return jsonify(task) if task else error_response(404, "Task does not exist", [])


@task_blueprint.route("/import", methods=["POST"])
@login_required()
@get_upload_file
def import_tasks(
    file_,
    request: Request,
    ctx: AppContext,
):
    df = pandas.read_excel(
        file_,
        usecols=["Name", "Process", "Is Complete", "Person Responsible"],  # , "Due By"
    )
    tasks = [
        TaskImport(
            name=name,
            process=proc,
            is_complete=completion,
            user_owner=person_responsible.strip(),
        )
        for name, proc, completion, person_responsible in df.values.tolist()
    ]
    task_ids = ctx.task_facade.import_tasks(cast(AuthUser, ctx.auth_user), tasks)

    return jsonify(task_ids)
