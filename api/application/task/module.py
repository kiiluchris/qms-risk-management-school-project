from domain.user.ports.storage import UserDatabase
from domain.email.ports import EmailClient
from domain.sms.ports import SmsClient
from typing import Union

from injector import Module, provider, singleton
from sqlalchemy.orm import scoped_session

from domain.task.ports.storage import TaskDatabase
from domain.task.facade import TaskFacadeT, TaskFacade
from domain.task.infrastructure.database import TaskDatabaseAdapter


class TaskModule(Module):
    @provider
    @singleton
    def task_db(self, db: scoped_session, user_db: UserDatabase) -> TaskDatabase:
        return TaskDatabaseAdapter(db, user_db)

    @provider
    @singleton
    def task_facade(
        self, task_db: TaskDatabase, sms: SmsClient, email: EmailClient
    ) -> TaskFacadeT:
        return TaskFacade(task_db, sms, email)
