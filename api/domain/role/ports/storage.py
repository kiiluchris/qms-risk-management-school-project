#!/usr/bin/env python3

from typing import Protocol, List, Optional
from abc import abstractmethod

from domain.auth.models import AuthUser

from domain.role.models import Role


class RoleDatabase(Protocol):
    @abstractmethod
    def get_roles(self, auth_role: AuthUser) -> List[Role]:
        raise NotImplementedError

    @abstractmethod
    def get_role(self, auth_role: AuthUser, role_id: int) -> Optional[Role]:
        raise NotImplementedError

    @abstractmethod
    def get_role(self, auth_role: AuthUser, role_id: int) -> Optional[Role]:
        raise NotImplementedError

    @abstractmethod
    def create_role(self, auth_role: AuthUser, role: Role) -> int:
        raise NotImplementedError

    @abstractmethod
    def find_role(self, auth_role: AuthUser, role: str) -> Optional[Role]:
        raise NotImplementedError

    @abstractmethod
    def update_role(self, auth_role: AuthUser, role_id: int, role: Role) -> int:
        raise NotImplementedError

    @abstractmethod
    def delete_role(self, auth_role: AuthUser, role_id: int) -> int:
        raise NotImplementedError
