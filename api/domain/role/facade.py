#!/usr/bin/env python3

from domain.auth.models import AuthUser
from domain.role.models import Role
from domain.role.ports.storage import RoleDatabase


class RoleFacade:
    def __init__(self, db: RoleDatabase):
        self._db = db

    def get_roles(self, auth_user: AuthUser):
        return self._db.get_roles(auth_user)

    def get_role(self, auth_user: AuthUser, role_id: int):
        return self._db.get_role(auth_user, role_id)

    def create_role(self, auth_user: AuthUser, role: Role):
        if self._db.find_role(auth_user, role.name):
            return -1
        return self._db.create_role(auth_user, role)

    def update_role(self, auth_user: AuthUser, role_id: int, role: Role):
        return self._db.update_role(auth_user, role_id, role)

    def delete_role(self, auth_user: AuthUser, role_id: int):
        return self._db.delete_role(auth_user, role_id)


RoleFacadeT = RoleFacade
