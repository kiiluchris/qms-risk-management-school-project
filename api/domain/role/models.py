#!/usr/bin/env python3

from dataclasses import dataclass, field

from domain.shared.models import RolePermission
from dataclasses_json import dataclass_json, config
from typing import Optional


@dataclass_json
@dataclass(frozen=True)
class Role:
    name: str
    permission: Optional[RolePermission] = field(
        metadata=config(
            encoder=lambda p: p and p.name,
            decoder=lambda s: RolePermission[s]
            if s in RolePermission.__members__
            else None,
        ),
        default=None,
    )
    id: Optional[int] = None
    organization_id: Optional[int] = None
    organization_domain: Optional[str] = None
