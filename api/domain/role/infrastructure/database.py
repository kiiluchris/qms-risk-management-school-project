#!/usr/bin/env python3


from sqlalchemy.orm import scoped_session

from infrastructure.models import OrganizationModel, UserModel, RoleModel, SessionModel
from domain.auth.models import AuthUser

from domain.role.ports.storage import RoleDatabase
from domain.role.models import Role


class RoleDatabaseAdapter(RoleDatabase):
    def __init__(self, session: scoped_session):
        self._session = session

    def get_roles(self, auth_user: AuthUser):
        roles = (
            self._session.query(RoleModel, OrganizationModel.domain)
            .join(OrganizationModel)
            .filter(RoleModel.organization_id == auth_user.organization_id)
        )

        return [
            Role(
                name=r.name,
                organization_id=r.organization_id,
                organization_domain=organization,
                id=r.id,
                permission=r.permissions,
            )
            for r, organization in roles
        ]

    def get_role(self, auth_user: AuthUser, role_id: int):
        result = (
            self._session.query(RoleModel, OrganizationModel.domain)
            .join(OrganizationModel)
            .filter(
                RoleModel.organization_id == auth_user.organization_id,
                RoleModel.id == role_id,
            )
            .first()
        )
        if not result:
            return None
        r, organization = result

        return Role(
            name=r.name,
            organization_id=r.id,
            organization_domain=organization,
            id=r.id,
            permission=r.permissions,
        )

    def find_role(
        self,
        auth_user: AuthUser,
        role_str: str,
    ):
        role = (
            self._session.query(RoleModel)
            .filter(
                RoleModel.organization_id == auth_user.organization_id,
                RoleModel.name == role_str,
            )
            .first()
        )
        return role and Role(
            name=role.name,
            id=role.id,
            organization_domain=auth_user.organization,
            organization_id=auth_user.organization_id,
            permission=role.permissions,
        )

    def create_role(self, auth_user: AuthUser, role: Role):
        r = RoleModel(
            name=role.name,
            organization_id=auth_user.organization_id,
            permissions=role.permission,
        )
        self._session.add(r)
        self._session.commit()

        return r.id

    def update_role(self, auth_user: AuthUser, role_id: int, role: Role):
        r = self._session.query(RoleModel).filter(RoleModel.id == role_id).first()
        other_roles = self._session.query(RoleModel).filter(RoleModel.name == role.name)
        l = other_roles.count()
        if l == 0 or l == 1 and r.id == other_roles[0].id:
            r.name = role.name
            r.permissions = role.permission
            self._session.commit()
            return r.id
        else:
            return -1

    def delete_role(self, auth_user: AuthUser, role_id: int):
        r = self._session.query(RoleModel).filter(RoleModel.id == role_id).first()
        if r is not None:
            id = r.id
            self._session.delete(r)
            self._session.commit()
            return id

        return -1
