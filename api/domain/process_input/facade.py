#!/usr/bin/env python3

from typing import List

from domain.auth.models import AuthUser
from domain.process_input.models import (
    ProcessInput,
    ProcessInputQueryParams,
    NewProcessInput,
    ProcessInputUpdate,
)
from domain.process_input.ports.storage import ProcessInputDatabase


class ProcessInputFacade:
    def __init__(self, db: ProcessInputDatabase):
        self._db = db

    def get_process_inputs(self, auth_user: AuthUser, query_params: ProcessInputQueryParams):
        return self._db.query(auth_user, query_params)

    def get_process_input(self, auth_user: AuthUser, process_input_id: int):
        return self._db.get_by_id(auth_user, process_input_id)

    def create_process_input(self, auth_user: AuthUser, process_input: NewProcessInput):
        if self._db.exists(auth_user, ProcessInputQueryParams(name = process_input.name)):
            return -1
        return self._db.create(auth_user, process_input)

    def update_process_input(self, auth_user: AuthUser, process_input_id: int, process_input: ProcessInputUpdate):
        return self._db.update_by_id(auth_user, process_input_id, process_input)

    def delete_process_input(self, auth_user: AuthUser, process_input_id: int):
        return self._db.delete_by_id(auth_user, process_input_id)

    def import_process_inputs(self, auth_user: AuthUser, process_inputs: List[NewProcessInput]):
        return self._db.create_many(auth_user, process_inputs)

        
        


ProcessInputFacadeT = ProcessInputFacade