from datetime import datetime, date
from typing import Optional

from dataclasses import dataclass, field
from dataclasses_json import dataclass_json, config

from domain.shared.models import (
    RelatedItem,
)
from domain.shared.mixins import PartialMixin


@dataclass_json
@dataclass(frozen=True)
class NewProcessInput:
    name: str
    value: int
    unit: RelatedItem


ProcessInputUpdate = NewProcessInput


@dataclass_json
@dataclass(frozen=True)
class ProcessInput(NewProcessInput):
    organization_id: Optional[int] = None
    id: Optional[int] = None


@dataclass_json
@dataclass(frozen=True)
class ProcessInputPartial(PartialMixin):
    name: Optional[str] = None
    value: Optional[int] = None
    unit: Optional[RelatedItem] = None


@dataclass(frozen=True)
class ProcessInputQueryParams:
    id: Optional[int] = None
    risk_id: Optional[int] = None
    name: Optional[str] = None
    value: Optional[int] = None
    unit: Optional[RelatedItem] = None
