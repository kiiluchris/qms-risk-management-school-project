from typing import List

from sqlalchemy import insert, select, update, delete, bindparam, func, alias
from sqlalchemy.sql import Select
from sqlalchemy.sql.expression import and_
from sqlalchemy.orm import scoped_session

from infrastructure.models import (
    ProcessInputModel,
    UnitModel,
    risk_input_assoc,
)
from domain.auth.models import AuthUser

from domain.process_input.ports.storage import ProcessInputDatabase
from domain.process_input.models import (
    ProcessInput,
    ProcessInputQueryParams,
    ProcessInputUpdate,
    NewProcessInput,
)
from dataclasses import asdict
from domain.shared.models import RelatedItem


class ProcessInputDatabaseAdapter(ProcessInputDatabase):
    def __init__(self, session: scoped_session):
        self._session = session

    def exists(
        self, auth_user: AuthUser, query_params: ProcessInputQueryParams
    ) -> bool:
        query = (
            select(func.count())
            .select_from(ProcessInputModel)
            .where(
                and_(
                    ProcessInputModel.organization_id == auth_user.organization_id,
                    ProcessInputModel.name == query_params.name,
                )
            )
            .limit(1)
        )
        count = self._session.scalar(query)

        return count > 0

    def record_to_domain_model(
        self,
        process_input: ProcessInputModel,
        unit: UnitModel,
    ) -> ProcessInput:
        return ProcessInput(
            id=process_input.id,
            name=process_input.name,
            value=process_input.value,
            unit=RelatedItem(
                id=unit.id,
                value=unit.name,
                readable=unit.name,
            ),
        )

    def select_query(
        self, auth_user: AuthUser, query_params: ProcessInputQueryParams = None
    ) -> Select:
        query = (
            select(
                ProcessInputModel,
                UnitModel,
            )
            .select_from(ProcessInputModel)
            .join(UnitModel, ProcessInputModel.unit == UnitModel.id)
            .where(ProcessInputModel.organization_id == auth_user.organization_id)
        )

        if query_params:
            if query_params.risk_id is not None:
                query = query.join(
                    risk_input_assoc,
                    risk_input_assoc.columns.process_input_id == ProcessInputModel.id,
                ).where(risk_input_assoc.columns.risk_id == query_params.risk_id)

        return query

    def query(self, auth_user: AuthUser, query_params: ProcessInputQueryParams):
        query = self.select_query(auth_user, query_params)
        process_inputs = self._session.execute(query)

        return [self.record_to_domain_model(*row) for row in process_inputs]

    def get_by_id(self, auth_user: AuthUser, process_input_id: int):
        query = self.select_query(auth_user).where(
            ProcessInputModel.id == process_input_id
        )
        result = self._session.execute(query).first()
        if result is None:
            return None

        return self.record_to_domain_model(*result)

    def create(self, auth_user: AuthUser, process_input: NewProcessInput):
        new_process_input = ProcessInputModel(
            name=process_input.name,
            value=process_input.value,
            unit=process_input.unit.id,
        )
        self._session.add(new_process_input)
        self._session.commit()

        return new_process_input.id

    def process_input_to_dict(
        self, auth_user: AuthUser, process_input: NewProcessInput
    ):
        result = {
            **asdict(process_input),
            "unit": process_input.unit.id,
        }

        return result

    def create_many(self, auth_user: AuthUser, process_inputs: List[NewProcessInput]):
        query = insert(ProcessInputModel)  # .returning(ProcessInputModel.id)
        ids = self._session.execute(
            query,
            [
                self.process_input_to_dict(auth_user, process_input)
                for process_input in process_inputs
            ],
        )
        self._session.commit()

        return ids

    # Edit before usage
    def update_by_id(
        self,
        auth_user: AuthUser,
        process_input_id: int,
        process_input_updates: ProcessInputUpdate,
    ):
        query = (
            update(ProcessInputModel)
            .where(
                and_(
                    ProcessInputModel.id == process_input_id,
                    ProcessInputModel.organization_id == auth_user.organization_id,
                )
            )
            .values(self.process_input_to_dict(auth_user, process_input_updates))
            # .returning(ProcessInputModel.id)
        )
        update_cursor = self._session.execute(query)
        if update_cursor is not None:
            self._session.commit()
            return process_input_id

        return -1

    def delete_by_id(self, auth_user: AuthUser, process_input_id: int):
        query = delete(ProcessInputModel).where(
            and_(
                ProcessInputModel.id == process_input_id,
                ProcessInputModel.organization_id == auth_user.organization_id,
            )
        )  # .returning(ProcessInputModel.id)
        delete_cursor = self._session.execute(query)
        print("Deleted ID", delete_cursor)
        if delete_cursor is not None:
            self._session.commit()
            return process_input_id

        return -1
