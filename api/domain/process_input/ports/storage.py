#!/usr/bin/env python3

from typing import Protocol, List, Optional
from abc import abstractmethod

from domain.auth.models import AuthUser

from domain.process_input.models import (
    ProcessInput,
    ProcessInputQueryParams,
    ProcessInputUpdate,
    NewProcessInput,
)


class ProcessInputDatabase(Protocol):
    @abstractmethod
    def query(self, auth_user: AuthUser, query_params: ProcessInputQueryParams) -> List[ProcessInput]:
        raise NotImplementedError

    @abstractmethod
    def get_by_id(self, auth_user: AuthUser, process_input_id: int) -> Optional[ProcessInput]:
        raise NotImplementedError

    @abstractmethod
    def exists(self, auth_user: AuthUser, query_params: ProcessInputQueryParams) -> bool:
        raise NotImplementedError
    
    @abstractmethod
    def create(self, auth_user: AuthUser, process_input: NewProcessInput) -> int:
        raise NotImplementedError

    @abstractmethod
    def create_many(self, auth_user: AuthUser, process_inputs: List[NewProcessInput]) -> List[int]:
        raise NotImplementedError
    
    @abstractmethod
    def update_by_id(self, auth_user: AuthUser, process_input_id: int, process_input_updates: ProcessInputUpdate) -> int:
        raise NotImplementedError

    @abstractmethod
    def delete_by_id(self, auth_user: AuthUser, process_input_id: int) -> int:
        raise NotImplementedError