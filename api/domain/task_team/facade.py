#!/usr/bin/env python3

from typing import List

from domain.auth.models import AuthUser
from domain.task_team.models import (
    TaskTeam,
    TaskTeamImport,
    TaskTeamQueryParams,
    NewTaskTeam,
    TaskTeamUpdate,
)
from domain.task_team.ports.storage import TaskTeamDatabase


class TaskTeamFacade:
    def __init__(self, db: TaskTeamDatabase):
        self._db = db

    def get_task_teams(self, auth_user: AuthUser, query_params: TaskTeamQueryParams):
        return self._db.query(auth_user, query_params)

    def get_task_team(self, auth_user: AuthUser, task_team_id: int):
        return self._db.get_by_id(auth_user, task_team_id)

    def create_task_team(self, auth_user: AuthUser, task_team: NewTaskTeam):
        if self._db.exists(auth_user, TaskTeamQueryParams(name=task_team.name)):
            return -1
        return self._db.create(auth_user, task_team)

    def update_task_team(
        self, auth_user: AuthUser, task_team_id: int, task_team: TaskTeamUpdate
    ):
        return self._db.update_by_id(auth_user, task_team_id, task_team)

    def delete_task_team(self, auth_user: AuthUser, task_team_id: int):
        return self._db.delete_by_id(auth_user, task_team_id)

    def import_task_teams(self, auth_user: AuthUser, task_teams: List[TaskTeamImport]):
        return self._db.import_many(auth_user, task_teams)


TaskTeamFacadeT = TaskTeamFacade
