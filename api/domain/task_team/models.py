from datetime import datetime, date
from typing import Optional

from dataclasses import dataclass, field
from dataclasses_json import dataclass_json, config

from domain.shared.models import (
    RelatedItem,
)
from domain.shared.mixins import PartialMixin


@dataclass_json
@dataclass(frozen=True)
class NewTaskTeam:
    name: str
    leader: RelatedItem
    members: list[RelatedItem] = field(default_factory=list)


TaskTeamUpdate = NewTaskTeam


@dataclass_json
@dataclass(frozen=True)
class TaskTeamImport:
    name: str
    leader: str
    members: list[str] = field(default_factory=list)


@dataclass_json
@dataclass(frozen=True)
class TaskTeam(NewTaskTeam):
    organization_id: Optional[int] = None
    id: Optional[int] = None


@dataclass_json
@dataclass(frozen=True)
class TaskTeamPartial(PartialMixin):
    name: Optional[str] = None


@dataclass(frozen=True)
class TaskTeamQueryParams:
    name: Optional[str] = None
