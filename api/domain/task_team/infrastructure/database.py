from domain.user.infrastructure.database import UserDatabaseAdapter
from domain import organization
from typing import List, NamedTuple, Optional
from domain.user.models import user_full_name
import itertools

from sqlalchemy import insert, select, update, delete, bindparam, func
from sqlalchemy.sql import Select
from sqlalchemy.sql.expression import and_
from sqlalchemy.orm import scoped_session

from infrastructure.models import (
    TaskTeamModel,
    UserModel,
    task_team_user_assoc,
)
from domain.auth.models import AuthUser

from domain.task_team.ports.storage import TaskTeamDatabase
from domain.task_team.models import (
    TaskTeam,
    TaskTeamImport,
    TaskTeamQueryParams,
    TaskTeamUpdate,
    NewTaskTeam,
)
from dataclasses import asdict
from domain.shared.models import RelatedItem


class TaskTeamImportKeys(NamedTuple):
    teams: set[str]
    members: set[str]


class TaskTeamDatabaseAdapter(TaskTeamDatabase):
    def __init__(self, session: scoped_session, user_db: UserDatabaseAdapter):
        self._session = session
        self._user_db = user_db

    def exists(self, auth_user: AuthUser, query_params: TaskTeamQueryParams) -> bool:
        query = (
            select(func.count())
            .select_from(TaskTeamModel)
            .where(
                and_(
                    TaskTeamModel.organization_id == auth_user.organization_id,
                    TaskTeamModel.name == query_params.name,
                )
            )
            .limit(1)
        )
        count = self._session.scalar(query)

        return count > 0

    def record_to_domain_model(
        self,
        auth_user: AuthUser,
        task_team: TaskTeamModel,
        leader: UserModel,
    ) -> TaskTeam:
        return TaskTeam(
            id=task_team.id,
            name=task_team.name,
            leader=RelatedItem(
                id=leader.id, value=leader.email, readable=user_full_name(leader)
            ),
            members=[
                RelatedItem(
                    id=m.id, value=m.email, readable=m.first_name + " " + m.last_name
                )
                for m in task_team.members
            ],
            organization_id=auth_user.organization_id,
        )

    def select_query(self, auth_user: AuthUser) -> Select:
        return (
            select(TaskTeamModel, UserModel)
            .select_from(TaskTeamModel)
            .join(UserModel, UserModel.id == TaskTeamModel.leader)
            .where(TaskTeamModel.organization_id == auth_user.organization_id)
        )

    def query(self, auth_user: AuthUser, query_params: TaskTeamQueryParams):
        query = self.select_query(auth_user)
        task_teams = self._session.execute(query)

        return [self.record_to_domain_model(auth_user, *row) for row in task_teams]

    def get_by_id(self, auth_user: AuthUser, task_team_id: int):
        query = self.select_query(auth_user).where(TaskTeamModel.id == task_team_id)
        result = self._session.execute(query).first()
        if result is None:
            return None

        return self.record_to_domain_model(auth_user, *result)

    def create(
        self, auth_user: AuthUser, task_team: NewTaskTeam, *, nocommit: bool = False
    ):
        new_task_team = TaskTeamModel(
            name=task_team.name,
            leader=task_team.leader.id,
            organization_id=auth_user.organization_id,
        )
        self._session.add(new_task_team)
        self._session.flush()
        self._session.execute(
            insert(task_team_user_assoc),
            [dict(task_team_id=new_task_team.id, user_id=task_team.leader.id)]
            + [
                dict(task_team_id=new_task_team.id, user_id=m.id)
                for m in task_team.members
            ],
        )
        if not nocommit:
            self._session.commit()

        return new_task_team.id

    def task_team_to_dict(self, auth_user: AuthUser, task_team: NewTaskTeam):
        result = {
            **asdict(task_team),
        }

        return result

    def create_many(self, auth_user: AuthUser, task_teams: List[NewTaskTeam]):
        new_task_teams = []
        for team in task_teams:
            new_task_team = TaskTeamModel(
                name=team.name,
                leader=team.leader,
                organization_id=auth_user.organization_id,
            )
            self._session.add(new_task_team)
            new_task_teams.append(new_task_team)
        self._session.flush()
        self._session.execute(
            insert(task_team_user_assoc),
            list(
                itertools.chain.from_iterable(
                    [
                        [
                            dict(
                                task_team_id=new_task_team.id,
                                user_id=new_task_team.leader.id,
                            )
                        ]
                        + [
                            dict(task_team_id=new_task_team.id, user_id=m.id)
                            for m in task_team_input.members
                        ]
                        for task_team_input, new_task_team in zip(
                            task_teams, new_task_teams
                        )
                    ]
                )
            ),
        )
        self._session.commit()

        return [t.id for t in new_task_teams]

    def _extract_task_team_import_keys(self, task_teams: List[TaskTeamImport]):
        members = []
        teams = set()
        for team in task_teams:
            teams.add(team.name)
            members = itertools.chain(members, team.members, [team.leader])

        return TaskTeamImportKeys(members=set(members), teams=teams)

    def fetch_existing_task_teams(self, auth_user: AuthUser, team_keys: set[str]):
        task_team_results = self._session.execute(
            select(TaskTeamModel).where(
                and_(
                    TaskTeamModel.organization_id == auth_user.organization_id,
                    TaskTeamModel.name.in_(team_keys),
                )
            )
        )

        return {t.name: t for t, *_, in task_team_results}

    def create_new_task_teams(
        self,
        auth_user: AuthUser,
        imported_task_teams: list[TaskTeamImport],
        task_teams: dict[str, TaskTeamModel],
        users: dict[str, UserModel],
    ):
        traversed = set()
        for team in imported_task_teams:
            if team.name in traversed:
                continue
            task_team = task_teams.get(team.name)
            leader = users.get(team.leader)
            if not task_team and leader:
                t = TaskTeamModel(
                    name=team.name,
                    leader=leader.id,
                    organization_id=auth_user.organization_id,
                )
                self._session.add(t)
                task_teams[team.name] = t
            traversed.add(team.name)
        self._session.flush()

    def import_many(self, auth_user: AuthUser, task_teams: List[TaskTeamImport]):
        import_keys = self._extract_task_team_import_keys(task_teams)
        related_imported_users = self._user_db.fetch_existing_users(
            auth_user, import_keys.members
        )
        related_imported_teams = self.fetch_existing_task_teams(
            auth_user, import_keys.teams
        )
        self.create_new_task_teams(
            auth_user, task_teams, related_imported_teams, related_imported_users
        )

        for team in task_teams:
            team_model = related_imported_teams[team.name]
            team_model.members = [
                user for m in team.members if (user := related_imported_users.get(m))
            ]

        self._session.commit()

        return len(task_teams)

    # Edit before usage
    def update_by_id(
        self, auth_user: AuthUser, task_team_id: int, task_team_updates: TaskTeamUpdate
    ):
        query = (
            select(TaskTeamModel).where(
                and_(
                    TaskTeamModel.id == task_team_id,
                    TaskTeamModel.organization_id == auth_user.organization_id,
                )
            )
            # .returning(TaskTeamModel.id)
        )
        update_member_ids = [m.id for m in task_team_updates.members]
        result = self._session.execute(query).first()
        if result is None:
            return -1
        task_team = result[0]
        users_in_team_result = self._session.execute(
            select(UserModel).where(
                and_(
                    UserModel.id.in_(update_member_ids),
                    UserModel.organization_id == auth_user.organization_id,
                )
            )
        )
        task_team.name = task_team_updates.name
        task_team.leader = task_team_updates.leader.id
        task_team.members = [row[0] for row in users_in_team_result]
        self._session.commit()
        return task_team.id

    def delete_by_id(self, auth_user: AuthUser, task_team_id: int):
        query = delete(TaskTeamModel).where(
            and_(
                TaskTeamModel.id == task_team_id,
                TaskTeamModel.organization_id == auth_user.organization_id,
            )
        )  # .returning(TaskTeamModel.id)
        delete_cursor = self._session.execute(query)
        print("Deleted ID", delete_cursor)
        if delete_cursor is not None:
            self._session.commit()
            return task_team_id

        return -1
