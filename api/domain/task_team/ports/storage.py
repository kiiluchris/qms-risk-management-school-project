#!/usr/bin/env python3

from typing import Protocol, List, Optional
from abc import abstractmethod

from domain.auth.models import AuthUser

from domain.task_team.models import (
    TaskTeam,
    TaskTeamImport,
    TaskTeamQueryParams,
    TaskTeamUpdate,
    NewTaskTeam,
)


class TaskTeamDatabase(Protocol):
    @abstractmethod
    def query(
        self, auth_user: AuthUser, query_params: TaskTeamQueryParams
    ) -> List[TaskTeam]:
        raise NotImplementedError

    @abstractmethod
    def get_by_id(self, auth_user: AuthUser, task_team_id: int) -> Optional[TaskTeam]:
        raise NotImplementedError

    @abstractmethod
    def exists(self, auth_user: AuthUser, query_params: TaskTeamQueryParams) -> bool:
        raise NotImplementedError

    @abstractmethod
    def create(self, auth_user: AuthUser, task_team: NewTaskTeam) -> int:
        raise NotImplementedError

    @abstractmethod
    def create_many(
        self, auth_user: AuthUser, task_teams: List[NewTaskTeam]
    ) -> List[int]:
        raise NotImplementedError

    @abstractmethod
    def import_many(
        self, auth_user: AuthUser, task_teams: List[TaskTeamImport]
    ) -> List[int]:
        raise NotImplementedError

    @abstractmethod
    def update_by_id(
        self, auth_user: AuthUser, task_team_id: int, task_team_updates: TaskTeamUpdate
    ) -> int:
        raise NotImplementedError

    @abstractmethod
    def delete_by_id(self, auth_user: AuthUser, task_team_id: int) -> int:
        raise NotImplementedError
