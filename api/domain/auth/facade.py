from typing import Union


from domain.auth.ports.incoming import (
    VerifyOrganizationExists,
    RegisterOrganizationAccount,
    SignIn,
)
from domain.auth.ports.outgoing import AuthDatabase
from domain.auth.ports.utils import PasswordEncryption, Jwt
from domain.auth.models import (
    AccountRegistrationDetails,
    LoginCredentials,
    ResetPasswordDetails,
    ResetPasswordUser,
    AuthUser,
)
from domain.auth.models.errors import (
    ExpiredToken,
    InvalidPassword,
    LoginUserDoesNotExist,
    TokenDoesNotExist,
)
from domain.email.ports import EmailClient


class AuthFacade:
    def __init__(
        self, db: AuthDatabase, pe: PasswordEncryption, jwt: Jwt, email: EmailClient
    ):
        self._db = db
        self._pass_enc = pe
        self._jwt = jwt
        self._mail = email

    def verify_organization_exists(self, domain: str) -> bool:
        return self._db.check_organization_exists(domain)

    def register_account(self, details: AccountRegistrationDetails):
        if not self._db.check_organization_exists(details.domain):
            account_id, auth_user = self._db.create_account(details)
            # TODO: Handle email fail error by rolling back database or cron job to resend
            token = self._mail.activate_account(
                details.callback_url, details.email, details.domain
            )
            self._db.create_registration_session(auth_user, token)

            return account_id
        return -1

    def sign_in(self, credentials: LoginCredentials):
        user = self._db.find_login_user(credentials)
        if not user:
            raise LoginUserDoesNotExist(credentials.organization, credentials.email)
        if not self._pass_enc.verify_same(user.password, credentials.password):
            raise InvalidPassword("Password incorrect")
        session = self._db.find_login_session(user)
        if not session:
            session = self._db.create_login_session(user)
        return session

    def refresh_session(self, refresh_token_str: str):
        refresh_token = self._jwt.parse_token(refresh_token_str)
        print("Refresh Token", refresh_token)
        if not refresh_token:
            raise ExpiredToken

        print("Refreshing")
        tokens = self._db.refresh_session(
            refresh_token_str, refresh_token["e"], refresh_token["o"]
        )
        if tokens is None:
            raise TokenDoesNotExist
        return tokens

    def request_password_reset(self, details: ResetPasswordUser):
        self._db.request_password_reset(details)
        token = self._mail.request_password_reset(
            details.callback_url, details.email, details.organization
        )
        return token

    def reset_password(self, details: ResetPasswordDetails):
        self._db.reset_password(details)

    def sign_out(self, auth_user: AuthUser):
        self._db.clear_login_session(auth_user)


AuthFacadeT = AuthFacade
