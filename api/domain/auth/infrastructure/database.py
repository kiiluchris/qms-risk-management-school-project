from domain.shared.models import RelatedItem
from sqlalchemy.orm import scoped_session

from infrastructure.models import (
    OrganizationModel,
    UserModel,
    RoleModel,
    SessionModel,
    RiskCategoryModel,
)
from domain.auth.ports.outgoing import AuthDatabase
from domain.auth.ports.utils import PasswordEncryption, Jwt
from domain.auth.models import (
    AccountRegistrationDetails,
    AuthUser,
    LoginCredentials,
    SessionToken,
    ResetPasswordDetails,
    ResetPasswordUser,
    AuthUser,
)
from domain.auth.models.errors import (
    InvalidPassword,
    LoginUserDoesNotExist,
    SessionDoesNotExist,
)
from domain.user.models import User
from domain.risk.infrastructure.modelling.generation import ModelNames


class AuthDatabaseAdapter(AuthDatabase):
    def __init__(
        self, session: scoped_session, password_encrypter: PasswordEncryption, jwt: Jwt
    ):
        self._session = session
        self._pass_enc = password_encrypter
        self._jwt = jwt

    def check_organization_exists(self, domain: str) -> bool:
        organization = (
            self._session.query(OrganizationModel)
            .filter(OrganizationModel.domain == domain)
            .first()
        )
        return organization is not None

    def create_account(self, details: AccountRegistrationDetails):
        organization = OrganizationModel(
            name=details.name,
            domain=details.domain,
            ml_model_to_use=ModelNames.NEURAL_NETWORK,
        )
        self._session.add(organization)
        self._session.flush()
        admin_role = RoleModel(name="Admin", organization_id=organization.id)
        employee_role = RoleModel(name="Employee", organization_id=organization.id)
        manager_role = RoleModel(name="Manager", organization_id=organization.id)
        self._session.add(admin_role)
        self._session.add(employee_role)
        self._session.add(manager_role)
        risk_categories = [
            "Financial",
            "Geographic",
            "Operational",
            "Compliance",
            "Privacy",
            "Reputational",
            "Security",
            "Strategy",
        ]
        for c in risk_categories:
            risk = RiskCategoryModel(
                name=c,
                organization_id=organization.id,
            )
            self._session.add(risk)
        self._session.flush()
        admin_user = UserModel(
            first_name=details.first_name,
            last_name=details.last_name,
            email=details.email,
            password=self._pass_enc.encrypt(details.password),
            phone=details.phone,
            organization_id=organization.id,
            role_id=admin_role.id,
        )
        user_id = self._session.add(admin_user)
        self._session.commit()
        return organization.id, AuthUser(
            email=details.email,
            organization=details.domain,
            organization_id=organization.id,
            user_id=admin_user.id,
        )

    def find_login_user(self, credentials: LoginCredentials):
        u = (
            self._session.query(
                UserModel, OrganizationModel.domain, OrganizationModel.id
            )
            .join(OrganizationModel)
            .filter(
                UserModel.email == credentials.email,
                OrganizationModel.domain == credentials.organization,
            )
            .first()
        )
        if u is None:
            return None
        user, org, org_id = u
        if not user.is_verified:
            if credentials.token == "":
                return None
            session = (
                self._session.query(SessionModel)
                .filter(
                    SessionModel.user_id == user.id,
                    SessionModel.organization_id == org_id,
                    SessionModel.access_token == credentials.token,
                )
                .first()
            )
            if session is None:
                return None
            user.is_verified = True
            self._session.delete(session)
            self._session.commit()

        return User(
            id=user.id,
            first_name=user.first_name,
            last_name=user.last_name,
            email=user.email,
            phone=user.phone,
            organization_domain=org,
            role=RelatedItem(id=user.role.id, value=user.role.name),
            password=user.password,
            organization_id=org_id,
        )

    def find_login_session(self, user: UserModel):
        session = (
            self._session.query(SessionModel)
            .filter(
                SessionModel.organization_id == user.organization_id,
                SessionModel.user_id == user.id,
            )
            .first()
        )
        if session and (
            not (
                self._jwt.parse_token(session.access_token)
                or self._jwt.parse_token(session.refresh_token)
            )
        ):
            self._session.delete(session)
            self._session.commit()
            return None
        return session and SessionToken(
            access_token=session.access_token,
            refresh_token=session.refresh_token,
            role=user.role.value,
        )

    def create_login_session(self, user: User):
        access_token = self._jwt.make_access_token(user.organization_domain, user.email)
        refresh_token = self._jwt.make_refresh_token(
            access_token, user.organization_domain, user.email
        )
        session = SessionModel(
            user_id=user.id,
            organization_id=user.organization_id,
            access_token=access_token,
            refresh_token=refresh_token,
        )
        self._session.add(session)
        self._session.commit()
        return SessionToken(
            access_token=access_token, refresh_token=refresh_token, role=user.role.value
        )

    def create_registration_session(self, auth_user: AuthUser, token: str):
        session = SessionModel(
            user_id=auth_user.user_id,
            organization_id=auth_user.organization_id,
            access_token=token,
            refresh_token="",
        )
        self._session.add(session)
        self._session.commit()

    def request_password_reset(self, details: ResetPasswordUser):
        user = (
            self._session.query(UserModel)
            .join(OrganizationModel)
            .filter(
                UserModel.email == details.email,
                OrganizationModel.domain == details.organization,
            )
            .first()
        )
        if user is None:
            raise LoginUserDoesNotExist(details.organization, details.email)
        access_token = self._jwt.make_access_token(details.organization, details.email)
        session = SessionModel(
            user_id=user.id,
            organization_id=user.organization_id,
            access_token=access_token,
            refresh_token="",
        )
        self._session.add(session)
        self._session.commit()

    def reset_password(self, details: ResetPasswordDetails):
        user = (
            self._session.query(UserModel)
            .join(OrganizationModel)
            .filter(
                UserModel.email == details.email,
                OrganizationModel.domain == details.organization,
            )
            .first()
        )
        if user is None:
            raise LoginUserDoesNotExist(details.organization, details.email)
        user.password = self._pass_enc.encrypt(details.password)

        session = (
            self._session.query(SessionModel)
            .filter(
                SessionModel.user_id == user.id,
                SessionModel.organization_id == user.organization_id,
                SessionModel.access_token == details.token,
            )
            .first()
        )
        if session is None:
            raise SessionDoesNotExist

        self._session.delete(session)
        self._session.commit()

    def refresh_session(self, refresh_token: str, email: str, organization: str):
        result = (
            self._session.query(SessionModel, UserModel)
            .join(UserModel)
            .filter(
                SessionModel.refresh_token == refresh_token, UserModel.email == email
            )
            .first()
        )
        if not result:
            return None
        session, user = result
        access_token = self._jwt.make_access_token(organization, email)
        new_refresh_token = self._jwt.make_refresh_token(
            access_token, organization, email
        )
        session.refresh_token = new_refresh_token
        session.access_token = access_token
        self._session.commit()

        return SessionToken(
            refresh_token=new_refresh_token,
            access_token=access_token,
            role=user.role.name,
        )

    def clear_login_session(self, auth_user: AuthUser):
        session = (
            self._session.query(SessionModel)
            .filter(
                SessionModel.organization_id == auth_user.organization_id,
                SessionModel.user_id == auth_user.user_id,
            )
            .first()
        )
        if session is None:
            raise SessionDoesNotExist
        self._session.delete(session)
        self._session.commit()
