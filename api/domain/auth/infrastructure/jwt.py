#!/usr/bin/env python3
import os
from datetime import datetime, timedelta
from typing import Optional, cast

import jwt

from domain.auth.ports.utils import Jwt

_secret = cast(str, os.getenv("SECRET"))

_algorithm = "HS256"


def _encode(data, secret: Optional[str] = None):
    return jwt.encode(data, secret or _secret, algorithm=_algorithm)


class JwtEncoder(Jwt):
    def make_access_token(
        self, organization_domain, user_email, *, minutes=None, days=1
    ):
        delta = dict(days=days)
        if minutes:
            delta = dict(minutes=minutes)
        expires = datetime.utcnow() + timedelta(**delta)
        print(expires)
        return _encode({"o": organization_domain, "e": user_email, "exp": expires})

    def make_refresh_token(self, access_token, organization_domain, user_email):
        expires = datetime.utcnow() + timedelta(days=7)
        return _encode(
            {
                "o": organization_domain,
                "e": user_email,
                "t": access_token,
                "exp": expires,
            }
        )

    def make_api_token(self, organization_domain, secret):
        return _encode(
            {
                "o": organization_domain,
            },
            secret=secret,
        )

    def parse_token(self, token, secret: Optional[str] = None):
        try:
            return jwt.decode(token, (secret or _secret), algorithms=[_algorithm])
        except jwt.ExpiredSignatureError:
            return None
