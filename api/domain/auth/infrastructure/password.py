from passlib.context import CryptContext

from domain.auth.ports.utils import PasswordEncryption

pwd_context = CryptContext(
    schemes=["pbkdf2_sha256"],
    default="pbkdf2_sha256",
    pbkdf2_sha256__default_rounds=30000,
)


class PasslibEncrypter(PasswordEncryption):
    def verify_same(self, hashed_password: str, given_password: str) -> bool:
        return pwd_context.verify(given_password, hashed_password)

    def encrypt(self, password: str) -> str:
        return pwd_context.encrypt(password)
