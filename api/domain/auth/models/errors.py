class AuthError(Exception):
    message: str


class LoginUserDoesNotExist(AuthError):
    def __init__(self, organization: str, email: str):
        self.message = f"User {email} does not exist in organization {organization}"
        super().__init__(organization, email)


class InvalidPassword(AuthError):
    def __init__(self, message: str):
        self.message = message
        super().__init__(message)


class ExpiredToken(AuthError):
    pass


class TokenDoesNotExist(AuthError):
    pass


class SessionDoesNotExist(AuthError):
    pass
