from dataclasses import dataclass
from dataclasses_json import dataclass_json


@dataclass_json
@dataclass(frozen=True)
class AccountRegistrationDetails:
    name: str
    domain: str
    first_name: str
    last_name: str
    password: str
    email: str
    phone: str
    callback_url: str


@dataclass_json
@dataclass(frozen=True)
class LoginCredentials:
    organization: str
    email: str
    password: str
    token: str


@dataclass(frozen=True)
class AuthUser:
    organization: str
    email: str
    user_id: int
    organization_id: int


@dataclass_json
@dataclass(frozen=True)
class ResetPasswordUser:
    organization: str
    email: str
    callback_url: str


@dataclass_json
@dataclass(frozen=True)
class ResetPasswordDetails:
    organization: str
    email: str
    password: str
    token: str


@dataclass_json
@dataclass
class SessionToken:
    access_token: str
    refresh_token: str
    role: str = ""
