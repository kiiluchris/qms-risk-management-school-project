from typing import Protocol, Optional
from abc import abstractmethod
from domain.auth.models import (
    AccountRegistrationDetails,
    LoginCredentials,
    SessionToken,
    ResetPasswordDetails,
    ResetPasswordUser,
)
from domain.auth.models import AuthUser
from domain.user.models import User


class AuthDatabase(Protocol):
    @abstractmethod
    def check_organization_exists(self, domain: str) -> bool:
        raise NotImplementedError

    @abstractmethod
    def create_account(self, details: AccountRegistrationDetails) -> (int, AuthUser):
        raise NotImplementedError

    @abstractmethod
    def find_login_user(self, credentials: LoginCredentials) -> User:
        raise NotImplementedError

    @abstractmethod
    def find_login_session(self, user: User) -> SessionToken:
        raise NotImplementedError

    @abstractmethod
    def create_login_session(self, user: User) -> SessionToken:
        raise NotImplementedError

    @abstractmethod
    def create_registration_session(self, auth_user: AuthUser, token: str):
        raise NotImplementedError

    @abstractmethod
    def refresh_session(
        self, refresh_token, credentials: LoginCredentials
    ) -> Optional[SessionToken]:
        raise NotImplementedError

    @abstractmethod
    def request_password_reset(self, details: ResetPasswordUser):
        raise NotImplementedError

    @abstractmethod
    def reset_password(self, details: ResetPasswordDetails):
        raise NotImplementedError

    @abstractmethod
    def clear_login_session(self, auth_user: AuthUser):
        raise NotImplementedError
