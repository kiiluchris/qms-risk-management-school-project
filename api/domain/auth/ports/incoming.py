from typing import Protocol
from abc import abstractmethod

from ..models import AccountRegistrationDetails, LoginCredentials


class VerifyOrganizationExists(Protocol):
    @abstractmethod
    def verify_organization_exists(self, domain: str) -> bool:
        raise NotImplementedError


class RegisterOrganizationAccount(Protocol):
    @abstractmethod
    def register_account(self, details: AccountRegistrationDetails) -> int:
        raise NotImplementedError


class SignIn(Protocol):
    @abstractmethod
    def sign_in(self, credentials: LoginCredentials) -> str:
        raise NotImplementedError
