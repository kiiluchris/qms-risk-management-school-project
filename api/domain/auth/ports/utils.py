from typing import Optional, Protocol
from abc import abstractmethod


class PasswordEncryption(Protocol):
    @abstractmethod
    def verify_same(self, hashed_password: str, given_password: str) -> bool:
        raise NotImplementedError

    @abstractmethod
    def encrypt(self, password: str) -> str:
        raise NotImplementedError


class Jwt(Protocol):
    @abstractmethod
    def make_access_token(
        self, organization_domain: str, user_email: str, *, minutes: int, days: int
    ) -> str:
        raise NotImplementedError

    def make_api_token(self, organization_domain: str, secret: str):
        raise NotImplementedError

    def make_refresh_token(
        self, access_token: str, organization_domain: str, user_email: str
    ) -> str:
        raise NotImplementedError

    def parse_token(self, token, *, secret: Optional[str] = None):
        raise NotImplementedError
