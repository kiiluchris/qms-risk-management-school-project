from typing import Union


from domain.auth.ports.incoming import (
    VerifyOrganizationExists,
    RegisterOrganizationAccount,
    SignIn,
)
from domain.auth.ports.outgoing import AuthDatabase
from domain.auth.ports.utils import PasswordEncryption
from domain.auth.models import (
    AccountRegistrationDetails,
    LoginCredentials,
)
from domain.auth.models.errors import LoginUserDoesNotExist, InvalidPassword


class AuthFacade(VerifyOrganizationExists, RegisterOrganizationAccount, SignIn):
    def __init__(self, db: AuthDatabase, pe: PasswordEncryption):
        self._db = db
        self._pass_enc = pe

    def verify_organization_exists(self, domain: str) -> bool:
        return self._db.check_organization_exists(domain)

    def register_account(self, details: AccountRegistrationDetails):
        if not self._db.check_organization_exists(details.domain):
            return self._db.create_account(details)
        return -1

    def sign_in(self, credentials: LoginCredentials):
        user = self._db.find_login_user(credentials)
        if not user:
            raise LoginUserDoesNotExist(credentials.organization, credentials.email)
        if not self._pass_enc.verify_same(user.password, credentials.password):
            raise InvalidPassword(credentials.password)
        session = self._db.find_login_session(user)
        if not session:
            session = self._db.create_login_session(user)
        return session


AuthFacadeT = Union[VerifyOrganizationExists, RegisterOrganizationAccount, SignIn]
