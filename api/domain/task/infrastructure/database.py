import operator
from typing import List, Optional, cast
import itertools

from sqlalchemy import insert, select, update, delete, bindparam, func
from sqlalchemy.sql import Select
from sqlalchemy.sql.elements import or_
from sqlalchemy.sql.expression import and_
from sqlalchemy.orm import scoped_session

from infrastructure.models import (
    TaskModel,
    OrganizationModel,
    ProcessModel,
    TaskTeamModel,
    UserModel,
    RoleModel,
    task_team_user_assoc,
)
from domain.auth.models import AuthUser

from domain.task.ports.storage import TaskDatabase
from domain.task.models import (
    Task,
    TaskQueryParams,
    TaskUpdate,
    NewTask,
    TaskImport,
)
from dataclasses import asdict
from domain.shared.models import RelatedItem
from domain.user.models import user_full_name
from domain.user.ports.storage import UserDatabase
from domain.user.infrastructure.database import UserDatabaseAdapter


class TaskDatabaseAdapter(TaskDatabase):
    def __init__(self, session: scoped_session, user_db: UserDatabase):
        self._session = session
        self._user_db = cast(UserDatabaseAdapter, user_db)

    def exists(self, auth_user: AuthUser, query_params: TaskQueryParams) -> bool:
        query = (
            select(func.count())
            .select_from(TaskModel)
            .where(
                and_(
                    TaskModel.organization_id == auth_user.organization_id,
                    TaskModel.name == query_params.name,
                )
            )
            .limit(1)
        )
        count = self._session.scalar(query)

        return count > 0

    def get_team_member_contacts(self, auth_user: AuthUser, task_id: int):
        task = self._session.query(TaskModel).get(task_id)
        team = task.process.team_responsible_obj
        leader = self._session.query(UserModel).get(team.leader)
        contacts = []
        for member in itertools.chain([leader], team.members):
            contacts.append(
                {
                    "email": member.email,
                    "phone": member.phone,
                    "use_sms": member.use_sms,
                }
            )
        return contacts

    def record_to_domain_model(
        self,
        task: TaskModel,
        organization: OrganizationModel,
        process: ProcessModel,
        user_owner: Optional[UserModel],
        # role_owner: RoleModel,
    ) -> Task:
        return Task(
            id=task.id,
            name=task.name,
            is_complete=task.is_complete,
            due_by=task.due_by,
            organization_id=organization.id,
            process=RelatedItem(
                id=process.id,
                value=process.name,
                readable=process.name,
            ),
            user_owner=user_owner
            and RelatedItem(
                id=user_owner.id,
                value=user_full_name(user_owner),
                readable=user_owner.email,
            ),
            # role_owner=RelatedItem(
            #    id=role_owner.id,
            #    value=role_owner.name,
            #    readable=role_owner.name,
            # ),
        )

    def select_query(
        self, auth_user: AuthUser, query_params: Optional[TaskQueryParams] = None
    ) -> Select:
        query = (
            select(
                TaskModel,
                OrganizationModel,
                ProcessModel,
                UserModel,
                # RoleModel,
            )
            .select_from(TaskModel)
            .join(ProcessModel, TaskModel.process_id == ProcessModel.id)
            .join(UserModel, UserModel.id == TaskModel.user_owner_id, isouter=True)
            # .join_from(TaskModel, RoleModel)
            .join(OrganizationModel, TaskModel.organization_id == OrganizationModel.id)
            .where(TaskModel.organization_id == auth_user.organization_id)
        )
        user_id = auth_user.user_id
        if query_params:
            if query_params.is_complete is not None:
                query = query.where(TaskModel.is_complete == query_params.is_complete)
            if query_params.user_id is not None:
                user_id = query_params.user_id
                # query = query.join(
                #     task_team_user_assoc,
                #     task_team_user_assoc.columns.task_team_id
                #     == ProcessModel.team_responsible,
                # ).where(
                #     or_(
                #         ProcessModel.person_responsible == user_id,
                #         task_team_user_assoc.columns.user_id == user_id,
                #     )
                # )
            if query_params.process_id is not None:
                query = query.where(ProcessModel.id == query_params.process_id)
        user = self._session.query(UserModel).get(user_id)
        if user:
            role = user.role.name
            # if role == "Employee":
            #     query = query.where(TaskModel.user_owner_id == user_id)
            # elif role == "Manager":
            print("ASSS", user, role)
            if role != "Admin":
                query = (
                    query.join(
                        task_team_user_assoc,
                        task_team_user_assoc.columns.user_id == UserModel.id,
                    )
                    .join(
                        TaskTeamModel,
                        TaskTeamModel.id == task_team_user_assoc.columns.task_team_id,
                    )
                    .where(
                        or_(
                            TaskTeamModel.leader == user_id,
                            task_team_user_assoc.columns.user_id == user_id,
                        )
                    )
                )

        return query

    def query(self, auth_user: AuthUser, query_params: TaskQueryParams):
        query = self.select_query(auth_user, query_params)
        tasks = self._session.execute(query)

        return [self.record_to_domain_model(*row) for row in tasks]

    def get_by_id(self, auth_user: AuthUser, task_id: int):
        query = self.select_query(auth_user).where(TaskModel.id == task_id)
        result = self._session.execute(query).first()
        if result is None:
            return None

        return self.record_to_domain_model(*result)

    def create_obj(self, auth_user: AuthUser, task: NewTask, *, nocommit: bool = False):
        new_task = TaskModel(
            name=task.name,
            is_complete=task.is_complete,
            due_by=task.due_by,
            organization_id=auth_user.organization_id,
            process_id=task.process.id,
            user_owner_id=task.user_owner and task.user_owner.id,
            # role_owner_id=task.role_owner.id,
        )
        self._session.add(new_task)
        if not nocommit:
            self._session.commit()

        return new_task

    def create(self, auth_user: AuthUser, task: NewTask, *, nocommit: bool = False):
        return self.create_obj(auth_user, task, nocommit=nocommit).id

    def task_to_dict(self, auth_user: AuthUser, task: NewTask):
        result = {
            **asdict(task),
            "organization_id": auth_user.organization_id,
            "process_id": task.process.id,
            "user_owner_id": task.user_owner and task.user_owner.id,
            # "role_owner_id": task.role_owner.id,
        }
        del result["process"]
        result.pop("user_owner", None)
        # del result["role_owner"]

        return result

    def create_many(self, auth_user: AuthUser, tasks: List[NewTask]):
        query = insert(TaskModel)  # .returning(TaskModel.id)
        ids = self._session.execute(
            query, [self.task_to_dict(auth_user, task) for task in tasks]
        )
        self._session.commit()

        return ids

    def import_many(self, auth_user: AuthUser, tasks: List[TaskImport]):
        group_key = operator.attrgetter("process")
        grouped_tasks = {
            key: list(group)
            for key, group in itertools.groupby(
                sorted(tasks, key=group_key), key=group_key
            )
        }
        user_keys = set(t.user_owner for t in tasks)
        users = self._user_db.fetch_existing_users(auth_user, user_keys)
        process_results = self._session.execute(
            select(ProcessModel).where(
                and_(
                    ProcessModel.organization_id == auth_user.organization_id,
                    ProcessModel.name.in_(grouped_tasks.keys()),
                )
            )
        )
        inserts = []
        for process, *_ in process_results:
            group = grouped_tasks[process.name]
            for task in group:
                user = users.get(task.user_owner)
                inserts.append(
                    {
                        "process_id": process.id,
                        "organization_id": auth_user.organization_id,
                        "name": task.name,
                        "is_complete": task.is_complete,
                        "due_by": task.due_by,
                        "user_owner_id": user and user.id,
                    }
                )
        query = insert(TaskModel)  # .returning(TaskModel.id)
        cursor_result = self._session.execute(query, inserts)
        self._session.commit()

        return cursor_result.rowcount

    # Edit before usage
    def toggle_completion(self, auth_user: AuthUser, task_id: int):
        query = (
            select(TaskModel).where(
                and_(
                    TaskModel.id == task_id,
                    TaskModel.organization_id == auth_user.organization_id,
                )
            )
            # .returning(TaskModel.id)
        )
        result = list(self._session.execute(query))
        if not result:
            return -1
        task, *_ = result
        task.is_complete = not task.is_complete
        self._session.commit()
        return task_id

    def update_by_id(self, auth_user: AuthUser, task_id: int, task_updates: TaskUpdate):
        query = (
            update(TaskModel)
            .where(
                and_(
                    TaskModel.id == task_id,
                    TaskModel.organization_id == auth_user.organization_id,
                )
            )
            .values(self.task_to_dict(auth_user, task_updates))
            # .returning(TaskModel.id)
        )
        update_cursor = self._session.execute(query)
        if update_cursor is not None:
            self._session.commit()
            return task_id

        return -1

    def delete_by_id(self, auth_user: AuthUser, task_id: int):
        query = delete(TaskModel).where(
            and_(
                TaskModel.id == task_id,
                TaskModel.organization_id == auth_user.organization_id,
            )
        )  # .returning(TaskModel.id)
        delete_cursor = self._session.execute(query)
        print("Deleted ID", delete_cursor)
        if delete_cursor is not None:
            self._session.commit()
            return task_id

        return -1
