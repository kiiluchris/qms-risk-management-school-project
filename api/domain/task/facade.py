#!/usr/bin/env python3

from domain.email.ports import EmailClient
from domain.sms.ports import SmsClient
from typing import List

from domain.auth.models import AuthUser
from domain.task.models import (
    Task,
    TaskImport,
    TaskQueryParams,
    NewTask,
    TaskUpdate,
)
from domain.task.ports.storage import TaskDatabase


class TaskFacade:
    def __init__(self, db: TaskDatabase, sms: SmsClient, email: EmailClient):
        self._db = db
        self._email = email
        self._sms = sms

    def get_tasks(self, auth_user: AuthUser, query_params: TaskQueryParams):
        return self._db.query(auth_user, query_params)

    def get_task(self, auth_user: AuthUser, task_id: int):
        return self._db.get_by_id(auth_user, task_id)

    def create_task(self, auth_user: AuthUser, task: NewTask):
        id_ = self._db.create(auth_user, task)
        if id_ and id_ != -1:
            reason = "New Task"
            process_name = task.process.value
            message = (
                f'A new task has been created for process "{process_name}"\n'
                "with the following details:\n"
                f" Name: {task.name}"
            )
            contacts = self._db.get_team_member_contacts(auth_user, id_)
            to_email = []
            to_sms = []
            for contact in contacts:
                to_email.append(contact["email"])
                if contact["use_sms"]:
                    to_sms.append(contact["phone"])
            self._email.send_notifications(to_email, reason, message)
            if to_sms:
                self._sms.send(reason + "\n\n" + message)

        return id_

    def complete_task(self, auth_user: AuthUser, task_id: int):
        return self._db.toggle_completion(auth_user, task_id)

    def update_task(self, auth_user: AuthUser, task_id: int, task: TaskUpdate):
        return self._db.update_by_id(auth_user, task_id, task)

    def delete_task(self, auth_user: AuthUser, task_id: int):
        return self._db.delete_by_id(auth_user, task_id)

    def import_tasks(self, auth_user: AuthUser, tasks: List[TaskImport]):
        return self._db.import_many(auth_user, tasks)


TaskFacadeT = TaskFacade
