#!/usr/bin/env python3

from domain.process.infrastructure.mixins.get_process import GetProcess
from typing import Protocol, List, Optional
from abc import abstractmethod

from domain.auth.models import AuthUser

from domain.task.models import (
    Task,
    TaskImport,
    TaskQueryParams,
    TaskUpdate,
    NewTask,
)


class TaskDatabase(GetProcess):
    @abstractmethod
    def query(self, auth_user: AuthUser, query_params: TaskQueryParams) -> List[Task]:
        raise NotImplementedError

    @abstractmethod
    def get_by_id(self, auth_user: AuthUser, task_id: int) -> Optional[Task]:
        raise NotImplementedError

    @abstractmethod
    def exists(self, auth_user: AuthUser, query_params: TaskQueryParams) -> bool:
        raise NotImplementedError

    @abstractmethod
    def create(self, auth_user: AuthUser, task: NewTask) -> int:
        raise NotImplementedError

    @abstractmethod
    def create_many(self, auth_user: AuthUser, tasks: List[NewTask]) -> List[int]:
        raise NotImplementedError

    @abstractmethod
    def import_many(self, auth_user: AuthUser, tasks: List[TaskImport]) -> List[int]:
        raise NotImplementedError

    @abstractmethod
    def toggle_completion(self, auth_user: AuthUser, task_id: int) -> int:
        raise NotImplementedError

    @abstractmethod
    def update_by_id(
        self, auth_user: AuthUser, task_id: int, task_updates: TaskUpdate
    ) -> int:
        raise NotImplementedError

    @abstractmethod
    def delete_by_id(self, auth_user: AuthUser, task_id: int) -> int:
        raise NotImplementedError

    @abstractmethod
    def get_team_member_contacts(self, auth_user: AuthUser, task_id: int):
        raise NotImplementedError
