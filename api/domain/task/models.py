from datetime import datetime, date
from typing import Optional

from dataclasses import dataclass, field
from dataclasses_json import dataclass_json, config

from domain.shared.models import (
    RelatedItem,
)
from domain.shared.mixins import PartialMixin


def wrap_non_nullable_fn(f):
    def go(x):
        return f(x) if x is not None else x

    return go


@dataclass_json
@dataclass(frozen=True)
class ProcessNewTask:
    name: str
    is_complete: bool
    due_by: Optional[date] = field(
        metadata=config(
            encoder=wrap_non_nullable_fn(date.isoformat),
            decoder=wrap_non_nullable_fn(date.fromisoformat),
        ),
        default=None,
    )

    def to_new_task(self, process: RelatedItem) -> "NewTask":
        return NewTask(
            name=self.name,
            is_complete=self.is_complete,
            due_by=self.due_by,
            process=process,
        )


@dataclass_json
@dataclass(frozen=True)
class TaskImport:
    process: str
    name: str
    is_complete: bool
    user_owner: str
    due_by: Optional[date] = field(
        metadata=config(
            encoder=wrap_non_nullable_fn(date.isoformat),
            decoder=wrap_non_nullable_fn(date.fromisoformat),
        ),
        default=None,
    )


@dataclass_json
@dataclass(frozen=True)
class NewTask:
    process: RelatedItem
    name: str
    is_complete: bool
    user_owner: Optional[RelatedItem] = None
    due_by: Optional[date] = field(
        metadata=config(
            encoder=wrap_non_nullable_fn(date.isoformat),
            decoder=wrap_non_nullable_fn(date.fromisoformat),
        ),
        default=None,
    )
    # role_owner: RelatedItem


TaskUpdate = NewTask


@dataclass_json
@dataclass(frozen=True)
class Task(NewTask):
    organization_id: Optional[int] = None
    id: Optional[int] = None


ProcessTaskUpdate = Task


@dataclass_json
@dataclass(frozen=True)
class TaskPartial(PartialMixin):
    name: Optional[str] = None
    is_complete: Optional[bool] = None
    due_by: Optional[date] = field(
        metadata=config(
            encoder=wrap_non_nullable_fn(date.isoformat),
            decoder=wrap_non_nullable_fn(date.fromisoformat),
        ),
        default=None,
    )
    organization_id: Optional[RelatedItem] = None
    process_id: Optional[RelatedItem] = None
    # user_owner_id: Optional[RelatedItem] = None
    # role_owner_id: Optional[RelatedItem] = None


@dataclass(frozen=True)
class TaskQueryParams:
    user_id: Optional[int] = None
    name: Optional[str] = None
    is_complete: Optional[bool] = None
    due_by: Optional[date] = field(
        metadata=config(
            encoder=wrap_non_nullable_fn(date.isoformat),
            decoder=wrap_non_nullable_fn(date.fromisoformat),
        ),
        default=None,
    )
    organization_id: Optional[int] = None
    process_id: Optional[int] = None
    # user_owner_id: Optional[RelatedItem] = None
    # role_owner_id: Optional[RelatedItem] = None
