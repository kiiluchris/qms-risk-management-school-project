import enum
from dataclasses import dataclass
from dataclasses_json import dataclass_json


@dataclass_json
@dataclass(frozen=True)
class RelatedItem:
    id: int
    value: str
    readable: str = ""


class MonitoringSchedule(enum.Enum):
    Daily = 1
    Weekly = 2
    Monthly = 3
    Quarterly = 4
    Biannually = 5
    Annually = 6


class ObjectiveStatus(enum.Enum):
    Open = 1
    Closed = 2


class CapaClassification(enum.Enum):
    Corrective = 1
    Preventive = 2
    Both = 3


class CapaRating(enum.Enum):
    Low = 1
    Moderate = 2
    High = 3


class RolePermission(enum.Enum):
    Admin = 1
    Manager = 2
    Employee = 3


class RiskStatus(enum.Enum):
    Identified = "Identified"
    Assessed = "Assessed"
    Controlled = "Controlled"


class RiskCategory(enum.Enum):
    Financial = "Financial"
    HealthAndSafety = "Health & Safety"
    Operational = "Operational"
    RegulationAndCompliance = "Regulation & Compliance"
    Reputation = "Reputational"
    SecurityAndPrivacy = "Security & Privacy"
    Strategic = "Strategic"
