#!/usr/bin/env python3

from enum import Enum
from typing import Type
from dataclasses_json import config


def dataclasses_json_enum_config(enum: Type[Enum], **kwargs):
    return config(encoder=lambda s: s.name, decoder=lambda s: enum[s], **kwargs)
