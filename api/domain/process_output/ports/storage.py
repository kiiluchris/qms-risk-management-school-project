#!/usr/bin/env python3

from typing import Protocol, List, Optional
from abc import abstractmethod

from domain.auth.models import AuthUser

from domain.process_output.models import (
    ProcessOutput,
    ProcessOutputQueryParams,
    ProcessOutputUpdate,
    NewProcessOutput,
)


class ProcessOutputDatabase(Protocol):
    @abstractmethod
    def query(self, auth_user: AuthUser, query_params: ProcessOutputQueryParams) -> List[ProcessOutput]:
        raise NotImplementedError

    @abstractmethod
    def get_by_id(self, auth_user: AuthUser, process_output_id: int) -> Optional[ProcessOutput]:
        raise NotImplementedError

    @abstractmethod
    def exists(self, auth_user: AuthUser, query_params: ProcessOutputQueryParams) -> bool:
        raise NotImplementedError
    
    @abstractmethod
    def create(self, auth_user: AuthUser, process_output: NewProcessOutput) -> int:
        raise NotImplementedError

    @abstractmethod
    def create_many(self, auth_user: AuthUser, process_outputs: List[NewProcessOutput]) -> List[int]:
        raise NotImplementedError
    
    @abstractmethod
    def update_by_id(self, auth_user: AuthUser, process_output_id: int, process_output_updates: ProcessOutputUpdate) -> int:
        raise NotImplementedError

    @abstractmethod
    def delete_by_id(self, auth_user: AuthUser, process_output_id: int) -> int:
        raise NotImplementedError