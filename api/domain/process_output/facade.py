#!/usr/bin/env python3

from typing import List

from domain.auth.models import AuthUser
from domain.process_output.models import (
    ProcessOutput,
    ProcessOutputQueryParams,
    NewProcessOutput,
    ProcessOutputUpdate,
)
from domain.process_output.ports.storage import ProcessOutputDatabase


class ProcessOutputFacade:
    def __init__(self, db: ProcessOutputDatabase):
        self._db = db

    def get_process_outputs(self, auth_user: AuthUser, query_params: ProcessOutputQueryParams):
        return self._db.query(auth_user, query_params)

    def get_process_output(self, auth_user: AuthUser, process_output_id: int):
        return self._db.get_by_id(auth_user, process_output_id)

    def create_process_output(self, auth_user: AuthUser, process_output: NewProcessOutput):
        if self._db.exists(auth_user, ProcessOutputQueryParams(name = process_output.name)):
            return -1
        return self._db.create(auth_user, process_output)

    def update_process_output(self, auth_user: AuthUser, process_output_id: int, process_output: ProcessOutputUpdate):
        return self._db.update_by_id(auth_user, process_output_id, process_output)

    def delete_process_output(self, auth_user: AuthUser, process_output_id: int):
        return self._db.delete_by_id(auth_user, process_output_id)

    def import_process_outputs(self, auth_user: AuthUser, process_outputs: List[NewProcessOutput]):
        return self._db.create_many(auth_user, process_outputs)

        
        


ProcessOutputFacadeT = ProcessOutputFacade