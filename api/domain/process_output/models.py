from datetime import datetime, date
from typing import Optional

from dataclasses import dataclass, field
from dataclasses_json import dataclass_json, config

from domain.shared.models import (
    RelatedItem,
)
from domain.shared.mixins import PartialMixin


@dataclass_json
@dataclass(frozen=True)
class NewProcessOutput:
    name: str
    value: int
    unit: RelatedItem


ProcessOutputUpdate = NewProcessOutput


@dataclass_json
@dataclass(frozen=True)
class ProcessOutput(NewProcessOutput):
    organization_id: Optional[int] = None
    id: Optional[int] = None


@dataclass_json
@dataclass(frozen=True)
class ProcessOutputPartial(PartialMixin):
    name: Optional[str] = None
    value: Optional[int] = None
    unit: Optional[RelatedItem] = None


@dataclass(frozen=True)
class ProcessOutputQueryParams:
    id: Optional[int] = None
    risk_id: Optional[int] = None
    name: Optional[str] = None
    value: Optional[int] = None
    unit: Optional[RelatedItem] = None
