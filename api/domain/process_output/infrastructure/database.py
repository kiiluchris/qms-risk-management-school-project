from typing import List, Optional

from sqlalchemy import insert, select, update, delete, bindparam, func, alias
from sqlalchemy.sql import Select
from sqlalchemy.sql.expression import and_
from sqlalchemy.orm import scoped_session

from infrastructure.models import (
    ProcessOutputModel,
    UnitModel,
    risk_output_assoc,
)
from domain.auth.models import AuthUser

from domain.process_output.ports.storage import ProcessOutputDatabase
from domain.process_output.models import (
    ProcessOutput,
    ProcessOutputQueryParams,
    ProcessOutputUpdate,
    NewProcessOutput,
)
from dataclasses import asdict
from domain.shared.models import RelatedItem


class ProcessOutputDatabaseAdapter(ProcessOutputDatabase):
    def __init__(self, session: scoped_session):
        self._session = session

    def exists(
        self, auth_user: AuthUser, query_params: ProcessOutputQueryParams
    ) -> bool:
        query = (
            select(func.count())
            .select_from(ProcessOutputModel)
            .where(
                and_(
                    ProcessOutputModel.organization_id == auth_user.organization_id,
                    ProcessOutputModel.name == query_params.name,
                )
            )
            .limit(1)
        )
        count = self._session.scalar(query)

        return count > 0

    def record_to_domain_model(
        self,
        process_output: ProcessOutputModel,
        unit: UnitModel,
    ) -> ProcessOutput:
        return ProcessOutput(
            id=process_output.id,
            name=process_output.name,
            value=process_output.value,
            unit=RelatedItem(
                id=unit.id,
                value=unit.name,
                readable=unit.name,
            ),
        )

    def select_query(
        self,
        auth_user: AuthUser,
        query_params: Optional[ProcessOutputQueryParams] = None,
    ) -> Select:
        query = (
            select(
                ProcessOutputModel,
                UnitModel,
            )
            .select_from(ProcessOutputModel)
            .join(UnitModel, ProcessOutputModel.unit == UnitModel.id)
            .where(ProcessOutputModel.organization_id == auth_user.organization_id)
        )
        if query_params:
            if query_params.risk_id is not None:
                query = query.join(
                    risk_output_assoc,
                    risk_output_assoc.columns.process_output_id
                    == ProcessOutputModel.id,
                ).where(risk_output_assoc.columns.risk_id == query_params.risk_id)

        return query

    def query(self, auth_user: AuthUser, query_params: ProcessOutputQueryParams):
        query = self.select_query(auth_user)
        process_outputs = self._session.execute(query)

        return [self.record_to_domain_model(*row) for row in process_outputs]

    def get_by_id(self, auth_user: AuthUser, process_output_id: int):
        query = self.select_query(auth_user).where(
            ProcessOutputModel.id == process_output_id
        )
        result = self._session.execute(query).first()
        if result is None:
            return None

        return self.record_to_domain_model(*result)

    def create(self, auth_user: AuthUser, process_output: NewProcessOutput):
        new_process_output = ProcessOutputModel(
            name=process_output.name,
            value=process_output.value,
            unit=process_output.unit.id,
        )
        self._session.add(new_process_output)
        self._session.commit()

        return new_process_output.id

    def process_output_to_dict(
        self, auth_user: AuthUser, process_output: NewProcessOutput
    ):
        result = {
            **asdict(process_output),
            "unit": process_output.unit.id,
        }

        return result

    def create_many(self, auth_user: AuthUser, process_outputs: List[NewProcessOutput]):
        query = insert(ProcessOutputModel)  # .returning(ProcessOutputModel.id)
        ids = self._session.execute(
            query,
            [
                self.process_output_to_dict(auth_user, process_output)
                for process_output in process_outputs
            ],
        )
        self._session.commit()

        return ids

    # Edit before usage
    def update_by_id(
        self,
        auth_user: AuthUser,
        process_output_id: int,
        process_output_updates: ProcessOutputUpdate,
    ):
        query = (
            update(ProcessOutputModel)
            .where(
                and_(
                    ProcessOutputModel.id == process_output_id,
                    ProcessOutputModel.organization_id == auth_user.organization_id,
                )
            )
            .values(self.process_output_to_dict(auth_user, process_output_updates))
            # .returning(ProcessOutputModel.id)
        )
        update_cursor = self._session.execute(query)
        if update_cursor is not None:
            self._session.commit()
            return process_output_id

        return -1

    def delete_by_id(self, auth_user: AuthUser, process_output_id: int):
        query = delete(ProcessOutputModel).where(
            and_(
                ProcessOutputModel.id == process_output_id,
                ProcessOutputModel.organization_id == auth_user.organization_id,
            )
        )  # .returning(ProcessOutputModel.id)
        delete_cursor = self._session.execute(query)
        print("Deleted ID", delete_cursor)
        if delete_cursor is not None:
            self._session.commit()
            return process_output_id

        return -1
