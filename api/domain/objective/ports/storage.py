#!/usr/bin/env python3

from typing import Protocol, List, Optional
from abc import abstractmethod

from domain.auth.models import AuthUser

from domain.objective.models import (
    Objective,
    ObjectiveImport,
    ObjectiveQueryParams,
    NewObjective,
    ObjectiveUpdate,
)


class ObjectiveDatabase(Protocol):
    @abstractmethod
    def query(
        self, auth_user: AuthUser, query_params: ObjectiveQueryParams
    ) -> List[Objective]:
        raise NotImplementedError

    @abstractmethod
    def get_by_id(self, auth_user: AuthUser, objective_id: int) -> Optional[Objective]:
        raise NotImplementedError

    @abstractmethod
    def exists(self, auth_user: AuthUser, query_params: ObjectiveQueryParams) -> bool:
        raise NotImplementedError

    @abstractmethod
    def create(self, auth_user: AuthUser, objective: NewObjective) -> int:
        raise NotImplementedError

    @abstractmethod
    def create_many(
        self, auth_user: AuthUser, objectives: List[ObjectiveImport]
    ) -> List[int]:
        raise NotImplementedError

    @abstractmethod
    def update_by_id(
        self, auth_user: AuthUser, objective_id: int, objective: ObjectiveUpdate
    ) -> int:
        raise NotImplementedError

    @abstractmethod
    def delete_by_id(self, auth_user: AuthUser, objective_id: int) -> int:
        raise NotImplementedError
