#!/usr/bin/env python3


from domain.user.infrastructure.database import UserDatabaseAdapter
from itertools import groupby
import operator
from sqlalchemy import insert, select, update, delete, bindparam, func
from sqlalchemy.sql import Select
from sqlalchemy.orm import scoped_session
from typing import List, NamedTuple

from sqlalchemy.sql.expression import and_
from infrastructure.models import (
    ObjectiveModel,
    UserModel,
    ProcessOutputModel,
    OrganizationModel,
)
from domain.auth.models import AuthUser

from domain.objective.ports.storage import ObjectiveDatabase
from domain.objective.models import (
    Objective,
    ObjectiveImport,
    ObjectiveQueryParams,
    ObjectiveUpdate,
)
from dataclasses import asdict
from domain.shared.models import RelatedItem


class ObjectiveImportKeys(NamedTuple):
    users: set[str]


class ObjectiveDatabaseAdapter(ObjectiveDatabase):
    def __init__(self, session: scoped_session, user_db: UserDatabaseAdapter):
        self._session = session
        self._user_db = user_db

    def exists(self, auth_user: AuthUser, query_params: ObjectiveQueryParams) -> bool:
        query = (
            select(func.count())
            .select_from(ObjectiveModel)
            .where(
                ObjectiveModel.organization_id == auth_user.organization_id,
                ObjectiveModel.name == query_params.name,
            )
            .limit(1)
        )
        count = self._session.scalar(query)

        return count > 0

    def fetch_existing_objectives(
        self, auth_user: AuthUser, objective_keys: set[str]
    ) -> dict[str, ObjectiveModel]:
        objective_results = self._session.execute(
            select(ObjectiveModel).where(
                and_(
                    ObjectiveModel.organization_id == auth_user.organization_id,
                    ObjectiveModel.name.in_(objective_keys),
                )
            )
        )
        return {o.name: o for o, *_ in objective_results}

    def record_to_domain_model(
        self,
        objective: ObjectiveModel,
        user: UserModel,
        organization: OrganizationModel,
    ) -> Objective:
        return Objective(
            id=objective.id,
            name=objective.name,
            person_responsible=RelatedItem(
                id=user.id, value=user.email, readable=user.last_name
            ),
            target_date=objective.target_date,
            current_status=objective.current_status,
            description=objective.description,
            kpi=objective.kpi,
            organization_id=RelatedItem(
                id=organization.id,
                value=organization.domain,
                readable=organization.domain,
            ),
        )

    def select_query(self, auth_user: AuthUser) -> Select:
        return (
            select(
                ObjectiveModel,
                UserModel,
                OrganizationModel,
            )
            .select_from(ObjectiveModel)
            .join(UserModel, UserModel.id == ObjectiveModel.person_responsible)
            .join(
                OrganizationModel,
                OrganizationModel.id == ObjectiveModel.organization_id,
            )
            .where(ObjectiveModel.organization_id == auth_user.organization_id)
        )

    def query(self, auth_user: AuthUser, query_params: ObjectiveQueryParams):
        query = self.select_query(auth_user)
        objectives = self._session.execute(query)

        return [self.record_to_domain_model(*row) for row in objectives]

    def get_by_id(self, auth_user: AuthUser, objective_id: int):
        query = self.select_query(auth_user).where(
            ObjectiveModel.id == objective_id,
        )
        result = self._session.execute(query).first()
        if result is None:
            return None

        return self.record_to_domain_model(*result)

    def create(self, auth_user: AuthUser, objective: Objective):
        new_objective = ObjectiveModel(
            name=objective.name,
            person_responsible=objective.person_responsible.id,
            target_date=objective.target_date,
            current_status=objective.current_status,
            description=objective.description,
            kpi=objective.kpi,
            organization_id=auth_user.organization_id,
        )
        self._session.add(new_objective)
        self._session.commit()

        return new_objective.id

    def _extract_import_keys(
        self, objectives: list[ObjectiveImport]
    ) -> ObjectiveImportKeys:
        users = set()
        for o in objectives:
            users.add(o.person_responsible)

        return ObjectiveImportKeys(
            users=users,
        )

    def create_many(self, auth_user: AuthUser, objectives: List[ObjectiveImport]):
        import_keys = self._extract_import_keys(objectives)
        users = self._user_db.fetch_existing_users(auth_user, import_keys.users)
        insert_values = []
        for o in objectives:
            user = users.get(o.person_responsible)
            if not user:
                continue
            insert_values.append(
                {
                    **asdict(o),
                    "organization_id": auth_user.organization_id,
                    "person_responsible": user.id,
                }
            )

        query = insert(ObjectiveModel)  # .returning(ObjectiveModel.id)
        cursor_result = self._session.execute(query, insert_values)
        self._session.commit()

        return cursor_result.rowcount

    # Edit before usage
    def update_by_id(
        self,
        auth_user: AuthUser,
        objective_id: int,
        objective_updates: ObjectiveUpdate,
    ):
        print(
            objective_updates,
            objective_updates.target_date,
            repr(objective_updates.target_date),
        )
        print(objective_updates.to_dict())
        query = (
            update(ObjectiveModel)
            .where(
                ObjectiveModel.id == objective_id,
                ObjectiveModel.organization_id == auth_user.organization_id,
            )
            .values(
                {
                    **asdict(objective_updates),
                    "person_responsible": objective_updates.person_responsible.id,
                }
            )
            # .returning(ObjectiveModel.id)
        )
        update_cursor = self._session.execute(query)
        if update_cursor is not None:
            self._session.commit()
            return objective_id

        return -1

    def delete_by_id(self, auth_user: AuthUser, objective_id: int):
        query = (
            delete(ObjectiveModel).where(ObjectiveModel.id == objective_id)
            # .returning(ObjectiveModel.id)
        )
        delete_cursor = self._session.execute(query)
        print("Deleted ID", delete_cursor)
        if delete_cursor is not None:
            self._session.commit()
            return objective_id

        return -1
