import enum
from datetime import datetime, date
from typing import Optional

from dataclasses import dataclass, field
from dataclasses_json import dataclass_json, config

from domain.shared.models import RelatedItem, ObjectiveStatus
from domain.shared.mixins import PartialMixin


@dataclass_json
@dataclass(frozen=True)
class NewObjective:
    name: str
    person_responsible: RelatedItem
    target_date: date = field(
        metadata=config(
            encoder=date.isoformat,
            decoder=date.fromisoformat,
        ),
    )
    current_status: ObjectiveStatus = field(
        metadata=config(encoder=lambda s: s.name, decoder=lambda s: ObjectiveStatus[s])
    )
    description: str
    kpi: str


@dataclass_json
@dataclass(frozen=True)
class ObjectiveImport(NewObjective):
    person_responsible: str


@dataclass_json
@dataclass(frozen=True)
class Objective(NewObjective):
    organization_id: RelatedItem
    id: Optional[int] = None


ObjectiveUpdate = NewObjective
# @dataclass_json
# @dataclass(frozen=True)
# class ObjectivePartial(PartialMixin):
#     name: Optional[str] = None
#     person_responsible: Optional[RelatedItem] = None
#     target_date: Optional[date] = field(
#         metadata=config(
#             encoder=date.isoformat,
#             decoder=date.fromisoformat,
#         ),
#         default=None,
#     )
#     current_status: Optional[bool] = None
#     description: Optional[str] = None
#     desired_output: Optional[RelatedItem] = None
#     kpi: Optional[str] = None
#     organization_id: Optional[RelatedItem] = None


@dataclass(frozen=True)
class ObjectiveQueryParams:
    name: Optional[str] = None
    person_responsible: Optional[RelatedItem] = None
    target_date: Optional[date] = field(
        metadata=config(
            encoder=date.isoformat,
            decoder=date.fromisoformat,
        ),
        default=None,
    )
    current_status: Optional[bool] = None
    description: Optional[str] = None
    desired_output: Optional[RelatedItem] = None
    kpi: Optional[str] = None
    organization_id: Optional[RelatedItem] = None
