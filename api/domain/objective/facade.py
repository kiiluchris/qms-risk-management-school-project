#!/usr/bin/env python3

from typing import List

from domain.auth.models import AuthUser
from domain.objective.models import Objective, ObjectiveImport, ObjectiveQueryParams
from domain.objective.ports.storage import ObjectiveDatabase


class ObjectiveFacade:
    def __init__(self, db: ObjectiveDatabase):
        self._db = db

    def get_objectives(self, auth_user: AuthUser, query_params: ObjectiveQueryParams):
        return self._db.query(auth_user, query_params)

    def get_objective(self, auth_user: AuthUser, objective_id: int):
        return self._db.get_by_id(auth_user, objective_id)

    def create_objective(self, auth_user: AuthUser, objective: Objective):
        if self._db.exists(auth_user, ObjectiveQueryParams(name=objective.name)):
            return -1
        return self._db.create(auth_user, objective)

    def update_objective(
        self, auth_user: AuthUser, objective_id: int, objective: Objective
    ):
        return self._db.update_by_id(auth_user, objective_id, objective)

    def delete_objective(self, auth_user: AuthUser, objective_id: int):
        return self._db.delete_by_id(auth_user, objective_id)

    def import_objectives(self, auth_user: AuthUser, objectives: List[ObjectiveImport]):
        return self._db.create_many(auth_user, objectives)


ObjectiveFacadeT = ObjectiveFacade
