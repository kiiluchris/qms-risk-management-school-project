from datetime import datetime, date
from typing import Optional

from dataclasses import dataclass, field
from dataclasses_json import dataclass_json, config

from domain.shared.models import (
    RelatedItem,
)
from domain.shared.mixins import PartialMixin


@dataclass_json
@dataclass(frozen=True)
class NewRiskCategory:
    name: str
    

RiskCategoryUpdate = NewRiskCategory
    
@dataclass_json
@dataclass(frozen=True)
class RiskCategory(NewRiskCategory):
    organization_id: Optional[int] = None
    id: Optional[int] = None

    
@dataclass_json
@dataclass(frozen=True)
class RiskCategoryPartial(PartialMixin):
    name: Optional[str] = None
    


@dataclass(frozen=True)
class RiskCategoryQueryParams:
    id: Optional[int] = None
    name: Optional[str] = None
    