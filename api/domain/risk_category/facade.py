#!/usr/bin/env python3

from typing import List

from domain.auth.models import AuthUser
from domain.risk_category.models import (
    RiskCategory,
    RiskCategoryQueryParams,
    NewRiskCategory,
    RiskCategoryUpdate,
)
from domain.risk_category.ports.storage import RiskCategoryDatabase


class RiskCategoryFacade:
    def __init__(self, db: RiskCategoryDatabase):
        self._db = db

    def get_risk_categories(self, auth_user: AuthUser, query_params: RiskCategoryQueryParams):
        return self._db.query(auth_user, query_params)

    def get_risk_category(self, auth_user: AuthUser, risk_category_id: int):
        return self._db.get_by_id(auth_user, risk_category_id)

    def create_risk_category(self, auth_user: AuthUser, risk_category: NewRiskCategory):
        if self._db.exists(auth_user, RiskCategoryQueryParams(name = risk_category.name)):
            return -1
        return self._db.create(auth_user, risk_category)

    def update_risk_category(self, auth_user: AuthUser, risk_category_id: int, risk_category: RiskCategoryUpdate):
        return self._db.update_by_id(auth_user, risk_category_id, risk_category)

    def delete_risk_category(self, auth_user: AuthUser, risk_category_id: int):
        return self._db.delete_by_id(auth_user, risk_category_id)

    def import_risk_categories(self, auth_user: AuthUser, risk_categories: List[NewRiskCategory]):
        return self._db.create_many(auth_user, risk_categories)

        
        


RiskCategoryFacadeT = RiskCategoryFacade