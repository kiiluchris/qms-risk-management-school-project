#!/usr/bin/env python3

from typing import Protocol, List, Optional
from abc import abstractmethod

from domain.auth.models import AuthUser

from domain.risk_category.models import (
    RiskCategory,
    RiskCategoryQueryParams,
    RiskCategoryUpdate,
    NewRiskCategory,
)


class RiskCategoryDatabase(Protocol):
    @abstractmethod
    def query(self, auth_user: AuthUser, query_params: RiskCategoryQueryParams) -> List[RiskCategory]:
        raise NotImplementedError

    @abstractmethod
    def get_by_id(self, auth_user: AuthUser, risk_category_id: int) -> Optional[RiskCategory]:
        raise NotImplementedError

    @abstractmethod
    def exists(self, auth_user: AuthUser, query_params: RiskCategoryQueryParams) -> bool:
        raise NotImplementedError
    
    @abstractmethod
    def create(self, auth_user: AuthUser, risk_category: NewRiskCategory) -> int:
        raise NotImplementedError

    @abstractmethod
    def create_many(self, auth_user: AuthUser, risk_categories: List[NewRiskCategory]) -> List[int]:
        raise NotImplementedError
    
    @abstractmethod
    def update_by_id(self, auth_user: AuthUser, risk_category_id: int, risk_category_updates: RiskCategoryUpdate) -> int:
        raise NotImplementedError

    @abstractmethod
    def delete_by_id(self, auth_user: AuthUser, risk_category_id: int) -> int:
        raise NotImplementedError