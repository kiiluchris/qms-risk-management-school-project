from typing import List

from sqlalchemy import insert, select, update, delete, bindparam, func, alias
from sqlalchemy.sql import Select
from sqlalchemy.sql.expression import and_
from sqlalchemy.orm import scoped_session

from infrastructure.models import (
    RiskCategoryModel,
)
from domain.auth.models import AuthUser

from domain.risk_category.ports.storage import RiskCategoryDatabase
from domain.risk_category.models import (
    RiskCategory,
    RiskCategoryQueryParams,
    RiskCategoryUpdate,
    NewRiskCategory,
)
from dataclasses import asdict
from domain.shared.models import RelatedItem


class RiskCategoryDatabaseAdapter(RiskCategoryDatabase):
    def __init__(self, session: scoped_session):
        self._session = session

    def exists(
        self, auth_user: AuthUser, query_params: RiskCategoryQueryParams
    ) -> bool:
        query = (
            select(func.count())
            .select_from(RiskCategoryModel)
            .where(
                and_(
                    RiskCategoryModel.organization_id == auth_user.organization_id,
                    RiskCategoryModel.name == query_params.name,
                )
            )
            .limit(1)
        )
        count = self._session.scalar(query)

        return count > 0

    def record_to_domain_model(
        self,
        risk_category: RiskCategoryModel,
    ) -> RiskCategory:
        return RiskCategory(
            id=risk_category.id,
            name=risk_category.name,
        )

    def select_query(self, auth_user: AuthUser) -> Select:
        return (
            select(
                RiskCategoryModel,
            )
            .select_from(RiskCategoryModel)
            .where(RiskCategoryModel.organization_id == auth_user.organization_id)
        )

    def query(self, auth_user: AuthUser, query_params: RiskCategoryQueryParams):
        query = self.select_query(auth_user)
        risk_categories = self._session.execute(query)

        return [self.record_to_domain_model(*row) for row in risk_categories]

    def get_by_id(self, auth_user: AuthUser, risk_category_id: int):
        query = self.select_query(auth_user).where(
            RiskCategoryModel.id == risk_category_id
        )
        result = self._session.execute(query).first()
        if result is None:
            return None

        return self.record_to_domain_model(*result)

    def create(self, auth_user: AuthUser, risk_category: NewRiskCategory):
        new_risk_category = RiskCategoryModel(
            name=risk_category.name,
        )
        self._session.add(new_risk_category)
        self._session.commit()

        return new_risk_category.id

    def risk_category_to_dict(
        self, auth_user: AuthUser, risk_category: NewRiskCategory
    ):
        result = {
            **asdict(risk_category),
        }

        return result

    def create_many(
        self, auth_user: AuthUser, risk_categories: List[NewRiskCategory]
    ):
        query = insert(RiskCategoryModel)  # .returning(RiskCategoryModel.id)
        ids = self._session.execute(
            query,
            [
                self.risk_category_to_dict(auth_user, risk_category)
                for risk_category in risk_categories
            ],
        )
        self._session.commit()

        return ids

    # Edit before usage
    def update_by_id(
        self,
        auth_user: AuthUser,
        risk_category_id: int,
        risk_category_updates: RiskCategoryUpdate,
    ):
        query = (
            update(RiskCategoryModel)
            .where(
                and_(
                    RiskCategoryModel.id == risk_category_id,
                    RiskCategoryModel.organization_id == auth_user.organization_id,
                )
            )
            .values(
                self.risk_category_to_dict(auth_user, risk_category_updates)
            )
            # .returning(RiskCategoryModel.id)
        )
        update_cursor = self._session.execute(query)
        if update_cursor is not None:
            self._session.commit()
            return risk_category_id

        return -1

    def delete_by_id(self, auth_user: AuthUser, risk_category_id: int):
        query = delete(RiskCategoryModel).where(
            and_(
                RiskCategoryModel.id == risk_category_id,
                RiskCategoryModel.organization_id == auth_user.organization_id,
            )
        )  # .returning(RiskCategoryModel.id)
        delete_cursor = self._session.execute(query)
        print("Deleted ID", delete_cursor)
        if delete_cursor is not None:
            self._session.commit()
            return risk_category_id

        return -1
