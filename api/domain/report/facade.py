#!/usr/bin/env python3

from domain.auth.models import AuthUser
from domain.report.ports.storage import ReportDatabase


class ReportFacade:
    def __init__(self, db: ReportDatabase):
        self._db = db

    def get_reports(self, auth_user: AuthUser):
        return self._db.get_reports(auth_user)

    def static_model_analysis(self, auth_user: AuthUser):
        return self._db.static_model_analysis(auth_user)


ReportFacadeT = ReportFacade
