#!/usr/bin/env python3

from datetime import date, timedelta
from typing import Any, Union
from domain.risk.infrastructure.modelling.generation import (
    ModelNames,
    RiskClassificationMixin,
)
from domain.shared.models import CapaClassification, ObjectiveStatus
from domain.auth.models import AuthUser


from sklearn.feature_extraction.text import CountVectorizer
from sklearn.neural_network import MLPClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import (
    accuracy_score,
    precision_recall_fscore_support,
    classification_report,
    confusion_matrix,
)
import pandas
import numpy as np
from sqlalchemy import select, and_
from sqlalchemy.orm import scoped_session
from domain.report.ports.storage import ReportDatabase
from infrastructure.models import *

PRECISION = "precision"
RECALL = "recall"
FSCORE = "fscore"
ACCURACY = "accuracy"


class ReportDatabaseAdapter(ReportDatabase, RiskClassificationMixin):
    def __init__(self, session: scoped_session):
        self._session = session

    def _get_objectives(self, auth_user: AuthUser):
        query = select(ObjectiveModel).where(
            and_(ObjectiveModel.organization_id == auth_user.organization_id)
        )

        return [row for row, *_ in self._session.execute(query)]

    def _get_risks(self, auth_user: AuthUser):
        query = select(RiskModel).where(
            and_(RiskModel.organization_id == auth_user.organization_id)
        )

        return [row for row, *_ in self._session.execute(query)]

    def _get_incidents(self, auth_user: AuthUser):
        query = select(IncidentModel).where(
            and_(IncidentModel.organization_id == auth_user.organization_id)
        )

        return [row for row, *_ in self._session.execute(query)]

    def _get_capas(self, auth_user: AuthUser):
        query = select(CapaModel).where(
            and_(CapaModel.organization_id == auth_user.organization_id)
        )

        return [row for row, *_ in self._session.execute(query)]

    def _objective_metrics(self, objectives: list[ObjectiveModel]):
        today = date.today()
        metrics = {
            "all_time": {
                "closed": 0,
                "total": 0,
                "late": 0,
            }
        }
        for objective in objectives:
            year = f"_{objective.target_date.year}"
            metrics[year] = metrics.get(
                year,
                {
                    "closed": 0,
                    "total": 0,
                    "late": 0,
                },
            )
            metrics["all_time"]["total"] = metrics["all_time"]["total"] + 1
            metrics[year]["total"] = metrics[year]["total"] + 1
            if objective.current_status == ObjectiveStatus.Closed:
                metrics["all_time"]["closed"] = metrics["all_time"]["closed"] + 1
                metrics[year]["closed"] = metrics[year]["closed"] + 1
            elif objective.target_date < today:
                metrics["all_time"]["late"] = metrics["all_time"]["late"] + 1
                metrics[year]["late"] = metrics[year]["late"] + 1

        result = {}
        for key, values in metrics.items():
            result[key] = {}
            total = values["total"]
            result[key]["closed"] = [
                {"label": "Open", "count": total - values["closed"]},
                {"label": "Closed", "count": values["closed"]},
            ]
            result[key]["late"] = [
                {"label": "Timely", "count": total - values["late"]},
                {"label": "Delayed", "count": values["late"]},
            ]

        return result

    def _incident_metrics(self, incidents: list[IncidentModel]):
        yearly_metrics = {}
        monthly_metrics = {}
        for incident in incidents:
            year = f"_{incident.date_occurred.year}"
            month = str(incident.date_occurred.month)
            yearly_metrics[year] = yearly_metrics.get(year, 0) + 1
            monthly_metrics[year] = monthly_metrics.get(year, {})
            monthly_metrics[year][month] = monthly_metrics.get(month, 0) + 1

        for year in monthly_metrics.keys():
            group = monthly_metrics[year]
            for i_ in range(1, 13):
                i = str(i_)
                if i not in group:
                    monthly_metrics[year][i] = 0

        monthly = {
            year: [dict(count=count, label=label) for label, count in metrics.items()]
            for year, metrics in monthly_metrics.items()
        }
        return {
            "yearly": [
                dict(count=count, label=label)
                for label, count in yearly_metrics.items()
            ],
            "monthly": monthly,
        }

    def _default_response_times(self):
        return {
            self.WEEK: 0,
            self.MONTH: 0,
            self.MONTH: 0,
            self.QUARTER: 0,
            self.HALF_YEAR: 0,
            self.YEAR: 0,
            self.OTHER: 0,
            self.NOT_YET: 0,
        }

    NOT_YET = "Not Yet"
    WEEK = "Week"
    MONTH = "Month"
    QUARTER = "Quarter"
    HALF_YEAR = "Half Year"
    YEAR = "Year"
    OTHER = "Other"

    def _response_time_key(self, tt: int):
        if tt <= 7:
            return self.WEEK
        elif tt <= 30:
            return self.MONTH
        elif tt <= 90:
            return self.QUARTER
        elif tt <= 180:
            return self.HALF_YEAR
        elif tt <= 365:
            return self.YEAR
        else:
            return self.OTHER

    def _capa_metrics(self, capas: CapaModel):
        metrics = {
            "preventive": self._default_response_times(),
            "corrective": self._default_response_times(),
        }

        for capa in capas:
            date_created = capa.date_created
            if capa.action_type == CapaClassification.Corrective:
                if capa.incident is not None:
                    date_occurred = capa.incident.date_occurred
                    time_taken = (date_occurred - date_created).days
                    response_key = self._response_time_key(time_taken)
                    metrics["corrective"][response_key] = (
                        metrics["corrective"][response_key] + 1
                    )
                else:
                    metrics["corrective"][self.NOT_YET] = (
                        metrics["corrective"][self.NOT_YET] + 1
                    )
            else:
                earliest_risk = min(
                    capa.risks_to_prevent, key=lambda r: r.created_at, default=None
                )
                if earliest_risk is not None:
                    created_at = earliest_risk.created_at
                    time_taken = (created_at.date() - date_created).days
                    response_key = self._response_time_key(time_taken)
                    metrics["preventive"][response_key] = (
                        metrics["preventive"][response_key] + 1
                    )
                else:
                    metrics["preventive"][self.NOT_YET] = (
                        metrics["preventive"][self.NOT_YET] + 1
                    )

        return {
            key: [dict(label=label, count=count) for label, count in values.items()]
            for key, values in metrics.items()
        }

    def get_reports(self, auth_user: AuthUser):
        objectives = self._get_objectives(auth_user)
        objective_metrics = self._objective_metrics(objectives)
        incidents = self._get_incidents(auth_user)
        incident_metrics = self._incident_metrics(incidents)
        capas = self._get_capas(auth_user)
        capa_metrics = self._capa_metrics(capas)

        return {
            "incidents": incident_metrics,
            "objectives": objective_metrics,
            "capas": capa_metrics,
        }

    def split_dataframe(self, df: pandas.DataFrame):
        split = 0.9
        # Count for each group
        s = df.groupby("Category").Category.cumcount()
        # Top percent for each group
        s = s // (df.groupby("Category").Category.transform("count") * split).astype(
            int
        )
        Train = df.loc[s == 0].copy()
        Test = df.drop(Train.index)

        return Train, Test

    def split_dataframe_impact(self, df: pandas.DataFrame):
        split = 0.9
        # Count for each group
        s = df.groupby("Impact").Impact.cumcount()
        # Top percent for each group
        s = s // (df.groupby("Impact").Impact.transform("count") * split).astype(int)
        Train = df.loc[s == 0].copy()
        Test = df.drop(Train.index)

        return Train, Test

    def split_dataframe_likelihood(self, df: pandas.DataFrame):
        split = 0.9
        # Count for each group
        s = df.groupby("Likelihood").Likelihood.cumcount()
        # Top percent for each group
        s = s // (
            df.groupby("Likelihood").Likelihood.transform("count") * split
        ).astype(int)
        Train = df.loc[s == 0].copy()
        Test = df.drop(Train.index)

        return Train, Test

    def neural_network(self):
        return MLPClassifier(
            solver="adam",
            alpha=1e-5,
            hidden_layer_sizes=(800, 200),
            # random_state=6,
        )

    likelihood_neural_network = MLPClassifier(
        solver="lbfgs",
        alpha=1e-5,
        hidden_layer_sizes=(800, 200),
        # random_state=6,
    )
    impact_neural_network = MLPClassifier(
        solver="lbfgs",
        alpha=1e-5,
        hidden_layer_sizes=(800, 200),
        # random_state=6,
    )

    def models(self, optimization: str, first_layer, second_layer: int):
        return [
            (
                ModelNames.LOGISTIC_REGRESSION,
                LogisticRegression(multi_class="multinomial", random_state=6),
            ),
            (ModelNames.SVM, SVC(decision_function_shape="ovo", random_state=6)),
            (ModelNames.DECISION_TREE, DecisionTreeClassifier(random_state=6)),
            (
                ModelNames.RANDOM_FOREST,
                RandomForestClassifier(n_estimators=1000, max_depth=10, random_state=6),
            ),
            (
                ModelNames.NEURAL_NETWORK,
                MLPClassifier(
                    solver=optimization,
                    alpha=1e-5,
                    hidden_layer_sizes=(first_layer, second_layer),
                    random_state=6,
                ),
            ),
        ]

    def merge_prediction_metrics_by_model(
        self,
        labels: str,
        model_metrics: dict[str, dict[str, dict[str, int]]],
        model_name: str,
        precision: np.ndarray,
        recall: np.ndarray,
        fscore: np.ndarray,
        *,
        log: bool = False,
    ):
        model_metrics[PRECISION][model_name] = model_metrics[PRECISION].get(
            model_name, {}
        )
        model_metrics[RECALL][model_name] = model_metrics[RECALL].get(model_name, {})
        model_metrics[FSCORE][model_name] = model_metrics[FSCORE].get(model_name, {})
        for label_idx, label in enumerate(labels):
            if log:
                print(precision)
            model_metrics[PRECISION][model_name][label] = precision.item(label_idx)
            model_metrics[RECALL][model_name][label] = recall.item(label_idx)
            model_metrics[FSCORE][model_name][label] = fscore.item(label_idx)

    def merge_prediction_metrics_by_label(
        self,
        labels: str,
        model_metrics: dict[str, dict[str, dict[str, int]]],
        model_name: str,
        precision: np.ndarray,
        recall: np.ndarray,
        fscore: np.ndarray,
    ):
        for label_idx, label in enumerate(labels):
            model_metrics[PRECISION][label] = model_metrics[PRECISION].get(label, {})
            model_metrics[FSCORE][label] = model_metrics[FSCORE].get(label, {})
            model_metrics[RECALL][label] = model_metrics[RECALL].get(label, {})
            model_metrics[PRECISION][label][model_name] = precision.item(label_idx)
            model_metrics[RECALL][label][model_name] = recall.item(label_idx)
            model_metrics[FSCORE][label][model_name] = fscore.item(label_idx)

    def flatten_merged_prediction_metrics(
        self, model_metrics: dict[str, dict[str, int]]
    ):
        result = []
        for model, label_groups in model_metrics.items():
            group: dict[str, Union[int, str]] = {
                label: value for label, value in label_groups.items()
            }
            group["model"] = model
            result.append(group)

        return result

    def normalize_for_category(self, train: pandas.DataFrame, test: pandas.DataFrame):
        train_x, cv = self.category_x(train)
        test_x, _ = self.category_x(test, cv)
        train_y = self.category_y(train)
        test_y = self.category_y(test)
        labels = self.category_labels(pandas.concat([train, test]))
        return train_x, train_y, test_x, test_y, labels

    def normalize_for_impact(self, train: pandas.DataFrame, test: pandas.DataFrame):
        train_x = self.metric_x(train)
        test_x = self.metric_x(test)
        train_y = self.impact_y(train)
        test_y = self.impact_y(test)
        labels = self.as_labels(train_y)
        return train_x, train_y, test_x, test_y, labels

    def normalize_for_likelihood(self, train: pandas.DataFrame, test: pandas.DataFrame):
        train_x = self.metric_x(train)
        test_x = self.metric_x(test)
        train_y = self.likelihood_y(train)
        test_y = self.likelihood_y(test)
        labels = self.as_labels(train_y)
        return train_x, train_y, test_x, test_y, labels

    def analysis_of_ml_models(
        self,
        ml_models: dict[str, Any],
        train: pandas.DataFrame,
        test: pandas.DataFrame,
        normalize,
        *,
        log: bool = False,
    ):
        train_x, train_y, test_x, test_y, labels = normalize(train, test)
        models = []
        model_metrics_by_model = {PRECISION: {}, RECALL: {}, FSCORE: {}}
        model_metrics_by_label = {PRECISION: {}, RECALL: {}, FSCORE: {}}
        accuracies = []
        model_accuracies = {}
        label_accuracies = {label: [] for label in labels}
        for model_name, model in ml_models.items():
            model.fit(train_x, train_y)
            predictions = model.predict(test_x)
            accuracy = accuracy_score(test_y, predictions)
            precision, recall, fscore, _ = precision_recall_fscore_support(
                test_y, predictions
            )
            cm = confusion_matrix(test_y, predictions)
            cm = cm.astype("float") / cm.sum(axis=1)[:, np.newaxis]
            accuracy_per_class = cm.diagonal().tolist()
            self.merge_prediction_metrics_by_model(
                labels,
                model_metrics_by_model,
                model_name,
                precision,
                recall,
                fscore,
                log=log,
            )
            self.merge_prediction_metrics_by_label(
                labels, model_metrics_by_label, model_name, precision, recall, fscore
            )
            accuracies.append({"model": model_name, "accuracy": accuracy})
            models.append(model_name)
            # model_accuracies[model_name] = accuracy_per_class
            # for label, acc in zip(labels, accuracy_per_class):
            #     label_accuracies[label].append(acc)

        metrics = [
            ("model", labels, model_metrics_by_model),
            ("label", models, model_metrics_by_label),
        ]
        result: dict[str, Any] = {
            key: {
                PRECISION: self.flatten_merged_prediction_metrics(
                    model_metrics[PRECISION]
                ),
                FSCORE: self.flatten_merged_prediction_metrics(model_metrics[FSCORE]),
                ACCURACY: accuracies,
                RECALL: self.flatten_merged_prediction_metrics(model_metrics[RECALL]),
                "labels": labels,
            }
            for key, labels, model_metrics in metrics
        }
        result[ACCURACY] = accuracies

        return result

    def static_model_analysis(self, auth_user: AuthUser):
        risks = self.query_risks(auth_user)
        df = self.to_dataframe(risks.items)
        # df = pandas.read_csv("./notebook-media/database-dataset.csv")
        RANDOM_STATE = 42
        Train = df.sample(frac=0.75, random_state=RANDOM_STATE)
        Test = df.drop(Train.index)
        # train, test = self.split_dataframe(df)
        # train_i, test_i = self.split_dataframe_impact(df)
        # train_l, test_l = self.split_dataframe_likelihood(df)
        return {
            "category": self.analysis_of_ml_models(
                self.category_models, Train, Test, self.normalize_for_category
            ),
            "impact": self.analysis_of_ml_models(
                self.impact_models,
                Train,
                Test,
                self.normalize_for_impact,
            ),
            "likelihood": self.analysis_of_ml_models(
                self.likelihood_models, Train, Test, self.normalize_for_likelihood
            ),
        }
