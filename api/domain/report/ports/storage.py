#!/usr/bin/env python3

from abc import abstractmethod
from domain.auth.models import AuthUser
from typing import Protocol


class ReportDatabase(Protocol):
    @abstractmethod
    def get_reports(self, auth_user: AuthUser):
        raise NotImplementedError

    @abstractmethod
    def static_model_analysis(self, auth_user: AuthUser):
        raise NotImplementedError
