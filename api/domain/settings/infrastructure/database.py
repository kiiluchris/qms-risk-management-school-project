#!/usr/bin/env python3

import secrets
from typing import Optional

from sqlalchemy import select
from sqlalchemy.orm import scoped_session

from domain.auth.models import AuthUser
from domain.auth.ports.utils import Jwt
from domain.settings.ports.storage import SettingsDatabase
from domain.settings.models import Settings, SettingsUpdate
from infrastructure.models import OrganizationModel


class SettingsDatabaseAdapter(SettingsDatabase):
    def __init__(self, session: scoped_session, jwt_encoding: Jwt):
        self._session = session
        self._jwt = jwt_encoding

    def get_settings(self, auth_user: AuthUser) -> Optional[Settings]:
        query = select(OrganizationModel).where(
            OrganizationModel.id == auth_user.organization_id
        )
        row = self._session.execute(query).first()
        if not row:
            return None
        org = row[0]

        return Settings(
            name=org.name,
            domain=org.domain,
            api_token=org.api_token,
            ml_model=org.ml_model_to_use,
            risk_likelihood_weight=org.risk_likelihood_weight,
            risk_impact_weight=org.risk_impact_weight,
        )

    def update_settings(
        self, auth_user: AuthUser, setting_updates: SettingsUpdate
    ) -> int:
        query = select(OrganizationModel).where(
            OrganizationModel.id == auth_user.organization_id
        )
        row = self._session.execute(query).first()
        if not row:
            return -1
        org = row[0]
        print(setting_updates)
        org.name = setting_updates.name
        org.ml_model_to_use = setting_updates.ml_model
        org.risk_likelihood_weight = setting_updates.risk_likelihood_weight
        org.risk_impact_weight = setting_updates.risk_impact_weight
        self._session.commit()
        return auth_user.organization_id

    def generate_api_token(self, auth_user: AuthUser) -> Optional[str]:
        query = select(OrganizationModel).where(
            OrganizationModel.id == auth_user.organization_id
        )
        row = self._session.execute(query).first()
        if not row:
            return None
        org = row[0]
        secret = secrets.token_urlsafe(24)
        token = self._jwt.make_api_token(org.domain, secret)
        org.api_token = token
        org.api_token_secret = secret
        self._session.commit()
        return token

    def delete_account(self, auth_user: AuthUser) -> int:
        query = select(OrganizationModel).where(
            OrganizationModel.id == auth_user.organization_id
        )
        row = self._session.execute(query).first()
        if not row:
            return -1
        org = row[0]
        self._session.delete(org)
        self._session.commit()
        return auth_user.organization_id
