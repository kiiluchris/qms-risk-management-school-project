#!/usr/bin/env python3

from typing import Optional, Protocol
from abc import abstractmethod

from domain.auth.models import AuthUser
from domain.settings.models import Settings, SettingsUpdate


class SettingsDatabase(Protocol):
    @abstractmethod
    def get_settings(self, auth_user: AuthUser) -> Optional[Settings]:
        raise NotImplementedError

    @abstractmethod
    def update_settings(
        self, auth_user: AuthUser, setting_updates: SettingsUpdate
    ) -> int:
        raise NotImplementedError

    @abstractmethod
    def generate_api_token(self, auth_user: AuthUser) -> str:
        raise NotImplementedError

    @abstractmethod
    def delete_account(self, auth_user: AuthUser) -> int:
        raise NotImplementedError
