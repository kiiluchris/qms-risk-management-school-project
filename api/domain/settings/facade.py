#!/usr/bin/env python3

from domain.settings.models import SettingsUpdate
from domain.auth.models import AuthUser
from domain.settings.ports.storage import SettingsDatabase


class SettingsFacade:
    def __init__(self, setting_db: SettingsDatabase):
        self._db = setting_db

    def get_settings(self, auth_user: AuthUser):
        return self._db.get_settings(auth_user)

    def update_settings(self, auth_user: AuthUser, updates: SettingsUpdate):
        return self._db.update_settings(auth_user, updates)

    def generate_api_token(self, auth_user: AuthUser):
        return self._db.generate_api_token(auth_user)

    def delete_account(self, auth_user: AuthUser):
        return self._db.delete_account(auth_user)


SettingsFacadeT = SettingsFacade
