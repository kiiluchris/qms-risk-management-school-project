#!/usr/bin/env python3

from dataclasses import dataclass
from typing import Optional
from dataclasses_json import dataclass_json


@dataclass_json
@dataclass(frozen=True)
class Settings:
    name: str
    domain: str
    api_token: Optional[str]
    ml_model: str
    risk_likelihood_weight: int
    risk_impact_weight: int


@dataclass_json
@dataclass(frozen=True)
class SettingsUpdate:
    name: str
    ml_model: str
    risk_likelihood_weight: int
    risk_impact_weight: int
