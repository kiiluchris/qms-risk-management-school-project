#!/usr/bin/env python3


import africastalking
import os

from domain.sms.ports import SmsClient

africastalking.initialize(
    username=os.getenv("AT_USERNAME"), api_key=os.getenv("AT_API_KEY")
)

__sms = africastalking.SMS
__sender_id = os.getenv("AT_SENDERID")


class AfricasTalkingSmsClient(SmsClient):
    def send(self, message: str, to: list[str] = []):
        recipients = ["+254702495084"]
        try:
            response = __sms.send(message, recipients, __sender_id)
            print(response)
        except Exception as e:
            print(f"Houston, we have a problem: {e}")
