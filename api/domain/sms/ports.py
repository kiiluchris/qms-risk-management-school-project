#!/usr/bin/env python3


from abc import abstractclassmethod, abstractmethod
from typing import Protocol


class SmsClient(Protocol):
    @abstractmethod
    def send(self, message: str, phone_numbers: list[str] = []):
        raise NotImplementedError
