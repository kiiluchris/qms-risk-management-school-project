from datetime import datetime, date
from typing import Optional

from dataclasses import dataclass, field
from dataclasses_json import dataclass_json, config

from domain.shared.models import (
    CapaClassification,
    RelatedItem,
    CapaRating,
)
from domain.shared.mixins import PartialMixin


@dataclass_json
@dataclass(frozen=True)
class NewCapa:
    name: str
    description: str
    measures_taken: str
    date_created: date = field(
        metadata=config(
            encoder=date.isoformat,
            decoder=date.fromisoformat,
        ),
    )
    # reference: str
    target_effective_date: date = field(
        metadata=config(
            encoder=date.isoformat,
            decoder=date.fromisoformat,
        ),
    )
    comments: str
    # proposed_actions: str
    rating: CapaRating = field(
        metadata=config(
            encoder=lambda e: e and e.name,
            decoder=lambda s: CapaRating[s] if s in CapaRating.__members__ else None,
        ),
    )
    action_type: CapaClassification = field(
        metadata=config(
            encoder=lambda e: e and e.name,
            decoder=lambda s: CapaClassification[s]
            if s in CapaClassification.__members__
            else None,
        ),
    )
    prepared_by: RelatedItem
    category: RelatedItem
    approved_by: Optional[RelatedItem] = None
    incident: Optional[RelatedItem] = None
    risks_to_prevent: list[RelatedItem] = field(default_factory=list)


@dataclass_json
@dataclass(frozen=True)
class CapaImport(NewCapa):
    prepared_by: str
    category: str
    approved_by: Optional[str] = None
    incident: Optional[str] = None
    risks_to_prevent: list[str] = field(default_factory=list)


CapaUpdate = NewCapa


@dataclass_json
@dataclass(frozen=True)
class Capa(NewCapa):
    organization_id: Optional[int] = None
    id: Optional[int] = None


@dataclass_json
@dataclass(frozen=True)
class CapaPartial(PartialMixin):
    name: Optional[str] = None
    description: Optional[str] = None
    measures_taken: Optional[str] = None
    date_created: Optional[date] = field(
        metadata=config(
            encoder=date.isoformat,
            decoder=date.fromisoformat,
        ),
        default=None,
    )
    reference_id: Optional[str] = None
    target_effective_date: Optional[date] = field(
        metadata=config(
            encoder=date.isoformat,
            decoder=date.fromisoformat,
        ),
        default=None,
    )
    comments: Optional[str] = None
    proposed_actions: Optional[str] = None
    rating: Optional[CapaRating] = field(
        metadata=config(
            encoder=lambda e: e and e.name,
            decoder=lambda s: CapaRating[s] if s in CapaRating.__members__ else None,
        ),
        default=None,
    )
    action_type: Optional[CapaRating] = field(
        metadata=config(
            encoder=lambda e: e and e.name,
            decoder=lambda s: CapaClassification[s]
            if s in CapaClassification.__members__
            else None,
        ),
        default=None,
    )
    approved_by: Optional[RelatedItem] = None
    prepared_by: Optional[RelatedItem] = None
    category: Optional[RelatedItem] = None
    organization_id: Optional[RelatedItem] = None


@dataclass(frozen=True)
class CapaQueryParams:
    name: Optional[str] = None
    description: Optional[str] = None
    measures_taken: Optional[str] = None
    date_created: Optional[date] = field(
        metadata=config(
            encoder=date.isoformat,
            decoder=date.fromisoformat,
        ),
        default=None,
    )
    reference_id: Optional[str] = None
    target_effective_date: Optional[date] = field(
        metadata=config(
            encoder=date.isoformat,
            decoder=date.fromisoformat,
        ),
        default=None,
    )
    comments: Optional[str] = None
    proposed_actions: Optional[str] = None
    rating: Optional[CapaRating] = field(
        metadata=config(
            encoder=lambda e: e and e.name,
            decoder=lambda s: CapaRating[s] if s in CapaRating.__members__ else None,
        ),
        default=None,
    )
    action_type: Optional[CapaClassification] = field(
        metadata=config(
            encoder=lambda e: e and e.name,
            decoder=lambda s: CapaClassification[s]
            if s in CapaClassification.__members__
            else None,
        ),
        default=None,
    )
    approved_by: Optional[int] = None
    prepared_by: Optional[int] = None
    category: Optional[str] = None
    category_id: Optional[int] = None
    organization_id: Optional[int] = None
    incident_id: Optional[int] = None
    risk_id: Optional[int] = None
