#!/usr/bin/env python3

from domain.process.infrastructure.mixins.get_process import GetProcess
from typing import Protocol, List, Optional
from abc import abstractmethod

from domain.auth.models import AuthUser

from domain.capa.models import (
    CapaImport,
    Capa,
    CapaQueryParams,
    CapaUpdate,
    NewCapa,
)


class CapaDatabase(GetProcess):
    @abstractmethod
    def query(self, auth_user: AuthUser, query_params: CapaQueryParams) -> List[Capa]:
        raise NotImplementedError

    @abstractmethod
    def get_by_id(self, auth_user: AuthUser, capa_id: int) -> Optional[Capa]:
        raise NotImplementedError

    @abstractmethod
    def exists(self, auth_user: AuthUser, query_params: CapaQueryParams) -> bool:
        raise NotImplementedError

    @abstractmethod
    def create(self, auth_user: AuthUser, capa: NewCapa) -> int:
        raise NotImplementedError

    @abstractmethod
    def create_many(self, auth_user: AuthUser, capas: List[NewCapa]) -> List[int]:
        raise NotImplementedError

    @abstractmethod
    def import_many(self, auth_user: AuthUser, capas: List[CapaImport]) -> List[int]:
        raise NotImplementedError

    @abstractmethod
    def update_by_id(
        self, auth_user: AuthUser, capa_id: int, capa_updates: CapaUpdate
    ) -> int:
        raise NotImplementedError

    @abstractmethod
    def delete_by_id(self, auth_user: AuthUser, capa_id: int) -> int:
        raise NotImplementedError

    @abstractmethod
    def get_affected_team_member_contacts(self, auth_user: AuthUser, capa_id: int):
        raise NotImplementedError
