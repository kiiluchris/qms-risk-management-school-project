#!/usr/bin/env python3

from domain.sms.ports import SmsClient
from domain.email.ports import EmailClient
from typing import List

from domain.auth.models import AuthUser
from domain.capa.models import (
    CapaImport,
    Capa,
    CapaQueryParams,
    NewCapa,
    CapaUpdate,
)
from domain.capa.ports.storage import CapaDatabase


class CapaFacade:
    def __init__(self, db: CapaDatabase, email: EmailClient, sms: SmsClient):
        self._db = db
        self._email = email
        self._sms = sms

    def get_capas(self, auth_user: AuthUser, query_params: CapaQueryParams):
        return self._db.query(auth_user, query_params)

    def get_capa(self, auth_user: AuthUser, capa_id: int):
        return self._db.get_by_id(auth_user, capa_id)

    def create_capa(self, auth_user: AuthUser, capa: NewCapa):
        id_ = self._db.create(auth_user, capa)
        if capa.incident:
            for_ = f'the incident "{capa.incident.value}"'
        else:
            risks = "\n".join(["- " + r.value for r in capa.risks_to_prevent])
            for_ = f"the risks below: \n{risks}"
        if id_ and id_ != -1:
            reason = f"New {capa.action_type} Measure"
            message = "\n".join(
                [
                    f"A new {capa.action_type} has been created for {for_}",
                    f" Name: {capa.name}",
                    f" Category: {capa.category.value}",
                    f" Description: {capa.description}",
                    f" Measures: {capa.measures_taken}",
                    f" Targetted Date of Effectiveness: {capa.target_effective_date.isoformat()}",
                    f" Prepared_by: {capa.prepared_by.readable}",
                ]
            )
            contacts = self._db.get_affected_team_member_contacts(auth_user, id_)
            to_email = []
            to_sms = []
            for contact in contacts:
                to_email.append(contact["email"])
                if contact["use_sms"]:
                    to_sms.append(contact["phone"])
            self._email.send_notifications(to_email, reason, message)
            if to_sms:
                self._sms.send(reason + "\n\n" + message)

        return id_

    def update_capa(self, auth_user: AuthUser, capa_id: int, capa: CapaUpdate):
        return self._db.update_by_id(auth_user, capa_id, capa)

    def delete_capa(self, auth_user: AuthUser, capa_id: int):
        return self._db.delete_by_id(auth_user, capa_id)

    def import_capas(self, auth_user: AuthUser, capas: List[CapaImport]):
        return self._db.import_many(auth_user, capas)


CapaFacadeT = CapaFacade
