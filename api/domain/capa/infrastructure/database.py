from dataclasses import asdict
import itertools
from domain.capa.infrastructure.CapaToDomainModelMixin import CapaToDomainModelMixin
from domain.risk.ports.storage import RiskDatabase
from domain.risk.infrastructure.database import RiskDatabaseAdapter
from domain.incident.ports.storage import IncidentDatabase
from domain.user.ports.storage import UserDatabase
from domain.user.infrastructure.database import UserDatabaseAdapter
from itertools import chain
from typing import List, NamedTuple, Optional, cast

from domain.auth.models import AuthUser
from domain.capa.models import Capa, CapaImport, CapaQueryParams, CapaUpdate, NewCapa
from domain.capa.ports.storage import CapaDatabase
from domain.incident.infrastructure.database import IncidentDatabaseAdapter
from domain.shared.models import RelatedItem
from domain.user.models import user_full_name
from sqlalchemy import alias, bindparam, delete, func, insert, select, update
from sqlalchemy.orm import scoped_session
from sqlalchemy.orm.util import aliased
from sqlalchemy.sql import Select
from sqlalchemy.sql.expression import and_

from infrastructure.models import (
    CapaModel,
    RiskCategoryModel,
    IncidentModel,
    OrganizationModel,
    UserModel,
    capa_risk_assoc,
    capa_incidents_assoc,
    process_incident_assoc,
    task_team_user_assoc,
    ProcessModel,
    TaskTeamModel,
)


class CapaImportKeys(NamedTuple):
    users: set[str]
    incidents: set[str]
    risk_categories: set[str]


class CapaDatabaseAdapter(CapaDatabase, CapaToDomainModelMixin):
    def __init__(
        self,
        session: scoped_session,
        incident_db: IncidentDatabase,
        user_db: UserDatabase,
        risk_db: RiskDatabase,
    ):
        self._session = session
        self._incident_db = cast(IncidentDatabaseAdapter, incident_db)
        self._user_db = cast(UserDatabaseAdapter, user_db)
        self._risk_db = cast(RiskDatabaseAdapter, risk_db)

    def exists(self, auth_user: AuthUser, query_params: CapaQueryParams) -> bool:
        query = (
            select(func.count())
            .select_from(CapaModel)
            .where(
                and_(
                    CapaModel.organization_id == auth_user.organization_id,
                    CapaModel.name == query_params.name,
                )
            )
            .limit(1)
        )
        count = self._session.scalar(query)

        return count > 0

    def select_query(
        self, auth_user: AuthUser, query_params: Optional[CapaQueryParams] = None
    ) -> Select:
        prepared_by = aliased(UserModel)
        query = (
            select(
                CapaModel,
                UserModel,
                prepared_by,
                RiskCategoryModel,
                OrganizationModel,
                IncidentModel,
            )
            .select_from(CapaModel)
            .join(UserModel, CapaModel.approved_by == UserModel.id, isouter=True)
            .join(prepared_by, CapaModel.prepared_by == prepared_by.id)
            .join(RiskCategoryModel, CapaModel.category_id == RiskCategoryModel.id)
            .join(
                IncidentModel, IncidentModel.id == CapaModel.incident_id, isouter=True
            )
            .join(OrganizationModel, CapaModel.organization_id == OrganizationModel.id)
            .where(CapaModel.organization_id == auth_user.organization_id)
        )

        if query_params is not None:
            if query_params.risk_id is not None:
                query = query.join(
                    capa_risk_assoc, capa_risk_assoc.columns.capa_id == CapaModel.id
                ).where(capa_risk_assoc.columns.risk_id == query_params.risk_id)

        return query

    def query(self, auth_user: AuthUser, query_params: CapaQueryParams):
        query = self.select_query(auth_user, query_params)
        capas = self._session.execute(query)

        return [self.capa_record_to_domain_model(*row) for row in capas]

    def get_by_id(self, auth_user: AuthUser, capa_id: int):
        query = self.select_query(auth_user).where(CapaModel.id == capa_id)
        result = self._session.execute(query).first()
        if result is None:
            return None

        return self.capa_record_to_domain_model(*result)

    def create(self, auth_user: AuthUser, capa: NewCapa):
        risks = (
            list(
                self._risk_db.fetch_existing_risks_by_ids(
                    auth_user, [r.id for r in capa.risks_to_prevent]
                ).values()
            )
            if capa.risks_to_prevent
            else []
        )
        new_capa = CapaModel(
            name=capa.name,
            description=capa.description,
            measures_taken=capa.measures_taken,
            date_created=capa.date_created,
            # reference_id=capa.reference,
            target_effective_date=capa.target_effective_date,
            comments=capa.comments,
            # proposed_actions=capa.proposed_actions,
            rating=capa.rating,
            action_type=capa.action_type,
            approved_by=capa.approved_by and capa.approved_by.id,
            prepared_by=capa.prepared_by.id,
            category_id=capa.category.id,
            organization_id=auth_user.organization_id,
            incident_id=capa.incident and capa.incident.id,
            risks_to_prevent=risks,
        )
        self._session.add(new_capa)
        self._session.commit()

        return new_capa.id

    def capa_to_dict(self, auth_user: AuthUser, capa: NewCapa):
        result = {
            **asdict(capa),
            "approved_by": capa.approved_by and capa.approved_by.id,
            "prepared_by": capa.prepared_by.id,
            "organization_id": auth_user.organization_id,
            "incident_id": capa.incident and capa.incident.id,
            "category_id": capa.category.id,
        }
        del result["category"]
        del result["incident"]
        del result["risks_to_prevent"]

        return result

    def create_many(self, auth_user: AuthUser, capas: List[NewCapa]):
        risks = self._risk_db.query_risks(auth_user).key_map
        query = insert(CapaModel)  # .returning(CapaModel.id)
        ids = self._session.execute(
            query,
            [
                {
                    **self.capa_to_dict(auth_user, capa),
                    "risks_to_prevent": [
                        r_ for r in capa.risks_to_prevent if (r_ := risks.get(r.value))
                    ],
                }
                for capa in capas
            ],
        )
        self._session.commit()

        return ids

    def _extract_capa_import_keys(self, capas: List[CapaImport]):
        users = []
        categories = set()
        incidents = set()

        for capa in capas:
            users = chain(
                users,
                (
                    u
                    for u in (capa.prepared_by, capa.approved_by)
                    if u is not None and u != ""
                ),
            )
            categories.add(capa.category)
            if capa.incident:
                incidents.add(capa.incident)

        return CapaImportKeys(
            users=set(users), risk_categories=categories, incidents=incidents
        )

    def get_affected_team_member_contacts(self, auth_user: AuthUser, capa_id: int):
        query = (
            select(CapaModel, TaskTeamModel)
            .select_from(CapaModel)
            .join(
                IncidentModel,
                IncidentModel.id == CapaModel.incident_id,
            )
            .join(
                process_incident_assoc,
                process_incident_assoc.columns.incident_id == IncidentModel.id,
            )
            .join(
                ProcessModel,
                ProcessModel.id == process_incident_assoc.columns.process_id,
            )
            .join(TaskTeamModel, TaskTeamModel.id == ProcessModel.team_responsible)
        )
        teams = [row[1] for row in self._session.execute(query)]
        emails = set()
        contacts = []
        for team in teams:
            leader = self._session.query(UserModel).get(team.leader)
            for member in itertools.chain([leader], team.members):
                if member.email not in emails:
                    contacts.append(
                        {
                            "email": member.email,
                            "phone": member.phone,
                            "use_sms": member.use_sms,
                        }
                    )
                    emails.add(member.email)
        return contacts

    def import_many(self, auth_user: AuthUser, capas: List[CapaImport]):
        import_keys = self._extract_capa_import_keys(capas)
        risk_map = self._risk_db.query_risks(auth_user).key_map
        related_imported_users = self._user_db.fetch_existing_users(
            auth_user, import_keys.users
        )
        related_imported_incidents = self._incident_db.fetch_existing_incidents(
            auth_user, import_keys.incidents
        )
        related_imported_categories = self._incident_db.fetch_existing_risk_categories(
            auth_user, import_keys.risk_categories
        )
        self._incident_db.create_new_categories(
            auth_user, import_keys.risk_categories, related_imported_categories
        )

        for capa in capas:
            values = asdict(capa)
            prepared_by = related_imported_users[values.pop("prepared_by")]
            approved_by = related_imported_users.get(values.pop("approved_by"))
            category = related_imported_categories[values.pop("category")]
            incident = related_imported_incidents.get(values.pop("incident"))
            risks = [
                r[0] for r_ in values.pop("risks_to_prevent") if (r := risk_map.get(r_))
            ]
            capa = CapaModel(
                **{
                    **values,
                    "organization_id": auth_user.organization_id,
                    "category_id": category.id,
                    "prepared_by": prepared_by.id,
                    "approved_by": approved_by and approved_by.id,
                    "incident_id": incident and incident.id,
                    "risks_to_prevent": risks,
                }
            )
            self._session.add(capa)

        self._session.commit()

        return len(capas)

    # Edit before usage
    def update_by_id(self, auth_user: AuthUser, capa_id: int, capa_updates: CapaUpdate):
        risks = self._risk_db.query_risks(auth_user).key_map
        query = (
            update(CapaModel)
            .where(
                and_(
                    CapaModel.id == capa_id,
                    CapaModel.organization_id == auth_user.organization_id,
                )
            )
            .values(
                self.capa_to_dict(auth_user, capa_updates),
            )
            # .returning(CapaModel.id)
        )
        update_cursor = self._session.execute(query)
        if update_cursor is None:
            return -1
        capa = self._session.query(CapaModel).get(capa_id)
        capa.risks_to_prevent = [
            r_[0] for r in capa_updates.risks_to_prevent if (r_ := risks.get(r.value))
        ]
        self._session.commit()
        return capa_id

    def delete_by_id(self, auth_user: AuthUser, capa_id: int):
        query = delete(CapaModel).where(
            and_(
                CapaModel.id == capa_id,
                CapaModel.organization_id == auth_user.organization_id,
            )
        )  # .returning(CapaModel.id)
        delete_cursor = self._session.execute(query)
        print("Deleted ID", delete_cursor)
        if delete_cursor is not None:
            self._session.commit()
            return capa_id

        return -1
