#!/usr/bin/env python3

from domain.capa.models import Capa
from typing import Optional


from infrastructure.models import (
    OrganizationModel,
    UserModel,
    RiskCategoryModel,
    IncidentModel,
    CapaModel,
)

from domain.shared.models import RelatedItem


class CapaToDomainModelMixin:
    def capa_record_to_domain_model(
        self,
        capa: CapaModel,
        approved_by: Optional[UserModel],
        prepared_by: UserModel,
        category: RiskCategoryModel,
        organization: OrganizationModel,
        incident: Optional[IncidentModel],
    ) -> Capa:
        return Capa(
            id=capa.id,
            name=capa.name,
            description=capa.description,
            measures_taken=capa.measures_taken,
            date_created=capa.date_created,
            # reference_id=capa.reference_id,
            target_effective_date=capa.target_effective_date,
            comments=capa.comments,
            # proposed_actions=capa.proposed_actions,
            rating=capa.rating,
            action_type=capa.action_type,
            incident=incident
            and RelatedItem(
                id=incident.id, value=incident.name, readable=incident.name
            ),
            approved_by=approved_by
            and RelatedItem(
                id=approved_by.id,
                value=approved_by.email,
                readable=approved_by.email,
            ),
            prepared_by=RelatedItem(
                id=prepared_by.id,
                value=prepared_by.email,
                readable=prepared_by.email,
            ),
            category=RelatedItem(
                id=category.id,
                value=category.name,
                readable=category.name,
            ),
            # category=capa.category,
            organization_id=organization.id,
            risks_to_prevent=[
                RelatedItem(id=r.id, value=r.name, readable=r.name)
                for r in capa.risks_to_prevent
            ],
        )
