from datetime import datetime, date
from domain.process_value.models import (
    NewProcessValue,
    ProcessValueOfProcess,
    ProcessValueUpdate,
)
from typing import Optional, List

from dataclasses import dataclass, field
from dataclasses_json import dataclass_json, config

from domain.shared.models import (
    RelatedItem,
    MonitoringSchedule,
)
from domain.shared.mixins import PartialMixin
from domain.task.models import ProcessNewTask, ProcessTaskUpdate, Task


@dataclass_json
@dataclass(frozen=True)
class NewProcess:
    name: str
    objective: RelatedItem
    team_responsible: RelatedItem
    monitoring_schedule: Optional[MonitoringSchedule] = field(
        metadata=config(
            encoder=lambda e: e and e.name,
            decoder=lambda s: s and MonitoringSchedule[s],
        ),
        default=None,
    )
    tasks: List[ProcessNewTask] = field(default_factory=list)
    inputs: List[RelatedItem] = field(default_factory=list)
    outputs: List[RelatedItem] = field(default_factory=list)
    person_responsible: Optional[RelatedItem] = None


@dataclass_json
@dataclass(frozen=True)
class ProcessImport(NewProcess):
    inputs: list[str] = field(default_factory=list)
    outputs: list[str] = field(default_factory=list)
    objective: str
    team_responsible: str
    person_responsible: str = ""


@dataclass_json
@dataclass(frozen=True)
class ProcessUpdate(NewProcess):
    tasks: List[ProcessTaskUpdate] = field(default_factory=list)


@dataclass_json
@dataclass(frozen=True)
class Process(NewProcess):
    organization_id: Optional[int] = None
    id: Optional[int] = None
    tasks: List[Task] = field(default_factory=list)
    potential_risks: List[RelatedItem] = field(default_factory=list)


@dataclass_json
@dataclass(frozen=True)
class ProcessPartial(PartialMixin):
    name: Optional[str] = None
    organization_id: Optional[RelatedItem] = None
    objective_id: Optional[RelatedItem] = None
    person_responsible: Optional[RelatedItem] = None
    team_responsible: Optional[RelatedItem] = None
    monitoring_schedule: Optional[MonitoringSchedule] = None


@dataclass(frozen=True)
class ProcessQueryParams:
    name: Optional[str] = None
    organization_id: Optional[int] = None
    risk_id: Optional[int] = None
    incident_id: Optional[int] = None
    output_value_id: Optional[int] = None
    input_value_id: Optional[int] = None
    objective_id: Optional[int] = None
    person_responsible: Optional[int] = None
    team_responsible: Optional[int] = None
    monitoring_schedule: Optional[MonitoringSchedule] = None
