#!/usr/bin/env python3
#
from infrastructure.models import ProcessModel
from typing import Optional, Protocol
from sqlalchemy import select
from sqlalchemy.orm import scoped_session
from domain.auth.models import AuthUser


class GetProcess(Protocol):
    _session: scoped_session

    def get_process_name_by_id(
        self, auth_user: AuthUser, process_id: int
    ) -> Optional[str]:
        p = self._session.execute(
            select(ProcessModel).where(
                ProcessModel.organization_id == auth_user.organization_id,
                process_id == ProcessModel.id,
            )
        ).first()
        return p.name if p else None
