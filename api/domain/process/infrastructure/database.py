import itertools
from domain.user.models import user_full_name
from domain.user.ports.storage import UserDatabase
from domain.process_value.ports.storage import ProcessValueDatabase
from domain.objective.ports.storage import ObjectiveDatabase
from domain.task_team.ports.storage import TaskTeamDatabase
from domain.objective.infrastructure.database import ObjectiveDatabaseAdapter
from domain.task_team.infrastructure.database import TaskTeamDatabaseAdapter
from domain.user.infrastructure.database import UserDatabaseAdapter
from itertools import chain
from domain import organization
from sqlalchemy.sql.elements import not_
from domain.process_value.models import ProcessValueOfProcess
from domain.process_value.infrastructure.database import ProcessValueDatabaseAdapter
from domain.task.models import ProcessTaskUpdate
from domain.task.infrastructure.database import TaskDatabaseAdapter
from domain.task.ports.storage import TaskDatabase
from typing import List, NamedTuple, Optional, cast

from sqlalchemy import insert, select, update, delete, bindparam, func
from sqlalchemy.sql import Select
from sqlalchemy.sql.expression import and_
from sqlalchemy.orm import scoped_session

from infrastructure.models import (
    ProcessModel,
    OrganizationModel,
    ObjectiveModel,
    ProcessValueModel,
    RiskModel,
    TaskModel,
    UserModel,
    TaskTeamModel,
    process_input_value_assoc,
    process_output_value_assoc,
    process_incident_assoc,
    process_risk_assoc,
)
from domain.auth.models import AuthUser

from domain.process.ports.storage import ProcessDatabase
from domain.process.models import (
    Process,
    ProcessQueryParams,
    ProcessUpdate,
    NewProcess,
    ProcessImport,
)
from dataclasses import asdict
from domain.shared.models import RelatedItem, RiskStatus


class ProcessImportKeys(NamedTuple):
    users: set[str]
    task_teams: set[str]
    objectives: set[str]
    process_values: set[str]


class ProcessDatabaseAdapter(ProcessDatabase):
    def __init__(
        self,
        session: scoped_session,
        task_db: TaskDatabase,
        process_value_db: ProcessValueDatabase,
        user_db: UserDatabase,
        objective_db: ObjectiveDatabase,
        task_team_db: TaskTeamDatabase,
    ):
        self._session = session
        self._task_db = cast(TaskDatabaseAdapter, task_db)
        self._process_value_db = cast(ProcessValueDatabaseAdapter, process_value_db)
        self._user_db = cast(UserDatabaseAdapter, user_db)
        self._objective_db = objective_db
        self._task_team_db = cast(TaskTeamDatabaseAdapter, task_team_db)

    def exists(self, auth_user: AuthUser, query_params: ProcessQueryParams) -> bool:
        query = (
            select(func.count())
            .select_from(ProcessModel)
            .where(
                and_(
                    ProcessModel.organization_id == auth_user.organization_id,
                    ProcessModel.name == query_params.name,
                )
            )
            .limit(1)
        )
        count = self._session.scalar(query)

        return count > 0

    def record_to_domain_model(
        self,
        process: ProcessModel,
        organization: OrganizationModel,
        objective: ObjectiveModel,
        team_responsible: TaskTeamModel,
        person_responsible: Optional[UserModel],
    ) -> Process:
        return Process(
            id=process.id,
            name=process.name,
            organization_id=organization.id,
            objective=RelatedItem(
                id=objective.id,
                value=objective.name,
                readable=objective.name,
            ),
            person_responsible=person_responsible
            and RelatedItem(
                id=person_responsible.id,
                readable=person_responsible.first_name
                + " "
                + person_responsible.last_name,
                value=person_responsible.email,
            ),
            team_responsible=RelatedItem(
                id=team_responsible.id,
                value=team_responsible.name,
                readable=team_responsible.name,
            ),
            monitoring_schedule=process.monitoring_schedule,
            tasks=[
                self._task_db.record_to_domain_model(t, organization, process, t.user)
                for t in process.tasks
            ],
            inputs=[
                RelatedItem(id=i.id, value=i.name, readable=i.name)
                for i in process.inputs
            ],
            outputs=[
                RelatedItem(id=i.id, value=i.name, readable=i.name)
                for i in process.outputs
            ],
        )

    def select_query(
        self, auth_user: AuthUser, query_params: Optional[ProcessQueryParams] = None
    ) -> Select:
        query = (
            select(
                ProcessModel,
                OrganizationModel,
                ObjectiveModel,
                TaskTeamModel,
                UserModel,
            )
            .select_from(ProcessModel)
            .join(ObjectiveModel, ObjectiveModel.id == ProcessModel.objective_id)
            .join(
                UserModel,
                UserModel.id == ProcessModel.person_responsible,
                isouter=True,
            )
            .join(
                TaskTeamModel,
                TaskTeamModel.id == ProcessModel.team_responsible,
            )
            .join(
                OrganizationModel, OrganizationModel.id == ProcessModel.organization_id
            )
            .where(ProcessModel.organization_id == auth_user.organization_id)
        )

        if query_params:
            if query_params.input_value_id is not None:
                query = query.join(
                    process_input_value_assoc,
                    process_input_value_assoc.columns.process_id == ProcessModel.id,
                ).where(
                    process_input_value_assoc.columns.process_value_id
                    == query_params.input_value_id
                )
            if query_params.output_value_id is not None:
                query = query.join(
                    process_output_value_assoc,
                    process_output_value_assoc.columns.process_id == ProcessModel.id,
                ).where(
                    process_output_value_assoc.columns.process_value_id
                    == query_params.output_value_id
                )

            if query_params.incident_id is not None:
                query = query.join(
                    process_incident_assoc,
                    process_incident_assoc.columns.process_id == ProcessModel.id,
                ).where(
                    process_incident_assoc.columns.incident_id
                    == query_params.incident_id
                )
            if query_params.risk_id is not None:
                query = query.join(
                    process_risk_assoc,
                    process_risk_assoc.columns.process_id == ProcessModel.id,
                ).where(process_risk_assoc.columns.risk_id == query_params.risk_id)

            if query_params.objective_id is not None:
                query = query.where(ObjectiveModel.id == query_params.objective_id)

        return query

    def query(self, auth_user: AuthUser, query_params: ProcessQueryParams):
        query = self.select_query(auth_user, query_params)
        processes = self._session.execute(query)

        return [self.record_to_domain_model(*row) for row in processes]

    def get_by_id(self, auth_user: AuthUser, process_id: int):
        query = self.select_query(auth_user).where(ProcessModel.id == process_id)
        result = self._session.execute(query).first()
        if result is None:
            return None

        return self.record_to_domain_model(*result)

    def create_process_values(
        self, auth_user: AuthUser, values: list[RelatedItem]
    ) -> list[ProcessValueModel]:
        items = []
        ids_to_fetch = []
        for input_ in values:
            if input_.id is None:
                item = self._process_value_db.create_obj_rel_item(
                    auth_user, input_, nocommit=True
                )
                items.append(item)
            else:
                ids_to_fetch.append(input_.id)
        self._session.flush()
        fetch_result = self._session.execute(
            select(ProcessValueModel).where(
                and_(
                    ProcessValueModel.id.in_(ids_to_fetch),
                    ProcessValueModel.organization_id == auth_user.organization_id,
                )
            )
        )
        items.extend([row[0] for row in fetch_result])
        return items

    def possible_risks_for_inputs(
        self, auth_user: AuthUser, process_id: int, input_ids: list[int]
    ):
        risk_query = (
            select(RiskModel)
            .select_from(process_output_value_assoc)
            .join(
                process_risk_assoc,
                process_risk_assoc.columns.process_id
                == process_output_value_assoc.columns.process_id,
            )
            .join(RiskModel, RiskModel.id == process_risk_assoc.columns.risk_id)
            .where(RiskModel.organization_id == auth_user.organization_id)
            .where(process_output_value_assoc.columns.process_id != process_id)
            .where(process_output_value_assoc.columns.process_value_id.in_(input_ids))
            .where(process_risk_assoc.columns.process_id != process_id)
            .where(RiskModel.current_status != RiskStatus.Controlled)
            .distinct()
        )
        return (row[0] for row in self._session.execute(risk_query))

    def known_risks(self, auth_user: AuthUser, process_id: int):
        risk_query = (
            select(RiskModel)
            .join(
                process_risk_assoc, process_risk_assoc.columns.risk_id == RiskModel.id
            )
            .where(process_risk_assoc.columns.process_id == process_id)
            .where(RiskModel.organization_id == auth_user.organization_id)
            .distinct()
        )

        return (row[0] for row in self._session.execute(risk_query))

    def get_team_member_contacts(self, auth_user: AuthUser, process_id: int):
        process = self._session.query(ProcessModel).get(process_id)
        team = process.team_responsible_obj
        leader = self._session.query(UserModel).get(team.leader)
        contacts = []
        for member in itertools.chain([leader], team.members):
            contacts.append(
                {
                    "email": member.email,
                    "phone": member.phone,
                    "use_sms": member.use_sms,
                }
            )
        return contacts

    def create(self, auth_user: AuthUser, process: NewProcess):
        inputs = self.create_process_values(auth_user, process.inputs)
        outputs = self.create_process_values(auth_user, process.outputs)
        new_process = ProcessModel(
            name=process.name,
            organization_id=auth_user.organization_id,
            objective_id=process.objective.id,
            team_responsible=process.team_responsible.id,
            monitoring_schedule=process.monitoring_schedule,
            person_responsible=process.person_responsible
            and process.person_responsible.id,
            inputs=inputs,
            outputs=outputs,
        )
        self._session.add(new_process)
        self._session.flush()
        possible_risks = [
            risk.name
            for risk in self.possible_risks_for_inputs(
                auth_user, new_process.id, [i.id for i in inputs]
            )
        ]
        process_related_item = RelatedItem(id=new_process.id, value=process.name)
        self._task_db.create_many(
            auth_user, [t.to_new_task(process_related_item) for t in process.tasks]
        )
        self._session.commit()

        return new_process.id, possible_risks

    def process_to_dict(self, auth_user: AuthUser, process: NewProcess):
        result = {
            **asdict(process),
            "organization_id": auth_user.organization_id,
            "objective_id": process.objective.id,
            "team_responsible": process.team_responsible.id,
        }
        del result["objective"]

        return result

    def create_many(self, auth_user: AuthUser, processes: List[NewProcess]):
        query = insert(ProcessModel).returning(ProcessModel.id)
        ids = self._session.execute(
            query, [self.process_to_dict(auth_user, process) for process in processes]
        )
        self._session.commit()

        return ids

    def _extract_imported_related(
        self, processes: List[ProcessImport]
    ) -> ProcessImportKeys:
        users = set()
        process_values = []
        objectives = set()
        task_teams = set()
        for p in processes:
            objectives.add(p.objective)
            task_teams.add(p.team_responsible)
            process_values = chain(process_values, p.inputs, p.outputs)

        return ProcessImportKeys(
            users=users,
            process_values=set(process_values),
            objectives=objectives,
            task_teams=task_teams,
        )

    def fetch_existing_processes(
        self, auth_user: AuthUser, process_keys: set[str]
    ) -> dict[str, ProcessModel]:
        process_results = self._session.execute(
            select(ProcessModel).where(
                and_(
                    ProcessModel.organization_id == auth_user.organization_id,
                    ProcessModel.name.in_(process_keys),
                )
            )
        )

        return {p.name: p for p, *_ in process_results}

    def fetch_existing_process_values(
        self, auth_user: AuthUser, process_value_keys: set[str]
    ) -> dict[str, ProcessValueModel]:
        process_value_results = self._session.execute(
            select(ProcessValueModel).where(
                and_(
                    ProcessValueModel.organization_id == auth_user.organization_id,
                    ProcessValueModel.name.in_(process_value_keys),
                )
            )
        )

        return {p.name: p for p, *_ in process_value_results}

    def create_new_process_values(
        self,
        auth_user: AuthUser,
        process_value_keys: set[str],
        process_values: dict[str, ProcessValueModel],
    ):
        for proc_value_name in process_value_keys:
            process_value = process_values.get(proc_value_name)
            if not process_value:
                c = ProcessValueModel(
                    name=proc_value_name,
                    organization_id=auth_user.organization_id,
                )
                self._session.add(c)
                process_values[proc_value_name] = c
        self._session.flush()

    def import_many(self, auth_user: AuthUser, processes: List[ProcessImport]):
        import_keys = self._extract_imported_related(processes)
        users = self._user_db.fetch_existing_users(auth_user, import_keys.users)
        process_values = self.fetch_existing_process_values(
            auth_user, import_keys.process_values
        )
        objectives = self._objective_db.fetch_existing_objectives(
            auth_user, import_keys.objectives
        )

        task_teams = self._task_team_db.fetch_existing_task_teams(
            auth_user, import_keys.task_teams
        )
        self.create_new_process_values(
            auth_user, import_keys.process_values, process_values
        )
        ids = 0

        for proc in processes:
            values = asdict(proc)
            objective = objectives[values.pop("objective")]
            team_responsible = task_teams[values.pop("team_responsible")]
            inputs = [process_values[val] for val in proc.inputs]
            outputs = [process_values[val] for val in proc.outputs]
            p = ProcessModel(
                **{
                    **values,
                    "organization_id": auth_user.organization_id,
                    "inputs": inputs,
                    "outputs": outputs,
                    "objective_id": objective.id,
                    "team_responsible": team_responsible.id,
                }
            )
            self._session.add(p)
            ids += 1

        self._session.commit()

        return ids

    def update_process_values(
        self, auth_user: AuthUser, values: list[RelatedItem]
    ) -> list[ProcessValueModel]:
        items = []
        ids_to_fetch = []
        for item in values:
            if item.id != -1:
                self._process_value_db.update_by_id_rel_item(auth_user, item.id, item)
                ids_to_fetch.append(item.id)
            else:
                item = self._process_value_db.create_obj_rel_item(
                    auth_user, item, nocommit=True
                )
                self._session.flush()
                items.append(item)
        fetch_result = self._session.execute(
            select(ProcessValueModel).where(
                and_(
                    ProcessValueModel.id.in_(ids_to_fetch),
                    ProcessValueModel.organization_id == auth_user.organization_id,
                )
            )
        )
        items.extend([row[0] for row in fetch_result])
        return items

    def update_tasks(
        self, auth_user: AuthUser, process_updates: ProcessUpdate
    ) -> list[TaskModel]:
        items = []
        ids_to_fetch = []
        for task in process_updates.tasks:
            if task.id is not None:
                ids_to_fetch.append(task.id)
                self._task_db.update_by_id(auth_user, task.id, task)
            else:
                item = self._task_db.create_obj(auth_user, task, nocommit=True)
                self._session.flush()
                items.append(item)

        fetch_result = self._session.execute(
            select(TaskModel).where(
                and_(
                    TaskModel.id.in_(ids_to_fetch),
                    TaskModel.organization_id == auth_user.organization_id,
                )
            )
        )
        items.extend([row[0] for row in fetch_result])

        return items

    def update_by_id(
        self, auth_user: AuthUser, process_id: int, process_updates: ProcessUpdate
    ):
        inputs = process_updates.inputs
        outputs = process_updates.outputs
        current_process_result = self._session.execute(
            select(ProcessModel).where(
                and_(
                    ProcessModel.id == process_id,
                    ProcessModel.organization_id == auth_user.organization_id,
                )
            )
        ).first()
        if not current_process_result:
            return -1
        current_process = current_process_result[0]
        current_process.name = process_updates.name
        current_process.objective_id = process_updates.objective.id
        current_process.team_responsible = process_updates.team_responsible.id
        current_process.person_responsible = (
            process_updates.person_responsible and process_updates.person_responsible.id
        )
        current_process.monitoring_schedule = process_updates.monitoring_schedule
        input_objs = self.update_process_values(auth_user, inputs)
        output_objs = self.update_process_values(auth_user, outputs)
        task_objs = self.update_tasks(auth_user, process_updates)
        current_process.inputs = input_objs
        current_process.outputs = output_objs
        current_process.tasks = task_objs
        self._session.execute(
            delete(TaskModel).where(
                and_(
                    and_(
                        not_(TaskModel.id.in_([t.id for t in current_process.tasks])),
                        TaskModel.organization_id == auth_user.organization_id,
                    ),
                    TaskModel.process_id == process_id,
                )
            )
        )
        self._session.commit()
        return process_id

    def delete_by_id(self, auth_user: AuthUser, process_id: int):
        query = (
            delete(ProcessModel).where(
                and_(
                    ProcessModel.id == process_id,
                    ProcessModel.organization_id == auth_user.organization_id,
                )
            )
            # .returning(ProcessModel.id)
        )
        delete_cursor = self._session.execute(query)
        print("Deleted ID", delete_cursor)
        if delete_cursor is not None:
            self._session.commit()
            return process_id

        return -1
