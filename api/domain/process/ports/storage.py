#!/usr/bin/env python3

from typing import Protocol, List, Optional
from abc import abstractmethod

from domain.auth.models import AuthUser

from domain.process.models import (
    Process,
    ProcessQueryParams,
    ProcessUpdate,
    NewProcess,
    ProcessImport,
)


class ProcessDatabase(Protocol):
    @abstractmethod
    def query(
        self, auth_user: AuthUser, query_params: ProcessQueryParams
    ) -> List[Process]:
        raise NotImplementedError

    @abstractmethod
    def get_by_id(self, auth_user: AuthUser, process_id: int) -> Optional[Process]:
        raise NotImplementedError

    @abstractmethod
    def exists(self, auth_user: AuthUser, query_params: ProcessQueryParams) -> bool:
        raise NotImplementedError

    @abstractmethod
    def create(self, auth_user: AuthUser, process: NewProcess) -> tuple[int, list[str]]:
        raise NotImplementedError

    @abstractmethod
    def create_many(
        self, auth_user: AuthUser, processes: List[NewProcess]
    ) -> List[int]:
        raise NotImplementedError

    @abstractmethod
    def import_many(
        self, auth_user: AuthUser, processes: List[ProcessImport]
    ) -> List[int]:
        raise NotImplementedError

    @abstractmethod
    def update_by_id(
        self, auth_user: AuthUser, process_id: int, process_updates: ProcessUpdate
    ) -> int:
        raise NotImplementedError

    @abstractmethod
    def delete_by_id(self, auth_user: AuthUser, process_id: int) -> int:
        raise NotImplementedError

    @abstractmethod
    def get_team_member_contacts(self, auth_user: AuthUser, process_id: int):
        raise NotImplementedError

    @abstractmethod
    def known_risks(
        self,
        auth_user: AuthUser,
        process_id: int,
    ):
        raise NotImplementedError

    @abstractmethod
    def possible_risks_for_inputs(
        self, auth_user: AuthUser, process_id: int, input_ids: list[int]
    ):
        raise NotImplementedError
