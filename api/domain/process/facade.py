#!/usr/bin/env python3

from domain.shared.models import RelatedItem
from domain.sms.ports import SmsClient
from domain.email.ports import EmailClient
from typing import List

from domain.auth.models import AuthUser
from domain.process.models import (
    Process,
    ProcessQueryParams,
    NewProcess,
    ProcessUpdate,
    ProcessImport,
)
from domain.process.ports.storage import ProcessDatabase


class ProcessFacade:
    def __init__(self, db: ProcessDatabase, email: EmailClient, sms: SmsClient):
        self._db = db
        self._email = email
        self._sms = sms

    def get_processes(self, auth_user: AuthUser, query_params: ProcessQueryParams):
        return self._db.query(auth_user, query_params)

    def get_process(self, auth_user: AuthUser, process_id: int):
        return self._db.get_by_id(auth_user, process_id)

    def create_process(self, auth_user: AuthUser, process: NewProcess):
        id_, possible_risks = self._db.create(auth_user, process)
        if id_ and id_ != None:
            reason = "Possible Risks for new process"
            message = "\n".join(
                [
                    f'A new process "{process.name}" has been created with the following possible risks',
                    *("  - " + risk for risk in possible_risks),
                ]
            )
            contacts = self._db.get_team_member_contacts(auth_user, id_)
            to_email = []
            to_sms = []
            for contact in contacts:
                to_email.append(contact["email"])
                if contact["use_sms"]:
                    to_sms.append(contact["phone"])
            self._email.send_notifications(to_email, reason, message)
            if to_sms:
                self._sms.send(reason + "\n\n" + message)

        return id_

    def known_risks_for_process(self, auth_user: AuthUser, process_id: int):
        process = self.get_process(auth_user, process_id)
        if process is not None:
            return [
                RelatedItem(id=r.id, value=r.name)
                for r in self._db.known_risks(auth_user, process.id)
            ]
        else:
            return []

    def possible_risks_for_process_inputs(self, auth_user: AuthUser, process_id: int):
        process = self.get_process(auth_user, process_id)
        if process is not None:
            return [
                RelatedItem(id=r.id, value=r.name)
                for r in self._db.possible_risks_for_inputs(
                    auth_user, process.id, [i.id for i in process.inputs]
                )
            ]
        else:
            return []

    def update_process(
        self, auth_user: AuthUser, process_id: int, process: ProcessUpdate
    ):
        return self._db.update_by_id(auth_user, process_id, process)

    def delete_process(self, auth_user: AuthUser, process_id: int):
        return self._db.delete_by_id(auth_user, process_id)

    def import_processes(self, auth_user: AuthUser, processes: List[ProcessImport]):
        return self._db.import_many(auth_user, processes)


ProcessFacadeT = ProcessFacade
