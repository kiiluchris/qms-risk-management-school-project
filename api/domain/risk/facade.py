#!/usr/bin/env python3

from typing import List

from domain.auth.models import AuthUser
from domain.risk.models import (
    Risk,
    RiskQueryParams,
    NewRisk,
    RiskUpdate,
    RiskImport,
)
from domain.risk.ports.storage import RiskDatabase


class RiskFacade:
    def __init__(self, db: RiskDatabase):
        self._db = db

    def get_risks(self, auth_user: AuthUser, query_params: RiskQueryParams):
        return self._db.query(auth_user, query_params)

    def get_risk(self, auth_user: AuthUser, risk_id: int):
        return self._db.get_by_id(auth_user, risk_id)

    def create_risk(self, auth_user: AuthUser, risk: NewRisk):
        if self._db.exists(auth_user, RiskQueryParams(name=risk.name)):
            return -1
        return self._db.create(auth_user, risk)

    def update_risk(self, auth_user: AuthUser, risk_id: int, risk: RiskUpdate):
        return self._db.update_by_id(auth_user, risk_id, risk)

    def delete_risk(self, auth_user: AuthUser, risk_id: int):
        return self._db.delete_by_id(auth_user, risk_id)

    def import_risks(self, auth_user: AuthUser, risks: List[RiskImport]):
        return self._db.import_many(auth_user, risks)

    def get_modelling_data(self, auth_user: AuthUser):
        return self._db.get_classification_metrics(auth_user)

    def get_dashboard_risk_data(self, auth_user: AuthUser):
        return self._db.dashboard_risk_data(auth_user)

    def get_ml_models(self, auth_user: AuthUser):
        return self._db.ml_models(auth_user)

    def get_suggested_capas(self, auth_user: AuthUser, risk: Risk):
        return self._db.suggest_capas(auth_user, risk)


RiskFacadeT = RiskFacade
