from domain.user.infrastructure.database import UserDatabaseAdapter
from domain.user.ports.storage import UserDatabase
from domain.capa.infrastructure.capa_to_domain_model_mixin import CapaToDomainModelMixin
from application.graphql.ariadne import resolve_capa_incident
from domain.settings.infrastructure.database import SettingsDatabaseAdapter
from domain.settings.ports.storage import SettingsDatabase
from domain.user.models import user_full_name
from infrastructure.database import Base
from domain.process.ports.storage import ProcessDatabase
from domain.process.infrastructure.database import ProcessDatabaseAdapter
from typing import Generic, List, Optional, NamedTuple, Sequence, Type, TypeVar, cast

import itertools
from operator import attrgetter
from sqlalchemy import insert, select, update, delete, bindparam, func, alias
from sqlalchemy.sql import Select
from sqlalchemy.sql.expression import and_
from sqlalchemy.orm import scoped_session, aliased

from infrastructure.models import (
    ObjectiveModel,
    ProcessValueModel,
    RiskClassificationDataModel,
    RiskModel,
    UserModel,
    risk_value_assoc,
    capa_risk_assoc,
    RiskCategoryModel,
    ProcessModel,
    TaskTeamModel,
    IncidentModel,
    CapaModel,
    capa_incidents_assoc,
    process_incident_assoc,
    process_risk_assoc,
)
from domain.auth.models import AuthUser

from domain.risk.ports.storage import RiskDatabase
from domain.risk.models import (
    Risk,
    RiskImport,
    RiskQueryParams,
    RiskUpdate,
    NewRisk,
)
from dataclasses import asdict, dataclass, field
from domain.shared.models import RelatedItem, RiskStatus
from domain.risk.infrastructure.modelling.generation import (
    RiskClassificationMixin,
    RiskModelBuilderMixin,
)
from domain.risk.infrastructure.modelling.shared import *


class RiskImportKeys(NamedTuple):
    processes: set[str]
    users: set[str] = field(default_factory=set)
    categories: set[str] = field(default_factory=set)


class RiskDatabaseAdapter(
    RiskDatabase, RiskModelBuilderMixin, RiskClassificationMixin, CapaToDomainModelMixin
):
    def __init__(
        self,
        session: scoped_session,
        process_db: ProcessDatabase,
        setting_db: SettingsDatabase,
        user_db: UserDatabase,
    ):
        self._session = session
        self._process_db = cast(ProcessDatabaseAdapter, process_db)
        self._setting_db = cast(SettingsDatabaseAdapter, setting_db)
        self._user_db = cast(UserDatabaseAdapter, user_db)

    def exists(self, auth_user: AuthUser, query_params: RiskQueryParams) -> bool:
        query = (
            select(func.count())
            .select_from(RiskModel)
            .where(
                and_(
                    RiskModel.organization_id == auth_user.organization_id,
                    RiskModel.name == query_params.name,
                )
            )
            .limit(1)
        )
        count = self._session.scalar(query)

        return count > 0

    def record_to_domain_model(
        self,
        risk: RiskModel,
        risk_owner: Optional[UserModel],
        category: RiskCategoryModel,
    ) -> Risk:
        return Risk(
            id=risk.id,
            name=risk.name,
            description=risk.description,
            is_finalized=risk.is_finalized,
            risk_owner=risk_owner
            and RelatedItem(
                id=risk_owner.id,
                value=risk_owner.email,
                readable=risk_owner.email,
            ),
            likelihood=risk.likelihood,
            impact=risk.impact,
            recommended_actions=risk.recommended_actions,
            affected_processes=[
                RelatedItem(id=p.id, value=p.name, readable=p.name)
                for p in risk.affected_processes
            ],
            category=RelatedItem(
                id=category.id, value=category.name, readable=category.name
            ),
            current_status=risk.current_status,
        )

    def select_query(
        self, auth_user: AuthUser, query_params: Optional[RiskQueryParams] = None
    ) -> Select:
        query = (
            select(
                RiskModel,
                UserModel,
                RiskCategoryModel
                # prepared_by,
            )
            .select_from(RiskModel)
            .join(UserModel, RiskModel.risk_owner == UserModel.id, isouter=True)
            .join(RiskCategoryModel, RiskCategoryModel.id == RiskModel.category_id)
            .where(RiskModel.organization_id == auth_user.organization_id)
        )
        if query_params:
            if query_params.process_value_id is not None:
                query = query.join(
                    risk_value_assoc, risk_value_assoc.columns.risk_id == RiskModel.id
                ).where(
                    risk_value_assoc.columns.process_value_id
                    == query_params.process_value_id
                )
            if query_params.capa_id is not None:
                query = query.join(
                    capa_risk_assoc, capa_risk_assoc.columns.risk_id == RiskModel.id
                ).where(capa_risk_assoc.columns.capa_id == query_params.capa_id)

        return query

    def query(self, auth_user: AuthUser, query_params: RiskQueryParams):
        query = self.select_query(auth_user, query_params)
        risks = self._session.execute(query)

        return [self.record_to_domain_model(*row) for row in risks]

    def get_by_id(self, auth_user: AuthUser, risk_id: int):
        query = self.select_query(auth_user).where(RiskModel.id == risk_id)
        result = self._session.execute(query).first()
        if result is None:
            return None

        return self.record_to_domain_model(*result)

    def _extract_new_risk_keys(self, risks: List[NewRisk]):
        processes = []
        for risk in risks:
            processes = itertools.chain(
                processes, [p.value for p in risk.affected_processes]
            )

        return RiskImportKeys(processes=set(processes))

    def create(self, auth_user: AuthUser, risk: NewRisk):
        """
        When entering a new risk it should associate with similar existing risks:
        - Classify the new risk
        - Estimate the impact and likelihood of the new risk,
        hence calculating the level
        - Notifies the user of the risk for them to respond to it
        - algorithm? classification problem - text classification
        - Before the risk and its classification gets saved the user should be
        able to seek the opinion of a professional
        """
        import_keys = self._extract_new_risk_keys([risk])
        risk_model_deps = self.fetch_dependencies(auth_user)
        existing_processes = risk_model_deps.processes.key_map
        affected_processes = [
            p
            for proc in risk.affected_processes
            if (p := existing_processes.get(proc.value))
        ]
        attrs = self.generate_attributes(auth_user, risk_model_deps, affected_processes)
        risk_records = self.query_risks(auth_user)
        risk_df = self.to_dataframe(risk_records.items)
        new_risk = RiskModel(
            organization_id=auth_user.organization_id,
            name=risk.name,
            description=risk.description,
            recommended_actions=risk.recommended_actions,
            affected_processes=affected_processes,
            risk_owner=risk.risk_owner and risk.risk_owner.id,
            current_status=RiskStatus.Identified,
        )
        self._session.add(new_risk)
        self._session.flush()
        cls_data = self.create_risk_model_record(auth_user, new_risk, attrs)
        new_risk_df = self.to_dataframe([(new_risk, cls_data)], is_new=True)
        new_risk.category = self.classify_category(risk_df, new_risk_df)[0].item()
        new_risk.category_id = risk_model_deps.risk_categories.key_map[
            new_risk.category
        ].id
        new_risk.likelihood = self.classify_likelihood(risk_df, new_risk_df)[0].item()
        new_risk.impact = self.classify_impact(risk_df, new_risk_df)[0].item()
        self._session.commit()

        return new_risk.id

    def risk_to_dict(self, auth_user: AuthUser, risk: NewRisk):
        result = {
            **asdict(risk),
            # "risk_owner": risk.risk_owner.id,
            # "prepared_by": risk.prepared_by.id,
        }

        return result

    def create_many(self, auth_user: AuthUser, risks: List[NewRisk]):
        query = insert(RiskModel)  # .returning(RiskModel.id)
        ids = self._session.execute(
            query, [self.risk_to_dict(auth_user, risk) for risk in risks]
        )
        self._session.commit()

        return ids

    def create_risk_category(
        self, auth_user: AuthUser, category_name: str, *, nocommit: bool = False
    ):
        c = RiskCategoryModel(
            name=category_name,
            organization_id=auth_user.organization_id,
        )
        self._session.add(c)
        if not nocommit:
            self._session.commit()
        return c

    def _extract_risk_import_keys(self, risks: List[RiskImport]):
        processes = []
        categories = set()
        users = set()
        for risk in risks:
            processes = itertools.chain(processes, risk.affected_processes)
            categories.add(risk.category)
            if risk.risk_owner:
                users.add(risk.risk_owner)

        return RiskImportKeys(
            processes=set(processes), categories=categories, users=users
        )

    def fetch_existing_risks_by_ids(self, auth_user: AuthUser, risk_ids: Sequence[int]):
        results = self._session.execute(
            select(RiskModel).where(
                and_(
                    RiskModel.organization_id == auth_user.organization_id,
                    RiskModel.id.in_(risk_ids),
                )
            )
        )
        return {i.name: i for i, *_ in results}

    def fetch_existing_risk_categories(
        self, auth_user: AuthUser, imported_risk_categories: set[str]
    ):
        risk_category_results = self._session.execute(
            select(RiskCategoryModel).where(
                and_(
                    RiskCategoryModel.organization_id == auth_user.organization_id,
                    RiskCategoryModel.name.in_(imported_risk_categories),
                )
            )
        )
        return {i.name: i for i, *_ in risk_category_results}

    def create_risk_model_record(
        self, auth_user: AuthUser, risk: RiskModel, attrs: RiskModelAttrs
    ):
        cls_data = RiskClassificationDataModel(
            organization_id=auth_user.organization_id,
            risk_id=risk.id,
            fraction_affected_processes=attrs.fraction_affected_processes,
            fraction_staff_affected=attrs.fraction_staff_affected,
            ratio_num_affected_inputs_to_all_inputs=attrs.ratio_num_affected_inputs_to_all_inputs,
            fraction_incomplete_tasks=attrs.fraction_incomplete_tasks,
            fraction_other_processes_past_deadline=attrs.fraction_other_processes_past_deadline,
            fraction_nearing_objective_deadlines=attrs.fraction_nearing_objective_deadlines,
            fraction_passed_objective_deadlines=attrs.fraction_passed_objective_deadlines,
            closest_deadline_binned=attrs.closest_deadline_binned,
            fraction_incidents=attrs.fraction_incidents,
        )
        self._session.add(cls_data)

        return cls_data

    def import_many(self, auth_user: AuthUser, risks: List[RiskImport]):
        import_keys = self._extract_risk_import_keys(risks)
        existing_categories = self.fetch_existing_risk_categories(
            auth_user, import_keys.categories
        )
        existing_processes = self._process_db.fetch_existing_processes(
            auth_user, import_keys.processes
        )
        existing_users = self._user_db.fetch_existing_users(
            auth_user, import_keys.users
        )
        risk_model_deps = self.fetch_dependencies(auth_user)
        ids = []
        for risk in risks:
            category = existing_categories.get(risk.category)
            risk_owner = existing_users.get(risk.risk_owner)
            if not category:
                category = self.create_risk_category(
                    auth_user, risk.category, nocommit=True
                )
                existing_categories[risk.category] = category
                self._session.flush()

            affected_processes = [
                p
                for proc in risk.affected_processes
                if (p := existing_processes.get(proc))
            ]
            r = RiskModel(
                organization_id=auth_user.organization_id,
                name=risk.name,
                description=risk.description,
                recommended_actions=risk.recommended_actions,
                impact=risk.impact,
                likelihood=risk.likelihood,
                category=category.name,
                category_id=category.id,
                affected_processes=affected_processes,
                is_finalized=True,
                risk_owner=risk_owner and risk_owner.id,
                current_status=RiskStatus.Assessed,
            )
            self._session.add(r)
            self._session.flush()
            attrs = self.generate_attributes(
                auth_user, risk_model_deps, affected_processes
            )
            self.create_risk_model_record(auth_user, r, attrs)
            ids.append(r.id)
        self._session.commit()

        return ids

    def approve_risk(self, auth_user: AuthUser, risk_id: int):
        return

    # Edit before usage
    def update_by_id(self, auth_user: AuthUser, risk_id: int, risk_updates: RiskUpdate):
        query = (
            select(RiskModel).where(
                and_(
                    RiskModel.id == risk_id,
                    RiskModel.organization_id == auth_user.organization_id,
                )
            )
            # .returning(RiskModel.id)
        )
        risk_result = self._session.execute(query).first()
        if risk_result is None:
            return -1
        print(dir(risk_updates))
        risk = risk_result[0]
        risk.name = risk_updates.name
        risk.risk_owner = risk_updates.risk_owner and risk_updates.risk_owner.id
        risk.description = risk_updates.description
        risk.impact = risk_updates.impact
        risk.likelihood = risk_updates.likelihood
        risk.current_status = risk_updates.current_status
        affected_process_ids = [p.id for p in risk_updates.affected_processes]
        risk.affected_processes.clear()
        risk.affected_processes[:] = [
            row[0]
            for row in self._session.execute(
                select(ProcessModel).where(ProcessModel.id.in_(affected_process_ids))
            )
        ]
        # risk.prepared_by = risk_updates.prepared_by and risk_updates.prepared_by.id
        self._session.commit()
        return risk.id

    def delete_by_id(self, auth_user: AuthUser, risk_id: int):
        query = delete(RiskModel).where(
            and_(
                RiskModel.id == risk_id,
                RiskModel.organization_id == auth_user.organization_id,
            )
        )  # .returning(RiskModel.id)
        delete_cursor = self._session.execute(query)
        print("Deleted ID", delete_cursor)
        if delete_cursor is not None:
            self._session.commit()
            return risk_id

        return -1

    def get_classification_metrics(self, auth_user: AuthUser):
        risk_records = self.query_risks(auth_user)
        risk_df = self.to_dataframe(risk_records.items)
        category_labels = self.category_labels(risk_df)
        return {
            "category": self.category_metrics(risk_df),
            "category_labels": category_labels,
            "impact": self.impact_metrics(risk_df),
            "likelihood": self.likelihood_metrics(risk_df),
        }

    def __flatten_risk_data(self, data: dict[str, int]):
        return [dict(key=key, value=count) for key, count in data.items()]

    def __flatten_double_nested_risk_data(self, data: dict[str, dict[str, int]]):
        result = []
        for name, group in data.items():
            for sname, count in group.items():
                result.append({"main_key": name, "sub_key": sname, "value": count})

        return result

    def ml_models(self, auth_user: AuthUser):
        return self.models.keys()

    def suggest_capas(self, auth_user: AuthUser, risk: Risk):
        self.__flatten_double_nested_risk_data
        prepared_by = aliased(UserModel)
        query = (
            select(
                CapaModel,
                UserModel,
                prepared_by,
                RiskCategoryModel,
                OrganizationModel,
                IncidentModel,
            )
            .join(RiskCategoryModel, RiskCategoryModel.id == CapaModel.category_id)
            .join(
                IncidentModel,
                IncidentModel.id == CapaModel.incident_id,
            )
            .join(
                process_incident_assoc,
                process_incident_assoc.columns.incident_id == IncidentModel.id,
            )
            .join(
                ProcessModel,
                process_incident_assoc.columns.process_id == ProcessModel.id,
            )
            .join(
                process_risk_assoc,
                process_risk_assoc.columns.process_id == ProcessModel.id,
            )
            .join(RiskModel, process_risk_assoc.columns.risk_id == RiskModel.id)
            .join(UserModel, CapaModel.approved_by == UserModel.id, isouter=True)
            .join(prepared_by, CapaModel.prepared_by == prepared_by.id)
            .join(OrganizationModel, CapaModel.organization_id == OrganizationModel.id)
            .where(
                and_(
                    and_(
                        RiskModel.id != risk.id,
                        CapaModel.organization_id == auth_user.organization_id,
                    ),
                    RiskModel.category_id == RiskCategoryModel.id,
                )
            )
        )
        capas = self._session.execute(query)
        return [self.capa_record_to_domain_model(*row) for row in capas]

    def dashboard_risk_data(self, auth_user: AuthUser):
        risks = list(self._session.execute(self.select_query(auth_user)))
        teams = {
            team.leader: team
            for team, *_ in self._session.execute(
                select(TaskTeamModel).where(
                    TaskTeamModel.organization_id == auth_user.organization_id
                )
            )
        }
        settings = self._setting_db.get_settings(auth_user)
        risk_level_bounds = make_weighted_risk_level_bounds(
            settings.risk_likelihood_weight, settings.risk_impact_weight
        )
        risk_count_grouped_by_level = {}
        risk_count_grouped_by_owner = {}
        risk_count_grouped_by_category = {}
        risk_count_grouped_by_team_and_category = {}
        categories = set()
        for risk, *_ in risks:
            level = map_weighted_risk_level_name(
                risk.likelihood * risk.impact, risk_level_bounds
            )
            level_count = risk_count_grouped_by_level.get(level, 0)
            risk_count_grouped_by_level[level] = level_count + 1
            owner = "None"
            if risk.risk_owner:
                owner = user_full_name(risk.risk_owner_obj)
                team = teams.get(risk.risk_owner)
                if team:
                    owner = "HOD " + team.name
            risk_count_grouped_by_owner[owner] = (
                risk_count_grouped_by_owner.get(owner, 0) + 1
            )

            category = risk.category
            categories.add(category)
            risk_count_grouped_by_category[category] = (
                risk_count_grouped_by_category.get(category, 0) + 1
            )

            for process in risk.affected_processes:
                team = process.team_responsible_obj.name
                risk_count_grouped_by_team_and_category[
                    team
                ] = risk_count_grouped_by_team_and_category.get(team, {})
                risk_count_grouped_by_team_and_category[team][category] = (
                    risk_count_grouped_by_team_and_category[team].get(category, 0) + 1
                )

        return {
            "level": self.__flatten_risk_data(risk_count_grouped_by_level),
            "owner": self.__flatten_risk_data(risk_count_grouped_by_owner),
            "category": self.__flatten_risk_data(risk_count_grouped_by_category),
            "team_and_category": self.__flatten_double_nested_risk_data(
                risk_count_grouped_by_team_and_category
            ),
            "categories": list(categories),
        }
