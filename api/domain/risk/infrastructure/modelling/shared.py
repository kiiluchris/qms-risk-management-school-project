#!/usr/bin/env python3

import math
from dataclasses import dataclass
from datetime import date
from itertools import chain
from operator import attrgetter
from typing import Callable, Generic, Iterable, NamedTuple, Type, TypeVar, Union, cast

from infrastructure.models import *

ModelT = TypeVar("ModelT")


@dataclass
class ModelDep(Generic[ModelT]):
    items: list[ModelT]
    key_map: dict[str, ModelT]
    id_map: dict[int, ModelT]


class RiskModelDeps(NamedTuple):
    objectives: ModelDep[ObjectiveModel]
    processes: ModelDep[ProcessModel]
    process_values: ModelDep[ProcessValueModel]
    task_teams: ModelDep[TaskTeamModel]
    users: ModelDep[UserModel]
    incidents: ModelDep[IncidentModel]
    risk_categories: ModelDep[RiskCategoryModel]


@dataclass
class RiskModelAttrs:
    fraction_affected_processes: float
    fraction_staff_affected: float
    ratio_num_affected_inputs_to_all_inputs: float
    fraction_incomplete_tasks: float
    fraction_other_processes_past_deadline: float
    fraction_nearing_objective_deadlines: float
    fraction_passed_objective_deadlines: float
    closest_deadline_binned: float
    fraction_incidents: float


T = TypeVar("T")


def get_matching(
    xs: Union[Iterable[T], Iterable[Iterable[T]]],
    predicate: Callable[[T], bool],
    *,
    flatten: bool = False,
):
    values = (
        chain.from_iterable(cast(Iterable[Iterable[T]], xs))
        if flatten
        else cast(Iterable[T], xs)
    )
    for x in values:
        if predicate(x):
            yield x


def get_unique_matching(
    xs: Union[Iterable[T], Iterable[Iterable[T]]],
    predicate: Callable[[T], bool],
    *,
    flatten: bool = False,
    item_key: Callable[[T], str] = attrgetter("id"),
):
    traversed_values = set()
    for x in get_matching(xs, predicate, flatten=flatten):
        key = item_key(x)
        if key not in traversed_values:
            traversed_values.add(key)
            yield x


def count_matching(
    xs: Union[Iterable[T], Iterable[Iterable[T]]],
    predicate: Callable[[T], bool],
    *,
    flatten: bool = False,
):
    count = 0
    for _ in get_matching(xs, predicate, flatten=flatten):
        count += 1
    return count


def safe_div(b, a):
    if a == 0:
        return 0
    return b / a


def remaining_time_binned(adate):
    delta = (adate - date.today()).days
    if delta < 0:
        return 1
    if delta < 30:
        return 0.8
    elif delta < 90:
        return 0.6
    elif delta < 180:
        return 0.4
    elif delta < 360:
        return 0.2
    else:
        return 0


class RiskLevelBounds(NamedTuple):
    lower: int
    lower_mid: int
    upper_mid: int
    upper: int


def make_weighted_risk_level_bounds(likelihood: int, impact: int):
    risk_level = likelihood * impact
    upper_range_size = 25 - risk_level
    upper_mid_bound = risk_level + (upper_range_size // 6)
    upper_bound = risk_level + math.ceil((25 - upper_mid_bound) // 2)
    lower_range_size = risk_level - 1
    lower_mid_bound = risk_level - (lower_range_size // 6)
    lower_bound = ((lower_mid_bound - 1) // 2) + 1

    return RiskLevelBounds(lower_bound, lower_mid_bound, upper_mid_bound, upper_bound)


T = TypeVar("T")


def make_risk_level_bounds_mapper(low: T, low_mid: T, mid: T, low_high: T, high: T):
    def run(level: int, bounds: RiskLevelBounds) -> T:
        if level == 25 and bounds.upper_mid == 24:
            return mid
        elif level < bounds.lower:
            return low
        elif level < bounds.lower_mid:
            return low_mid
        elif level < bounds.upper_mid:
            return mid
        elif level < bounds.upper:
            return low_high
        else:
            return high

    return run


map_weighted_risk_level_name = make_risk_level_bounds_mapper(
    "Low", "Low Med", "Medium", "Low High", "High"
)
