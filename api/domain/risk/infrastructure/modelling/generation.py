#!/usr/bin/env python3


from dataclasses import dataclass
from dataclasses_json import dataclass_json
from datetime import date, timedelta
from itertools import chain
from operator import attrgetter
from typing import Callable, Type, Union, cast

from sqlalchemy import select
from sqlalchemy.orm import scoped_session
import pandas
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
from sklearn.neural_network import MLPClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import (
    confusion_matrix,
    classification_report,
    accuracy_score,
    precision_recall_fscore_support,
    SCORERS,
)
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.neural_network import MLPClassifier
from domain.auth.models import AuthUser
from domain.risk.infrastructure.modelling.shared import *
from domain.shared.models import ObjectiveStatus
from domain.user.models import user_full_name
from infrastructure.models import *
from infrastructure.models import Base


@dataclass_json
@dataclass(frozen=True)
class ModelScore:
    model_name: str
    f1_score: list[float]
    precision: list[float]
    recall: list[float]


class RiskModelQueries:
    _session: scoped_session

    def query_dependency(
        self, auth_user: AuthUser, model: Type[Base], *, item_key=attrgetter("name")
    ) -> ModelDep:
        query = select(model).where(model.organization_id == auth_user.organization_id)
        rows = [row[0] for row in self._session.execute(query)]
        key_map = {}
        id_map = {}
        for item in rows:
            key_map[item_key(item)] = item
            id_map[item.id] = item
        return ModelDep(items=rows, id_map=id_map, key_map=key_map)


class RiskModelBuilderMixin(RiskModelQueries):
    def fetch_dependencies(self, auth_user: AuthUser):
        return RiskModelDeps(
            objectives=self.query_dependency(auth_user, ObjectiveModel),
            processes=self.query_dependency(auth_user, ProcessModel),
            process_values=self.query_dependency(auth_user, ProcessValueModel),
            task_teams=self.query_dependency(auth_user, TaskTeamModel),
            users=self.query_dependency(auth_user, UserModel, item_key=user_full_name),
            incidents=self.query_dependency(auth_user, IncidentModel),
            risk_categories=self.query_dependency(auth_user, RiskCategoryModel),
        )

    def generate_attributes(
        self,
        auth_user: AuthUser,
        deps: RiskModelDeps,
        affected_processes: list[ProcessModel],
    ):

        affected_process_ids = set(p.id for p in affected_processes)
        affected_task_teams_ids = [p.team_responsible for p in affected_processes]
        affected_task_teams = [
            deps.task_teams.id_map[id_] for id_ in affected_task_teams_ids
        ]
        affected_process_outputs = list(
            chain.from_iterable(proc.outputs for proc in affected_processes)
        )
        other_process_inputs = list(
            chain.from_iterable(
                proc.inputs
                for proc in deps.processes.items
                if proc.id not in affected_process_ids
            )
        )
        affected_process_output_names = set([o.name for o in affected_process_outputs])

        affected_objectives = [
            deps.objectives.id_map[p.objective_id] for p in affected_processes
        ]
        today = date.today()
        last_week = today - timedelta(days=7)
        affected_objective_deadlines_passed = [
            o for o in affected_objectives if today > o.target_date
        ]
        affected_objective_deadlines_nearing = [
            o
            for o in affected_objectives
            if today < o.target_date and o.target_date > last_week
        ]

        affected_process_tasks = list(
            chain.from_iterable([p.tasks for p in affected_processes])
        )
        affected_process_incomplete_tasks = [
            task for task in affected_process_tasks if not task.is_complete
        ]

        affected_task_teams_other_processes = [
            p for p in deps.processes.items if p.id not in affected_process_ids
        ]
        affected_task_teams_other_processes_past_deadline = [
            p
            for p in affected_task_teams_other_processes
            if today > (o := deps.objectives.id_map[p.objective_id]).target_date
        ]

        last_year = today.replace(year=today.year - 1)
        # Recent meaning past year
        recent_incidents_in_affected_processes = list(
            chain.from_iterable(
                [
                    (i for i in p.incidents if i.date_occurred > last_year)
                    for p in affected_processes
                ]
            )
        )

        number_of_processes = len(deps.processes.items)
        # number_of_objectives = len(deps.objectives.items)
        number_of_users = len(deps.users.items)
        number_of_incidents = len(deps.incidents.items)

        number_of_affected_processes = len(affected_processes)

        unique_members_of_affected_task_teams = list(
            get_unique_matching(
                (
                    chain([deps.users.id_map[team.leader]], team.members)
                    for team in affected_task_teams
                ),
                lambda _: True,
                flatten=True,
                item_key=cast(Callable[[T], str], user_full_name),
            )
        )
        number_of_unique_members_of_affected_task_teams = len(
            unique_members_of_affected_task_teams
        )

        number_of_other_process_inputs = len(other_process_inputs)
        number_of_affected_other_process_inputs = count_matching(
            other_process_inputs,
            lambda input_: input_.name in affected_process_output_names,
        )

        number_of_affected_objectives = len(affected_objectives)
        number_of_affected_objective_deadlines_nearing = len(
            affected_objective_deadlines_nearing
        )
        number_of_affected_objective_deadlines_passed = len(
            affected_objective_deadlines_passed
        )

        number_of_affected_process_tasks = len(affected_process_tasks)
        number_of_affected_process_incomplete_tasks = len(
            affected_process_incomplete_tasks
        )

        number_of_affected_task_teams_other_processes = len(
            affected_task_teams_other_processes
        )
        number_of_affected_task_teams_other_processes_past_deadline = len(
            affected_task_teams_other_processes_past_deadline
        )

        number_of_recent_incidents_in_affected_processes = len(
            recent_incidents_in_affected_processes
        )

        # ATTRS
        # Impact
        fraction_affected_processes = safe_div(
            number_of_affected_processes, number_of_processes
        )
        # Impact
        fraction_staff_affected = safe_div(
            number_of_unique_members_of_affected_task_teams, number_of_users
        )
        # Impact
        ratio_num_affected_inputs_to_all_inputs = safe_div(
            number_of_affected_other_process_inputs, number_of_other_process_inputs
        )
        # Likelihood | Impact
        fraction_incomplete_tasks = safe_div(
            number_of_affected_process_incomplete_tasks,
            number_of_affected_process_tasks,
        )
        # Impact
        fraction_other_processes_past_deadline = safe_div(
            number_of_affected_task_teams_other_processes_past_deadline,
            number_of_affected_task_teams_other_processes,
        )
        # Likelihood
        fraction_nearing_objective_deadlines = safe_div(
            number_of_affected_objective_deadlines_nearing,
            number_of_affected_objectives,
        )
        # Impact
        fraction_passed_objective_deadlines = safe_div(
            number_of_affected_objective_deadlines_passed, number_of_affected_objectives
        )
        # Likelihood
        closest_deadline_binned = max(
            map(
                lambda o: remaining_time_binned(o.target_date)
                if o.current_status == ObjectiveStatus.Open
                else 0,
                affected_objectives,
            ),
            default=0,
        )
        # Likelihood
        fraction_incidents = safe_div(
            number_of_recent_incidents_in_affected_processes, number_of_incidents
        )

        return RiskModelAttrs(
            fraction_affected_processes=fraction_affected_processes,
            fraction_staff_affected=fraction_staff_affected,
            ratio_num_affected_inputs_to_all_inputs=ratio_num_affected_inputs_to_all_inputs,
            fraction_incomplete_tasks=fraction_incomplete_tasks,
            fraction_other_processes_past_deadline=fraction_other_processes_past_deadline,
            fraction_nearing_objective_deadlines=fraction_nearing_objective_deadlines,
            fraction_passed_objective_deadlines=fraction_passed_objective_deadlines,
            closest_deadline_binned=closest_deadline_binned,
            fraction_incidents=fraction_incidents,
        )


class RiskFeatureSelectionMixin:
    pass


class RiskTrainingMixin:
    pass


class ModelNames:
    NEURAL_NETWORK = "neural_network"
    SVM = "svm"
    DECISION_TREE = "decision_tree"
    RANDOM_FOREST = "random_forest"
    LOGISTIC_REGRESSION = "logistic_regression"


class RiskClassificationMixin(RiskModelQueries):
    models = dict(
        [
            (
                ModelNames.LOGISTIC_REGRESSION,
                LogisticRegression(multi_class="multinomial"),
            ),
            (ModelNames.SVM, SVC(decision_function_shape="ovo")),
            (ModelNames.DECISION_TREE, DecisionTreeClassifier()),
            (
                ModelNames.RANDOM_FOREST,
                RandomForestClassifier(
                    n_estimators=1000, max_depth=10  # , random_state=5
                ),
            ),
            (
                ModelNames.NEURAL_NETWORK,
                MLPClassifier(
                    solver="adam",
                    alpha=1e-5,
                    hidden_layer_sizes=(800, 200),  # , random_state=6
                ),
            ),
        ]
    )

    category_models = dict(
        [
            (
                "logistic_regression",
                LogisticRegression(
                    multi_class="multinomial",
                    C=10.0,
                    solver="saga",
                ),
            ),
            (
                "decision_tree",
                DecisionTreeClassifier(
                    criterion="entropy",
                    max_features=17,
                ),
            ),
            (
                "random_forest",
                RandomForestClassifier(
                    criterion="entropy",
                    max_features=2,
                    n_estimators=1000,
                ),
            ),
            (
                "svm",
                SVC(
                    decision_function_shape="ovo",
                    C=1,
                    kernel="sigmoid",
                ),
            ),
            (
                "neural_network",
                MLPClassifier(
                    solver="adam", hidden_layer_sizes=(1000, 20), max_iter=2000
                ),
            ),
        ]
    )

    impact_models = dict(
        [
            (
                "logistic_regression",
                LogisticRegression(
                    multi_class="multinomial",
                    C=100,
                    solver="saga",
                ),
            ),
            (
                "decision_tree",
                DecisionTreeClassifier(
                    criterion="gini",
                    max_features=9,
                ),
            ),
            (
                "random_forest",
                RandomForestClassifier(
                    criterion="gini",
                    max_features=6,
                    n_estimators=10,
                ),
            ),
            (
                "svm",
                SVC(
                    decision_function_shape="ovo",
                    C=1.0,
                    kernel="rbf",
                ),
            ),
            (
                "neural_network",
                MLPClassifier(
                    solver="lbfgs", hidden_layer_sizes=(31, 25), max_iter=2000
                ),
            ),
        ]
    )

    likelihood_models = dict(
        [
            (
                "logistic_regression",
                LogisticRegression(
                    multi_class="multinomial",
                    C=100,
                    solver="lbfgs",
                ),
            ),
            (
                "decision_tree",
                DecisionTreeClassifier(
                    criterion="entropy",
                    max_features=2,
                ),
            ),
            (
                "random_forest",
                RandomForestClassifier(
                    criterion="gini",
                    max_features=6,
                    n_estimators=10,
                ),
            ),
            (
                "svm",
                SVC(
                    decision_function_shape="ovo",
                    kernel="poly",
                    C=1,
                ),
            ),
            (
                "neural_network",
                MLPClassifier(
                    solver="adam", hidden_layer_sizes=(22, 10), max_iter=2000
                ),
            ),
        ]
    )

    def query_risks(self, auth_user: AuthUser) -> ModelDep:
        query = (
            select(RiskModel, RiskClassificationDataModel)
            .select_from(RiskModel)
            .join(
                RiskClassificationDataModel,
                RiskClassificationDataModel.risk_id == RiskModel.id,
            )
            .where(RiskModel.organization_id == auth_user.organization_id)
        )
        rows = list(self._session.execute(query))
        print(
            "NUM RISKS", len(rows), len(list(self._session.execute(select(RiskModel))))
        )
        key_map = {}
        id_map = {}
        for row in rows:
            item = row[0]
            key_map[item.name] = row
            id_map[item.id] = item
        return ModelDep(items=rows, id_map=id_map, key_map=key_map)

    def to_dataframe(
        self,
        risks: list[tuple[RiskModel, RiskClassificationDataModel]],
        *,
        is_new: bool = False,
    ):
        risks_as_dict_list = [
            {
                "Name": risk.name,
                "Description": risk.description,
                "Actions": risk.recommended_actions,
                "Category": risk.category,
                "Likelihood": risk.likelihood,
                "Impact": risk.impact,
                "Fraction Affected Processes": cls.fraction_affected_processes,
                "Fraction Staff Affected": cls.fraction_staff_affected,
                "Fraction Affected Inputs": cls.ratio_num_affected_inputs_to_all_inputs,
                "Fraction Incomplete Tasks": cls.fraction_incomplete_tasks,
                "Fraction Other Processes Past Deadline": cls.fraction_other_processes_past_deadline,
                "Fraction Nearing Objective Deadline": cls.fraction_nearing_objective_deadlines,
                "Fraction Past Objective Deadline": cls.fraction_passed_objective_deadlines,
                "Closest Deadline": cls.closest_deadline_binned,
                "Fraction Incidents": cls.fraction_incidents,
            }
            for risk, cls in risks
            if is_new or risk.is_finalized
        ]
        return pandas.DataFrame(risks_as_dict_list)

    def category_x_unnormalized(self, df):
        return (
            df["Name"].str.strip()
            + " "
            + df["Description"].str.strip()
            + " "
            + df["Actions"].str.strip()
        )

    def normalize_category_x(self, x, count_vectorizer=None):
        if not count_vectorizer:
            count_vectorizer = CountVectorizer(stop_words="english").fit(x)
        return (
            pandas.DataFrame(
                count_vectorizer.transform(x).todense(),
                columns=count_vectorizer.get_feature_names(),
            ),
            count_vectorizer,
        )

    def category_x(self, df, count_vectorizer=None):
        x = self.category_x_unnormalized(df)
        return self.normalize_category_x(x, count_vectorizer)

    def category_y(self, df):
        return df["Category"].str.strip().astype("category")

    def as_labels(self, df: pandas.DataFrame):
        return df.cat.categories.astype("str").values.tolist()

    def category_labels(self, df: pandas.DataFrame):
        return self.as_labels(self.category_y(df))

    def metric_x(self, df):
        return df[
            [
                "Fraction Affected Processes",
                "Fraction Staff Affected",
                "Fraction Affected Inputs",
                "Fraction Incomplete Tasks",
                "Fraction Other Processes Past Deadline",
                "Fraction Nearing Objective Deadline",
                "Fraction Past Objective Deadline",
                "Closest Deadline",
                "Fraction Incidents",
            ]
        ]

    def likelihood_y(self, df):
        return df["Likelihood"].astype("category")

    def impact_y(self, df):
        return df["Impact"].astype("category")

    def classify_category(
        self,
        df: pandas.DataFrame,
        new_risk_df: pandas.DataFrame,
    ):
        X, cv = self.category_x(df)
        y = self.category_y(df)
        model = self.category_models["neural_network"]
        model.fit(X, y)
        return model.predict(self.category_x(new_risk_df, cv)[0])

    def classify_likelihood(
        self,
        df: pandas.DataFrame,
        new_risk_df: pandas.DataFrame,
    ):
        X = self.metric_x(df)
        y = self.likelihood_y(df)
        model = self.likelihood_models["decision_tree"]
        model.fit(X, y)
        return model.predict(self.metric_x(new_risk_df))

    def classify_impact(
        self,
        df: pandas.DataFrame,
        new_risk_df: pandas.DataFrame,
    ):
        X = self.metric_x(df)
        y = self.impact_y(df)
        model = self.impact_models["random_forest"]
        model.fit(X, y)
        return model.predict(self.metric_x(new_risk_df))

    def category_metrics(self, df: pandas.DataFrame) -> list[ModelScore]:
        X = self.category_x_unnormalized(df)
        y = self.category_y(df)
        metrics = []
        (train_x, test_x, train_y, test_y) = train_test_split(X, y, test_size=0.3)
        train_x, cv = self.normalize_category_x(train_x)
        test_x, _ = self.normalize_category_x(test_x, cv)
        for name, model in self.category_models.items():
            model.fit(train_x, train_y)
            predictions = model.predict(test_x)
            precision, recall, fscore, _ = precision_recall_fscore_support(
                test_y, predictions
            )
            metrics.append(
                ModelScore(
                    model_name=name,
                    f1_score=fscore.tolist(),
                    recall=recall.tolist(),
                    precision=precision.tolist(),
                )
            )
        return metrics

    def likelihood_metrics(self, df):
        X = self.metric_x(df)
        y = self.likelihood_y(df)
        metrics = []
        (train_x, test_x, train_y, test_y) = train_test_split(X, y, test_size=0.3)
        for name, model in self.likelihood_models.items():
            model.fit(train_x, train_y)
            predictions = model.predict(test_x)
            precision, recall, fscore, _ = precision_recall_fscore_support(
                test_y, predictions
            )
            metrics.append(
                ModelScore(
                    model_name=name,
                    f1_score=fscore.tolist(),
                    recall=recall.tolist(),
                    precision=precision.tolist(),
                )
            )
        return metrics

    def impact_metrics(self, df):
        X = self.metric_x(df)
        y = self.impact_y(df)
        metrics = []
        (train_x, test_x, train_y, test_y) = train_test_split(X, y, test_size=0.3)
        for name, model in self.impact_models.items():
            model.fit(train_x, train_y)
            predictions = model.predict(test_x)
            precision, recall, fscore, _ = precision_recall_fscore_support(
                test_y, predictions
            )
            metrics.append(
                ModelScore(
                    model_name=name,
                    f1_score=fscore.tolist(),
                    recall=recall.tolist(),
                    precision=precision.tolist(),
                )
            )
        return metrics
