from datetime import datetime, date
from typing import Optional

from dataclasses import dataclass, field
from dataclasses_json import dataclass_json, config

from domain.shared.models import (
    RelatedItem,
    RiskStatus,
)
from domain.shared.mixins import PartialMixin


@dataclass_json
@dataclass(frozen=True)
class NewRisk:
    name: str
    description: str
    recommended_actions: str
    affected_processes: list[RelatedItem] = field(default_factory=list)
    risk_owner: Optional[RelatedItem] = None


@dataclass_json
@dataclass(frozen=True)
class RiskToValidate:
    name: str
    description: str
    recommended_actions: str
    likelihood: int
    impact: int
    affected_processes: list[RelatedItem] = field(default_factory=list)
    risk_owner: Optional[RelatedItem] = None


@dataclass_json
@dataclass(frozen=True)
class RiskUpdate:
    name: str
    description: str
    likelihood: int
    impact: int
    recommended_actions: str
    current_status: Optional[RiskStatus] = field(
        metadata=config(
            encoder=lambda e: e and e.name,
            decoder=lambda s: RiskStatus[s] if s in RiskStatus.__members__ else None,
        ),
        default=None,
    )
    affected_processes: list[RelatedItem] = field(default_factory=list)
    risk_owner: Optional[RelatedItem] = None


@dataclass_json
@dataclass(frozen=True)
class Risk:
    category: RelatedItem
    name: str
    description: str
    recommended_actions: str
    likelihood: int
    impact: int
    is_finalized: bool
    current_status: Optional[RiskStatus] = field(
        metadata=config(
            encoder=lambda e: e and e.name,
            decoder=lambda s: RiskStatus[s] if s in RiskStatus.__members__ else None,
        ),
        default=None,
    )
    affected_processes: list[RelatedItem] = field(default_factory=list)
    risk_owner: Optional[RelatedItem] = None
    organization_id: Optional[int] = None
    id: Optional[int] = None


@dataclass_json
@dataclass(frozen=True)
class RiskImport:
    category: str
    name: str
    description: str
    recommended_actions: str
    likelihood: int
    impact: int
    affected_processes: list[str] = field(default_factory=list)
    risk_owner: str = ""


@dataclass_json
@dataclass(frozen=True)
class RiskPartial(PartialMixin):
    name: Optional[str] = None
    person_responsible: Optional[RelatedItem] = None
    prepared_by: Optional[RelatedItem] = None
    is_positive: Optional[bool] = None


@dataclass(frozen=True)
class RiskQueryParams:
    capa_id: Optional[int] = None
    process_id: Optional[int] = None
    process_value_id: Optional[int] = None
    name: Optional[str] = None
    person_responsible: Optional[RelatedItem] = None
    prepared_by: Optional[RelatedItem] = None
    is_positive: Optional[bool] = None
