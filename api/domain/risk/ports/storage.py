#!/usr/bin/env python3

from typing import Protocol, List, Optional
from abc import abstractmethod

from domain.auth.models import AuthUser

from domain.risk.models import (
    Risk,
    RiskQueryParams,
    RiskUpdate,
    NewRisk,
    RiskImport,
)


class RiskDatabase(Protocol):
    @abstractmethod
    def query(self, auth_user: AuthUser, query_params: RiskQueryParams) -> List[Risk]:
        raise NotImplementedError

    @abstractmethod
    def get_by_id(self, auth_user: AuthUser, risk_id: int) -> Optional[Risk]:
        raise NotImplementedError

    @abstractmethod
    def exists(self, auth_user: AuthUser, query_params: RiskQueryParams) -> bool:
        raise NotImplementedError

    @abstractmethod
    def create(self, auth_user: AuthUser, risk: NewRisk) -> int:
        raise NotImplementedError

    @abstractmethod
    def create_many(self, auth_user: AuthUser, risks: List[NewRisk]) -> List[int]:
        raise NotImplementedError

    @abstractmethod
    def import_many(self, auth_user: AuthUser, risks: List[RiskImport]) -> List[int]:
        raise NotImplementedError

    @abstractmethod
    def update_by_id(
        self, auth_user: AuthUser, risk_id: int, risk_updates: RiskUpdate
    ) -> int:
        raise NotImplementedError

    @abstractmethod
    def delete_by_id(self, auth_user: AuthUser, risk_id: int) -> int:
        raise NotImplementedError

    @abstractmethod
    def get_classification_metrics(self, auth_user: AuthUser):
        raise NotImplementedError

    @abstractmethod
    def dashboard_risk_data(self, auth_user: AuthUser):
        raise NotImplementedError

    @abstractmethod
    def ml_models(self, auth_user: AuthUser):
        raise NotImplementedError

    @abstractmethod
    def suggest_capas(self, auth_user: AuthUser, risk: Risk):
        raise NotImplementedError
