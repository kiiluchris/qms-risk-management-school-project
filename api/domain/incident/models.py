from datetime import datetime, date
from typing import Optional

from dataclasses import dataclass, field
from dataclasses_json import dataclass_json, config

from domain.shared.models import (
    RelatedItem,
)
from domain.shared.mixins import PartialMixin


@dataclass_json
@dataclass(frozen=True)
class NewIncident:
    name: str
    location: str
    date_occurred: date = field(
        metadata=config(
            encoder=date.isoformat,
            decoder=date.fromisoformat,
        ),
    )
    lag_time: str
    damage: str
    category: RelatedItem
    reported_by: RelatedItem
    person_responsible: RelatedItem
    first_aid_provided_by: RelatedItem
    affected_processes: list[RelatedItem] = field(default_factory=list)
    first_aid: Optional[str] = None
    risk: Optional[RelatedItem] = None


@dataclass_json
@dataclass(frozen=True)
class IncidentImport(NewIncident):
    category: str
    reported_by: str
    person_responsible: str
    first_aid_provided_by: str
    affected_processes: str


IncidentUpdate = NewIncident


@dataclass_json
@dataclass(frozen=True)
class Incident(NewIncident):
    organization_id: Optional[int] = None
    id: Optional[int] = None


@dataclass_json
@dataclass(frozen=True)
class IncidentPartial(PartialMixin):
    name: Optional[str] = None
    location: Optional[str] = None
    date_occurred: Optional[date] = field(
        metadata=config(
            encoder=date.isoformat,
            decoder=date.fromisoformat,
        ),
        default=None,
    )
    lag_time: Optional[str] = None
    damage: Optional[str] = None
    category: Optional[RelatedItem] = None
    reported_by: Optional[RelatedItem] = None
    person_responsible: Optional[RelatedItem] = None
    first_aid_provided_by: Optional[RelatedItem] = None
    risk_id: Optional[RelatedItem] = None
    organization_id: Optional[RelatedItem] = None
    first_aid: Optional[str] = None


@dataclass(frozen=True)
class IncidentQueryParams:
    id: Optional[int] = None
    name: Optional[str] = None
    location: Optional[str] = None
    date_occurred: Optional[date] = field(
        metadata=config(
            encoder=date.isoformat,
            decoder=date.fromisoformat,
        ),
        default=None,
    )
    lag_time: Optional[str] = None
    damage: Optional[str] = None
    # category: Optional[str] = None
    category_id: Optional[int] = None
    reported_by: Optional[RelatedItem] = None
    person_responsible: Optional[RelatedItem] = None
    first_aid_provided_by: Optional[RelatedItem] = None
    risk_id: Optional[RelatedItem] = None
    organization_id: Optional[RelatedItem] = None
    first_aid: Optional[str] = None
