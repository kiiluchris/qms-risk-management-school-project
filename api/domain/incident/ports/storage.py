#!/usr/bin/env python3

from typing import Protocol, List, Optional
from abc import abstractmethod

from domain.auth.models import AuthUser

from domain.incident.models import (
    Incident,
    IncidentImport,
    IncidentQueryParams,
    IncidentUpdate,
    NewIncident,
)


class IncidentDatabase(Protocol):
    @abstractmethod
    def query(
        self, auth_user: AuthUser, query_params: IncidentQueryParams
    ) -> List[Incident]:
        raise NotImplementedError

    @abstractmethod
    def get_by_id(self, auth_user: AuthUser, incident_id: int) -> Optional[Incident]:
        raise NotImplementedError

    @abstractmethod
    def exists(self, auth_user: AuthUser, query_params: IncidentQueryParams) -> bool:
        raise NotImplementedError

    @abstractmethod
    def create(self, auth_user: AuthUser, incident: NewIncident) -> int:
        raise NotImplementedError

    @abstractmethod
    def create_many(
        self, auth_user: AuthUser, incidents: List[NewIncident]
    ) -> List[int]:
        raise NotImplementedError

    @abstractmethod
    def import_many(
        self, auth_user: AuthUser, incidents: List[IncidentImport]
    ) -> List[int]:
        raise NotImplementedError

    @abstractmethod
    def update_by_id(
        self, auth_user: AuthUser, incident_id: int, incident_updates: IncidentUpdate
    ) -> int:
        raise NotImplementedError

    @abstractmethod
    def delete_by_id(self, auth_user: AuthUser, incident_id: int) -> int:
        raise NotImplementedError

    @abstractmethod
    def get_affected_team_member_contacts(self, auth_user: AuthUser, incident_id: int):
        raise NotImplementedError
