#!/usr/bin/env python3

from domain.sms.ports import SmsClient
from domain.email.ports import EmailClient
from typing import List

from domain.auth.models import AuthUser
from domain.incident.models import (
    Incident,
    IncidentQueryParams,
    NewIncident,
    IncidentUpdate,
    IncidentImport,
)
from domain.incident.ports.storage import IncidentDatabase


class IncidentFacade:
    def __init__(self, db: IncidentDatabase, email: EmailClient, sms: SmsClient):
        self._db = db
        self._email = email
        self._sms = sms

    def get_incidents(self, auth_user: AuthUser, query_params: IncidentQueryParams):
        return self._db.query(auth_user, query_params)

    def get_incident(self, auth_user: AuthUser, incident_id: int):
        return self._db.get_by_id(auth_user, incident_id)

    def create_incident(self, auth_user: AuthUser, incident: NewIncident):
        id_ = self._db.create(auth_user, incident)
        if id_ and id_ != -1:
            reason = f"Incident Reported"
            message = "\n".join(
                [
                    f"A new incident has occured",
                    f" Name: {incident.name}",
                    f" Category: {incident.category.value}",
                    f" Location: {incident.location}",
                    f" Lag Time: {incident.lag_time or 'None'}",
                ]
            )
            contacts = self._db.get_affected_team_member_contacts(auth_user, id_)
            to_email = []
            to_sms = []
            for contact in contacts:
                to_email.append(contact["email"])
                if contact["use_sms"]:
                    to_sms.append(contact["phone"])
            self._email.send_notifications(to_email, reason, message)
            if to_sms:
                self._sms.send(reason + "\n\n" + message)

        return id_
        if id_ and id_ != -1:
            return
        return id_

    def update_incident(
        self, auth_user: AuthUser, incident_id: int, incident: IncidentUpdate
    ):
        return self._db.update_by_id(auth_user, incident_id, incident)

    def delete_incident(self, auth_user: AuthUser, incident_id: int):
        return self._db.delete_by_id(auth_user, incident_id)

    def import_incidents(self, auth_user: AuthUser, incidents: List[IncidentImport]):
        return self._db.import_many(auth_user, incidents)


IncidentFacadeT = IncidentFacade
