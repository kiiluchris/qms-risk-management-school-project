import itertools
from domain.risk.infrastructure.database import RiskDatabaseAdapter
from domain.process.ports.storage import ProcessDatabase
from domain.risk.ports.storage import RiskDatabase
from domain.process.infrastructure.database import ProcessDatabaseAdapter
from domain.user.infrastructure.database import UserDatabaseAdapter
from domain.user.models import user_full_name
from typing import List, Optional, NamedTuple, cast
from itertools import chain

from sqlalchemy import insert, select, update, delete, bindparam, func, alias
from sqlalchemy.orm.util import aliased
from sqlalchemy.sql import Select
from sqlalchemy.sql.expression import and_
from sqlalchemy.orm import scoped_session

from infrastructure.models import (
    IncidentModel,
    RiskCategoryModel,
    UserModel,
    ProcessModel,
    RiskModel,
    OrganizationModel,
    process_incident_assoc,
)
from domain.auth.models import AuthUser

from domain.user.ports.storage import UserDatabase
from domain.incident.ports.storage import IncidentDatabase
from domain.incident.models import (
    Incident,
    IncidentImport,
    IncidentQueryParams,
    IncidentUpdate,
    NewIncident,
)
from dataclasses import asdict
from domain.shared.models import RelatedItem


class IncidentImportKeys(NamedTuple):
    users: set[str]
    incidents: set[str]
    affected_processes: set[str]
    risk_categories: set[str]


class IncidentDatabaseAdapter(IncidentDatabase):
    def __init__(
        self,
        session: scoped_session,
        user_db: UserDatabase,
        process_db: ProcessDatabase,
        risk_db: RiskDatabase,
    ):
        self._user_db = cast(UserDatabaseAdapter, user_db)
        self._process_db = cast(ProcessDatabaseAdapter, process_db)
        self._risk_db = cast(RiskDatabaseAdapter, risk_db)
        self._session = session

    def exists(self, auth_user: AuthUser, query_params: IncidentQueryParams) -> bool:
        query = (
            select(func.count())
            .select_from(IncidentModel)
            .where(
                and_(
                    IncidentModel.organization_id == auth_user.organization_id,
                    IncidentModel.name == query_params.name,
                )
            )
            .limit(1)
        )
        count = self._session.scalar(query)

        return count > 0

    def record_to_domain_model(
        self,
        incident: IncidentModel,
        category: RiskCategoryModel,
        reported_by: UserModel,
        person_responsible: UserModel,
        first_aid_provided_by: Optional[UserModel],
        risk: Optional[RiskModel],
        organization: OrganizationModel,
    ) -> Incident:
        return Incident(
            id=incident.id,
            name=incident.name,
            location=incident.location,
            date_occurred=incident.date_occurred,
            lag_time=incident.lag_time,
            damage=incident.damage,
            # category=incident.category,
            category=RelatedItem(
                id=category.id,
                value=category.name,
                readable=category.name,
            ),
            reported_by=RelatedItem(
                id=reported_by.id,
                value=reported_by.email,
                readable=reported_by.first_name + " " + reported_by.last_name,
            ),
            person_responsible=RelatedItem(
                id=person_responsible.id,
                value=person_responsible.email,
                readable=person_responsible.first_name
                + " "
                + person_responsible.last_name,
            ),
            first_aid_provided_by=first_aid_provided_by
            and RelatedItem(
                id=first_aid_provided_by.id,
                value=first_aid_provided_by.email,
                readable=first_aid_provided_by.first_name
                + " "
                + first_aid_provided_by.last_name,
            ),
            risk=risk
            and RelatedItem(
                id=risk.id,
                value=risk.name,
                readable=risk.name,
            ),
            organization_id=organization.id,
            first_aid=incident.first_aid,
            affected_processes=[
                RelatedItem(id=p.id, value=p.name, readable=p.name)
                for p in incident.affected_processes
            ],
        )

    def select_query(self, auth_user: AuthUser) -> Select:
        person_responsible = aliased(UserModel)
        first_aid_provided_by = aliased(UserModel)
        return (
            select(
                IncidentModel,
                RiskCategoryModel,
                UserModel,
                person_responsible,
                first_aid_provided_by,
                RiskModel,
                OrganizationModel,
            )
            .select_from(IncidentModel)
            .join(
                RiskCategoryModel,
                IncidentModel.category_id == RiskCategoryModel.id,
            )
            .join(UserModel, IncidentModel.reported_by == UserModel.id)
            .join(
                person_responsible,
                IncidentModel.person_responsible == person_responsible.id,
            )
            .join(
                first_aid_provided_by,
                IncidentModel.first_aid_provided_by == first_aid_provided_by.id,
                isouter=True,
            )
            .join(RiskModel, IncidentModel.risk_id == RiskModel.id, isouter=True)
            .join(
                OrganizationModel, IncidentModel.organization_id == OrganizationModel.id
            )
            .where(IncidentModel.organization_id == auth_user.organization_id)
        )

    def query(self, auth_user: AuthUser, query_params: IncidentQueryParams):
        query = self.select_query(auth_user)
        incidents = self._session.execute(query)

        return [self.record_to_domain_model(*row) for row in incidents]

    def get_by_id(self, auth_user: AuthUser, incident_id: int):
        query = self.select_query(auth_user).where(IncidentModel.id == incident_id)
        result = self._session.execute(query).first()
        if result is None:
            return None

        return self.record_to_domain_model(*result)

    def get_affected_team_member_contacts(self, auth_user: AuthUser, incident_id: int):
        processes = list(
            self._session.execute(
                select(ProcessModel)
                .join(
                    process_incident_assoc,
                    process_incident_assoc.columns.process_id == ProcessModel.id,
                )
                .join(
                    IncidentModel,
                    IncidentModel.id == process_incident_assoc.columns.incident_id,
                )
                .where(
                    IncidentModel.id == incident_id,
                    IncidentModel.organization_id == auth_user.organization_id,
                )
            )
        )
        if not processes:
            return []
        emails = set()
        contacts = []
        for process, *_ in processes:
            team = process.team_responsible_obj
            leader = self._session.query(UserModel).get(team.leader)
            for member in itertools.chain([leader], team.members):
                if member.email not in emails:
                    contacts.append(
                        {
                            "email": member.email,
                            "phone": member.phone,
                            "use_sms": member.use_sms,
                        }
                    )
                    emails.add(member.email)
        return contacts

    def create(self, auth_user: AuthUser, incident: NewIncident):
        affected_processes_result = self._session.execute(
            select(ProcessModel).where(
                and_(
                    ProcessModel.organization_id == auth_user.organization_id,
                    ProcessModel.id.in_([p.id for p in incident.affected_processes]),
                )
            )
        )
        new_incident = IncidentModel(
            name=incident.name,
            location=incident.location,
            date_occurred=incident.date_occurred,
            lag_time=incident.lag_time,
            damage=incident.damage,
            category_id=incident.category.id,
            reported_by=incident.reported_by.id,
            person_responsible=incident.person_responsible.id,
            first_aid_provided_by=incident.first_aid_provided_by.id,
            risk_id=incident.risk and incident.risk.id,
            organization_id=auth_user.organization_id,
            first_aid=incident.first_aid,
            affected_processes=[row[0] for row in affected_processes_result],
        )
        self._session.add(new_incident)
        self._session.commit()

        return new_incident.id

    def incident_to_dict(self, auth_user: AuthUser, incident: NewIncident):
        result = {
            **asdict(incident),
            "reported_by": incident.reported_by.id,
            "person_responsible": incident.person_responsible.id,
            "first_aid_provided_by": incident.first_aid_provided_by.id,
            "risk_id": incident.risk and incident.risk.id,
            "organization_id": auth_user.organization_id,
            "category_id": incident.category.id,
        }
        del result["category"]
        del result["risk"]
        del result["affected_processes"]

        return result

    def create_many(self, auth_user: AuthUser, incidents: List[NewIncident]):
        query = insert(IncidentModel)  # .returning(IncidentModel.id)
        ids = self._session.execute(
            query,
            [self.incident_to_dict(auth_user, incident) for incident in incidents],
        )
        self._session.commit()

        return ids

    def _extract_user_keys(self, i: IncidentImport):
        return (
            person
            for person in (
                i.person_responsible,
                i.first_aid_provided_by,
                i.reported_by,
            )
            if person != ""
        )

    def _extract_affected_processes(self, i: IncidentImport):
        return i.affected_processes.split(",")

    def _extract_record_keys(
        self, auth_user: AuthUser, incidents: List[IncidentImport]
    ):
        imported_users = []
        imported_affected_processes = []
        imported_risk_categories = set()
        existing_incidents = set()
        for i in incidents:
            imported_users = chain(imported_users, self._extract_user_keys(i))
            imported_affected_processes = chain(
                imported_affected_processes, self._extract_affected_processes(i)
            )
            imported_risk_categories.add(i.category)
            existing_incidents.add(i.name)

        return IncidentImportKeys(
            users=set(imported_users),
            affected_processes=set(imported_affected_processes),
            risk_categories=imported_risk_categories,
            incidents=existing_incidents,
        )

    def fetch_existing_incidents(self, auth_user: AuthUser, incident_names: set[str]):
        risk_category_results = self._session.execute(
            select(IncidentModel).where(
                and_(
                    IncidentModel.organization_id == auth_user.organization_id,
                    IncidentModel.name.in_(incident_names),
                )
            )
        )
        return {i.name: i for i, *_ in risk_category_results}

    def fetch_existing_risk_categories(
        self, auth_user: AuthUser, imported_risk_categories: set[str]
    ):
        return self._risk_db.fetch_existing_risk_categories(
            auth_user, imported_risk_categories
        )

    def create_new_categories(
        self,
        auth_user: AuthUser,
        imported_risk_categories: set[str],
        risk_categories: dict[str, RiskCategoryModel],
    ):
        for category_name in imported_risk_categories:
            risk_category = risk_categories.get(category_name)
            if not risk_category:
                c = RiskCategoryModel(
                    name=category_name,
                    organization_id=auth_user.organization_id,
                )
                self._session.add(c)
                risk_categories[category_name] = c
        self._session.flush()

    def import_many(self, auth_user: AuthUser, incidents: List[IncidentImport]):
        import_keys = self._extract_record_keys(auth_user, incidents)
        users = self._user_db.fetch_existing_users(auth_user, import_keys.users)
        processes = self._process_db.fetch_existing_processes(
            auth_user, import_keys.affected_processes
        )
        risk_categories = self._risk_db.fetch_existing_risk_categories(
            auth_user, import_keys.risk_categories
        )
        existing_incidents = self.fetch_existing_incidents(
            auth_user, import_keys.incidents
        )
        inserted_values = []
        for incident in incidents:
            if existing_incidents.get(incident.name):
                continue
            values = asdict(incident)
            category = risk_categories[values.pop("category")]
            person_responsible = users[values.pop("person_responsible")]
            reported_by = users[values.pop("reported_by")]
            first_aid_provided_by = users.get(values.pop("first_aid_provided_by"))
            affected_processes = [
                p
                for pname in import_keys.affected_processes
                if (p := processes.get(pname))
            ]
            inserted_values.append(
                {
                    **values,
                    "organization_id": auth_user.organization_id,
                    "affected_processes": affected_processes,
                    "category_id": category.id,
                    "person_responsible": person_responsible.id,
                    "reported_by": reported_by.id,
                    "first_aid_provided_by": first_aid_provided_by
                    and first_aid_provided_by.id,
                }
            )

        query = insert(IncidentModel)  # .returning(IncidentModel.id)
        ids = self._session.execute(query, inserted_values)
        self._session.commit()

        return ids.rowcount

    # Edit before usage
    def update_by_id(
        self, auth_user: AuthUser, incident_id: int, incident_updates: IncidentUpdate
    ):
        query = (
            update(IncidentModel)
            .where(
                and_(
                    IncidentModel.id == incident_id,
                    IncidentModel.organization_id == auth_user.organization_id,
                )
            )
            .values(self.incident_to_dict(auth_user, incident_updates))
            # .returning(IncidentModel.id)
        )
        update_cursor = self._session.execute(query)
        if update_cursor is None:
            return -1
        affected_processes_result = self._session.execute(
            select(ProcessModel).where(
                and_(
                    ProcessModel.organization_id == auth_user.organization_id,
                    ProcessModel.id.in_(
                        [p.id for p in incident_updates.affected_processes]
                    ),
                )
            )
        )
        current_incident = self._session.query(IncidentModel).get(incident_id)
        current_incident.affected_processes = [
            row[0] for row in affected_processes_result
        ]

        self._session.commit()
        return incident_id

    def delete_by_id(self, auth_user: AuthUser, incident_id: int):
        query = delete(IncidentModel).where(
            and_(
                IncidentModel.id == incident_id,
                IncidentModel.organization_id == auth_user.organization_id,
            )
        )  # .returning(IncidentModel.id)
        delete_cursor = self._session.execute(query)
        print("Deleted ID", delete_cursor)
        if delete_cursor is not None:
            self._session.commit()
            return incident_id

        return -1
