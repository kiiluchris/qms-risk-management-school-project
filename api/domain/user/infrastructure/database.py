#!/usr/bin/env python3
from operator import and_
from typing import List
from itertools import groupby
import operator
from sqlalchemy import select, insert
from sqlalchemy.orm import scoped_session

from infrastructure.models import UserModel, RoleModel, task_team_user_assoc
from domain.user.models import (
    ImportUser,
    User,
    UserAccount,
    UserQueryParams,
    user_full_name,
)
from domain.auth.models import AuthUser
from domain.user.ports.storage import UserDatabase
from domain.shared.models import RelatedItem


class UserDatabaseAdapter(UserDatabase):
    def __init__(self, session: scoped_session):
        self._session = session

    def get_users(self, auth_user: AuthUser, query_params: UserQueryParams):
        query = (
            select(UserModel, RoleModel.name)
            .join(RoleModel, UserModel.role_id == RoleModel.id)
            .where(UserModel.organization_id == auth_user.organization_id)
        )
        if query_params.team_id is not None:
            query = query.join(
                task_team_user_assoc,
                task_team_user_assoc.columns.user_id == UserModel.id,
            ).where(task_team_user_assoc.columns.task_team_id == query_params.team_id)

        if query_params.has_team is not None:
            if query_params.has_team:
                query = query.join(
                    task_team_user_assoc,
                    task_team_user_assoc.columns.user_id == UserModel.id,
                )
        if query_params.is_manager is not None:
            if query_params.is_manager:
                query = query.where(RoleModel.name == "Manager")
            else:
                query = query.where(RoleModel.name != "Manager")
        users = self._session.execute(query)

        return [
            User(
                first_name=user.first_name,
                last_name=user.last_name,
                email=user.email,
                phone=user.phone,
                role=RelatedItem(
                    id=user.role_id,
                    value=role,
                    readable=role,
                ),
                id=user.id,
                organization_domain=auth_user.organization,
                use_sms=user.use_sms,
            )
            for (user, role) in users
        ]

    def fetch_existing_users(self, auth_user: AuthUser, imported_users: set[str]):
        user_results = self._session.execute(
            select(UserModel).where(
                and_(
                    UserModel.organization_id == auth_user.organization_id,
                    (UserModel.first_name + " " + UserModel.last_name).in_(
                        imported_users
                    ),
                )
            )
        )
        return {user_full_name(u): u for u, *_ in user_results}

    def get_user(self, auth_user: AuthUser, user_id: int):
        result = (
            self._session.query(UserModel, RoleModel.name)
            .join(RoleModel)
            .filter(
                UserModel.organization_id == auth_user.organization_id,
                UserModel.id == user_id,
            )
        ).first()
        if not result:
            return None
        user, role = result

        return User(
            first_name=user.first_name,
            last_name=user.last_name,
            email=user.email,
            phone=user.phone,
            role=RelatedItem(
                id=user.role_id,
                value=role,
                readable=role,
            ),
            id=user.id,
            organization_domain=auth_user.organization,
            use_sms=user.use_sms,
        )

    def find_user(
        self,
        auth_user: AuthUser,
        email: str,
    ):
        user = (
            self._session.query(UserModel)
            .filter(
                UserModel.organization_id == auth_user.organization_id,
                UserModel.email == email,
            )
            .first()
        )
        return user and User(
            name=user.name,
            id=user.id,
            organization_domain=auth_user.organization,
            organization_id=auth_user.organization_id,
        )

    def create_user(self, auth_user: AuthUser, user: User):
        r = UserModel(
            first_name=user.first_name,
            last_name=user.last_name,
            email=user.email,
            phone=user.phone,
            password="",
            organization_id=auth_user.organization_id,
            role_id=user.role.id,
        )
        self._session.add(r)
        self._session.commit()

        return r.id

    def create_users(self, auth_user: AuthUser, users: List[ImportUser]):
        roles = self._session.execute(
            select(RoleModel).where(
                auth_user.organization_id == RoleModel.organization_id
            )
        )
        grouped_users = {
            role: list(group)
            for role, group in groupby(
                sorted(users, key=operator.attrgetter("role")),
                key=operator.attrgetter("role"),
            )
        }
        insert_values = []
        emails = set()
        for role, *_ in roles:
            group = grouped_users.get(role.name)
            if not group:
                continue
            for user in group:
                emails.add(user.email)
                insert_values.append(
                    {
                        "first_name": user.first_name,
                        "last_name": user.last_name,
                        "role_id": role.id,
                        "email": user.email,
                        "phone": user.phone,
                        "organization_id": auth_user.organization_id,
                    }
                )
        self._session.execute(insert(UserModel), insert_values)
        self._session.commit()

        return emails

    def update_user(self, auth_user: AuthUser, user_id: int, user: User):
        r = self._session.query(UserModel).filter(UserModel.id == user_id).first()
        other_users = self._session.query(UserModel).filter(
            UserModel.email == user.email
        )
        l = other_users.count()
        if r and (l == 0 or (l == 1 and r.id == other_users[0].id)):
            r.first_name = user.first_name
            r.last_name = user.last_name
            r.email = user.email
            r.phone = user.phone
            r.role_id = user.role.id
            self._session.commit()
            return r.id
        else:
            return -1

    def update_account(self, auth_user: AuthUser, user: UserAccount):
        user_id = auth_user.user_id
        r = self._session.query(UserModel).filter(UserModel.id == user_id).first()
        if r:
            r.first_name = user.first_name
            r.last_name = user.last_name
            r.use_sms = user.use_sms
            r.phone = user.phone
            self._session.commit()
            return r.id
        else:
            return -1

    def delete_user(self, auth_user: AuthUser, user_id: int):
        r = self._session.query(UserModel).filter(UserModel.id == user_id).first()
        if r is not None:
            id = r.id
            self._session.delete(r)
            self._session.commit()
            return id

        return -1
