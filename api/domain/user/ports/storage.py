#!/usr/bin/env python3

from typing import Protocol, List, Optional
from abc import abstractmethod
from domain.user.models import (
    ImportUser,
    RequestUser,
    User,
    UserAccount,
    UserQueryParams,
)
from domain.auth.models import AuthUser


class UserDatabase(Protocol):
    @abstractmethod
    def get_users(
        self, auth_user: AuthUser, query_params: UserQueryParams
    ) -> List[User]:
        raise NotImplementedError

    @abstractmethod
    def get_user(self, auth_user: AuthUser, user_id: int) -> Optional[User]:
        raise NotImplementedError

    @abstractmethod
    def create_user(self, auth_user: AuthUser, user: RequestUser) -> int:
        raise NotImplementedError

    @abstractmethod
    def find_user(self, auth_user: AuthUser, user: str) -> Optional[User]:
        raise NotImplementedError

    @abstractmethod
    def update_user(self, auth_user: AuthUser, user_id: int, user: RequestUser) -> int:
        raise NotImplementedError

    @abstractmethod
    def update_account(self, auth_user: AuthUser, user: UserAccount) -> int:
        raise NotImplementedError

    @abstractmethod
    def delete_user(self, auth_user: AuthUser, user_id: int) -> int:
        raise NotImplementedError

    @abstractmethod
    def create_users(self, auth_user: AuthUser, users: List[ImportUser]):
        raise NotImplementedError
