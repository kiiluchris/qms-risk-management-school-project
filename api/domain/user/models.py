from typing import Optional, Protocol

from dataclasses import dataclass
from dataclasses_json import dataclass_json

from domain.shared.models import RelatedItem


@dataclass_json
@dataclass(frozen=True)
class User:
    id: int
    first_name: str
    last_name: str
    email: str
    phone: str
    organization_domain: str
    role: RelatedItem
    use_sms: bool = False
    password: Optional[str] = None
    organization_id: Optional[int] = None
    callback_url: Optional[str] = None


@dataclass_json
@dataclass(frozen=True)
class RequestUser:
    first_name: str
    last_name: str
    email: str
    phone: str
    role: RelatedItem
    callback_url: str


@dataclass_json
@dataclass(frozen=True)
class UserAccount:
    first_name: str
    last_name: str
    phone: str
    use_sms: bool = False


@dataclass_json
@dataclass(frozen=True)
class ImportUser(RequestUser):
    role: str
    callback_url: Optional[None] = None


@dataclass_json
@dataclass(frozen=True)
class UserQueryParams:
    team_id: Optional[int] = None
    is_manager: Optional[bool] = None
    has_team: Optional[bool] = None


class HasUserNames(Protocol):
    first_name: str
    last_name: str


def user_full_name(user: HasUserNames) -> str:
    return user.first_name + " " + user.last_name
