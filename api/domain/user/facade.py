#!/usr/bin/env python3
from typing import Union, List

from domain.email.ports import EmailClient
from domain.user.ports.storage import UserDatabase
from domain.auth.models import AuthUser
from domain.user.models import (
    ImportUser,
    User,
    RequestUser,
    UserAccount,
    UserQueryParams,
)


class UserFacade:
    def __init__(self, db: UserDatabase, mail: EmailClient):
        self._db = db
        self._mail = mail

    def get_users(self, auth_user: AuthUser, query_params: UserQueryParams):
        return self._db.get_users(auth_user, query_params)

    def get_user(self, auth_user: AuthUser, user_id: int):
        return self._db.get_user(auth_user, user_id)

    def create_user(self, auth_user: AuthUser, user: RequestUser):
        if self._db.find_user(auth_user, user.email):
            return -1
        self._mail.activate_account(
            user.callback_url, user.email, auth_user.organization
        )
        return self._db.create_user(auth_user, user)

    def update_user(self, auth_user: AuthUser, user_id: int, user: RequestUser):
        return self._db.update_user(auth_user, user_id, user)

    def update_account(self, auth_user: AuthUser, user: UserAccount):
        return self._db.update_account(auth_user, user)

    def delete_user(self, auth_user: AuthUser, user_id: int):
        return self._db.delete_user(auth_user, user_id)

    def import_users(
        self, auth_user: AuthUser, callback_url: str, users: List[ImportUser]
    ):
        emails = self._db.create_users(auth_user, users)
        for email in emails:
            self._mail.activate_account(callback_url, email, auth_user.organization)

        return []


UserFacadeT = UserFacade
