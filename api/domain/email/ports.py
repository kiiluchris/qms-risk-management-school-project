import os
from typing import Protocol
from abc import abstractmethod
from domain.auth.ports.utils import Jwt


class EmailClient(Protocol):
    jwt: Jwt

    @abstractmethod
    def send(self, subject: str, to: list[str], message: str):
        raise NotImplementedError

    def request_password_reset(
        self,
        callback_url: str,
        user_email: str,
        organization_domain: str,
    ):
        token = self.jwt.make_access_token(organization_domain, user_email, minutes=60)
        self.send(
            "Reset Password",
            [user_email],
            "\n".join(
                [
                    "Reset your account password",
                    "",
                    f"{callback_url}?t={token}&e={user_email}",
                ]
            ),
        )

        return token

    def activate_account(
        self,
        callback_url: str,
        user_email: str,
        organization_domain: str,
    ):
        token = self.jwt.make_access_token(organization_domain, user_email, minutes=60)
        self.send(
            "Activate Account",
            [user_email],
            "\n".join(
                [
                    "Activate Account",
                    "",
                    f"The email {user_email} has had an account registered for the organization {organization_domain}",
                    f"{callback_url}?t={token}",
                ]
            ),
        )
        return token

    def send_notifications(self, to: list[str], reason: str, message: str):
        return self.send(
            f"Notification: {reason}",
            to,
            "\n".join(["Notification", "", message]),
        )
