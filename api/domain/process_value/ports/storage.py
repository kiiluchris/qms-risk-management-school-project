#!/usr/bin/env python3

from typing import Protocol, List, Optional
from abc import abstractmethod

from domain.auth.models import AuthUser

from domain.process_value.models import (
    ProcessValue,
    ProcessValueQueryParams,
    ProcessValueUpdate,
    NewProcessValue,
)


class ProcessValueDatabase(Protocol):
    @abstractmethod
    def query(self, auth_user: AuthUser, query_params: ProcessValueQueryParams) -> List[ProcessValue]:
        raise NotImplementedError

    @abstractmethod
    def get_by_id(self, auth_user: AuthUser, process_value_id: int) -> Optional[ProcessValue]:
        raise NotImplementedError

    @abstractmethod
    def exists(self, auth_user: AuthUser, query_params: ProcessValueQueryParams) -> bool:
        raise NotImplementedError
    
    @abstractmethod
    def create(self, auth_user: AuthUser, process_value: NewProcessValue) -> int:
        raise NotImplementedError

    @abstractmethod
    def create_many(self, auth_user: AuthUser, process_values: List[NewProcessValue]) -> List[int]:
        raise NotImplementedError
    
    @abstractmethod
    def update_by_id(self, auth_user: AuthUser, process_value_id: int, process_value_updates: ProcessValueUpdate) -> int:
        raise NotImplementedError

    @abstractmethod
    def delete_by_id(self, auth_user: AuthUser, process_value_id: int) -> int:
        raise NotImplementedError