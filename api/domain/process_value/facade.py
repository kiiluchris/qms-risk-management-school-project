#!/usr/bin/env python3

from typing import List

from domain.auth.models import AuthUser
from domain.process_value.models import (
    ProcessValue,
    ProcessValueQueryParams,
    NewProcessValue,
    ProcessValueUpdate,
)
from domain.process_value.ports.storage import ProcessValueDatabase


class ProcessValueFacade:
    def __init__(self, db: ProcessValueDatabase):
        self._db = db

    def get_process_values(self, auth_user: AuthUser, query_params: ProcessValueQueryParams):
        return self._db.query(auth_user, query_params)

    def get_process_value(self, auth_user: AuthUser, process_value_id: int):
        return self._db.get_by_id(auth_user, process_value_id)

    def create_process_value(self, auth_user: AuthUser, process_value: NewProcessValue):
        if self._db.exists(auth_user, ProcessValueQueryParams(name = process_value.name)):
            return -1
        return self._db.create(auth_user, process_value)

    def update_process_value(self, auth_user: AuthUser, process_value_id: int, process_value: ProcessValueUpdate):
        return self._db.update_by_id(auth_user, process_value_id, process_value)

    def delete_process_value(self, auth_user: AuthUser, process_value_id: int):
        return self._db.delete_by_id(auth_user, process_value_id)

    def import_process_values(self, auth_user: AuthUser, process_values: List[NewProcessValue]):
        return self._db.create_many(auth_user, process_values)

        
        


ProcessValueFacadeT = ProcessValueFacade