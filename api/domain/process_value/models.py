from datetime import datetime, date
from typing import Optional

from dataclasses import dataclass, field
from dataclasses_json import dataclass_json, config

from domain.shared.models import (
    RelatedItem,
)
from domain.shared.mixins import PartialMixin


@dataclass_json
@dataclass(frozen=True)
class NewProcessValue:
    name: str
    target_value: int
    actual_value: int
    unit: Optional[RelatedItem] = None


@dataclass_json
@dataclass(frozen=True)
class ProcessValue(NewProcessValue):
    organization_id: Optional[int] = None
    id: Optional[int] = None


@dataclass_json
@dataclass(frozen=True)
class ProcessValueOfProcess(ProcessValue):
    process_id: Optional[int] = None


ProcessValueUpdate = ProcessValueOfProcess


@dataclass_json
@dataclass(frozen=True)
class ProcessValuePartial(PartialMixin):
    name: Optional[str] = None
    target_value: Optional[int] = None
    actual_value: Optional[int] = None
    unit: Optional[RelatedItem] = None
    organization_id: Optional[RelatedItem] = None


@dataclass(frozen=True)
class ProcessValueQueryParams:
    id: Optional[int] = None
    risk_id: Optional[int] = None
    input_process_id: Optional[int] = None
    output_process_id: Optional[int] = None
    name: Optional[str] = None
    target_value: Optional[int] = None
    actual_value: Optional[int] = None
    unit: Optional[RelatedItem] = None
    organization_id: Optional[RelatedItem] = None
