from typing import List, Optional

from sqlalchemy import insert, select, update, delete, bindparam, func, alias
from sqlalchemy.sql import Select
from sqlalchemy.sql.expression import and_
from sqlalchemy.orm import scoped_session

from infrastructure.models import (
    ProcessValueModel,
    UnitModel,
    OrganizationModel,
    risk_value_assoc,
    process_input_value_assoc,
    process_output_value_assoc,
)
from domain.auth.models import AuthUser

from domain.process_value.ports.storage import ProcessValueDatabase
from domain.process_value.models import (
    ProcessValue,
    ProcessValueQueryParams,
    ProcessValueUpdate,
    NewProcessValue,
)
from dataclasses import asdict
from domain.shared.models import RelatedItem


class ProcessValueDatabaseAdapter(ProcessValueDatabase):
    def __init__(self, session: scoped_session):
        self._session = session

    def exists(
        self, auth_user: AuthUser, query_params: ProcessValueQueryParams
    ) -> bool:
        query = (
            select(func.count())
            .select_from(ProcessValueModel)
            .where(
                and_(
                    ProcessValueModel.organization_id == auth_user.organization_id,
                    ProcessValueModel.name == query_params.name,
                )
            )
            .limit(1)
        )
        count = self._session.scalar(query)

        return count > 0

    def record_to_domain_model(
        self,
        process_value: ProcessValueModel,
        unit: Optional[UnitModel],
        organization: OrganizationModel,
    ) -> ProcessValue:
        return ProcessValue(
            id=process_value.id,
            name=process_value.name,
            target_value=process_value.target_value,
            actual_value=process_value.actual_value,
            unit=unit
            and RelatedItem(
                id=unit.id,
                value=unit.name,
                readable=unit.name,
            ),
            organization_id=organization.id,
        )

    def select_query(
        self,
        auth_user: AuthUser,
        query_params: Optional[ProcessValueQueryParams] = None,
    ) -> Select:
        query = (
            select(
                ProcessValueModel,
                UnitModel,
                OrganizationModel,
            )
            .select_from(ProcessValueModel)
            .join(UnitModel, ProcessValueModel.unit == UnitModel.id, isouter=True)
            .join(
                OrganizationModel,
                ProcessValueModel.organization_id == OrganizationModel.id,
            )
            .where(ProcessValueModel.organization_id == auth_user.organization_id)
        )
        if query_params:
            if query_params.risk_id is not None:
                query = query.join(
                    risk_value_assoc,
                    risk_value_assoc.columns.process_value_id == ProcessValueModel.id,
                ).where(risk_value_assoc.columns.risk_id == query_params.risk_id)

            if query_params.input_process_id is not None:
                query = query.join(
                    process_input_value_assoc,
                    process_input_value_assoc.columns.process_value_id
                    == ProcessValueModel.id,
                ).where(
                    process_input_value_assoc.columns.process_id
                    == query_params.input_process_id
                )

            if query_params.output_process_id is not None:
                query = query.join(
                    process_output_value_assoc,
                    process_output_value_assoc.columns.process_value_id
                    == ProcessValueModel.id,
                ).where(
                    process_output_value_assoc.columns.process_id
                    == query_params.output_process_id
                )

        print("========" * 5)
        print(str(query))
        print("========" * 5)
        return query

    def query(self, auth_user: AuthUser, query_params: ProcessValueQueryParams):
        query = self.select_query(auth_user, query_params)
        process_values = self._session.execute(query)

        return [self.record_to_domain_model(*row) for row in process_values]

    def get_by_id(self, auth_user: AuthUser, process_value_id: int):
        query = self.select_query(auth_user).where(
            ProcessValueModel.id == process_value_id
        )
        result = self._session.execute(query).first()
        if result is None:
            return None

        return self.record_to_domain_model(*result)

    def create_obj(
        self,
        auth_user: AuthUser,
        process_value: NewProcessValue,
        *,
        nocommit: bool = False
    ):
        new_process_value = ProcessValueModel(
            name=process_value.name,
            target_value=process_value.target_value,
            actual_value=process_value.actual_value,
            # unit=process_value.unit.id,
            organization_id=auth_user.organization_id,
        )
        self._session.add(new_process_value)
        if not nocommit:
            self._session.commit()

        return new_process_value

    def create_obj_rel_item(
        self, auth_user: AuthUser, process_value: RelatedItem, *, nocommit: bool = False
    ):
        new_process_value = ProcessValueModel(
            name=process_value.value,
            organization_id=auth_user.organization_id,
        )
        self._session.add(new_process_value)
        if not nocommit:
            self._session.commit()

        return new_process_value

    def create(
        self,
        auth_user: AuthUser,
        process_value: NewProcessValue,
        *,
        nocommit: bool = False
    ):
        return self.create_obj(auth_user, process_value, nocommit=nocommit).id

    def process_value_to_dict(
        self, auth_user: AuthUser, process_value: NewProcessValue
    ):
        result = {
            **asdict(process_value),
            # "unit": process_value.unit.id,
            "organization_id": auth_user.organization_id,
        }

        return result

    def create_many(self, auth_user: AuthUser, process_values: List[NewProcessValue]):
        query = insert(ProcessValueModel)  # .returning(ProcessValueModel.id)
        ids = self._session.execute(
            query,
            [
                self.process_value_to_dict(auth_user, process_value)
                for process_value in process_values
            ],
        )
        self._session.commit()

        return ids

    # Edit before usage
    def update_by_id_rel_item(
        self,
        auth_user: AuthUser,
        process_value_id: int,
        process_value_updates: RelatedItem,
    ):
        query = (
            update(ProcessValueModel)
            .where(
                and_(
                    ProcessValueModel.id == process_value_id,
                    ProcessValueModel.organization_id == auth_user.organization_id,
                )
            )
            .values({"name": process_value_updates.value})
            # .returning(ProcessValueModel.id)
        )
        update_cursor = self._session.execute(query)
        if update_cursor is not None:
            self._session.commit()
            return process_value_id

        return -1

    def update_by_id(
        self,
        auth_user: AuthUser,
        process_value_id: int,
        process_value_updates: ProcessValueUpdate,
    ):
        query = (
            update(ProcessValueModel)
            .where(
                and_(
                    ProcessValueModel.id == process_value_id,
                    ProcessValueModel.organization_id == auth_user.organization_id,
                )
            )
            .values(self.process_value_to_dict(auth_user, process_value_updates))
            # .returning(ProcessValueModel.id)
        )
        update_cursor = self._session.execute(query)
        if update_cursor is not None:
            self._session.commit()
            return process_value_id

        return -1

    def delete_by_id(self, auth_user: AuthUser, process_value_id: int):
        query = delete(ProcessValueModel).where(
            and_(
                ProcessValueModel.id == process_value_id,
                Model.organization_id == auth_user.organization_id,
            )
        )  # .returning(ProcessValueModel.id)
        delete_cursor = self._session.execute(query)
        print("Deleted ID", delete_cursor)
        if delete_cursor is not None:
            self._session.commit()
            return process_value_id

        return -1
