import PyInquirer as pyinquirer
import sys, itertools
from collections import namedtuple

NewTask = namedtuple("NewTask", ("name", "members"))

new_task_teams = [1, 2, 4]

task_teams = [
    NewTask("Task 1", [1, 2, 3]),
    NewTask("Task 2", [4, 5, 6]),
]
print(
    list(
        itertools.chain.from_iterable(
            [
                [
                    dict(task_team_id=new_task_team, user_id=m)
                    for m in task_team_input.members
                ]
                for task_team_input, new_task_team in zip(task_teams, new_task_teams)
            ]
        )
    )
)
sys.exit(0)

model_keys = ["User", "Organization", "Task", "Process"]
field_name = "organization_id"
to_default = lambda x: {"default": x}
is_model_key_in_field_name = lambda field: lambda model_key: model_key.lower() in field
default_model = next(
    map(to_default, filter(is_model_key_in_field_name(field_name), model_keys)), {}
)
print(default_model)
answers = pyinquirer.prompt(
    [
        {
            "type": "list",
            "name": "field_model",
            "message": f'What is the DB Model of the field "{field_name}"?',
            "choices": model_keys,
            "default": lambda _: print(_) or 1,
        },
    ]
)

print(answers)
