#!/usr/bin/env python3
#
import sys
import pandas
import seaborn as sns
import matplotlib
import matplotlib.pyplot as plt
from sklearn.feature_extraction.text import CountVectorizer
import random

matplotlib.use("TkAgg")
df = pandas.read_excel(
    "~/Development/School/Data/risk/risk_data.ods", sheet_name="Data"
)
text_columns = ["Name", "Description"]
random_columns = ["Impact", "Likelihood"]
# df = df.drop(["Likelihood", "Impact", "Actions", "Affected Processes"], 1)
print(df[random_columns].head())
df[random_columns] = df[random_columns].apply(lambda _: random.randrange(1, 100), 1)
print(df[random_columns].head())
count_vectorizer = CountVectorizer()
df["Name"] = count_vectorizer.fit_transform(df["Name"])
df["Description"] = count_vectorizer.fit_transform(df["Description"])
print(df[text_columns].head())
# vectorized_.fit_transform(df[text_columns])


attributes = df.drop("Category", 1)  # Feature Matrix
classes = df["Category"]
plt.figure()
cor = df.corr()
print(cor)
# sns.heatmap(cor, annot=True, cmap=plt.cm.Reds)
plt.show()

print(df)
