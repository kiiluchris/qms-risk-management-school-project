#!/usr/bin/env python3
import argparse
import sys

import inquirer

from add_module.run import run


def cli_argparse(parser):
    parser.add_argument("singular_name", help="name of the package being created")
    parser.add_argument("plural_name", help="name of the package being created")
    parser.add_argument(
        "class_prefix", help="class prefix of the classes related to the package"
    )

    args = parser.parse_args()
    return args


def tui_argparse(parser):
    parser.add_argument(
        "singular_name", help="name of the package being created", nargs="?"
    )
    args = parser.parse_args()
    questions = [
        inquirer.Text(
            "singular_name",
            message=f"What is the singular name of the model?",
            default=args.singular_name,
        ),
        inquirer.Text(
            "plural_name",
            message=f"What is the plural name of the model?",
            default="{singular_name}s",
        ),
        inquirer.Text(
            "class_prefix",
            message=f"What is the class prefix of the model?",
            default=lambda a: a["singular_name"][0].upper() + a["singular_name"][1:],
        ),
    ]
    try:
        answers = inquirer.prompt(questions, raise_keyboard_interrupt=True)
        return argparse.Namespace(**answers)
    except KeyboardInterrupt:
        sys.exit(1)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Script to bootstrap modules")
    parser.add_argument(
        "-c", "--cli", help="Select cli args or tui", action="store_true"
    )
    if parser.parse_known_args()[0].cli:
        args = cli_argparse(parser)
    else:
        args = tui_argparse(parser)
    run(args)
