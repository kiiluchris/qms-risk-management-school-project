#!/usr/bin/env python3

from dataclasses import dataclass
from typing import Dict, Optional, Callable


def _dataclass_field(encoder: str, decoder: str, default: Optional[str] = None) -> str:
    res = (
        "field(\n"
        "        metadata=config(\n"
        f"            encoder={encoder},\n"
        f"            decoder={decoder},\n"
        "        ),\n"
    )
    return res + (f"        default={default},\n" if default else "") + "    )"


@dataclass
class ModelField:
    name: str
    type_: str
    value: str = ""
    null_value: str = "None"
    is_enum: bool = False
    foreign_key: Optional[str] = None
    hidden: bool = False

    @classmethod
    def make_relation(cls, name: str, fk: str):
        return cls(name, "RelatedItem", foreign_key=fk)

    @classmethod
    def make_enum(cls, name: str, field_type: str):
        enum_name = field_type.replace("Enum(", "")
        return cls(
            name,
            enum_name,
            _dataclass_field(
                encoder="lambda e: e and e.name",
                decoder=f"lambda s: {enum_name}[s] if s in {enum_name}.__members__ else None",
            ),
            _dataclass_field(
                encoder="lambda e: e and e.name",
                decoder=f"lambda s: {enum_name}[s] if s in {enum_name}.__members__ else None",
                default="None",
            ),
            is_enum=True,
        )

    @classmethod
    def without_name(cls, type_: str, value: str = "", null_value: str = None):
        def go(name: str):
            return cls(name, type_, value, null_value)

        return go


@dataclass
class RelatedModelField:
    field: ModelField
    prefix: str
    unique_key: str
    hidden: bool
    repeated: bool


def _date_model() -> ModelField:
    return ModelField.without_name(
        "date",
        _dataclass_field(
            encoder="date.isoformat",
            decoder="date.fromisoformat",
        ),
        _dataclass_field(
            encoder="date.isoformat", decoder="date.fromisoformat", default="None"
        ),
    )


def _datetime_model() -> ModelField:
    return ModelField.without_name(
        "datetime",
        _dataclass_field(
            encoder="datetime.isoformat",
            decoder="datetime.fromisoformat",
        ),
        _dataclass_field(
            encoder="datetime.isoformat",
            decoder="datetime.fromisoformat",
            default="None",
        ),
    )


_model_type_convert_dict: Dict[str, Callable[[str], ModelField]] = {
    "String": ModelField.without_name("str"),
    "Integer": ModelField.without_name("int"),
    "Boolean": ModelField.without_name("bool"),
    "Date": _date_model(),
    "DateTime": _datetime_model(),
}


def get_custom_model(db_model_type: str, name: str) -> Optional[ModelField]:
    model_type_init = _model_type_convert_dict.get(db_model_type, None)
    if model_type_init is None:
        return None
    return model_type_init(name)


__all__ = ("get_custom_model", "ModelField", "RelatedModelField")
