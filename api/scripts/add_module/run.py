#!/usr/bin/env python3
import argparse
import sys
import os
from pathlib import Path
from dataclasses import astuple, dataclass, asdict
from typing import (
    Dict,
    Callable,
    Iterator,
    TypeVar,
    List,
    Optional,
    NoReturn,
    Any,
    Sequence,
    Tuple,
    cast,
)
import itertools as it
from collections import namedtuple
import re
from operator import itemgetter

import PyInquirer as pyinquirer
from jinja2 import Environment, FileSystemLoader

from .model_type import *

RawModelField = namedtuple("ModelField", ("name", "type_", "fk"))


@dataclass(frozen=True)
class RelatedFieldProps:
    field_model: str
    unique_key: str
    hidden: bool


@dataclass(frozen=True)
class MyPath:
    template_path: str
    destination_path: str
    callback: Optional[Callable[[str], Dict[str, List[Any]]]] = None


@dataclass(frozen=True)
class TemplateVars:
    singular_name: str
    plural_name: str
    class_prefix: str
    model_fields: List[ModelField]
    related_model_fields: List[RelatedModelField]
    related_props: Dict[str, RelatedFieldProps]
    unique_key: str


script_directory = Path(os.path.dirname(__file__))
template_directory = script_directory.joinpath("templates")
project_root = script_directory.parent.parent
application_directory = project_root.joinpath("application")
domain_directory = project_root.joinpath("domain")
model_file = project_root.joinpath("infrastructure/models.py")


jinja_env = Environment(loader=FileSystemLoader(template_directory.absolute()))

T = TypeVar("T")


def takewhile_checking_previous(
    predicate: Callable[[T, T], bool], xs: Iterator[T], empty_value: T
) -> Tuple[List[T], Iterator[T]]:
    result: List[T] = []
    try:
        previous = empty_value
        first = next(xs)
        if predicate(empty_value, first):
            previous = first
            result.append(first)
    except StopIteration:
        return result, iter([])
    sentinel = object()
    current = sentinel
    for current in xs:
        if not predicate(previous, current):
            break
        previous = current
        result.append(current)
    if current == sentinel:
        return result, xs
    else:
        return result, it.chain([cast(T, current)], xs)


def parse_model_fields(lines: str) -> List[RawModelField]:
    matches = re.findall(
        r"""\s+(?<!#)\s+(?P<name>[a-z_]+)\s*=\s*.*Column\((?P<type_>[^\s,)]+)(?P<others>(?:\s*,\s*(?:ForeignKey\(["']([^)]+)["']\)|[^,\n]+))*)\)""",
        lines,
    )
    return [RawModelField(name, type_, fk) for name, type_, others, fk in matches]


def parse_database_models(model_file_contents: str) -> Dict[str, List[RawModelField]]:
    models = {}
    iter_sentinel = object()
    model_name_re = re.compile(r"class\s+([a-zA-Z]+)Model")
    lines = model_file_contents.splitlines()
    last_line = ""

    while True:
        lines = (
            it.dropwhile(lambda line: "class" not in line or "Model" not in line, lines)
            if "class" not in last_line or "Model" not in last_line
            else lines
        )
        model_name = next(cast(Iterator[str], lines), iter_sentinel)
        if model_name is iter_sentinel:
            break
        model_name = model_name_re.search(cast(str, model_name)).group(1)
        lines = it.dropwhile(
            lambda line: line == "" or "__tablename__" in line or "class" in line,
            lines,
        )
        model_lines, lines = takewhile_checking_previous(
            lambda prev, line: not line.startswith("class")
            and (prev != "" or (last_line := line) != ""),
            lines,
            "",
        )
        models[model_name] = parse_model_fields("\n".join(model_lines))

    return models


def generate_domain_model(
    model_name: str, database_models: Dict[str, List[RawModelField]]
) -> List[ModelField]:
    parsed_model = database_models.get(model_name, [])
    fields = []
    for field in parsed_model:
        custom_model = get_custom_model(field.type_, field.name)
        if custom_model is not None and field.name != "id":
            if field.fk:
                fields.append(ModelField.make_relation(field.name, field.fk))
            else:
                fields.append(custom_model)
        elif field.type_.startswith("Enum("):
            fields.append(ModelField.make_enum(field.name, field.type_))
    return fields


def application_paths(module_name: str):
    return [
        MyPath(template_path="web.jinja2", destination_path=f"{module_name}/web.py"),
        MyPath(
            template_path="module.jinja2", destination_path=f"{module_name}/module.py"
        ),
    ]


def domain_paths(module_name: str):
    return [
        MyPath(
            template_path="models.jinja2",
            destination_path=f"{module_name}/models.py",
        ),
        MyPath(
            template_path="facade.jinja2", destination_path=f"{module_name}/facade.py"
        ),
        MyPath(
            template_path="database.jinja2",
            destination_path=f"{module_name}/infrastructure/database.py",
        ),
        MyPath(
            template_path="storage.jinja2",
            destination_path=f"{module_name}/ports/storage.py",
        ),
    ]


def generate_files(template_vars: TemplateVars, dir_path: Path, files: List[MyPath]):
    print(f"Generating files in {dir_path}")
    for file_ in files:
        destination_path = dir_path.joinpath(file_.destination_path)
        destination_path.parent.mkdir(parents=True, exist_ok=True)
        template = jinja_env.get_template(file_.template_path)
        print(f"{file_.template_path} -> {file_.destination_path}")
        callback_res = (
            file_.callback(template_vars.class_prefix) if file_.callback else {}
        )
        text = template.render(**asdict(template_vars), callback_res=callback_res)
        with destination_path.open("w") as f:
            f.write(text)


def make_ask_field_model(models: Dict[str, List[RawModelField]]):
    model_keys = list(models.keys())

    def go(field_name: str):
        try:
            return pyinquirer.prompt(
                [
                    {
                        "type": "list",
                        "name": "field_model",
                        "message": f'What is the DB Model of the field "{field_name}"?',
                        "choices": model_keys,
                    },
                    {
                        "type": "list",
                        "name": "unique_key",
                        "message": "What column is unique in this model?",
                        "choices": lambda answers: [
                            field.name for field in models[answers["field_model"]]
                        ],
                    },
                    {
                        "type": "confirm",
                        "name": "hidden",
                        "message": "Do you want to hide this field?",
                        "default": False,
                    },
                ],
                raise_keyboard_interrupt=True,
            )
        except KeyboardInterrupt:
            sys.exit(1)

    return go


def find_related_model_fields(
    database_models: Dict[str, List[RawModelField]],
    domain_model_fields: List[ModelField],
):
    ask_field_model = make_ask_field_model(database_models)
    fields = []
    cached_field_models: Dict[str, RelatedFieldProps] = {}

    for field in domain_model_fields:
        if field.type_ != "RelatedItem":
            continue
        fk = cast(str, field.foreign_key)
        print(fk)
        cached_field_model = cached_field_models.get(fk)
        if cached_field_model is None:
            if (answers := ask_field_model(field.name)) is None:
                print("Exiting program to to KeyboardInterrupt")
                sys.exit(1)
            field_model, unique_key, hidden = itemgetter(
                "field_model", "unique_key", "hidden"
            )(answers)
            cached_field_models[fk] = RelatedFieldProps(field_model, unique_key, hidden)
        else:
            field_model, unique_key, hidden = astuple(cached_field_model)
        model = database_models.get(field_model)
        if model is not None:
            if hidden:
                field.hidden = True
            fields.append(
                RelatedModelField(
                    field, field_model, unique_key, hidden, bool(cached_field_model)
                )
            )

    return fields, cached_field_models


def ask_for_field(model_fields: List[ModelField], message: str) -> str:
    name = "field_name"
    return pyinquirer.prompt(
        [
            {
                "type": "list",
                "name": name,
                "message": message,
                "choices": [f.name for f in model_fields],
            },
        ],
        raise_keyboard_interrupt=True,
    )[name]


def run(args: argparse.Namespace):
    with model_file.open() as f:
        model_file_contents = f.read()
    database_models = parse_database_models(model_file_contents)
    model_fields = generate_domain_model(args.class_prefix, database_models)
    unique_key = ask_for_field(model_fields, "What is the unique key of this model?")
    related_model_fields, related_props = find_related_model_fields(
        database_models, model_fields
    )
    template_vars = TemplateVars(
        plural_name=args.plural_name,
        singular_name=args.singular_name,
        class_prefix=args.class_prefix,
        model_fields=model_fields,
        related_model_fields=related_model_fields,
        related_props=related_props,
        unique_key=unique_key,
    )
    generate_files(
        template_vars,
        application_directory,
        application_paths(template_vars.singular_name),
    )
    generate_files(
        template_vars,
        domain_directory,
        domain_paths(template_vars.singular_name),
    )
