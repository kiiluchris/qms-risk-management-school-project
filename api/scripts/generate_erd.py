#!/usr/bin/env python3
import sys
from eralchemy import render_er

from infrastructure.models import Base
import os

script_dir = os.path.dirname(__file__)
report_dir = os.path.join(script_dir, "../../../report")
erd_file = os.path.join(report_dir, "media/erd.png")

render_er(Base, erd_file)
