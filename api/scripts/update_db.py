#!/usr/bin/env python3
import os
from dataclasses import asdict
from typing import Callable, Dict, Optional

# import PyInquirer as pyinquirer
from sqlalchemy import create_engine, select, update, alias
from sqlalchemy.orm import sessionmaker, aliased
from sqlalchemy.orm.scoping import scoped_session
from sqlalchemy.sql.elements import and_
from sqlalchemy.sql.expression import Select

from infrastructure.database import LocalSession
from infrastructure.models import *
from infrastructure.models import (
    process_risk_assoc,
    process_output_value_assoc,
    process_input_value_assoc,
)
from domain.auth.infrastructure.password import PasslibEncrypter
from domain.auth.infrastructure.jwt import JwtEncoder
from domain.objective.models import ObjectiveUpdate


def insert_risk_categories(session: scoped_session, *, nocommit: bool = False):
    risk_categories = [
        "Operational",
        "Security & Privacy",
        "Health & Safety",
        "Strategic",
        "Financial",
        "Regulatory & Compliance",
        "Reputational",
    ]
    for c in risk_categories:
        risk = RiskCategoryModel(
            name=c,
            organization_id=1,
        )
        session.add(risk)

    if not nocommit:
        session.commit()


def update_user_passwords(session: scoped_session):
    users = session.execute(
        select(UserModel).where(UserModel.first_name.in_(["Vicker", "Neliah"]))
    )
    password = PasslibEncrypter()
    for user, *_ in users:
        user.is_verified = True
        user.password = password.encrypt("password")
    session.commit()


def init_db(session: scoped_session):
    org = OrganizationModel(name="Organization", domain="organization")
    session.add(org)
    session.flush()
    role = RoleModel(name="Admin", organization_id=org.id)
    session.add(role)
    session.flush()
    password = PasslibEncrypter()
    user = UserModel(
        first_name="Chris",
        last_name="Ndeti",
        email="cndeti@gmail.com",
        password=password.encrypt("password"),
        phone="0702495084",
        role_id=role.id,
        organization_id=org.id,
        is_verified=True,
    )
    user2 = UserModel(
        first_name="Kiilu",
        last_name="Ndeti",
        email="kiiluchris@students.uonbi.ac.ke",
        password=password.encrypt("password"),
        phone="070000000",
        role_id=1,
        organization_id=1,
        is_verified=True,
    )
    employee_role = RoleModel(name="Employee", organization_id=1)
    manager_role = RoleModel(name="Manager", organization_id=1)
    session.add(employee_role)
    session.add(manager_role)
    session.add(user)
    session.add(user2)
    insert_risk_categories(session)
    session.commit()


def test_update_objective(session: scoped_session):
    obj_dict = {
        "name": "Objective 2",
        "person_responsible": {"id": 1, "value": "cndeti@gmail.com"},
        "target_date": "2021-04-12",
        "current_status": "Closed",
        "description": "Description",
        "kpi": "Kpi",
    }
    objective_updates = ObjectiveUpdate.from_dict(obj_dict)
    objective_updates = asdict(objective_updates)
    del objective_updates["person_responsible"]

    query = (
        update(ObjectiveModel)
        .where(and_(ObjectiveModel.id == 3, ObjectiveModel.organization_id == 1))
        .values(**objective_updates)
        # .returning(ObjectiveModel.id)
    )
    updated_id = session.execute(query)
    print("Update", updated_id, vars(updated_id))
    # if updated_id is not None:
    #     session.commit()
    #     return updated_id


def test_select_processes(session):
    query = (
        select(
            ProcessModel,
            OrganizationModel,
            ObjectiveModel,
            UserModel,
            # TaskTeamModel,
        )
        .select_from(ProcessModel)
        .join(OrganizationModel)
        .join(ObjectiveModel)
        .join(UserModel)
        # .join(TaskTeamModel)
    )
    print("First Process")
    for row in session.execute(query):
        print(row[0].tasks)
    print("Last Process")


def test_select(
    query_fn,
    name,
    *args,
):
    def go(session):
        print(f"First {name}")
        for row in session.execute(query_fn(*args)):
            print(row)
        print(f"Last {name}")

    return go


def incident_select_query(organization_id: Optional[int] = None) -> Select:
    person_responsible = aliased(UserModel)
    first_aid_provided_by = aliased(UserModel)
    query = (
        select(
            IncidentModel,
            RiskCategoryModel,
            UserModel,
            person_responsible,
            first_aid_provided_by,
            RiskModel,
            OrganizationModel,
        )
        .select_from(IncidentModel)
        .join(
            RiskCategoryModel,
            IncidentModel.category_id == RiskCategoryModel.id,
        )
        .join(UserModel, IncidentModel.reported_by == UserModel.id)
        .join(
            person_responsible,
            IncidentModel.person_responsible == person_responsible.id,
        )
        .join(
            first_aid_provided_by,
            IncidentModel.first_aid_provided_by == first_aid_provided_by.id,
        )
        .join(RiskModel, IncidentModel.risk_id == RiskModel.id, isouter=True)
        .join(OrganizationModel, IncidentModel.organization_id == OrganizationModel.id)
    )
    if organization_id is not None:
        query = query.where(IncidentModel.organization_id == organization_id)
    return query


def test_select_tasks(session):
    query = (
        select(
            TaskModel,
            OrganizationModel,
            ProcessModel,
            UserModel,
            RoleModel,
        )
        .select_from(TaskModel)
        .join_from(TaskModel, ProcessModel)
        .join_from(TaskModel, UserModel)
        .join_from(TaskModel, RoleModel)
        .join_from(TaskModel, OrganizationModel)
        .where(TaskModel.organization_id == 1)
    )
    print("First Task")
    for row in session.execute(query):
        print(row[0].id)
    print("Last Task")


def potential_risk_query(process_id: int, input_ids: list[int]):
    return (
        select(RiskModel.name)
        .select_from(process_output_value_assoc)
        .join(
            process_risk_assoc,
            process_risk_assoc.columns.process_id
            == process_output_value_assoc.columns.process_id,
        )
        .join(RiskModel, RiskModel.id == process_risk_assoc.columns.risk_id)
        .where(RiskModel.organization_id == 1)
        .where(process_output_value_assoc.columns.process_id != process_id)
        .where(process_output_value_assoc.columns.process_value_id.in_(input_ids))
        .distinct()
    )


def process_potential_risk_query(process_id: int, input_ids: list[int]):
    risk_query = (
        select(RiskModel.name)
        .select_from(process_output_value_assoc)
        .join(
            process_risk_assoc,
            process_risk_assoc.columns.process_id
            == process_output_value_assoc.columns.process_id,
        )
        .join(RiskModel, RiskModel.id == process_risk_assoc.columns.risk_id)
        .join(
            process_input_value_assoc,
            process_input_value_assoc.columns.process_id == ProcessModel.id,
        )
        .where(RiskModel.organization_id == 1)
        .where(process_output_value_assoc.columns.process_id != ProcessModel.id)
        .where(
            process_output_value_assoc.columns.process_value_id.in_(
                process_input_value_assoc.columns.process_value_id
            )
        )
        .distinct()
        .cte("risk_query")
    )
    return (
        select(ProcessModel.name, risk_query.columns.name)
        .select_from(ProcessModel)
        .join(risk_query, risk_query.columns.name)
        # .group_by(ProcessModel.name)
        .where(ProcessModel.id == process_id)
    )


def main():
    session = LocalSession()
    user_input = ""
    QUIT = "quit"
    choices: Dict[str, Callable[[scoped_session], None]] = {
        "Initialize Database": init_db,
        "Insert Risk Categories": insert_risk_categories,
        "Test Objective Update": test_update_objective,
        "Test Select Processes": test_select_processes,
        "Test Select Tasks": test_select_tasks,
        "Test Select Incidents": test_select(incident_select_query, "Incident"),
    }
    update_user_passwords(session)
    # test_select(process_potential_risk_query, "Potential Risks", 28, [31, 35])(session)
    return session.close()
    if input("Initialize Database? [y/N]").lower() == "y":
        init_db(session)
        session.close()
    return
    while True:
        user_input = pyinquirer.prompt(
            [
                {
                    "type": "list",
                    "name": "user_input",
                    "message": "What would you like to do?",
                    "choices": choices.keys(),
                },
            ],
        ).get("user_input")
        if not user_input or user_input == QUIT:
            break
        action = choices[user_input]
        action(session)

    session.close()


if __name__ == "__main__":
    main()
