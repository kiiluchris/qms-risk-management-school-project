#!/usr/bin/env python3
from datetime import date, timedelta
from domain.shared.models import ObjectiveStatus
import os
import csv
from dataclasses import asdict
from typing import Callable, Dict, Optional

import inquirer
import PyInquirer as pyinquirer
from sqlalchemy import create_engine, select, update, alias
from sqlalchemy.orm import sessionmaker, aliased
from sqlalchemy.orm.scoping import scoped_session
from sqlalchemy.sql.elements import and_
from sqlalchemy.sql.expression import Select

from infrastructure.database import LocalSession
from infrastructure.models import *
from infrastructure.models import process_incident_assoc
from domain.auth.infrastructure.password import PasslibEncrypter
from domain.auth.infrastructure.jwt import JwtEncoder
from domain.objective.models import ObjectiveUpdate


def create_csv(headers, rows):
    with open("test.csv", "w") as f:
        writer = csv.writer(f)
        writer.writerow(headers)
        for row in rows:
            writer.writerow(row)


def select_processes(session):
    query = (
        select(
            ProcessModel,
            OrganizationModel,
            ObjectiveModel,
            UserModel,
            # TaskTeamModel,
        )
        .select_from(ProcessModel)
        .join(OrganizationModel, OrganizationModel.id == ProcessModel.organization_id)
        .join(ObjectiveModel, ObjectiveModel.id == ProcessModel.objective_id)
        .join(UserModel, UserModel.id == ProcessModel.person_responsible)
        # .join(
        #     process_incident_assoc,
        #     process_incident_assoc.columns.process_id == ProcessModel.id,
        # )
        # .join(IncidentModel, process_incident_assoc.columns.incident_id == IncidentModel.id)
        # .join(TaskTeamModel)
    )
    return session.execute(query)


def select_incidents(session):
    query = (
        select(
            IncidentModel,
            ProcessModel,
            OrganizationModel,
            UserModel,
            # TaskTeamModel,
        )
        .select_from(IncidentModel)
        .join(
            process_incident_assoc,
            process_incident_assoc.columns.incident_id == IncidentModel.id,
        )
        .join(
            ProcessModel, process_incident_assoc.columns.process_id == ProcessModel.id
        )
        .join(OrganizationModel, OrganizationModel.id == IncidentModel.organization_id)
        .join(UserModel, UserModel.id == ProcessModel.person_responsible)
        # .join(TaskTeamModel)
    )
    return session.execute(query)


def select_risk(session):
    organization_id = 1
    query = select(RiskModel).where(OrganizationModel.id == organization_id)
    return session.execute(query)


def predict_category(session):
    # - The name of the risk (text classification)
    # - The description (text classification)
    # - Recommended actions to take
    # - Category of already defined risks (label)

    pass


def predict_likelihood(session):
    pass


def predict_impact(session):
    pass


def main():
    session = LocalSession()
    process_result = list(select_processes(session))
    incident_result = list(select_incidents(session))
    # Dataset row contains
    processes = [row[0] for row in process_result]
    headers = [
        "Len",
        "ObjComplete",
        # "RatioToEnd",
        "RatioComplete",
        "NumIncidents",
        "SharedIO",
        # "SameTeam",
        # "SamePerson",
        "HasIncident",
    ]

    def bin_len(td: timedelta):
        days = td.days
        if days < 30:
            return 1
        elif days < 90:
            return 2
        elif days < 180:
            return 3
        elif days < 360:
            return 4
        else:
            return 5

    def has_incident(
        num_incidents: int, ratio_complete: float, target_complete_date: date
    ) -> int:
        if num_incidents > 0 or (
            ratio_complete != 1.0 and target_complete_date < date.today()
        ):
            return 1
        else:
            return 0

    # bin len: 1 = Month, 2 = Quarter, 3 = Half Year, 4 = Quarter Year
    teams_responsible = []
    prediction_process = processes[-1]
    prediction_process_inputs = prediction_process.inputs
    rows = []
    for process, org, obj, person_responsible in process_result:
        len_ = bin_len(obj.target_date - obj.created_at.date())
        obj_complete = int(obj.current_status == ObjectiveStatus.Closed)
        outputs = [o.id for o in process.outputs]
        shared_io = int(any(inp.id in outputs for inp in prediction_process_inputs))
        same_person = any(
            [
                i.person_responsible == prediction_process.person_responsible
                for i in process.incidents
            ]
        )
        # same_team = any(
        #     [
        #         i.team_responsible == prediction_process.team_responsible
        #         for i in process.incidents
        #     ]
        # )
        tasks = process.tasks
        ratio_complete = len([t for t in tasks if t.is_complete]) / len(tasks)
        num_incidents = len(process.incidents)
        rows.append(
            [
                len_,
                obj_complete,
                ratio_complete,
                num_incidents,
                shared_io,
                has_incident(num_incidents, ratio_complete, obj.target_date),
            ]
        )
    create_csv(headers, rows)

    session.close()


if __name__ == "__main__":
    main()
