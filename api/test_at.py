from itertools import chain
from time import time
from datetime import datetime, date, timedelta
from collections import namedtuple
from operator import attrgetter

# Fast array operations
import numpy as np
import pandas
from dataclasses import dataclass

# ML
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.metrics import (
    confusion_matrix,
    classification_report,
    accuracy_score,
    precision_recall_fscore_support,
    SCORERS,
)
from sklearn.model_selection import GridSearchCV, RepeatedStratifiedKFold

# Graphs
import mpld3
import mplcursors
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns

# Database
from sqlalchemy import select
from sqlalchemy.orm import scoped_session

# From backend
from infrastructure.database import LocalSession
from infrastructure.models import *
from domain.risk.models import NewRisk
from domain.user.models import user_full_name
from domain.auth.models import AuthUser
from domain.shared.models import ObjectiveStatus, RelatedItem

# Classfication code
from domain.risk.infrastructure.modelling.generation import (
    RiskClassificationMixin,
    RiskModelBuilderMixin,
)

session = LocalSession()


@dataclass
class Modelling(RiskClassificationMixin, RiskModelBuilderMixin):
    _session: scoped_session


modelling = Modelling(session)

df = pandas.read_csv("./notebook-media/randomized-dataset.csv")

split = 0.9
# Count for each group
s = df.groupby("Category").Category.cumcount()
# Top percent for each group
s = s // (df.groupby("Category").Category.transform("count") * split).astype(int)
Train = df.loc[s == 0].copy()
Test = df.drop(Train.index)  #
category_train_x, cv = modelling.category_x(Train)
category_test_x, _ = modelling.category_x(Test, cv)
category_train_y = modelling.category_y(Train)
category_test_y = modelling.category_y(Test)
models = [
    ("logistic_regression", LogisticRegression(multi_class="multinomial")),
    ("svm", SVC(decision_function_shape="ovo")),
    ("decision_tree", DecisionTreeClassifier()),
    (
        "random_forest",
        RandomForestClassifier(n_estimators=1000, max_depth=10),  # , random_state=5
    ),
    (
        "neural_network",
        MLPClassifier(
            solver="adam", alpha=1e-5, hidden_layer_sizes=(800, 200)  # , random_state=6
        ),
    ),
]
Score = namedtuple("Score", ("model", "precision", "recall", "f1score"))


def score_models(train_x, train_y, test_x, test_y):
    scores = []
    for name, model in models:
        print("Model: ", name)
        start_time = time()
        model.fit(train_x, train_y)
        predictions = model.predict(test_x)
        end_time = time()
        score = accuracy_score(test_y, predictions)
        print(f"Time taken: {end_time - start_time:.4f} seconds")
        print(f"Score: {score}")
        #     print(f'Confusion Matrix')
        #     print(confusion_matrix(category_test_y, predictions))
        print(f"Classification Report")
        print(classification_report(test_y, predictions))
        precision, recall, fscore, _ = precision_recall_fscore_support(
            test_y, predictions
        )
        scores.append(Score(name, precision, recall, fscore))
    return scores


category_scores = score_models(
    category_train_x, category_train_y, category_test_x, category_test_y
)
categories = (
    df["Category"]
    .str.strip()
    .astype("category")
    .cat.categories.astype("str")
    .values.tolist()
)
