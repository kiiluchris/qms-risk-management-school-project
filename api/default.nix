{ pkgs ? import <nixpkgs> { } }:
with pkgs;
let
  myAppEnv = poetry2nix.mkPoetryEnv {
    projectDir = ./.;
    python = python39;
  };
in pkgs.mkShell {
  buildInputs = [ myAppEnv graphviz ];
  shellHook = ''
    # fixes libstdc++ issues and libgl.so issues
    LD_LIBRARY_PATH=${stdenv.cc.cc.lib}/lib/:/run/opengl-driver/lib/
  '';
}
