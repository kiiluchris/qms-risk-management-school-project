#!/usr/bin/env python3

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, declarative_base, scoped_session

SQLALCHEMY_TEST_DATABASE_URL = "sqlite:///./test.db"
SQLALCHEMY_DATABASE_URL = SQLALCHEMY_TEST_DATABASE_URL

engine = create_engine(
    SQLALCHEMY_DATABASE_URL,
    connect_args={"check_same_thread": False},
    echo=True,
    future=True,
)


LocalSession = sessionmaker(autocommit=False, autoflush=False, bind=engine, future=True)


Base = declarative_base()
Base.query = scoped_session(LocalSession).query_property()
