import os
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail

from domain.email.ports import EmailClient
from domain.auth.ports.utils import Jwt


class SendGridEmail(EmailClient):
    def __init__(self, jwt: Jwt):
        self.jwt = jwt

    def send(self, subject: str, to: list[str], message: str):
        message = Mail(
            from_email="kiiluchris@students.uonbi.ac.ke",
            to_emails="kiiluchris@students.uonbi.ac.ke",
            subject=subject,
            html_content=message.replace("\n", "<br/>"),
        )
        try:
            sg = SendGridAPIClient(os.environ.get("SENDGRID_API_KEY"))
            response = sg.send(message)
            print(response.status_code)
            print(response.body)
            print(response.headers)
        except Exception as e:
            print(e)
            print(e.message)
