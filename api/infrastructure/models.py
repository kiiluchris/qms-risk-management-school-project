import datetime
import enum

from sqlalchemy import (
    Column,
    Integer,
    String,
    ForeignKey,
    DateTime,
    Boolean,
    Table,
    Enum,
    Date,
    Float,
)
from sqlalchemy.orm import relationship, backref, deferred
from sqlalchemy.event import listens_for

from domain.auth.infrastructure.password import PasslibEncrypter
from .database import Base, LocalSession
from domain.shared.models import (
    CapaRating,
    CapaClassification,
    MonitoringSchedule,
    ObjectiveStatus,
    RolePermission,
    RiskStatus,
)


class TimestampMixin(object):
    created_at = Column(DateTime, default=datetime.datetime.now)
    last_modified = Column(
        DateTime, default=datetime.datetime.now, onupdate=datetime.datetime.now
    )


class OrganizationModel(TimestampMixin, Base):
    __tablename__ = "organization"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String)
    domain = Column(String, index=True)
    api_token = Column(String)
    api_token_secret = Column(String)
    risk_likelihood_weight = Column(Integer, default=3)
    risk_impact_weight = Column(Integer, default=3)
    ml_model_to_use = Column(String)

    # users = relationship("UserModel", backref="organization")
    # roles = relationship("RoleModel", backref="organization")
    # incidents = relationship("IncidentModel", backref="organization")
    # capas = relationship("CapaModel", backref="organization")
    # units = relationship("UnitModel", backref="organization")
    # processes = relationship("ProcessModel", backref="organization")
    # objectives = relationship("ObjectiveModel", backref="organization")
    # tasks = relationship("TaskModel", backref="organization")


class RoleModel(TimestampMixin, Base):
    __tablename__ = "role"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String)
    permissions = Column(Enum(RolePermission))
    organization_id = Column(Integer, ForeignKey("organization.id"))

    users = relationship("UserModel", back_populates="role")


task_team_user_assoc = Table(
    "task_team_user_association_table",
    Base.metadata,
    Column("user_id", Integer, ForeignKey("user.id")),
    Column("task_team_id", Integer, ForeignKey("task_team.id")),
)


class UserModel(TimestampMixin, Base):
    __tablename__ = "user"

    id = Column(Integer, primary_key=True, index=True)
    first_name = Column(String)
    last_name = Column(String)
    email = Column(String)
    phone_cc = Column(String, default="254")
    phone = Column(String)
    password = deferred(Column(String))
    is_verified = Column(Boolean, default=False)
    use_sms = Column(Boolean, default=False)

    organization_id = Column(Integer, ForeignKey("organization.id"))
    role_id = Column(Integer, ForeignKey("role.id"))

    role = relationship("RoleModel", uselist=False, back_populates="users")

    sessions = relationship("SessionModel", backref=backref("user", uselist=False))
    tasks = relationship("TaskModel", back_populates="user")
    teams = relationship(
        "TaskTeamModel",
        secondary=task_team_user_assoc,
        back_populates="members",
    )


class SessionModel(TimestampMixin, Base):
    __tablename__ = "session"

    id = Column(Integer, primary_key=True, index=True)
    organization_id = Column(Integer, ForeignKey("organization.id"))
    user_id = Column(Integer, ForeignKey("user.id"))
    access_token = Column(String)
    refresh_token = Column(String)
    created_at = Column(DateTime, default=datetime.datetime.now)


class UnitModel(TimestampMixin, Base):
    __tablename__ = "unit"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String)
    unit_type_id = Column(Integer, ForeignKey("unit_type.id"))
    organization_id = Column(Integer, ForeignKey("organization.id"))


class UnitTypeModel(TimestampMixin, Base):
    __tablename__ = "unit_type"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String)
    units = relationship("UnitModel", backref="unit_type")


risk_value_assoc = Table(
    "risk_value_assoc",
    Base.metadata,
    Column("risk_id", Integer, ForeignKey("risk.id")),
    Column("process_value_id", Integer, ForeignKey("process_value.id")),
)
risk_input_assoc = Table(
    "risk_input_assoc",
    Base.metadata,
    Column("risk_id", Integer, ForeignKey("risk.id")),
    Column("process_input_id", Integer, ForeignKey("process_input.id")),
)

risk_output_assoc = Table(
    "risk_output_assoc",
    Base.metadata,
    Column("risk_id", Integer, ForeignKey("risk.id")),
    Column("process_output_id", Integer, ForeignKey("process_output.id")),
)

process_risk_assoc = Table(
    "process_risk_assoc",
    Base.metadata,
    Column("risk_id", Integer, ForeignKey("risk.id")),
    Column("process_id", Integer, ForeignKey("process.id")),
)

capa_risk_assoc = Table(
    "capa_risks",
    Base.metadata,
    Column("risk_id", Integer, ForeignKey("risk.id")),
    Column("capa_id", Integer, ForeignKey("capa.id")),
)


class RiskModel(TimestampMixin, Base):
    __tablename__ = "risk"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String)
    description = Column(String)
    recommended_actions = Column(String)
    category = Column(String)
    category_id = Column(Integer, ForeignKey("risk_category.id"))
    likelihood = Column(Integer)
    impact = Column(Integer)
    risk_owner = Column(Integer, ForeignKey("user.id"))
    # monitoring_schedule = Column(Enum(MonitoringSchedule))
    organization_id = Column(Integer, ForeignKey("organization.id"))
    is_finalized = Column(Boolean, default=False)
    affected_processes = relationship(
        "ProcessModel",
        secondary=process_risk_assoc,
        back_populates="risks",
    )
    risk_owner_obj = relationship("UserModel")
    preventive_capas = relationship(
        "CapaModel",
        secondary=capa_risk_assoc,
        back_populates="risks_to_prevent",
    )
    classification_data = relationship(
        "RiskClassificationDataModel", uselist=False, back_populates="risk"
    )

    current_status = Column(Enum(RiskStatus), default=RiskStatus.Identified)


# class RiskDataModel(Base, TimestampMixin):
#     __tablename__ = 'risk_data'

#     id = Column(Integer, primary_key=True, index=True)
#     risk_id = Column(Integer, ForeignKey('risk.id'))
#     likelihood = Column(Integer)
#     impact = Column(Integer)
#     num_affected_processes = Column(Integer)
#     num_people_in_team = Column(Integer)
#     num_people_in_organization = Column(Integer)
#     num_shared_outputs = Column(Integer)
#     num_outputs = Column(Integer)
#     num_incomplete_tasks = Column(Integer)
#     num_tasks = Column(Integer)
#     num_processes_worked_on = Column(Integer)
#     num_processes_worked_on_with_incidents = Column(Integer)


class RiskClassificationDataModel(Base, TimestampMixin):
    __tablename__ = "risk_classification_data"

    id = Column(Integer, primary_key=True, index=True)
    risk_id = Column(Integer, ForeignKey("risk.id"))
    organization_id = Column(Integer, ForeignKey("organization.id"))

    fraction_affected_processes = Column(Float)
    fraction_staff_affected = Column(Float)
    ratio_num_affected_inputs_to_all_inputs = Column(Float)
    fraction_incomplete_tasks = Column(Float)
    fraction_other_processes_past_deadline = Column(Float)
    fraction_nearing_objective_deadlines = Column(Float)
    fraction_passed_objective_deadlines = Column(Float)
    closest_deadline_binned = Column(Float)
    fraction_incidents = Column(Float)

    risk = relationship("RiskModel", back_populates="classification_data")


capa_incidents_assoc = Table(
    "capa_incidents",
    Base.metadata,
    Column("incident_id", Integer, ForeignKey("incident.id")),
    Column("capa_id", Integer, ForeignKey("capa.id")),
)


class RiskCategoryModel(TimestampMixin, Base):
    __tablename__ = "risk_category"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String)
    organization_id = Column(Integer, ForeignKey("organization.id"))


class CapaModel(TimestampMixin, Base):
    __tablename__ = "capa"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String)
    description = Column(String)
    measures_taken = Column(String)
    proposed_actions = Column(String)
    date_created = Column(Date)
    reference_id = Column(String)
    target_effective_date = Column(Date)
    comments = Column(String)
    rating = Column(Enum(CapaRating))
    action_type = Column(Enum(CapaClassification))
    approved_by = Column(Integer, ForeignKey("user.id"))
    prepared_by = Column(Integer, ForeignKey("user.id"))
    incident_id = Column(Integer, ForeignKey("incident.id"))
    category = Column(String)
    category_id = Column(Integer, ForeignKey("risk_category.id"))
    organization_id = Column(Integer, ForeignKey("organization.id"))
    incident = relationship("IncidentModel", back_populates="capa", uselist=False)
    risks_to_prevent = relationship(
        "RiskModel",
        secondary=capa_risk_assoc,
        back_populates="preventive_capas",
    )


process_incident_assoc = Table(
    "process_incident_assoc",
    Base.metadata,
    Column("incident_id", Integer, ForeignKey("incident.id")),
    Column("process_id", Integer, ForeignKey("process.id")),
)


class IncidentModel(TimestampMixin, Base):
    __tablename__ = "incident"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String)
    location = Column(String)
    date_occurred = Column(Date)
    lag_time = Column(String)
    damage = Column(String)
    category = Column(String)
    category_id = Column(Integer, ForeignKey("risk_category.id"))
    reported_by = Column(Integer, ForeignKey("user.id"))
    person_responsible = Column(Integer, ForeignKey("user.id"))
    first_aid_provided_by = Column(Integer, ForeignKey("user.id"))
    risk_id = Column(Integer, ForeignKey("risk.id"))
    organization_id = Column(Integer, ForeignKey("organization.id"))
    first_aid = Column(String)
    capa = relationship(
        "CapaModel",
        back_populates="incident",
    )
    affected_processes = relationship(
        "ProcessModel",
        secondary=process_incident_assoc,
        back_populates="incidents",
    )


class TaskTeamModel(TimestampMixin, Base):
    __tablename__ = "task_team"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String)
    organization_id = Column(Integer, ForeignKey("organization.id"))
    leader = Column(Integer, ForeignKey("user.id"))
    members = relationship(
        "UserModel",
        secondary=task_team_user_assoc,
        back_populates="teams",
    )


objective_proc_out_assoc = Table(
    "objective_proc_out_assoc",
    Base.metadata,
    Column("objective_id", Integer, ForeignKey("objective.id")),
    Column("process_output_id", Integer, ForeignKey("process_output.id")),
)


class ObjectiveModel(TimestampMixin, Base):
    __tablename__ = "objective"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String)
    person_responsible = Column(Integer, ForeignKey("user.id"))
    target_date = Column(Date)
    current_status = Column(Enum(ObjectiveStatus))  #
    description = Column(String)
    # desired_output = Column(Integer, ForeignKey("process_output.id"))
    kpi = Column(String)
    organization_id = Column(Integer, ForeignKey("organization.id"))

    processes = relationship("ProcessModel", backref="objective")
    outputs = relationship(
        "ProcessOutputModel",
        secondary=objective_proc_out_assoc,
        back_populates="objectives",
    )


process_input_value_assoc = Table(
    "process_input_value_assoc",
    Base.metadata,
    Column("process_id", Integer, ForeignKey("process.id")),
    Column("process_value_id", Integer, ForeignKey("process_value.id")),
)
process_output_value_assoc = Table(
    "process_output_value_assoc",
    Base.metadata,
    Column("process_id", Integer, ForeignKey("process.id")),
    Column("process_value_id", Integer, ForeignKey("process_value.id")),
)


class ProcessModel(TimestampMixin, Base):
    __tablename__ = "process"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String)
    organization_id = Column(Integer, ForeignKey("organization.id"))
    objective_id = Column(Integer, ForeignKey("objective.id"))
    person_responsible = Column(Integer, ForeignKey("user.id"))
    team_responsible = Column(Integer, ForeignKey("task_team.id"))
    # previous_process_id = Column(Integer, ForeignKey("process.id"))
    # next_process_id = Column(Integer, ForeignKey("process.id"))
    monitoring_schedule = Column(Enum(MonitoringSchedule))

    tasks = relationship("TaskModel", backref="process", cascade="all, delete-orphan")
    inputs = relationship(
        "ProcessValueModel",
        secondary=process_input_value_assoc,
        back_populates="input_processes",
    )
    outputs = relationship(
        "ProcessValueModel",
        secondary=process_output_value_assoc,
        back_populates="output_processes",
    )
    # inputs = relationship("ProcessInputModel", backref="process")
    # outputs = relationship("ProcessOutputModel", backref="process")
    incidents = relationship(
        "IncidentModel",
        secondary=process_incident_assoc,
        back_populates="affected_processes",
    )
    risks = relationship(
        "RiskModel",
        secondary=process_risk_assoc,
        back_populates="affected_processes",
    )
    team_responsible_obj = relationship("TaskTeamModel")


class TaskModel(TimestampMixin, Base):
    __tablename__ = "task"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String)
    is_complete = Column(Boolean, default=False)
    due_by = Column(Date)
    organization_id = Column(Integer, ForeignKey("organization.id"))
    process_id = Column(Integer, ForeignKey("process.id"))
    user_owner_id = Column(Integer, ForeignKey("user.id"))
    role_owner_id = Column(Integer, ForeignKey("role.id"))
    sub_tasks = relationship("SubTaskModel", backref="task")

    user = relationship("UserModel", uselist=False, back_populates="tasks")


class SubTaskModel(TimestampMixin, Base):
    __tablename__ = "sub_task"

    id = Column(Integer, primary_key=True, index=True)
    task_id = Column(Integer, ForeignKey("task.id"))
    name = Column(String)
    is_complete = Column(Boolean, default=False)


class ProcessInputModel(TimestampMixin, Base):
    __tablename__ = "process_input"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String)
    # process_id = Column(Integer, ForeignKey("process.id"))
    value = Column(Integer)
    unit = Column(Integer, ForeignKey("unit.id"))
    organization_id = Column(Integer, ForeignKey("organization.id"))
    # source_id = Column(Integer, ForeignKey("process_input_source.id"))
    # possible_risks = relationship(
    #     "RiskModel",
    #     secondary=risk_input_assoc,
    #     back_populates="affected_inputs",
    # )


class ProcessValueModel(TimestampMixin, Base):
    __tablename__ = "process_value"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String)
    target_value = Column(Integer)
    actual_value = Column(Integer)
    unit = Column(Integer, ForeignKey("unit.id"))
    organization_id = Column(Integer, ForeignKey("organization.id"))
    # possible_risks = relationship(
    #     "RiskModel",
    #     secondary=risk_value_assoc,
    #     back_populates="affected_values",
    # )
    input_processes = relationship(
        "ProcessModel",
        secondary=process_input_value_assoc,
        back_populates="inputs",
    )
    output_processes = relationship(
        "ProcessModel",
        secondary=process_output_value_assoc,
        back_populates="outputs",
    )


# class ProcessInputSourceModel(TimestampMixin, Base):
#     __tablename__ = "process_input_source"

#     id = Column(Integer, primary_key=True, index=True)
#     name = Column(String)
#     inputs = relationship("ProcessInputModel", backref="source")


class ProcessOutputModel(TimestampMixin, Base):
    __tablename__ = "process_output"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String)
    value = Column(Integer)
    unit = Column(Integer, ForeignKey("unit.id"))
    organization_id = Column(Integer, ForeignKey("organization.id"))
    # process_id = Column(Integer, ForeignKey("process.id"))
    # receiver_id = Column(Integer, ForeignKey("process_output_receiver.id"))
    # possible_risks = relationship(
    #     "RiskModel",
    #     secondary=risk_output_assoc,
    #     back_populates="affected_outputs",
    # )
    objectives = relationship(
        "ObjectiveModel", secondary=objective_proc_out_assoc, back_populates="outputs"
    )


# class ProcessOutputReceiverModel(TimestampMixin, Base):
#     __tablename__ = "process_output_receiver"

#     id = Column(Integer, primary_key=True, index=True)
#     name = Column(String)

#     outputs = relationship("ProcessOutputModel", backref="process_output_receiver")


__all__ = (
    "OrganizationModel",
    "RoleModel",
    "UserModel",
    "SessionModel",
    "UnitModel",
    "UnitTypeModel",
    "RiskModel",
    "CapaModel",
    "IncidentModel",
    "TaskTeamModel",
    "ObjectiveModel",
    "ProcessModel",
    "TaskModel",
    "SubTaskModel",
    "ProcessInputModel",
    "ProcessOutputModel",
    "ProcessValueModel",
    "RiskCategoryModel",
    "RiskClassificationDataModel",
    # "ProcessInputSourceModel",
    # "ProcessOutputReceiverModel",
    # "RiskDataModel"
)
