#!/usr/bin/env bash

export FLASK_APP="./application/flaskapp.py:create_app('dev')"
export FLASK_ENV=development
export SECRET='c50f9e04-8ae8-4c3c-85c0-d0819c836821'

flask run --host "0.0.0.0"

